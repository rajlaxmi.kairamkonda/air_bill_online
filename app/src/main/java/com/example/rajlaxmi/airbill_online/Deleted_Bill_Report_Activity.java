package com.example.rajlaxmi.airbill_online;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.CardReader;
import com.aem.api.IAemCardScanner;
import com.aem.api.IAemScrybe;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cie.btp.CieBluetoothPrinter;
import com.cie.btp.DebugLog;

import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_DEVICE_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTING;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_LISTEN;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_NONE;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MESSAGES;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_FOUND;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_SAVED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_STATUS;

public class Deleted_Bill_Report_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IAemCardScanner, IAemScrybe {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;
    String empid="";

    List<String> printerList=new ArrayList<>();
    AEMScrybeDevice m_AemScrybeDevice;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    int glbPrinterWidth;
    Button btn_print;

    int pos_i=0;
    Cursor c;
    ListView lv;
    private MyAppAdapter myAppAdapter;
    private boolean success = false; // boolean
    ArrayList itemArrayList;
    Dialog dialog;
    String status="";

    List<String> list_item_name=new ArrayList<>();
    DatabaseHelper db;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    //    String print_option="";
    TextView et_from_date,et_to_date;
    Button btn_submit;
    int mYear1,mMonth1,mDay1,mYear2,mMonth2,mDay2;
    String getdate="",getdate1="",startdate="";
    long miliSecsDate;
    String catid="",cat_name="";
    private TextView tvStatus;
    private Button btnPrint;
    private CheckBox cbFindBlackMark;
    private boolean bFindBlackMark;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    //    ProgressDialog pdWorkInProgress;
    Button bt_ok,bt_cancel1;
    TextView tv_title,tv_errortext;

    private static final int BARCODE_WIDTH = 384;
    private static final int BARCODE_HEIGHT = 100;
    private static final int QRCODE_WIDTH = 100;
    static int kl=1;;
    public CieBluetoothPrinter mPrinter = CieBluetoothPrinter.INSTANCE;
    private int imageAlignment = 1;
    String print_option="";
    String gst_option="";
    String selectprinter_option="";
    String value="";
    TextView tvamt;
    String fromdate,todate;
    private SharedPreferences permissionStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deleted__bill__report_);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        headFootSetting=new HeadFootSetting(getApplicationContext());
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        empid=pref.getString("emp_id","");

        db = new DatabaseHelper(this);

        lv = (ListView) findViewById(R.id.lv_items);
        itemArrayList = new ArrayList<ClassListItems>();
//        itemArrayList.clear();
        et_from_date=(TextView)findViewById(R.id.et_from_date);
        et_to_date=(TextView)findViewById(R.id.et_to_date);
        m_AemScrybeDevice = new AEMScrybeDevice(this);
        // btn_print=(Button)findViewById(R.id.btn_print);
//        btn_print=(Button)findViewById(R.id.btn_print);
//        registerForContextMenu(btn_print);

        btn_submit=(Button)findViewById(R.id.btn_submit);

        print_option=pref.getString("print_option","");
        gst_option=pref.getString("gst_option","");
        selectprinter_option=pref.getString("selectprinter_option","");
        value=pref.getString("2inchprintername","");
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);

//        SyncData orderData = new SyncData();
//        orderData.execute("");
        tvamt=(TextView)findViewById(R.id.tvamt);
        getdata1();
        if(selectprinter_option.equals("Airbill")) {
            btnPrint=(Button)findViewById(R.id.btn_print);
            m_AemScrybeDevice = new AEMScrybeDevice(this);
        }
        //Dyno
        else if(selectprinter_option.equals("Dyno")) {

            btnPrint = (Button) findViewById(R.id.btn_print);
//            pdWorkInProgress = new ProgressDialog(this);
//            pdWorkInProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            tvStatus = findViewById(R.id.status_msg);

            BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mAdapter == null) {
                Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
                finish();
            }

            try {
                mPrinter.initService(Deleted_Bill_Report_Activity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean exe = false;
             /*   if(kl==1) {
                    mPrinter.disconnectFromPrinter();
                    mPrinter.selectPrinter(BillingScreenActivity.this);
                    kl++;
                }*/
//                    if (!exe) {
//                        exe = true;
//                        mPrinter.disconnectFromPrinter();
//                        mPrinter.selectPrinter(BillingScreenActivity.this);
//                    }

                    mPrinter.connectToPrinter();
                }
            });


        }
        et_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker1();
            }
        });

        et_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker2();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_from_date.getText().toString().equals("") || et_to_date.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Please select the From and To Date",Toast.LENGTH_SHORT).show();
                }else
                {
                    itemArrayList.clear();
//                    SyncData orderData = new SyncData();
//                    orderData.execute("");
                    getdata();
                }
            }
        });
    }

    private void datePicker1(){
        final Calendar c1 = Calendar.getInstance();
        mYear1 = c1.get(Calendar.YEAR);
        mMonth1 = c1.get(Calendar.MONTH);
        mDay1 = c1.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getdate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                        et_from_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>10)
                        {
                            fromdate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10)
                        {
                            fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        startdate=year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
                        miliSecsDate = milliseconds(startdate);
                    }
                }, mYear1, mMonth1, mDay1);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
            }
        });
        datePickerDialog.show();
    }

    private void datePicker2(){
        final Calendar c2 = Calendar.getInstance();
        mYear2 = c2.get(Calendar.YEAR);
        mMonth2 = c2.get(Calendar.MONTH);
        mDay2 = c2.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getdate1=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                        et_to_date.setText(dayOfMonth+"-"+(monthOfYear+1)+"-"+year);
                        if ((monthOfYear+1)<10 && dayOfMonth<10 ) {
                            todate = year + "-0" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)<10 && dayOfMonth>=10)
                        {
                            todate = year + "-0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth<10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-0" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth==10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                        else if((monthOfYear+1)>=10 && dayOfMonth>10)
                        {
                            todate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        }
                    }
                }, mYear2, mMonth2, mDay2);

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        if(startdate.equals("")){
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }else {
            datePickerDialog.getDatePicker().setMinDate(miliSecsDate);
        }
        datePickerDialog.show();
    }

    public long milliseconds(String date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date mDate = sdf.parse(date);
            long timeInMilliseconds = mDate.getTime();
            return timeInMilliseconds;
        }
        catch (ParseException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    protected void onResume() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        DebugLog.logTrace();if(selectprinter_option.equals("Dyno")){
            mPrinter.onActivityResume();
        }
        else if(selectprinter_option.equals("Airbill")) {
            m_AemScrybeDevice.getPairedPrinters();
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
//               m_AemScrybeDevice.pairPrinter(value);
//               m_AemScrybeDevice.connectToPrinter(value);
//               m_cardReader = m_AemScrybeDevice.getCardReader(this);
//               m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On Resume connected");

            } catch (IOException e) {
                e.printStackTrace();
            }
//        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
        }
        super.onResume();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if(selectprinter_option.equals("Airbill")) {
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On Restart connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onPause() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
            try {
            /*    Class<?> clazz = tmp.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[] {Integer.TYPE};

                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[] {Integer.valueOf(1)};

                fallbackSocket = (BluetoothSocket) m.invoke(tmp.getRemoteDevice(), params);
                fallbackSocket.connect();
*//*
                if(device.getBondState()==device.BOND_BONDED){
                    Log.d(TAG,device.getName());
                    //BluetoothSocket mSocket=null;
                    TagTechnology mSocket;
                    try {
                        mSocket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        Log.d("Socket","socket not created");
                        e1.printStackTrace();
                    }
                    try{
                        mSocket.connect();
                    }
                    catch(IOException e){
                        try {
                            mSocket.close();
                            Log.d(TAG,"Cannot connect");
                        } catch (IOException e1) {
                            Log.d(TAG,"Socket not closed");
                            e1.printStackTrace();
                        }
                    }*/

                Handler handler; BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;

                    socket =  bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {

                    try {
                        if (socket!=null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
//                BluetoothAdapter c =BluetoothAdapter.getDefaultAdapter();;
//                if (!c.isEnabled()) {
//                    Intent var3 = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
//                    ((Activity)this.getApplicationContext()).startActivityForResult(var3, 2);
//                }
//                else {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();


                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On pause connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                }


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else{
            DebugLog.logTrace();
            mPrinter.onActivityPause();

        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (selectprinter_option.equals("Airbill"))
        {




                /*m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
*/
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);

            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On destory connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);




            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(selectprinter_option.equals("Dyno"))
        {
            DebugLog.logTrace("onDestroy");
            mPrinter.onActivityDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
//            BluetoothAdapter c =BluetoothAdapter.getDefaultAdapter();;
//            if (!c.isEnabled()) {
//                Intent var3 = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
//                ((Activity)this.getApplicationContext()).startActivityForResult(var3, 0);
//            }
//            else {

            try {

                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.getPairedPrinters();

                m_AemScrybeDevice.getPairedPrinters();
                m_AemScrybeDevice.disConnectPrinter();
//                        m_AemScrybeDevice.pairPrinter(value);





            } catch (IOException e) {
                e.printStackTrace();
            }
//            }
        }
        else {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(RECEIPT_PRINTER_MESSAGES);
            LocalBroadcastManager.getInstance(this).registerReceiver(ReceiptPrinterMessageReceiver, intentFilter);
        }
    }




    @Override
    protected void onStop() {
        super.onStop();

        if (selectprinter_option.equals("Airbill"))
        {

            try {
                m_AemScrybeDevice.disConnectPrinter();
               /* m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                m_AemPrinter.print("On stop connected");*/
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(ReceiptPrinterMessageReceiver);
            } catch (Exception e) {
                DebugLog.logException(e);
            }
        }
    }
    private final BroadcastReceiver ReceiptPrinterMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DebugLog.logTrace("Printer Message Received");
            Bundle b = intent.getExtras();
            if (b == null) {
                return;
            }
            switch (b.getInt(RECEIPT_PRINTER_STATUS)) {
                case RECEIPT_PRINTER_CONN_STATE_NONE:
                    status= String.valueOf(R.string.printer_not_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_LISTEN:
                    status= String.valueOf(R.string.ready_for_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTING:
                    status= String.valueOf(R.string.printer_connecting);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTED:
                    status= String.valueOf(R.string.printer_connected);
                    // new AsyncPrint().execute();
                    itemArrayList.clear();
                    //new SyncData().execute();
                    onPrintBilldyno();
                    break;
                case RECEIPT_PRINTER_CONN_DEVICE_NAME:
                    savePrinterMac(b.getString(RECEIPT_PRINTER_NAME, ""));
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG:
                    String n = b.getString(RECEIPT_PRINTER_MSG);
                    status= String.valueOf(n);
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_MSG:
                    String m = b.getString(RECEIPT_PRINTER_MSG);
                    status= String.valueOf(m);
                    break;
                case RECEIPT_PRINTER_NOT_CONNECTED:
                    status= String.valueOf("Status : Printer Not Connected");
                    break;
                case RECEIPT_PRINTER_NOT_FOUND:
                    status= String.valueOf("Status : Printer Not Found");
                    break;
                case RECEIPT_PRINTER_SAVED:
                    status= String.valueOf(R.string.printer_saved);
                    break;
            }
        }
    };
    private void savePrinterMac(String sMacAddr) {
        if (sMacAddr.length() > 4) {
            status= String.valueOf("Preferred Printer saved");
        } else {
            status= String.valueOf("Preferred Printer cleared");
        }
    }


    public void onPrintBill(View v) {

//        if (m_AemPrinter == null) {
////            Toast.makeText(BillingScreenActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
////            return;
//            Toast.makeText(Deleted_Bill_Report_Activity.this, "Printer not connected Please select", Toast.LENGTH_SHORT).show();
//
//            printerList = m_AemScrybeDevice.getPairedPrinters();
//            if (printerList.size() > 0)
//                openContextMenu(v);
//            return;
//        }
        if (m_AemPrinter == null) {

            try {



                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {

                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                  //m_AemPrinter.print("On async connected");

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        if(print_option.equals("3 inch")&&selectprinter_option.equals("Airbill")){
            try {
                StringBuffer buffer = new StringBuffer();

                String data = null;

                data = "       " + "   Deleted Sales Bill Report" + "             \n";
                String d = "_____________________________________________\n";
                try {

                    m_AemPrinter.print(data);
                }catch (Exception ex)
                {
                    Message.message(getApplicationContext(),"Please connect to printer from main settings.");
                }
//                m_AemPrinter.print(d);
                data = "No |    Bill no     |   TotalAmt   |  Remark\n";

                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);

                int i=1;
                String date1= fromdate;//et_from_date.getText().toString();
                String date2=todate;// et_to_date.getText().toString();
                if(fromdate==""&&todate==""||fromdate==null&&todate==null){
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    String dateToStr = format.format(today);
                    c=db.getDeletedSalesBillReport(dateToStr,dateToStr);
                }
                else {
                    c = db.getDeletedSalesBillReport(date1, date2);
                }
                if(c.getCount() == 0) {
                    // show message
                    //Message.message(getApplicationContext(),"Nothing found");
                }
                double total=0.0;
                while (c.moveToNext()) {
                    String bdate = c.getString(1);

                    //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;

                    if (i >=10){
                        String totalamtrate=c.getString(6);
                        int length = totalamtrate.length();
                        int l1 = 0;
                        if (length < 8) {
                            l1 = 8 - length;
                            String itemname1 = totalamtrate.concat(l1 + " ");
                            totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                            buffer.append(i + "    " + c.getString(1) + "             " + totalamtrate + "  " + c.getString(21) + "\n");
                        }
                        else{
                            buffer.append(i + "     " + c.getString(1) + "              " + totalamtrate + "  " + c.getString(21) + "\n");
                        }
                    }

                    else
                    {
                        String totalamtrate=c.getString(5);
                        int length = totalamtrate.length();
                        int l1 = 0;
                        if (length < 8) {
                            l1 = 8 - length;
                            String itemname1 = totalamtrate.concat(l1 + " ");
                            totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                            buffer.append(i + "     " + c.getString(1) + "              " + totalamtrate + "  " + c.getString(21) + "\n");
                        }


                        else{
                            buffer.append(i + "     " + c.getString(0) + "              " + totalamtrate + "  " + c.getString(3) + "\n");
                        }

                    }
                    total= total+Double.parseDouble(c.getString(6));
                    i++;
                }

                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.print(String.valueOf(buffer));
                m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                data = "\n    Total Amount(Rs): " + f.format(total) + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if(print_option.equals("2 inch")&&selectprinter_option.equals("Airbill")){
            try {
                StringBuffer buffer = new StringBuffer();

                String data = null;

                data = "    " + "Deleted Sales Bill Report" + "             \n";
                String d = "_____________________________________________\n";
                try {

                    m_AemPrinter.print(data);
                }catch (Exception ex)
                {
                    Message.message(getApplicationContext(),"Please connect to printer from main settings.");
                }
//                m_AemPrinter.print(d);
                data = "No|Bill|Amt| Remark\n";

                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);

                int i=1;
                String date1= fromdate;//et_from_date.getText().toString();
                String date2=todate;// et_to_date.getText().toString();
                if(fromdate==""&&todate==""||fromdate==null&&todate==null){
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    String dateToStr = format.format(today);
                    c=db.getDeletedSalesBillReport(dateToStr,dateToStr);
                }
                else {
                    c = db.getDeletedSalesBillReport(date1, date2);
                }
                if(c.getCount() == 0) {
                    // show message
                    //Message.message(getApplicationContext(),"Nothing found");
                }
                double total=0.0;
                while (c.moveToNext()) {
                    String bdate = c.getString(2);

                    //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;

                    if (i >=10){
                        String totalamtrate=c.getString(6);
                        int length = totalamtrate.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            String itemname1 = totalamtrate.concat(l1 + " ");
                            totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                            buffer.append(i + "  " + c.getString(1) + "   " + totalamtrate + "  " + c.getString(21) + "\n");
                        }


                        else{
                            buffer.append(i + "  " + c.getString(1) + "   " + totalamtrate + "  " + c.getString(21) + "\n");
                        }

                    }

                    else
                    {
                        String totalamtrate=c.getString(6);
                        int length = totalamtrate.length();
                        int l1 = 0;
                        if (length < 5) {
                            l1 = 5 - length;
                            String itemname1 = totalamtrate.concat(l1 + " ");
                            totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                            buffer.append(i + "  " + c.getString(1) + "   " + totalamtrate + "  " + c.getString(21) + "\n");
                        }


                        else{
                            buffer.append(i + "  " + c.getString(1) + "   " + totalamtrate + "  " + c.getString(21) + "\n");
                        }

                    }
                    total= total+Double.parseDouble(c.getString(6));
                    i++;
                }

                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.print(String.valueOf(buffer));
                m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                data = "\nTotal Amount(Rs): " + f.format(total) + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
        for (int i = 0; i < printerList.size(); i++)
        {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        super.onContextItemSelected(item);

        String printerName = item.getTitle().toString();
        try
        {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(Deleted_Bill_Report_Activity.this,"Connected with " + printerName,Toast.LENGTH_SHORT ).show();

           /* SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
            Intent it=new Intent(getApplicationContext(),PrintActivity.class);
            it.putExtra("Printername",printerName);
            editor.putString("Printername",printerName);
            editor.apply();

            startActivity(it);*/



//            String value = printerName;
//
//            Intent intent = new Intent(BillingScreenActivity.this, PrintActivity.class);
//            SharedPreferences sharedPref = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPref.edit();
//            editor.putString("value", value);
//            editor.apply();
//            startActivity(intent);



            // jump to pass data name

//            SharedPreferences sharedPreferences = getSharedPreferences("myKey", MODE_PRIVATE);
//            String value = sharedPreferences.getString("value","");
        }
        catch (IOException e)
        {
            if (e.getMessage().contains("Service discovery failed"))
            {
                Toast.makeText(Deleted_Bill_Report_Activity.this,"Not Connected\n"+ printerName + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
            }
            else if (e.getMessage().contains("Device or resource busy"))
            {
                Toast.makeText(Deleted_Bill_Report_Activity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Toast.makeText(Deleted_Bill_Report_Activity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
            }
        }
        return true;
    }
    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+150;
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
    public class MyAppAdapter extends BaseAdapter       //has a class viewholder which holds
    {
        TextView tv_sr_no,tv_bill_no,tv_total_amt,tv_cash_credit,tv_delete,tv_print;
        String bdate="";
        public List<ClassListItems> parkingList;
        public Context context;
        public ArrayList<ClassListItems> arraylist;

        public class ViewHolder {
            TextView tv_sr_no,tv_bill_no,tv_total_amt,tv_cash_credit,tv_delete,tv_print;
        }

        private MyAppAdapter(ArrayList apps, Deleted_Bill_Report_Activity context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<ClassListItems>();
            arraylist.addAll(parkingList);
        }

        @Override
        public int getCount() {

            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) // inflating the layout and initializing widgets
        {

//            View rowView = convertView;
//         final ViewHolder viewHolder ;
//            if (rowView == null) {
//
//                //LayoutInflater inflater = getLayoutInflater();
//                LayoutInflater inflater = (LayoutInflater) Deleted_Bill_Report_Activity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                rowView = inflater.inflate(R.layout.sales_layout, parent, false);
//
//            }
            try {
                pos_i = position;
                final Deleted_Bill_Report_Activity.MyAppAdapter.ViewHolder holder;
                convertView = getLayoutInflater().inflate(R.layout.delete_sales_layout, null);
                holder = new Deleted_Bill_Report_Activity.MyAppAdapter.ViewHolder();
                LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//    holder = new ViewHolder();
                holder.tv_sr_no = (TextView) convertView.findViewById(R.id.tv_sr_no);
                holder.tv_bill_no = (TextView) convertView.findViewById(R.id.tv_bill_no);
                holder.tv_total_amt = (TextView) convertView.findViewById(R.id.tv_total_amt);
                holder.tv_cash_credit = (TextView) convertView.findViewById(R.id.tv_cash_credit);
                holder.tv_delete = (TextView) convertView.findViewById(R.id.tv_delete);
                holder.tv_delete.setVisibility(View.INVISIBLE);
                holder.tv_print = (TextView) convertView.findViewById(R.id.tv_print);
                holder.tv_print.setVisibility(View.INVISIBLE);
                convertView.setTag(holder);
                catid = parkingList.get(position).getQty();
                holder.tv_sr_no.setText(parkingList.get(position).getSr_no() + "");
                holder.tv_bill_no.setText(parkingList.get(position).getItem_name() + "");
                holder.tv_total_amt.setText(parkingList.get(position).getQty() + "");
                holder.tv_cash_credit.setText(parkingList.get(position).getDiscount() + "");
                bdate=parkingList.get(position).getRate();

//                holder.tv_delete.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
////            Toast.makeText(getApplicationContext(), "Delete " + holder.tv_sr_no.getText().toString(), Toast.LENGTH_SHORT).show();
//                        //RemoveMessageBox(finalHolder.tv_bill_no.getText().toString(), finalHolder.tv_total_amt.getText().toString(), "Remove", "Do you want to delete the Item " + finalHolder.tv_bill_no.getText().toString() + "?");
//                        RemoveMessageBox(holder.tv_bill_no.getText().toString(), bdate, "Remove", "Do you want to delete the Bill no  " + holder.tv_bill_no.getText().toString() + "?");
//                    }
//                });
            }catch (Exception ex)
            {
                ex.printStackTrace();
            }
            return convertView;
        }
    }

    public void getdata()
    {

        int i = 1;

        String date1 = fromdate;//et_from_date.getText().toString();
        String date2 = todate;// et_to_date.getText().toString();
        c = db.getDeletedSalesBillReport(date1, date2); double total=0.0;
        try {
            if (c.getCount() == 0) {
                // show message
                //Message.message(getApplicationContext(),"Nothing found");
            }

            StringBuffer buffer = new StringBuffer();
            while (c.moveToNext()) {
                String bdate = c.getString(2);
                itemArrayList.add(new ClassListItems("" + i, "" + c.getString(1), "" + c.getString(6), "" + c.getString(21), bdate));
                total= total+Double.parseDouble(c.getString(6));
                i++;
            }
            tvamt.setText(""+total);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        myAppAdapter = new MyAppAdapter(itemArrayList, Deleted_Bill_Report_Activity.this);
        lv.setAdapter(myAppAdapter);
        Deleted_Bill_Report_Activity.ListUtils.setDynamicHeight(lv);

    }
    public void getdata1()
    {

        int i = 1;double total=0.0;
        String date1 = fromdate;//et_from_date.getText().toString();
        String date2 = todate;// et_to_date.getText().toString();
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        c = db.getDeletedSalesBillReport(dateToStr, dateToStr);
        try {
            if (c.getCount() == 0)
            {
                // show message
                //Message.message(getApplicationContext(),"Nothing found");
            }

            StringBuffer buffer = new StringBuffer();
            while (c.moveToNext())
            {
                String bdate = c.getString(2);
                itemArrayList.add(new ClassListItems("" + i, "" + c.getString(1), "" + c.getString(6), "" + c.getString(21), bdate));
                total= total+Double.parseDouble(c.getString(6));
                i++;

            }
            tvamt.setText(""+total);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        myAppAdapter = new Deleted_Bill_Report_Activity.MyAppAdapter(itemArrayList, Deleted_Bill_Report_Activity.this);
        lv.setAdapter(myAppAdapter);
        Deleted_Bill_Report_Activity.ListUtils.setDynamicHeight(lv);

    }
    public void onPrintBilldyno()
    {
        if(print_option.equals("2 inch")&&selectprinter_option.equals("Dyno"))
        {
            String data = "";
            mPrinter.resetPrinter();
            mPrinter.setAlignmentCenter();
            mPrinter.setBoldOn();
            mPrinter.setCharRightSpacing(10);
            mPrinter.printTextLine("\n Deleted Sales Bill Report\n");
            mPrinter.setBoldOff();
            mPrinter.setCharRightSpacing(0);
            mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            mPrinter.pixelLineFeed(50);
            mPrinter.printTextLine( "No |Bill no | TotalAmt | Remark\n");
            mPrinter.printTextLine("--------------------------------\n");
            int i=1;
            String date1= fromdate;//et_from_date.getText().toString();
            String date2=todate;// et_to_date.getText().toString();

            if(fromdate==""&&todate==""||fromdate==null&&todate==null){
                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String dateToStr = format.format(today);
                c=db.getDeletedSalesBillReport(dateToStr,dateToStr);
            }
            else {
                c = db.getDeletedSalesBillReport(date1, date2);
            }

            if(c.getCount() == 0) {
                // show message
                //Message.message(getApplicationContext(),"Nothing found");
            }
            StringBuffer buffer = new StringBuffer();
            double total=0.0;
            while (c.moveToNext()) {
                String bdate = c.getString(2);
                //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;

                if (i >= 10){
                    String totalamtrate=c.getString(6);
                    int length = totalamtrate.length();
                    int l1 = 0;
                    if (length < 5) {
                        l1 = 5 - length;
                        String itemname1 = totalamtrate.concat(l1 + " ");
                        totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                        buffer.append(i + "   " + c.getString(1) + "    " + totalamtrate + "  " + c.getString(21) + "\n");
                    }


                    else{
                        buffer.append(i + "   " + c.getString(1) + "    " + totalamtrate + "  " + c.getString(21) + "\n");
                    }

                }

                else
                {
                    String totalamtrate=c.getString(6);
                    int length = totalamtrate.length();
                    int l1 = 0;
                    if (length < 5) {
                        l1 = 5 - length;
                        String itemname1 = totalamtrate.concat(l1 + " ");
                        totalamtrate = String.format(totalamtrate + "%" + (l1 ) + "s", "");
                        buffer.append(i + "    " + c.getString(1) + "    " + totalamtrate + "  " + c.getString(21) + "\n");
                    }


                    else{
                        buffer.append(i + "    " + c.getString(1) + "    " + totalamtrate + "  " + c.getString(21) + "\n");
                    }

                }
                total= total+Double.parseDouble(c.getString(6));
                i++;
            }
            mPrinter.setAlignmentLeft();
            mPrinter.printTextLine(String.valueOf(buffer));
            mPrinter.printTextLine("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            DecimalFormat f = new DecimalFormat("#####.00");
            data = "Total Amount(Rs): " + f.format(total) + "\n";
            mPrinter.printTextLine(data);
            mPrinter.printLineFeed();
            mPrinter.printLineFeed();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPrinter.onActivityRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Deleted_Bill_Report_Activity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(Deleted_Bill_Report_Activity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(Deleted_Bill_Report_Activity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(Deleted_Bill_Report_Activity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                try {

                } catch (Exception e) {
                    Message.message(getApplicationContext(),"Please grant permission");
                }
            }
        }
    }

    @Override
    public void onScanMSR(String s, CardReader.CARD_TRACK card_track) {

    }

    @Override
    public void onScanDLCard(String s) {

    }

    @Override
    public void onScanRCCard(String s) {

    }

    @Override
    public void onScanRFD(String s) {

    }

    @Override
    public void onScanPacket(String s) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> arrayList) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.deleted__bill__report_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

}
