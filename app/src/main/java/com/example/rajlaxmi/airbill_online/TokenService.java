package com.example.rajlaxmi.airbill_online;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

public class TokenService {

    private static final String TAG = "TokenService";
    //public static final String BACKEND_SERVER_IP = "103.71.64.153:85";
    public static final String BACKEND_URL_BASE = Config.hosturl;
    private Context context;
    private IRequestListener listener;

    public TokenService(Context context, Context listener) {
        this.context = context;
        this.listener = (IRequestListener) listener;
    }

    public void registerTokenInDB(final String token) {
        // The call should have a back off strategy
        final SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        editor.putString("Token",token);
        editor.commit();
    }
}
