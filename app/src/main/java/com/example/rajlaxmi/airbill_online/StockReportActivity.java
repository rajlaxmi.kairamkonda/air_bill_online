package com.example.rajlaxmi.airbill_online;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.CardReader;
import com.aem.api.IAemCardScanner;
import com.aem.api.IAemScrybe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.cie.btp.CieBluetoothPrinter;
import com.cie.btp.DebugLog;

import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_DEVICE_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTING;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_LISTEN;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_NONE;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MESSAGES;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_FOUND;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_SAVED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_STATUS;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.Map;

public class StockReportActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IAemCardScanner, IAemScrybe {
    String status="";
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final int BARCODE_WIDTH = 384;
    private static final int BARCODE_HEIGHT = 100;
    private static final int QRCODE_WIDTH = 100;
    static int kl=1;;
    public CieBluetoothPrinter mPrinter = CieBluetoothPrinter.INSTANCE;
    private int imageAlignment = 1;
    String print_option="";
    String gst_option="";
    String selectprinter_option="";
    String value="";
    Cursor c;

    ListView lv;
    private MyAppAdapter myAppAdapter;
    private boolean success = false; // boolean
    ArrayList itemArrayList;
    Dialog dialog;
    DatabaseHelper db;
    List<String> printerList=new ArrayList<>();

    AEMScrybeDevice m_AemScrybeDevice;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    int glbPrinterWidth;
    Button btn_print;
    private static final int DISCOVER_DURATION = 300;

    // our request code (must be greater than zero)
    private static final int REQUEST_BLU = 1;
    String empid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_report);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        headFootSetting=new HeadFootSetting(getApplicationContext());
        db = new DatabaseHelper(this);
        lv = (ListView) findViewById(R.id.lv_items);
        itemArrayList = new ArrayList<ClassListItems>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        print_option=pref.getString("print_option","");
        gst_option=pref.getString("gst_option","");
        selectprinter_option=pref.getString("selectprinter_option","");
        value=pref.getString("2inchprintername","");
        empid=pref.getString("emp_id","");
        m_AemScrybeDevice = new AEMScrybeDevice(this);
        // btn_print=(Button)findViewById(R.id.btn_print);
//        btn_print=(Button)findViewById(R.id.btn_print);
//        registerForContextMenu(btn_print);

//        SyncData orderData = new SyncData();
//        orderData.execute("");
        if(selectprinter_option.equals("Airbill")) {
            btn_print=(Button)findViewById(R.id.btn_print);
            m_AemScrybeDevice = new AEMScrybeDevice(this);

        }
        //Dyno
        else if(selectprinter_option.equals("Dyno")) {

            btn_print = (Button) findViewById(R.id.btn_print);
//            pdWorkInProgress = new ProgressDialog(this);
//            pdWorkInProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);


            BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mAdapter == null) {
                Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
                finish();
            }

            try {
                mPrinter.initService(StockReportActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }

            btn_print.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean exe = false;
             /*   if(kl==1) {
                    mPrinter.disconnectFromPrinter();
                    mPrinter.selectPrinter(BillingScreenActivity.this);
                    kl++;
                }*/
//                    if (!exe) {
//                        exe = true;
//                        mPrinter.disconnectFromPrinter();
//                        mPrinter.selectPrinter(BillingScreenActivity.this);
//                    }

                    mPrinter.connectToPrinter();
                }
            });


        }
        getdata();



    }



    public void onPrintBilldyno()
    {
        if(print_option.equals("2 inch")&&selectprinter_option.equals("Dyno"))
        {
            String data = "";
            mPrinter.resetPrinter();
            mPrinter.setAlignmentCenter();
            mPrinter.setBoldOn();
            mPrinter.setCharRightSpacing(10);
            mPrinter.printTextLine("\n Stock Report\n");
            mPrinter.setBoldOff();
            mPrinter.setCharRightSpacing(0);
            mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            mPrinter.pixelLineFeed(50);
            mPrinter.printTextLine( "No |     Item Name    | Stock\n");
            mPrinter.printTextLine("--------------------------------\n");
            int i=1;


            c = db.stock_details();


            if(c.getCount() == 0) {
                // show message
                //Message.message(getApplicationContext(),"Nothing found");
            }
            StringBuffer buffer = new StringBuffer();
            double total=0.0;
            while (c.moveToNext())
            {

                String itemname = c.getString(0).toString();
                if (itemname.length() > 15) {
                    if(i>=10)
                    {
                        buffer.append(i + "   " + itemname.substring(0, 15) + "     " + c.getString(1) + "\n"); //res2.getString(6)+"\n");
                    }
                    else{
                        buffer.append(i + "    " + itemname.substring(0, 15) + "     " + c.getString(1) + "\n"); //res2.getString(6)+"\n");
                    }



                    /*    rate=Double.parseDouble(c.getString(2));
                        totalrate=Double.parseDouble(c.getString(1))*rate;*/

                } else {
                    int length = itemname.length();
                    int l1 = 0;
                    if (length < 15) {
                        l1 = 15 - length;
                        String itemname1 = itemname.concat(l1 + " ");
                        itemname = String.format(itemname + "%" + (l1 ) + "s", "");
                    }
                    if(i>=10) {
                        buffer.append(i + "   " + itemname + "     " + c.getString(1) + "\n");
                    }
                    else
                    {
                        buffer.append(i + "    " + itemname + "     " + c.getString(1) + "\n");
                    }
                }
                i++;
            }
            mPrinter.setAlignmentRight();
            mPrinter.printTextLine(String.valueOf(buffer));
            mPrinter.printTextLine(data);
            mPrinter.printLineFeed();
            mPrinter.printLineFeed();
        }
    }
    public void getdata()
    {
        try {
            int i = 1;
            c = db.stock_details();
            try {
                if (c.getCount() == 0) {
                    Message.message(getApplicationContext(), "Nothing found");
                }
                while (c.moveToNext()) {
                    itemArrayList.add(new ClassListItems("" + i, "" + c.getString(0), "" + c.getString(1), "", ""));
                    i++;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            myAppAdapter = new MyAppAdapter(itemArrayList, StockReportActivity.this);
            lv.setAdapter(myAppAdapter);
            ListUtils.setDynamicHeight(lv);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }
    //    @Override
//    protected void onResume() {
//        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
//            BluetoothAdapter.getDefaultAdapter().enable();
//
//            try {
//                Thread.sleep(30L);
//            } catch (InterruptedException var5) {
//                var5.printStackTrace();
//                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
//            }
//        }
//        DebugLog.logTrace();if(selectprinter_option.equals("Dyno")){
//            mPrinter.onActivityResume();
//        }
//        else if(selectprinter_option.equals("Airbill")) {
//            m_AemScrybeDevice.getPairedPrinters();
//            try {
//                m_AemScrybeDevice.getPairedPrinters();
//                boolean b=m_AemScrybeDevice.BtConnStatus();
//
//                    m_AemScrybeDevice.disConnectPrinter();
////                m_AemScrybeDevice.pairPrinter(value);
////                m_AemScrybeDevice.connectToPrinter(value);
////                m_cardReader = m_AemScrybeDevice.getCardReader(this);
////                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
////                m_AemPrinter.print("On Resume connected");
//
//            } catch (IOException e) {
//                e.printStackTrace();
//                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
//            }
////        m_cardReader = m_AemScrybeDevice.getCardReader(this);
////        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//        }
//        super.onResume();
//    }
    @Override
    protected void onResume() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        DebugLog.logTrace();if(selectprinter_option.equals("Dyno")){
            mPrinter.onActivityResume();
        }
        else if(selectprinter_option.equals("Airbill")) {
            m_AemScrybeDevice.getPairedPrinters();
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
//            m_AemScrybeDevice.pairPrinter(value);
//            m_AemScrybeDevice.connectToPrinter(value);
//            m_cardReader = m_AemScrybeDevice.getCardReader(this);
//            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On Resume connected");

            } catch (IOException e) {
                e.printStackTrace();
            }
//        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
        }
        super.onResume();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
        if(selectprinter_option.equals("Airbill")) {
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On Restart connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);

            } catch (IOException e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
    }
    @Override
    protected void onPause() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
            try {
            /*    Class<?> clazz = tmp.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[] {Integer.TYPE};

                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[] {Integer.valueOf(1)};

                fallbackSocket = (BluetoothSocket) m.invoke(tmp.getRemoteDevice(), params);
                fallbackSocket.connect();
*//*
                if(device.getBondState()==device.BOND_BONDED){
                    Log.d(TAG,device.getName());
                    //BluetoothSocket mSocket=null;
                    TagTechnology mSocket;
                    try {
                        mSocket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        Log.d("Socket","socket not created");
                        e1.printStackTrace();
                    }
                    try{
                        mSocket.connect();
                    }
                    catch(IOException e){
                        try {
                            mSocket.close();
                            Log.d(TAG,"Cannot connect");
                        } catch (IOException e1) {
                            Log.d(TAG,"Socket not closed");
                            e1.printStackTrace();
                        }
                    }*/

                Handler handler; BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;

                    socket =  bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {

                    try {
                        if (socket!=null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
//                BluetoothAdapter c =BluetoothAdapter.getDefaultAdapter();;
//                if (!c.isEnabled()) {
//                    Intent var3 = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
//                    ((Activity)this.getApplicationContext()).startActivityForResult(var3, 2);
//                }
//                else {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();


                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On pause connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                }


            } catch (IOException e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }

        }
        else{
            DebugLog.logTrace();
            mPrinter.onActivityPause();

        }

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (selectprinter_option.equals("Airbill"))
        {




                /*m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
*/
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);

            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On destory connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);




            } catch (IOException e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
        else if(selectprinter_option.equals("Dyno"))
        {
            DebugLog.logTrace("onDestroy");
            mPrinter.onActivityDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
//            BluetoothAdapter c =BluetoothAdapter.getDefaultAdapter();;
//            if (!c.isEnabled()) {
//                Intent var3 = new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE");
//                ((Activity)this.getApplicationContext()).startActivityForResult(var3, 0);
//            }
//            else {

            try {

                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.getPairedPrinters();

                m_AemScrybeDevice.getPairedPrinters();
                m_AemScrybeDevice.disConnectPrinter();
//                        m_AemScrybeDevice.pairPrinter(value);
//                        m_AemScrybeDevice.connectToPrinter(value);
//                        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
////                m_AemPrinter.print("On Start connected");
//                        m_cardReader = m_AemScrybeDevice.getCardReader(this);




            } catch (IOException e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
//            }
        }
        else {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(RECEIPT_PRINTER_MESSAGES);
            LocalBroadcastManager.getInstance(this).registerReceiver(ReceiptPrinterMessageReceiver, intentFilter);
        }
    }




    @Override
    protected void onStop() {
        super.onStop();

        if (selectprinter_option.equals("Airbill"))
        {

            try {
                m_AemScrybeDevice.disConnectPrinter();
               /* m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                m_AemPrinter.print("On stop connected");*/
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);


            } catch (IOException e) {
                e.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }
        else {
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(ReceiptPrinterMessageReceiver);
            } catch (Exception e) {
                DebugLog.logException(e);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPrinter.onActivityRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private final BroadcastReceiver ReceiptPrinterMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DebugLog.logTrace("Printer Message Received");
            Bundle b = intent.getExtras();
            if (b == null) {
                return;
            }
            switch (b.getInt(RECEIPT_PRINTER_STATUS)) {
                case RECEIPT_PRINTER_CONN_STATE_NONE:
                    status= String.valueOf(R.string.printer_not_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_LISTEN:
                    status= String.valueOf(R.string.ready_for_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTING:
                    status= String.valueOf(R.string.printer_connecting);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTED:
                    status= String.valueOf(R.string.printer_connected);
                    // new AsyncPrint().execute();
                    itemArrayList.clear();
                    //new SyncData().execute();
                    onPrintBilldyno();
                    break;
                case RECEIPT_PRINTER_CONN_DEVICE_NAME:
                    savePrinterMac(b.getString(RECEIPT_PRINTER_NAME, ""));
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG:
                    String n = b.getString(RECEIPT_PRINTER_MSG);
                    status= String.valueOf(n);
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_MSG:
                    String m = b.getString(RECEIPT_PRINTER_MSG);
                    status= String.valueOf(m);
                    break;
                case RECEIPT_PRINTER_NOT_CONNECTED:
                    status= String.valueOf("Status : Printer Not Connected");
                    break;
                case RECEIPT_PRINTER_NOT_FOUND:
                    status= String.valueOf("Status : Printer Not Found");
                    break;
                case RECEIPT_PRINTER_SAVED:
                    status= String.valueOf(R.string.printer_saved);
                    break;
            }
        }
    };
    private void savePrinterMac(String sMacAddr) {
        if (sMacAddr.length() > 4) {
            status= String.valueOf("Preferred Printer saved");
        } else {
            status= String.valueOf("Preferred Printer cleared");
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.stock_report, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
        for (int i = 0; i < printerList.size(); i++)
        {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        super.onContextItemSelected(item);

        String printerName = item.getTitle().toString();
        try
        {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(StockReportActivity.this,"Connected with " + printerName,Toast.LENGTH_SHORT ).show();


        }
        catch (IOException e)
        {
            if (e.getMessage().contains("Service discovery failed"))
            {
                Toast.makeText(StockReportActivity.this,"Not Connected\n"+ printerName + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
            }
            else if (e.getMessage().contains("Device or resource busy"))
            {
                Toast.makeText(StockReportActivity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Toast.makeText(StockReportActivity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
            }
        }
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    public void onPrintBill(View v)
    {


//        if (m_AemPrinter == null) {
////            Toast.makeText(BillingScreenActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
////            return;
//            Toast.makeText(StockReportActivity.this, "Printer not connected Please select", Toast.LENGTH_SHORT).show();
//
//            printerList = m_AemScrybeDevice.getPairedPrinters();
//            if (printerList.size() > 0)
//                openContextMenu(v);
//            return;
//        }
        if (m_AemPrinter == null) {

            try {



                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {



                }

                try {

                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                  //m_AemPrinter.print("On async connected");

                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Please connect to printer from main settings.");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        if(print_option.equals("3 inch")) {
            try {

                StringBuffer buffer = new StringBuffer();
                String data = null;

                data = "          " + " Stock  Report" + "             \n";
                String d = "_____________________________________________\n";
                m_AemPrinter.print(data);
                m_AemPrinter.print(d);
                data = "No.|         Item Name     |    Stock\n";

                m_AemPrinter.print(data);
                m_AemPrinter.print(d);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);


               /* String date1= fromdate;//et_from_date.getText().toString();
                String date2=todate;// et_to_date.getText().toString();*/
                c=db.stock_details();

                if(c.getCount() == 0) {
                    // show message
                    //Message.message(getApplicationContext(),"Nothing found");
                }

                int i=1;
                while (c.moveToNext())
                {
                    String itemname = c.getString(0).toString();
                    if (itemname.length() > 23) {


                        buffer.append(i + "   " + itemname.substring(0, 22) + "        " + c.getString(1) +"\n"); //res2.getString(6)+"\n");


                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 22) {
                            l1 = 22 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1 - 2) + "s", "");
                        }
                        buffer.append(i + "   " + itemname + "          " + c.getString(1)+"\n");
                    }
//                    String bdate=c.getString(1);
//                    //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;
//                    buffer.append(i+"    "+c.getString(0)+"            "+c.getString(1)+"          \n");
////                    total= total+Double.parseDouble(c.getString(5));
                    i++;

                }


                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.print(String.valueOf(buffer));
                m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                DecimalFormat f = new DecimalFormat("#####.00");
//                //System.out.println(f.format(dda));
//
//                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
//
//                data = "Total Amount(Rs): " + f.format(total) + "\n";
                m_AemPrinter.print(d);
//                m_AemPrinter.print(data);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            } catch (Exception ex) {
                ex.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

        if(print_option.equals("2 inch")&&selectprinter_option.equals("Airbill")){
            try {

                StringBuffer buffer = new StringBuffer();

                String data = null;

                data = "  " + "       Stock Report" + "             \n";
                String d = "_____________________________________________\n";

                try{
                    m_AemPrinter.print(data);
                }
                catch (IOException ex){
                    Message.message(getApplicationContext(),"Please connect to printer from main settings.");
                }
//                m_AemPrinter.print(d);
                data = "No |     Item Name    | Stock\n";

                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);

                int i=1;
               /* String date1= fromdate;//et_from_date.getText().toString();
                String date2=todate;// et_to_date.getText().toString();
                c=db.Itemwisesalesbillreport(date1,date2);*/
                c=db.stock_details();

                if(c.getCount() == 0) {
                    // show message
                    //Message.message(getApplicationContext(),"Nothing found");
                }
                double total=0.0;
//                while (c.moveToNext())
//                {
//                    String bdate=c.getString(1);
//                    //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;
//                    buffer.append(i+"    "+c.getString(1)+"          "+c.getString(0)+"\n");
////                    total= total+Double.parseDouble(c.getString(5));
//                    i++;
//                }
                Double rate=0.0,totalrate=0.0;

                while (c.moveToNext())
                {

                    String itemname = c.getString(0).toString();
                    if (itemname.length() > 15) {
                        if(i>=10)
                        {
                            buffer.append(i + "   " + itemname.substring(0, 15) + "     " + c.getString(1) + "\n"); //res2.getString(6)+"\n");
                        }
                        else{
                            buffer.append(i + "    " + itemname.substring(0, 15) + "     " + c.getString(1) + "\n"); //res2.getString(6)+"\n");
                        }



                    /*    rate=Double.parseDouble(c.getString(2));
                        totalrate=Double.parseDouble(c.getString(1))*rate;*/

                    } else {
                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1 ) + "s", "");
                        }
                        if(i>=10) {
                            buffer.append(i + "   " + itemname + "     " + c.getString(1) + "\n");
                        }
                        else
                        {
                            buffer.append(i + "    " + itemname + "     " + c.getString(1) + "\n");
                        }
                       /* rate=Double.parseDouble(c.getString(2));
                        totalrate=Double.parseDouble(c.getString(1))*rate;
*/
                    }
//                    String bdate=c.getString(1);
//                    //itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(5),""+c.getString(3),""));                        i++;
//                    buffer.append(i+"    "+c.getString(0)+"            "+c.getString(1)+"          \n");
////                    total= total+Double.parseDouble(c.getString(5));
                    i++;
                }

                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.print(String.valueOf(buffer));
                m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                data = "\n          Total (Rs): " + f.format(totalrate) + "\n";
//                m_AemPrinter.print(data);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            } catch (Exception ex) {
                ex.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

    }

    @Override
    public void onScanMSR(String s, CardReader.CARD_TRACK card_track) {

    }

    @Override
    public void onScanDLCard(String s) {

    }

    @Override
    public void onScanRCCard(String s) {

    }

    @Override
    public void onScanRFD(String s) {

    }

    @Override
    public void onScanPacket(String s) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> arrayList) {

    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+150;
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
    public class MyAppAdapter extends BaseAdapter       //has a class viewholder which holds
    {
        public List<ClassListItems> parkingList;
        public Context context;
        public ArrayList<ClassListItems> arraylist;

        private class ViewHolder {
            TextView tv_sr_no,tv_item_name,tv_total_stock;
        }

        private MyAppAdapter(ArrayList apps, StockReportActivity context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<ClassListItems>();
            arraylist.addAll(parkingList);
        }

        @Override
        public int getCount() {

            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) // inflating the layout and initializing widgets
        {
            final ViewHolder holder;
            view = getLayoutInflater().inflate(R.layout.layout_stock,null);
            holder = new ViewHolder(); LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            holder.tv_sr_no = (TextView) view.findViewById(R.id.tv_sr_no);
            holder.tv_item_name = (TextView) view.findViewById(R.id.tv_item_name);
            holder.tv_total_stock=(TextView) view.findViewById(R.id.tv_total_stock);

            holder.tv_sr_no.setText(parkingList.get(position).getSr_no() + "");
            holder.tv_item_name.setText(parkingList.get(position).getItem_name() + "");
            holder.tv_total_stock.setText(parkingList.get(position).getQty() + "");

            return view;
        }
    }

    private class SyncData extends AsyncTask<String, String, String> {
        String msg = "Internet/DB_Credentials/Windows_FireWall_TurnOn Error, See Android Monitor in the bottom For details!";

        @Override
        protected void onPreExecute() //Starts the progress dailog
        {

        }

        @Override
        protected String doInBackground(String... strings)  // Connect to the database, write query and add items to array list
        {
            try {
                int i=1;
                c=db.stock_details();
                try
                {
                    if(c.getCount() == 0) {
                        Message.message(getApplicationContext(),"Nothing found");
                    }
                    while (c.moveToNext())
                    {
                        itemArrayList.add(new ClassListItems(""+i,""+c.getString(0),""+c.getString(1),"",""));
                        i++;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                msg = "Found";
                success = true;

            } catch (Exception e) {
                e.printStackTrace();
                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                msg = writer.toString();
                success = false;
            }
            // progress.dismiss();
            return msg;
        }
        @Override
        protected void onPostExecute(String msg)
        {
            if (success == false) {
            } else {
                try {
                    myAppAdapter = new MyAppAdapter(itemArrayList, StockReportActivity.this);
                    lv.setAdapter(myAppAdapter);
                    ListUtils.setDynamicHeight(lv);
                } catch (Exception ex) {
                    Log.d("Error ", "" + ex);
                    Log.e("Error ", "" + ex);
                }
            }
        }

    }

}