package com.example.rajlaxmi.airbill_online;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DatabaseHelper extends SQLiteOpenHelper {
    DatabaseHelper myhelper;
    public boolean a;
    public int active=0;
    int i=1;
    List<String> itemnameforinventory=new ArrayList<>();
    String print_option="";
    String gst_option="";
    String selectprinter_option="";
    String billwithprint="";
    String multipleprint="";
    String resetbill="";


    public void addItem (String IemName , String IRate,String dis1,String dis1rate,String taxname,String taxvalue,String finalvalue ,String ICategory, String IUnits,String IStock,String Barcode  ) {
        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table

        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.ItemName, IemName);
        Itemvalues.put(DatabaseHelper.Rate, Double.parseDouble(IRate));
        Itemvalues.put(DatabaseHelper.dis, Double.parseDouble(dis1));
        Itemvalues.put(DatabaseHelper.disrate, Double.parseDouble(dis1rate));
        Itemvalues.put(DatabaseHelper.TaxName, Double.parseDouble(taxname));
        Itemvalues.put(DatabaseHelper.itemTaxValue, Double.parseDouble(taxvalue));
        Itemvalues.put(DatabaseHelper.FinalRate, Double.parseDouble(finalvalue));
        Itemvalues.put(DatabaseHelper.Category, ICategory);
        Itemvalues.put(DatabaseHelper.Units, IUnits);
        Itemvalues.put(DatabaseHelper.Stock, IStock);
        Itemvalues.put(DatabaseHelper.Barcode, Barcode);
        Itemvalues.put(DatabaseHelper.isactive, active);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP, dateFormat.format(date));
        db.insert(tbl_AddItems, null, Itemvalues);
        Message.message(context, "Item Saved Sucessfully....");
    }

    public void AddCategory (String catName)
    {
        int a=1;
        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.CatName,catName);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,active);
        db.insert(tbl_AddCategory, null, Itemvalues);
    }

    public void AddHeaderFooter (String headername,String hsize,String h2,String h2_size,String h3,String h3_size,String h4,String h4_size,String h5,String h5_size,String footername,String footerSize,String f2,String f2_size,String f3,String f3_size,String f4,String f4_size,String f5,String f5_size )
    {
        SQLiteDatabase db = this.getWritableDatabase();

        String size="NA";
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.header, headername);
        Itemvalues.put(DatabaseHelper.headersize, size);
        Itemvalues.put(DatabaseHelper.h2, h2);
        Itemvalues.put(DatabaseHelper.h2_size, size);
        Itemvalues.put(DatabaseHelper.h3, h3);
        Itemvalues.put(DatabaseHelper.h3_size, size);
        Itemvalues.put(DatabaseHelper.h4, h4);
        Itemvalues.put(DatabaseHelper.h4_size, size);
        Itemvalues.put(DatabaseHelper.h5, h5);
        Itemvalues.put(DatabaseHelper.h5_size, size);
        Itemvalues.put(DatabaseHelper.footer, footername);
        Itemvalues.put(DatabaseHelper.footersize, size);
        Itemvalues.put(DatabaseHelper.f2, f2);
        Itemvalues.put(DatabaseHelper.f2_size, size);
        Itemvalues.put(DatabaseHelper.f3, f3);
        Itemvalues.put(DatabaseHelper.f3_size, size);
        Itemvalues.put(DatabaseHelper.f4, f4);
        Itemvalues.put(DatabaseHelper.f4_size, size);
        Itemvalues.put(DatabaseHelper.f5, f5);
        Itemvalues.put(DatabaseHelper.f5_size, size);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP, dateFormat.format(date));

        db.insert(tbl_AddFooter, null, Itemvalues);
    }

    public void Addbill (int Billno,String customername,String cashcredit, String itemname, Double itemqty, Double itemrate, Double itemtotalrate,
                         Double itemtotalamt,String cid,String lid,String emp_id,String app_bil_id,String pointofcontact
            ,String paymentdetails,String orderdetails,String gst_settings1,String emp_code,String item_dis,String item_tax,Double basicrate ) {

        Log.v("TAGFF", String.valueOf(itemtotalamt));
        int bd=get_maxid_bill_details();
        int bm=get_maxid_bill_master();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date1 = new Date();
        String d1=dateFormat1.format(date);
        SQLiteDatabase db = this.getWritableDatabase();
        int bno=0;
        int id=0; String yes=null;
        String d=null;
        try {
            Cursor c = getAllbillID();
            while (c.moveToNext()) {
                d = c.getString(0);
                yes = c.getString(1);
            }
            if(yes==null||d==""||d==null)
            {
                bno=0;
            }
            else if (!d1.equals(yes) ) {
//
                bno=0;

            }
            else if(d1.equals(yes))
            {
                bno=Integer.parseInt(d);
            }

            if (bno == Billno) {
                ContentValues Itemvalues = new ContentValues();
                Itemvalues.put(DatabaseHelper.Billnofk, Billno);
                Itemvalues.put(DatabaseHelper.BillItemName, itemname);
                Itemvalues.put(DatabaseHelper.BillItemqty, itemqty);
                Itemvalues.put(DatabaseHelper.BillItemrate, itemrate);
                Itemvalues.put(DatabaseHelper.Billtotalrate, itemtotalrate);
                Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
                Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
                Itemvalues.put(DatabaseHelper.isactive,0);
                Itemvalues.put(DatabaseHelper.WebBillDeatilsId,("App"+"-"+cid+"-"+lid+"-"+emp_id+"-"+dateFormat.format(date)+"-"+bd));
                Itemvalues.put(DatabaseHelper.cid,cid);
                Itemvalues.put(DatabaseHelper.lid,lid);
                Itemvalues.put(DatabaseHelper.emp_id,emp_id);
                Itemvalues.put(DatabaseHelper.sync_flag,2);
                Itemvalues.put(DatabaseHelper.bill_code,emp_code);
                Itemvalues.put(DatabaseHelper.BillDate, dateFormat1.format(date));
                Itemvalues.put(DatabaseHelper.BillItemdiscount,Double.parseDouble(item_dis));
                Itemvalues.put(DatabaseHelper.BillItemtax,Double.parseDouble(item_tax));
                Itemvalues.put(DatabaseHelper.BillItembasicrate, basicrate);

                db.insert(tbl_AddBillDetails, null, Itemvalues);
            } else {

                ContentValues itemValues = new ContentValues();
                itemValues.put(DatabaseHelper.Billno, Billno);
                itemValues.put(DatabaseHelper.BillDate, dateFormat1.format(date));
                itemValues.put(DatabaseHelper.BillCustomerName, customername);
                if(cashcredit=="")
                {
                    itemValues.put(DatabaseHelper.Billcashorcredit, "Cash");
                }
                else {
                    itemValues.put(DatabaseHelper.Billcashorcredit, cashcredit);
                }

                itemValues.put(DatabaseHelper.Billdiscount, 0);
                itemValues.put(DatabaseHelper.Billtotalamt, itemtotalamt);
                itemValues.put(DatabaseHelper.Billtax, 0);
                itemValues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
                itemValues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
                itemValues.put(DatabaseHelper.isactive,0);
                itemValues.put(DatabaseHelper.WebbillmasterId,("App"+"-"+cid+"-"+lid+"-"+emp_id+"-"+dateFormat.format(date)+"-"+bm));
                itemValues.put(DatabaseHelper.cid,cid);
                itemValues.put(DatabaseHelper.lid,lid);
                itemValues.put(DatabaseHelper.emp_id,emp_id);
                itemValues.put(DatabaseHelper.sync_flag,2);
                itemValues.put(DatabaseHelper.bill_code,emp_code);
                if(pointofcontact.equals(""))
                {
                    pointofcontact="0";
                }
                itemValues.put(DatabaseHelper.pointofcontact,Integer.parseInt(pointofcontact));
                itemValues.put(DatabaseHelper.paymentdetails,paymentdetails);
                itemValues.put(DatabaseHelper.orderdetails,orderdetails);
                if(gst_settings1.equals("GST Enable")){
                    itemValues.put(DatabaseHelper.gst_settings,"Yes");
                }
                else{
                    itemValues.put(DatabaseHelper.gst_settings,"No");
                }

//                itemValues.put(DatabaseHelper.bil_payement_type,0);
//                itemValues.put(DatabaseHelper.bil_point_of_contact,0);
                db.insert(tbl_AddBillMaster, null, itemValues);

                ContentValues Itemvalues = new ContentValues();
                Itemvalues.put(DatabaseHelper.Billnofk, Billno);
                Itemvalues.put(DatabaseHelper.BillItemName, itemname);
                Itemvalues.put(DatabaseHelper.BillItemqty, itemqty);
                Itemvalues.put(DatabaseHelper.BillItemrate, itemrate);
                Itemvalues.put(DatabaseHelper.Billtotalrate, itemtotalrate);
                Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
                Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
                Itemvalues.put(DatabaseHelper.isactive,0);
                Itemvalues.put(DatabaseHelper.WebBillDeatilsId,("App"+"-"+cid+"-"+lid+"-"+emp_id+"-"+dateFormat.format(date)+"-"+bd));
                Itemvalues.put(DatabaseHelper.cid,app_bil_id);
                Itemvalues.put(DatabaseHelper.lid,app_bil_id);
                Itemvalues.put(DatabaseHelper.emp_id,emp_id);
                Itemvalues.put(DatabaseHelper.sync_flag,2);
                Itemvalues.put(DatabaseHelper.bill_code,emp_code);
                Itemvalues.put(DatabaseHelper.BillDate, dateFormat1.format(date));
                Itemvalues.put(DatabaseHelper.BillItemdiscount,Double.parseDouble(item_dis));
                Itemvalues.put(DatabaseHelper.BillItemtax,Double.parseDouble(item_tax));
                Itemvalues.put(DatabaseHelper.BillItembasicrate, basicrate);
                db.insert(tbl_AddBillDetails, null, Itemvalues);
            }
        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }



    public void AddCutomer (String companyName,String customername,String Address, String mobile,String email,String gst)
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");

        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table
        int a=1;
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.CCompanyName,companyName);
        Itemvalues.put(DatabaseHelper.CName,customername);
        Itemvalues.put(DatabaseHelper.CAddress,Address);
        Itemvalues.put(DatabaseHelper.CMobile,mobile);
        Itemvalues.put(DatabaseHelper.CEmail,email);
        Itemvalues.put(DatabaseHelper.CompanyIdorGST,gst);
        Itemvalues.put(DatabaseHelper.cid,c_id);
        Itemvalues.put(DatabaseHelper.lid,l_id);
        Itemvalues.put(DatabaseHelper.emp_id,e_id);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,active);
        db.insert(tbl_AddCutomer, null, Itemvalues);
    }
    public void AddSupplier (String companyName,String customername,String Address, String mobile,String email,String gst)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table
        int a=1;
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.sup_CompanyName,companyName);
        Itemvalues.put(DatabaseHelper.sup_name,customername);
        Itemvalues.put(DatabaseHelper.sup_address,Address);
        Itemvalues.put(DatabaseHelper.sup_mobile_no,mobile);
        Itemvalues.put(DatabaseHelper.sup_email_id,email);
        Itemvalues.put(DatabaseHelper.sup_cust_companyId_or_GST,gst);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,active);
        db.insert(tbl_AddSupplier, null, Itemvalues);
    }

    public void AddTax (String tname,String tvalue)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table

        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.ATaxName,tname);
        Itemvalues.put(DatabaseHelper.ATaxName,tname);
        Itemvalues.put(DatabaseHelper.TaxValue,tvalue);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        db.insert(tbl_AddTax, null, Itemvalues);
    }

    public void AddUnits(String Unitname,String Unittaxname )
    {
        SQLiteDatabase db = this.getWritableDatabase();
        //adding user name in users table

        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.UnitName,Unitname);
        Itemvalues.put(DatabaseHelper.UnitTaxName,Unittaxname);
        Itemvalues.put(DatabaseHelper.isactive,active);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        db.insert(tbl_AddIUnits, null, Itemvalues);
    }

    public void deletecustomerlist(final String catnam){
        int a=1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            db.update("tbl_Addcustomer", cv, "cust_id" + "= ?", new String[] {catnam});

        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }

    public void deleteCategoryList(String catnam){
        int a=1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            db.update("tbl_AddCategory", cv, "cat_id" + "= ?", new String[] {catnam});

        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }
    public void deleteUnitsList(String catnam){
        int a=1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            db.update("tbl_AddIUnits", cv, "_Unit_Id" + "= ?", new String[] {catnam});


        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }
    public void deleteItemList(String catnam){
        int a=1;
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            db.update("tbl_AddItems", cv, "item_id" + "= ?", new String[] {catnam});


        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }
    /* public Cursor getAllbillID()
     {
         List<String> categorylist=new ArrayList<>();
         //get readable database
         SQLiteDatabase db=this.getReadableDatabase();
         Cursor cursor=db.rawQuery("Select MAX(bill_no),bill_date from tbl_AddBillMaster  ",null);

         //close the cursor
         //cursor.close();
         //close the database
         //db.close();
         return cursor;
     }*/
    public Cursor getAllbillID()
    {    Cursor cursor = null;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        print_option=pref.getString("print_option","");
        gst_option=pref.getString("gst_option","");
        selectprinter_option=pref.getString("selectprinter_option","");
        billwithprint=pref.getString("bill_with_print","");
        multipleprint=pref.getString("multiple_printing","");
        resetbill=pref.getString("reset_bill","");
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        if(resetbill.equals("Yes")) {

            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String d1 = dateFormat.format(date);
            List<String> categorylist = new ArrayList<>();
            //get readable database

            SQLiteDatabase db = this.getReadableDatabase();
            try {
                cursor = db.rawQuery("Select MAX(bill_no),bill_date from tbl_AddBillMaster where bill_date='" + d1 + "' and cid="+c_id+" and lid="+l_id+"", null);
            } catch (Exception ex) {
                Message.message(context, "" + ex.getMessage());
            }
        }
        else if(resetbill.equals("No")){

            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String d1 = dateFormat.format(date);
            List<String> categorylist = new ArrayList<>();
            //get readable database

            SQLiteDatabase db = this.getReadableDatabase();
            try {
                cursor = db.rawQuery("Select MAX(bill_no),bill_date from tbl_AddBillMaster", null);

            } catch (Exception ex) {
                Message.message(context, "" + ex.getMessage());
            }
        }

        return cursor;
    }
    public Cursor getAllbill()
    {
        List<String> categorylist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddBillDetails",null);

        //close the cursor
        cursor.close();
        //close the database
//        db.close();
        return cursor;
    }
    public Cursor getAllbillWithID(int id)
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        List<String> categorylist = new ArrayList<>();
        Cursor cursor = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String bdate=dateFormat.format(date);

            //get readable database
            SQLiteDatabase db = this.getReadableDatabase();

            if(l_id.equals("null")){
                cursor=db.rawQuery("select tbl_AddBillMaster.bill_date,tbl_AddBillMaster.bill_totalamt,tbl_AddBillMaster.bill_no,tbl_AddBillDetail.item_name,tbl_AddBillDetail.item_qty,tbl_AddBillDetail.item_rate,tbl_AddBillDetail.item_totalrate,tbl_AddBillDetail.item_discount,tbl_AddBillDetail.item_tax,tbl_AddBillDetail.item_basicrate FROM tbl_AddBillDetail LEFT JOIN tbl_AddBillMaster ON tbl_AddBillDetail.bill_no = tbl_AddBillMaster.bill_no and tbl_AddBillDetail.bill_date = tbl_AddBillMaster.bill_date  where tbl_AddBillDetail.bill_date='"+bdate+"' and tbl_AddBillDetail.bill_no="+id+" and cid="+c_id+"",null);
            }else {
                cursor=db.rawQuery("select tbl_AddBillMaster.bill_date,tbl_AddBillMaster.bill_totalamt,tbl_AddBillMaster.bill_no,tbl_AddBillDetail.item_name,tbl_AddBillDetail.item_qty,tbl_AddBillDetail.item_rate,tbl_AddBillDetail.item_totalrate,tbl_AddBillDetail.item_discount,tbl_AddBillDetail.item_tax,tbl_AddBillDetail.item_basicrate FROM tbl_AddBillDetail LEFT JOIN tbl_AddBillMaster ON tbl_AddBillDetail.bill_no = tbl_AddBillMaster.bill_no and tbl_AddBillDetail.bill_date = tbl_AddBillMaster.bill_date  where tbl_AddBillDetail.bill_date='"+bdate+"' and tbl_AddBillDetail.bill_no="+id+" and cid="+c_id+" and lid="+l_id+"",null);
            }
            //cursor = db.rawQuery("SELECT tbl_AddBillMaster.bill_date,tbl_AddBillMaster.bill_totalamt, tbl_AddBillDetail.bill_no,tbl_AddBillDetail.item_name,tbl_AddBillDetail.item_qty,tbl_AddBillDetail.item_rate,tbl_AddBillDetail.item_totalrate,tbl_AddBillDetail.item_discount,tbl_AddBillDetail.item_tax from tbl_AddBillMaster Inner join tbl_AddBillDetail on tbl_AddBillMaster.bill_no=tbl_AddBillDetail.bill_no  where  (tbl_AddBillDetail.bill_no = "+id+") and (tbl_AddBillDetail.bill_date = '"+bdate+"') " , null);
//            cursor = db.rawQuery("SELECT tbl_AddBillMaster.bill_date,tbl_AddBillMaster.bill_totalamt, tbl_AddBillDetail.bill_no,tbl_AddBillDetail.item_name,tbl_AddBillDetail.item_qty,tbl_AddBillDetail.item_rate,tbl_AddBillDetail.item_totalrate,tbl_AddBillDetail.item_discount,tbl_AddBillDetail.item_tax from tbl_AddBillMaster Inner join tbl_AddBillDetail on tbl_AddBillMaster.bill_no=tbl_AddBillDetail.bill_no where  (tbl_AddBillDetail.bill_no = "+id+") and (tbl_AddBillDetail.bill_date = '"+bdate+"') " , null);

        }
        catch (Exception ex)
        {
            //Message.message(context,ex.getMessage());
            String s=ex.getMessage().toString();
        }
        return cursor;
    }
    public List<String> getAllCategory()
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        int a=0;
        List<String> categorylist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")) {
            cursor = db.rawQuery("SELECT * FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + "", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }
        if(cursor.moveToFirst())
        {
            do {
                categorylist.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        return categorylist;
    }
    public List<String> getAllCategoryId()
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        int a=0;
        List<String> categorylistid=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT cat_web_id FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + "", null);
        }else {
            cursor = db.rawQuery("SELECT cat_web_id FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }
        if(cursor.moveToFirst())
        {
            do {
                categorylistid.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        return categorylistid;
    }
    public Cursor getAllCategorywiseItem(String catName)
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        int a=0;
        List<String> categorylist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        String name=catName;Cursor c=null;
        try {

            if(l_id.equals("null")){
                c = db.rawQuery("SELECT * FROM tbl_AddItems WHERE item_category='" + catName + "' and isactive=" + a + " and cid=" + c_id + "", null);//",null);// WHERE item_category ='"+catName+"'", null);
            }else {
                c = db.rawQuery("SELECT * FROM tbl_AddItems WHERE item_category='" + catName + "' and isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);//",null);// WHERE item_category ='"+catName+"'", null);
            }

//                     if(c.moveToFirst()) {
//                         do {
//                             categorylist.add(c.getString(0));
//
//                         } while (c.moveToNext());
//
//                     }
            //close the cursor
            //cursor.close();
            //close the database
            //db.close();
        }
        catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
        return c;
    }
    public List<String> getAllTax()
    {
        List<String> taxlist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddTax  ",null);
        if(cursor.moveToFirst())
        {
            do {
                taxlist.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        //close the cursor
        //cursor.close();
        //close the database
        // db.close();
        return taxlist;
    }

    public Cursor getAllHeaderFooter()
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        List<String> headerfooter=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor=db.rawQuery("SELECT * FROM tbl_AddFooter where cid=" + c_id + " and emp_id="+emp_id+"", null);
        }else {
            cursor=db.rawQuery("SELECT * FROM tbl_AddFooter where cid=" + c_id + " and lid=" + l_id + " and emp_id="+emp_id+"", null);
        }
        //Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddFooter WHERE ID = (SELECT MAX(headerfooter_id) FROM tbl_AddFooter)",null);
        return cursor;
    }


    public List<String> getAllUnits()
    {
        int a=0;
        List<String> unitslist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddIUnits where isactive="+a+"",null);
        if(cursor.moveToFirst())
        {
            do {
                unitslist.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        //close the cursor
        //cursor.close();
        //close the database
        // db.close();
        return unitslist;
    }
    public Cursor getAllItems()
    {  int a=0;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT * FROM tbl_AddItems where isactive=" + a + " and cid=" + c_id + "  ORDER BY item_id", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_AddItems where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + " ORDER BY item_id", null);
        }
        return cursor;
    }
    public  List<String> getAllItem()
    {
        List<String> itemlist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddItems",null);
        if(cursor.moveToFirst())
        {
            do {
                itemlist.add(cursor.getString(1));
                //itemlist.add(cursor.getString(2));
            }while (cursor.moveToNext());
        }
        //close the cursor
        //cursor.close();
        //close the database
        // db.close();
        return itemlist;
    }
    public  List<String> getUnits()
    {
        List<String> pricelist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddIUnits",null);
        if(cursor.moveToFirst())
        {
            do {
                pricelist.add(cursor.getString(2));
                //itemlist.add(cursor.getString(2));
            }while (cursor.moveToNext());
        }
        //close the cursor
        // cursor.close();
        //close the database
        // db.close();
        return pricelist;
    }
    public  List<String> getAllprice()
    {
        List<String> pricelist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddItems",null);
        if(cursor.moveToFirst())
        {
            do {
                pricelist.add(cursor.getString(7));
                //itemlist.add(cursor.getString(2));
            }while (cursor.moveToNext());
        }
        //close the cursor
        //  cursor.close();
        //close the database
        //  db.close();
        return pricelist;
    }



//    public void Units (String units )
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        //adding user name in users table
//
//        ContentValues Itemvalues = new ContentValues();
//        Itemvalues.put(DatabaseHelper.CatName,catName);
//        db.insert(tbl_AddCategory, null, Itemvalues);
//    }


    public String getItemFromcode(int item_code){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        String str_itemName = "";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            int a=0;
            Cursor c;
            if(l_id.equals("null"))
            {
                c = db.rawQuery("SELECT item_name,item_dis,item_tax,item_final_rate,item_rate,item_disrate FROM tbl_AddItems WHERE item_code=" + item_code + " and isactive= " + a + " and cid=" + c_id +"", null);
            }else {
                c = db.rawQuery("SELECT item_name,item_dis,item_tax,item_final_rate,item_rate,item_disrate FROM tbl_AddItems WHERE item_code=" + item_code + " and isactive= " + a + " and cid=" + c_id + " and lid= " + l_id + "", null);
            }
            if (c != null) {
                if (c.moveToFirst()) {

                    str_itemName = c.getString(0);
                    str_itemName = str_itemName + "," + c.getString(1);
                    str_itemName = str_itemName + "," + c.getString(2);
                    str_itemName = str_itemName + "," + c.getString(3);
                    str_itemName = str_itemName + "," + c.getString(4);
                    str_itemName = str_itemName + "," + c.getString(5);
                /*if (!c.isClosed()) {
                    c.close();
                }*/
                }
            } else {
                str_itemName = "Item Not Exist";
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return str_itemName;
    }

    public void AddInventory(String str_Supplier_id,String Sup_Name,String str_item_id,String item_name,String str_qty,String str_status)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int total_quantity = 0, item_qty = 0;
        int str=0;

        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.inventorysupid,str_Supplier_id);
        Itemvalues.put(DatabaseHelper.inventorysupname,Sup_Name);
        Itemvalues.put(DatabaseHelper.inventoryitemid,str_item_id);
        Itemvalues.put(DatabaseHelper.inventoryitemname,item_name);
        Itemvalues.put(DatabaseHelper.inventoryitemquantity,str_qty);
        Itemvalues.put(DatabaseHelper.inventorystatus,str_status);

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        db.insert(tbl_inventory, null, Itemvalues);

        Cursor c = db.rawQuery("SELECT item_stock FROM tbl_AddItems WHERE item_id=" + str_item_id + "", null);
        c.moveToFirst();
        if (c==null){Message.message(context,"Item not exists!!!!");}
        else {


            item_qty = Integer.parseInt(c.getString(0));
            if (str_status.equals("Add")) {
                total_quantity = item_qty + Integer.parseInt(str_qty);
            } else if (str_status.equals("Subtract")) {
                total_quantity = item_qty - Integer.parseInt(str_qty);
            } else {
                total_quantity = Integer.parseInt(str_qty);
            }

            ContentValues cv = new ContentValues();
            cv.put(DatabaseHelper.Stock, total_quantity);
            db.update("tbl_AddItems", cv, "item_id" + "= ?", new String[]{str_item_id});
        }
        Cursor cursor=db.rawQuery("SELECT item_name FROM tbl_AddItems where item_id="+str_item_id+"",null);
        if(cursor.moveToFirst())
        {
            do {
                itemnameforinventory.add(cursor.getString(0));

            }while (cursor.moveToNext());
        }
    }


    public Cursor getAllInvetory()
    {
        List<String> itemlist=new ArrayList<>();

        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_inventory",null);
        if(cursor.moveToFirst())
        {
            do {
                itemlist.add(cursor.getString(2));

            }while (cursor.moveToNext());
        }
        return cursor;
    }

    public Cursor getInventoryReport(String d1,String d2)
    {
        List<String> itemlist=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor c=null;
        try {
            c=db.rawQuery("SELECT * FROM tbl_inventory as tblinv, tbl_AddItems as tbladditem where tblinv.item_id=tbladditem.item_id and tblinv.created_at between'"+d1+"' and '"+d2+"'",null);
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return c;
    }

    public Cursor getSalesBillReport( String d1,String d2)
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        List<String> itemlist=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try {
            if(l_id.equals("null")){
                cursor = db.rawQuery("SELECT * FROM tbl_AddBillMaster WHERE bill_date between'" + d1 + "' and '" + d2 + "' and  isactive=0 and cid=" + c_id + " and emp_id="+e_id+"", null);
            }else {
                cursor = db.rawQuery("SELECT * FROM tbl_AddBillMaster WHERE bill_date between'" + d1 + "' and '" + d2 + "' and  isactive=0 and cid=" + c_id + " and lid=" + l_id + " and emp_id="+e_id+"", null);
            }

        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }
    public Cursor Itemwisesalesbillreport( String d1,String d2)
    {

        List<String> itemlist=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try
        { //, (quantity*item_rate)as totalrate
            cursor = db.rawQuery("SELECT item_name,sum(item_qty) as quantity,item_rate FROM tbl_AddBillDetail WHERE created_at between'"+d1+"' and '"+d2+"' group by item_name",null);
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }

    public Cursor getItemDetailsOfItemName(String itemname){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=null;
        try {
            cursor = db.rawQuery("SELECT * FROM tbl_AddItems WHERE item_name='"+itemname+"'", null);
        }catch (Exception e){
            e.printStackTrace();
        }
        return cursor;
    }

    public void EditItem(String itemid,String item_name,String rate,String dis,String after_discount,String tax,String tax2,String tax3,String units,String category,String stock,String barcode){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("item_name", item_name);
        cv.put("item_rate", rate);
        cv.put("item_dis", dis);
        cv.put("item_disrate", after_discount);
        cv.put("item_tax", tax);
        cv.put("item_taxvalue", tax2);
        cv.put("item_final_rate", tax3);
        cv.put("item_units", units);
        cv.put("item_category", category);
        cv.put("item_stock", stock);
        cv.put("item_barcode", barcode);
        db.update("tbl_AddItems", cv, "item_id" + "= ?", new String[] {itemid});
    }

    public Cursor getCategoryDetails() {
        int a=0;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
//        Cursor cursor=db.rawQuery("SELECT * FROM tbl_AddCategory where isactive="+a+"" , null);
        if (l_id.equals("null")) {
            cursor = db.rawQuery("SELECT * FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + "", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_AddCategory where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }
        return cursor;
    }

    public void Update_cat(String cat_id,String cat,String oldcat){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("cat_name", cat);
        db.update("tbl_AddCategory", cv, "cat_id" + "= ?", new String[] {cat_id});
        ContentValues cv1 = new ContentValues();
        cv1.put("item_category", cat);
        db.update("tbl_AddItems", cv1, "item_category" + "= ?", new String[] {oldcat});
//        Cursor cursor=db.rawQuery("update tbl_AddItems set item_category="+cat+" where item_category="+oldcat+"", null);



    }
    public void Update_inventory( String itemname, String qty1){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        List<String> categorylist=new ArrayList<>();Double totalqty=0.0;

        try {

            Cursor cursor;
            if(l_id.equals("null")){
                cursor = db.rawQuery("SELECT * from tbl_AddItems WHERE item_name = '" + itemname + "' and cid=" + c_id + "", null);
            }else {
                cursor = db.rawQuery("SELECT * from tbl_AddItems WHERE item_name = '" + itemname + "' and cid=" + c_id + " and lid=" + l_id + "", null);
            }


            if(cursor.moveToFirst())
            {
                do {
                    String qty=cursor.getString(10);
                    totalqty= Double.parseDouble(qty)- Double.parseDouble(qty1);
                    db.execSQL("UPDATE tbl_AddItems SET item_stock ='"+totalqty+"' WHERE item_name = '"+itemname+"' and cid="+c_id+" and lid="+l_id+"");

                }while (cursor.moveToNext());
            }



        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }
    public Cursor getdistinct(String itemtax)
    {  int a=0;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT distinct(item_name) FROM tbl_AddItems where item_tax="+itemtax+" and isactive="+a+"", null);
        return cursor;
    }
    public Cursor getUnitDetails() {
        int a=0;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT * FROM tbl_AddIUnits where isactive=" + a + " and cid=" + c_id + "", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_AddIUnits where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }
        return cursor;
    }

    public void Update_unit(String unit_id,String unit_name,String unit_code,String oldunit){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("Unit_name", unit_name);
        cv.put("Unit_Taxvalue", unit_code);
        db.update("tbl_AddIUnits", cv, "_Unit_Id" + "= ?", new String[] {unit_id});
        ContentValues cv1 = new ContentValues();
        cv1.put("item_units", unit_name);
        db.update("tbl_AddItems", cv1, "item_units" + "= ?", new String[] {oldunit});

    }

    public Cursor getCustomerDetails() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        int a=0;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT * FROM tbl_Addcustomer where isactive=" + a + " and cid=" + c_id +  "", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_Addcustomer where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }

//        cursor = db.rawQuery("SELECT * FROM tbl_Addcustomer where isactive=" + a + " ", null);
        return cursor;
    }

    public Cursor getCustomerDetailsByContact(String cust_id)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("SELECT * FROM tbl_Addcustomer WHERE cust_id='"+cust_id+"'", null);
        return cursor;
    }

    public void updateCustomerDetails(String cust_id, String company_name,String name,String address,String mobile, String etemail, String company_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("cust_CompanyName", company_name);
        cv.put("cust_name", name);
        cv.put("address", address);
        cv.put("mobile_no", mobile);
        cv.put("email_id", etemail);
        cv.put("cust_companyId_or_GST", company_id);
        db.update("tbl_Addcustomer", cv, "cust_id" + "= ?", new String[] {cust_id});
    }

    public Cursor stock_details(){
        int a=0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT item_name,item_stock FROM tbl_AddItems  where isactive="+a+"", null);
        return c;
    }

    public Cursor getSupplierDetails() {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        int a=0;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT * FROM tbl_AddSupplier where isactive=" + a + " and cid=" + c_id + "", null);
        }else {
            cursor = db.rawQuery("SELECT * FROM tbl_AddSupplier where isactive=" + a + " and cid=" + c_id + " and lid=" + l_id + "", null);
        }
        return cursor;
    }

    public void checkItemAlreadyExist(String email)
    {  SQLiteDatabase db=this.getReadableDatabase();
        boolean a;int isactive=0;
        String query = "SELECT item_name,isactive  FROM tbl_AddItems WHERE item_name=?";
        Cursor cursor = db.rawQuery(query, new String[]{email});
        if (cursor.getCount() > 0)
        {
            a=false;
//            return false;
        }
        else {
//            return true;
            a=true;
            isactive=Integer.parseInt(cursor.getString(1));
        }
        if(a==true && isactive==0)
        {
            isactive=1;
            ContentValues cv = new ContentValues();
            cv.put("isactive", isactive);
            db.update("tbl_AddItems", cv, "item_name" + "= ?", new String[] {email});


        }
        else if(a==true && isactive==1)
        {
            Message.message(context,"Item already exist");

        }
    }

    //emp_id,sync_flag

    public void SyncCategory (String catName,String catWebId,String cat_description,String cat_image,String type_id,String created_at,String updated_at,String is_active,String cid,String lid,String emp_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.CatWebID,catWebId);
        Itemvalues.put(DatabaseHelper.CatName,catName);
        Itemvalues.put(DatabaseHelper.cat_description,cat_description);
        Itemvalues.put(DatabaseHelper.cat_image,cat_image);
        Itemvalues.put(DatabaseHelper.type_id,0);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,is_active);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        db.insertWithOnConflict(tbl_AddCategory, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
        //db.insert(tbl_AddCategory, null, Itemvalues);
    }


    public void SyncItem(String id,String item_name,String item_id,String item_rate,String item_dis,String item_disrate,String item_tax,String item_taxvalue,String item_final_rate,
                         String item_category,String item_units,String item_stock,String item_barcode,String item_hsncode,String isactive,String cid,String lid,String emp_id,String sub_emp_id,String item_code){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.ItemID,id);
        Itemvalues.put(DatabaseHelper.WebItemID,item_id);
        Itemvalues.put(DatabaseHelper.ItemName,item_name);
        Itemvalues.put(DatabaseHelper.Rate,item_rate);
        Itemvalues.put(DatabaseHelper.dis,item_dis);
        Itemvalues.put(DatabaseHelper.disrate,item_disrate);
        Itemvalues.put(DatabaseHelper.TaxName,item_tax);
        Itemvalues.put(DatabaseHelper.itemTaxValue,item_taxvalue);
        Itemvalues.put(DatabaseHelper.FinalRate,item_final_rate);
        Itemvalues.put(DatabaseHelper.Category,item_category);
        Itemvalues.put(DatabaseHelper.Units,item_units);
        Itemvalues.put(DatabaseHelper.Stock,item_stock);
        Itemvalues.put(DatabaseHelper.Barcode,item_barcode);
        Itemvalues.put(DatabaseHelper.HSNCode,item_hsncode);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sub_emp_id,sub_emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,0);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,isactive);
        Itemvalues.put(DatabaseHelper.item_code,item_code);
        db.insertWithOnConflict(tbl_AddItems, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void SyncCust(String cust_id,String cust_CompanyName,String cust_name,String address,String mobile_no,String email_id,String cust_companyId_or_GST,String cretedat,String updatedat,String isactive,String cid,String lid,String emp_id){

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();
        int a=0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.Web_cust_id,cust_id);
        Itemvalues.put(DatabaseHelper.CCompanyName,cust_CompanyName);
        Itemvalues.put(DatabaseHelper.CName,cust_name);
        Itemvalues.put(DatabaseHelper.CMobile,mobile_no);
        Itemvalues.put(DatabaseHelper.CEmail,email_id);
        Itemvalues.put(DatabaseHelper.CAddress,address);
        Itemvalues.put(DatabaseHelper.CompanyIdorGST,cust_companyId_or_GST);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,cretedat);
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,isactive);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,1);
        db.insertWithOnConflict(tbl_AddCutomer, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void SyncUnit (String UnitName,String UnitWebId,String unit_tax_val,String unit_created_at,String unit_is_active,String unit_cid,String unit_lid,String unit_emp_id)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.WebUnitsID,UnitWebId);
        Itemvalues.put(DatabaseHelper.UnitName,UnitName);
        Itemvalues.put(DatabaseHelper.UnitTaxName,unit_tax_val);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,unit_is_active);
        Itemvalues.put(DatabaseHelper.Unitcid,unit_cid);
        Itemvalues.put(DatabaseHelper.Unitlid,unit_lid);
        Itemvalues.put(DatabaseHelper.Unitemp_id,unit_emp_id);
        Itemvalues.put(DatabaseHelper.Unitsync_flag,1);
        db.insertWithOnConflict(tbl_AddIUnits, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void SyncSup(String sup_id,String sup_name,String sup_address,String sup_mobile_no,String sup_email_id,String sup_gst_no,String created_at,String UpdatedAt,String is_active,String cid,String lid,String emp_id)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.WebSID,sup_id);
        Itemvalues.put(DatabaseHelper.sup_CompanyName,sup_name);
        Itemvalues.put(DatabaseHelper.sup_name,sup_name);
        Itemvalues.put(DatabaseHelper.sup_address,sup_address);
        Itemvalues.put(DatabaseHelper.sup_mobile_no,sup_mobile_no);
        Itemvalues.put(DatabaseHelper.sup_email_id,sup_email_id);
        Itemvalues.put(DatabaseHelper.sup_cust_companyId_or_GST,sup_gst_no);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,created_at);
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,is_active);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,1);
        db.insertWithOnConflict(tbl_AddSupplier, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void Syncinvetory(String inventoryid,String inventorysupid,String inventoryitemid,String inventoryitemquantity,String inventorystatus,String created_at,String updated_at,String is_active,String cid,String lid,String emp_id){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.inventorysupid,inventorysupid);
        Itemvalues.put(DatabaseHelper.inventorysupname,"");
        Itemvalues.put(DatabaseHelper.inventoryitemid,inventoryitemid);
        Itemvalues.put(DatabaseHelper.inventoryitemname,"");
        Itemvalues.put(DatabaseHelper.inventoryitemquantity,inventoryitemquantity);
        Itemvalues.put(DatabaseHelper.inventorystatus,inventorystatus);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        Itemvalues.put(DatabaseHelper.isactive,1);
        Itemvalues.put(DatabaseHelper.web_inv_id,inventoryid);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,1);
        db.insertWithOnConflict(tbl_inventory, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public Cursor getbillmaster(int start, int end)
    {

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try {
            cursor = db.rawQuery("SELECT * FROM tbl_AddBillMaster WHERE sync_flag=2 LIMIT "+start+","+end+"",null);
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }
    public Cursor getbillmasterdetails(int start,int end)
    {
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;

        try {
            cursor = db.rawQuery("SELECT * FROM tbl_AddBillDetail WHERE sync_flag=2 LIMIT "+start+","+end+"",null);
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }
    public int get_maxid_bill_master(){
        SQLiteDatabase db=this.getReadableDatabase();
        int max_master_id=0;
        Cursor cursor=null;
        cursor = db.rawQuery("SELECT MAX(bill_ID) FROM tbl_AddBillMaster", null);
        int r;
        while (cursor.moveToNext()) {
            String d = cursor.getString(0);
            if (d == "" || d == null) {
                max_master_id=1;
            } else {
                r = Integer.parseInt(cursor.getString(0));
                r = r + 1;

                max_master_id= r;
            }
        }
        return max_master_id;
    }

    public int get_maxid_bill_details(){
        SQLiteDatabase db=this.getReadableDatabase();
        int max_master_id=0;
        Cursor cursor=null;

        cursor = db.rawQuery("SELECT MAX(billD_id) FROM tbl_AddBillDetail", null);

        int r;
        while (cursor.moveToNext()) {
            String d = cursor.getString(0);
            if (d == "" || d == null) {
                max_master_id=1;
            } else {
                r = Integer.parseInt(cursor.getString(0));
                r = r + 1;

                max_master_id= r;
            }
        }
        return max_master_id;
    }


    public int total_rows_bill_deatils(){
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Cursor cursor;
        cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillDetail WHERE sync_flag=2",null);
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }
        return numRows;
    }

    public int total_rows_bill_master(){
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Cursor cursor;
        cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster WHERE sync_flag=2",null);
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }
        return numRows;
    }

    public void updateBillMasterIDs(ArrayList master_bill_id_list){
        SQLiteDatabase db = this.getWritableDatabase();
        for(int u=0;u<master_bill_id_list.size();u++) {
            ContentValues cv = new ContentValues();
            cv.put("sync_flag", 3);
            db.update("tbl_AddBillMaster", cv, "bill_ID" + "= ?", new String[]{""+master_bill_id_list.get(u)});
        }
    }

    public void updateBillDetailsIDs(ArrayList details_bill_id_list){
        SQLiteDatabase db = this.getWritableDatabase();
        for(int u=0;u<details_bill_id_list.size();u++) {
            ContentValues cv = new ContentValues();
            cv.put("sync_flag", 3);
            db.update("tbl_AddBillDetail", cv, "billD_id" + "= ?", new String[]{""+details_bill_id_list.get(u)});
        }
    }

    public void Syncpointcontact(String id,String point_of_contact,String is_active,String cid,String lid,String emp_id,String sync_flag,String created_at,String updated_at){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.Web_point_contact_id,id);
        Itemvalues.put(DatabaseHelper.point_of_contact,point_of_contact);
        Itemvalues.put(DatabaseHelper.is_active,is_active);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,sync_flag);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,created_at);
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        db.insertWithOnConflict(bil_point_of_contact, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void Syncpayment(String id,String paymnt,String is_active,String cid,String lid,String emp_id,String sync_flag,String created_at,String updated_at){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.getDefault());
        Date date = new Date();

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues Itemvalues = new ContentValues();
        Itemvalues.put(DatabaseHelper.Web_paymnet_id,id);
        Itemvalues.put(DatabaseHelper.payment_type,paymnt);
        Itemvalues.put(DatabaseHelper.is_active,is_active);
        Itemvalues.put(DatabaseHelper.cid,cid);
        Itemvalues.put(DatabaseHelper.lid,lid);
        Itemvalues.put(DatabaseHelper.emp_id,emp_id);
        Itemvalues.put(DatabaseHelper.sync_flag,sync_flag);
        Itemvalues.put(DatabaseHelper.created_at_TIMESTAMP,created_at);
        Itemvalues.put(DatabaseHelper.updated_at_TIMESTAMP,dateFormat.format(date));
        db.insertWithOnConflict(bil_payement_type, null, Itemvalues,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public Cursor payment_details(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try {
            if(l_id.equals("null")) {
                cursor = db.rawQuery("SELECT * FROM bil_payement_type where is_active=0 and cid=" + c_id + "", null);
            }else {
                cursor = db.rawQuery("SELECT * FROM bil_payement_type where is_active=0 and cid=" + c_id + " and lid=" + lid + "", null);
            }
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }

    public Cursor point_contact_details(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");

        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try {
            if(l_id.equals("null")){
                cursor = db.rawQuery("SELECT * FROM bil_point_of_contact where is_active=0 and cid=" + c_id + "", null);
            }else {
                cursor = db.rawQuery("SELECT * FROM bil_point_of_contact where is_active=0 and cid=" + c_id + " and lid=" + lid + "", null);
            }
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }

    public int get_total_bills(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster where isactive=0 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster where isactive=0 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }
        return numRows;
    }

    public double get_total_sale(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        double numRows = 0;
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT SUM (bill_totalamt) FROM tbl_AddBillMaster where isactive=0 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT SUM (bill_totalamt) FROM tbl_AddBillMaster where isactive=0 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Double.parseDouble(cursor.getString(0));
        }else {
            numRows=0;
        }
        return numRows;
    }

    public int get_todays_bill(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=0 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=0 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }

        return numRows;
    }

    public double get_todays_sales(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        double numRows = 0;
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT SUM (bill_totalamt) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=0 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT SUM (bill_totalamt) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=0 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Double.parseDouble(cursor.getString(0));
        }else {
            numRows=0;
        }

        return numRows;
    }

    public void deleteSaleBill(String catnam,String billDate,String remark){
        int a=1;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            cv.put("sync_flag", 2);
            cv.put("remark", remark);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            cv.put("modified_at", dateFormat.format(date));
            if(l_id.equals("null")){
                db.update("tbl_AddBillMaster", cv, "bill_no=? and bill_date=? and cid=?", new String[]{catnam, billDate, c_id});
            }else {
                db.update("tbl_AddBillMaster", cv, "bill_no=? and bill_date=? and cid=? and lid=?", new String[]{catnam, billDate, c_id, l_id});
            }

            String itemname="";
            double qty=0.0;
            Cursor cursor= getAllbilldetailsid(Integer.parseInt(catnam),billDate);
            int check_sync_flag=0;

            while (cursor.moveToNext())
            {
                check_sync_flag=Integer.parseInt(cursor.getString(13));
                itemname=cursor.getString(2);
                qty=Double.parseDouble(cursor.getString(3));
                //Update_inventoryafterdeletebill(itemname, String.valueOf(qty));
            }

            //Message.message(context,"Check sync flagg="+check_sync_flag);
            Log.d("Check sync flagg=",""+check_sync_flag);

            ContentValues cv1 = new ContentValues();
            cv1.put("isactive", a);
            if(check_sync_flag==3) {
                cv1.put("sync_flag", 2);
            }else {
                cv1.put("sync_flag", 4);
            }
            cv1.put("modified_at", dateFormat.format(date));
            db.update("tbl_AddBillDetail", cv1, "bill_no=? AND bill_date=?", new String[] {catnam,billDate});

        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }
    public void deleteAllSaleBill(String fromdate, String todate){
        int a=2;
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        try {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("isactive", a);
            cv.put("sync_flag", 2);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            cv.put("modified_at", dateFormat.format(date));

            if(l_id.equals("null")){
                db.update("tbl_AddBillMaster", cv, "bill_date between ? and ? and cid=?", new String[]{fromdate, todate, c_id});
            }else {
                db.update("tbl_AddBillMaster", cv, "bill_date between ? and ? and cid=? and lid=?", new String[]{fromdate, todate, c_id, l_id});
            }

        }catch (Exception ex)
        {
            Message.message(context,""+ex.getMessage());
        }
    }

    public Cursor getAllbilldetailsid(int id,String date)
    { List<String> categorylist = new ArrayList<>();
        Cursor cursor = null;
        try {

            String itemname="";Double qty=0.0;
            //get readable database
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery("SELECT * from tbl_AddBillDetail where bill_no="+id+" and bill_date='"+date+"'" , null);
//            while (cursor.moveToNext())
//            {
//                itemname=cursor.getString(1);
//                qty=Double.parseDouble(cursor.getString(2));
//            }
//
        }
        catch (Exception ex)
        {
            //Message.message(context,ex.getMessage());
            String s=ex.getMessage().toString();
        }
        return cursor;
    }

    public Cursor getDeletedSalesBillReport( String d1,String d2)
    {
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        int a=1;

        List<String> itemlist=new ArrayList<>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=null;
        try {
            if(l_id.equals("null")){
                cursor = db.rawQuery("SELECT * FROM tbl_AddBillMaster WHERE (isactive= " + a + ") and (bill_date between '" + d1 + "' and '" + d2 + "') and cid=" + c_id + " ", null);
            }else {
                cursor = db.rawQuery("SELECT * FROM tbl_AddBillMaster WHERE (isactive= " + a + ") and (bill_date between '" + d1 + "' and '" + d2 + "') and cid=" + c_id + " and lid=" + l_id + "", null);
            }
        }catch (Exception ex)
        {
            Message.message(context,"error "+ex.getMessage());
        }
        return cursor;
    }

    public boolean checkBillNoExistOrNot(int billNo){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(String.format("SELECT bill_no FROM tbl_AddBillMaster WHERE bill_no=%d",billNo),null);
        if (c.getCount()>0)
            return false;
        else
            return true;
    }

    public int get_total_deleted_bills(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster where isactive=1 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster where isactive=1 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }
        return numRows;
    }

    public int get_todays_deleted_bill(){
        SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        String c_id=pref.getString("cid","");
        String l_id=pref.getString("lid","");
        String e_id=pref.getString("emp_id","");
        SQLiteDatabase db=this.getReadableDatabase();
        int numRows = 0;
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        Cursor cursor;
        if(l_id.equals("null")){
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=1 and cid=" + cid + " and emp_id="+e_id+"", null);
        }else {
            cursor = db.rawQuery("SELECT COUNT (*) FROM tbl_AddBillMaster WHERE bill_date >='" + dateToStr + "' and isactive=1 and cid=" + cid + " and lid=" + l_id + " and emp_id="+e_id+"", null);
        }
        if(cursor!=null && cursor.getCount()!=0) {
            cursor.moveToNext();
            numRows=Integer.parseInt(cursor.getString(0));
        }

        return numRows;
    }

    //*************Registration  Database **********************************
    private static final String DATABASE_NAME = "AirBillApp";    // Database Name

    private static final String TABLE_NAME = "tbl_Registration";   // Table Name
    private static final int DATABASE_Version = 1;   // Database Version
    private static final String RID = "rid";     // Column I (Primary Key)
    private static final String CompanyName = "reg_companyname";
    private static final String PersonName = "reg_personname";
    private static final String MobileNo = "reg_mobileno";
    private static final String EmailId = "reg_emailid";
    private static final String Address = "reg_address";
    private static final String UserName = "reg_username";
    private static final String UserPassword = "reg_userpassword";
    private static final String CompanyId = "reg_companyid";
    private static final String DealerCode = "re_dealercode";
    private static final String  created_at_TIMESTAMP = "created_at";
    private static final String  updated_at_TIMESTAMP = "modified_at";
    private static final String isactive = "isactive";
    private static final String lid = "lid";    //Column II
    private static final String cid = "cid";    //Column II
    private static final String emp_id = "emp_id";    //Column II
    private static final String sync_flag = "sync_flag";    //Column II

    private Context context;

    // **************** tbl_registration ****************
    private static final String Create_Table_Registration = "CREATE TABLE " + TABLE_NAME + "(" +
            RID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CompanyName + " VARCHAR(150) NOT NULL, " +
            PersonName + " VARCHAR(50)NOT NULL, " +
            MobileNo + " INTEGER(10) NOT NULL, " +
            EmailId + " VARCHAR(20) NOT NULL, " +
            Address + " VARCHAR(150) NOT NULL, " +
            UserName + " VARCHAR(50) NOT NULL," +
            UserPassword + " VARCHAR(50) NOT NULL," +
            CompanyId + " VARCHAR(50) NOT NULL," +
            DealerCode  + " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER );";

//*************Add Customer********************************************

    private static final String tbl_AddCutomer  = "tbl_Addcustomer";   // Table Name

    private static final String CID = "cust_id";
    private static final String CCompanyName = "cust_CompanyName";
    private static final String CName = "cust_name";
    private static final String CAddress = "address";
    private static final String CMobile = "mobile_no";
    private static final String CEmail = "email_id";
    private static final String CompanyIdorGST = "cust_companyId_or_GST";
    // private static final String isactive = "isactive";
    private static final String created_on = "created_on";
    private static final String Web_cust_id = "Web_cust_id";

    private static final String CREATE_TABLE_AddCustomer = "CREATE TABLE " + tbl_AddCutomer + "(" +
            CID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CCompanyName + " VARCHAR(150) NOT NULL, " +
            CName + " VARCHAR(150) NOT NULL, " +
            CMobile + " INTEGER(10), " +
            CEmail + " VARCHAR(50) NOT NULL," +
            CAddress + " VARCHAR(250) NOT NULL, " +
            CompanyIdorGST + " VARCHAR(30) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            Web_cust_id +" INTEGER  UNIQUE,"+
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag +" INTEGER );";

//*************Add Supplier********************************************

    private static final String tbl_AddSupplier  = "tbl_AddSupplier";   // Table Name

    private static final String SID = "sup_id";
    private static final String WebSID = "web_sup_id";
    private static final String sup_CompanyName = "sup_CompanyName";
    private static final String sup_name = "sup_name";
    private static final String sup_address = "sup_address";
    private static final String sup_mobile_no = "sup_mobile_no";
    private static final String sup_email_id = "sup_email_id";
    private static final String sup_cust_companyId_or_GST = "sup_cust_companyId_or_GST";
    // private static final String isactive = "isactive";

    private static final String CREATE_TABLE_AddSupplier = "CREATE TABLE " + tbl_AddSupplier + "(" +
            SID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            sup_CompanyName + " VARCHAR(150) NOT NULL, " +
            sup_name + " VARCHAR(150) NOT NULL, " +
            sup_address + " INTEGER(10), " +
            sup_mobile_no + " VARCHAR(50) NOT NULL," +
            sup_email_id + " VARCHAR(250) NOT NULL, " +
            sup_cust_companyId_or_GST + " VARCHAR(30) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            WebSID + " INTEGER UNIQUE, " +
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";

//*************Add Items**************************************

    private static final String tbl_AddItems  = "tbl_AddItems";   // Table Name
    private static final String ItemID = "item_id";
    private static final String WebItemID = "web_item_id";
    private static final String ItemName = "item_name";
    private static final String Rate = "item_rate";
    private static final String dis = "item_dis";
    private static final String disrate = "item_disrate";
    private static final String TaxName = "item_tax";
    private static final String itemTaxValue = "item_taxvalue";
    private static final String FinalRate = "item_final_rate";
    private static final String Category = "item_category";
    private static final String Units = "item_units";    // Column III
    //private static final String ItemGST = "ItemGST";    // Column III// Column III
    private static final String Stock = "item_stock";    // Column III
    private static final String Barcode = "item_barcode";
    private static final String Image = "item_image";
    private static final String HSNCode = "item_hsncode";
    private static final String sub_emp_id = "sub_emp_id";
    private static final String item_code = "item_code";

    private static final String CREATE_TABLE_AddItem = "CREATE TABLE " + tbl_AddItems + "(" +
            ItemID + " INTEGER , " +
            ItemName + " VARCHAR(50) NOT NULL, " +
            Rate + " DOUBLE NOT NULL, " +
            dis + " DOUBLE NOT NULL, " +
            disrate + " DOUBLE NOT NULL, " +
            TaxName + " DOUBLE NOT NULL, " +
            itemTaxValue + " DOUBLE NOT NULL, " +
            FinalRate + " DOUBLE NOT NULL, " +
            Category + " VARCHAR(50) NOT NULL, " +
            Units + " VARCHAR(50)  NOT NULL, " +
            Stock + " DOUBLE NOT NULL," +
            Barcode + " VARCHAR(50) NOT NULL,"+
            HSNCode + " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive + " INTEGER,"+
            lid + " INTEGER,"+
            cid + " INTEGER,"+
            emp_id + " INTEGER,"+
            sub_emp_id + " INTEGER,"+
            WebItemID + " INTEGER UNIQUE, " +
            sync_flag + " INTEGER, " +
            item_code +" INTEGER );";

//*************Add Category****************************************

    private static final String tbl_AddCategory  = "tbl_AddCategory";  // Table Name
    private static final String  CatId = "cat_id";     // Column I (Primary Key)
    private static final String CatWebID = "cat_web_id";    //Column II
    private static final String CatName = "cat_name";    //Column II
    private static final String cat_description = "cat_description";    //Column II
    private static final String cat_image = "cat_image";    //Column II
    private static final String type_id = "type_id";    //Column II

    private static final String CREATE_TABLE_AddCategory = "CREATE TABLE " + tbl_AddCategory + "(" +
            CatId + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CatName+ " VARCHAR(100) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive+ " INTEGER,"+
            cat_description+ " VARCHAR(250) NOT NULL,"+
            cat_image+ " VARCHAR(250) NOT NULL,"+
            type_id+ " INTEGER,"+
            CatWebID+ " VARCHAR(50) UNIQUE,"+
            lid+ " INTEGER,"+
            cid+ " INTEGER,"+
            emp_id+ " INTEGER,"+
            sync_flag+" INTEGER );";

//*************Add Header footer****************************************

    private static final String tbl_AddFooter  = "tbl_AddFooter";   // Table Name
    private static final String  HFID = "headerfooter_id";     // Column I (Primary Key)
    private static final String  header = "header_name";    //Column II
    private static final String  headersize = "header_size";    //Column II
    private static final String  footer = "footer_name";
    private static final String  footersize = "footer_size";
    private static final String  h2 = "header_h2";
    private static final String  h2_size = "headersize_h2";
    private static final String  h3 = "header_h3";
    private static final String  h3_size = "headersize_h3";
    private static final String  h4 = "header_h4";
    private static final String  h4_size = "headersize_h4";
    private static final String  h5 = "header_h5";
    private static final String  h5_size = "headersize_h5";
    private static final String  f2 = "header_f2";
    private static final String  f2_size = "headersize_f2";
    private static final String  f3 = "header_f3";
    private static final String  f3_size = "headersize_f3";
    private static final String  f4 = "header_f4";
    private static final String  f4_size = "headersize_f4";
    private static final String  f5 = "header_f5";
    private static final String  f5_size = "headersize_f5";
    private static final String  WebHeadId = "webid";

    private static final String CREATE_TABLE_AddFooter = "CREATE TABLE " + tbl_AddFooter + "(" +
            HFID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            header+ " VARCHAR(150) NOT NULL,"+
            headersize+ " VARCHAR(150) NOT NULL,"+
            h2+ " VARCHAR(150) NOT NULL,"+
            h2_size+ " VARCHAR(50) NOT NULL,"+
            h3+ " VARCHAR(50) NOT NULL,"+
            h3_size+ " VARCHAR(50) NOT NULL,"+
            h4+ " VARCHAR(50) NOT NULL,"+
            h4_size+ " VARCHAR(50) NOT NULL,"+
            h5+ " VARCHAR(50) NOT NULL,"+
            h5_size+ " VARCHAR(50) NOT NULL,"+
            footer +"  VARCHAR(50) NOT NULL,"+
            footersize+ " VARCHAR(50) NOT NULL,"+
            f2+ " VARCHAR(50) NOT NULL,"+
            f2_size+ " VARCHAR(50) NOT NULL,"+
            f3+ " VARCHAR(50) NOT NULL,"+
            f3_size+ " VARCHAR(50) NOT NULL,"+
            f4+ " VARCHAR(50) NOT NULL,"+
            f4_size+ " VARCHAR(50) NOT NULL,"+
            f5+ " VARCHAR(50) NOT NULL,"+
            f5_size+ " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            WebHeadId + " INTEGER UNIQUE, " +
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";



    // *************Add TAX****************************************

    private static final String tbl_AddTax  = "tbl_AddTax";   // Table Name
    private static final String  Tid = "tax_id";     // Column I (Primary Key)
    private static final String ATaxName = "tax_name";    //Column II
    private static final String TaxValue = "tax_value";// Column III
    private static final String WebTaxId = "webtaxid";// Column III

    private static final String CREATE_TABLE_AddTax = "CREATE TABLE " + tbl_AddTax + "(" +
            Tid + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ATaxName+ " VARCHAR(50) NOT NULL, " +
            TaxValue + " DOUBLE NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            WebTaxId + " INTEGER UNIQUE, " +
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";


    // *************Add Units****************************************
    private static final String tbl_AddIUnits  = "tbl_AddIUnits";   // Table Name
    private static final String UnitsID = "_Unit_Id";     // Column I (Primary Key)
    private static final String WebUnitsID = "Web_Unit_Id";     // Column I (Primary Key)
    private static final String UnitName = "Unit_name";
    private static final String UnitTaxName = "Unit_Taxvalue";
    private static final String Unitlid = "lid";    //Column II
    private static final String Unitcid = "cid";    //Column II
    private static final String Unitemp_id = "emp_id";
    private static final String Unitsync_flag = "sync_flag";//Column II

    private static final String CREATE_TABLE_AddUnits = "CREATE TABLE " + tbl_AddIUnits + "(" +
            UnitsID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            UnitName + " VARCHAR(50) NOT NULL, " +
            UnitTaxName + " DOUBLE  NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive+ " INTEGER,"+
            WebUnitsID + " INTEGER UNIQUE, " +
            Unitcid+ " INTEGER,"+
            Unitlid+ " INTEGER,"+
            Unitemp_id+ " INTEGER,"+
            Unitsync_flag+" INTEGER );";

    //******************Login**************************************
    private static final String tbl_Login  = "tbl_Login";   // Table Name
    private static final String LID = "login_id";     // Column I (Primary Key)
    private static final String Mno = "login_Mobileno";
    private static final String pass = "login_Pass";    //Column II

    private static final String CREATE_TABLE_Login = "CREATE TABLE " + tbl_Login + "(" +
            LID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Mno + " INTEGER(10) NOT NULL, " +
            pass+ " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER );";

//******************************************************************************

    //******************users**************************************
    private static final String tbl_Users  = "tbl_users";   // Table Name
    private static final String Userid = "user_id";     // Column I (Primary Key)
    private static final String username = "user_name";
    private static final String userpass = "user_pass";    //Column II

    private static final String CREATE_TABLE_Users = "CREATE TABLE " + tbl_Users + "(" +
            Userid + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            username + "  VARCHAR(50) NOT NULL, " +
            userpass+ " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER );";

    /******************************inventory table*********************************************/
    private static final String tbl_inventory  = "tbl_inventory";   // Table Name
    private static final String inventoryid = "inventory_Id";// Column I (Primary Key)
    private static final String inventorysupid = "inventory_SupId";// Column I (Primary Key)
    private static final String inventorysupname = "inventory_SupName";// Column I (Primary Key)
    private static final String inventoryitemid = "item_id";
    private static final String inventoryitemname = "item_name";
    private static final String inventoryitemquantity = "invetory_item_quantity";    //Column II
    private static final String inventorystatus = "inventory_status";    //Column II
    private static final String web_inv_id = "web_inv_id";    //Column II

    private static final String CREATE_TABLE_inventory = "CREATE TABLE " + tbl_inventory + "(" +
            inventoryid + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            inventorysupid + " INTEGER NOT NULL, " +
            inventorysupname + " VARCHAR(50), " +
            inventoryitemid + "  INTEGER NOT NULL, " +
            inventoryitemname + "  VARCHAR(150), " +
            inventoryitemquantity+ " VARCHAR(50) NOT NULL,"+
            inventorystatus+ " VARCHAR(50) NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            web_inv_id +" INTEGER UNIQUE,"+
            cid +" INTEGE ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag +" INTEGER );";

    /***********************************************************/

    // *************Add BillMaster***************************************
    private static final String tbl_AddBillMaster  = "tbl_AddBillMaster";   // Table Name
    private static final String BillIDno = "bill_ID";     // Column I (Primary Key)
    private static final String Billno= "bill_no";
    private static final String BillDate = "bill_date";    //Column II
    //private static final String BillCustomerId = "cust_id";
    private static final String BillCustomerName = "cust_name";
    private static final String Billcashorcredit = "cash_or_credit";//Column II
    private static final String pointofcontact = "point_of_contact";//Column II

    private static final String Billdiscount = "discount";//Column II
    private static final String Billtotalamt = "bill_totalamt";//Column II
    private static final String Billtax = "bill_tax";//Column II
    private static final String WebbillmasterId = "web_id";//Column II
    private static final String paymentdetails = "payment_details";//Column II
    private static final String orderdetails = "order_details";//Column II
    private static final String gst_settings = "gst_setting";//Column II
    private static final String bill_code = "bill_code";//Column II
    private static final String remark = "remark";//Column II


    private static final String CREATE_TABLE_AddBillMaster = "CREATE TABLE " + tbl_AddBillMaster+ "(" +
            BillIDno + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Billno + " INTEGER ,"+
            BillDate + " DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            BillCustomerName + " VARCHAR(50) NOT NULL,"+
            Billcashorcredit + " VARCHAR(50)  NOT NULL,"+
            Billdiscount + " DOUBLE  NOT NULL,"+
            Billtotalamt + " DOUBLE  NOT NULL,"+
            Billtax+" DOUBLE  NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            WebbillmasterId + " VARCHAR(255) UNIQUE, " +
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag +" INTEGER ,"+
            pointofcontact +" INTEGER ,"+
            paymentdetails +" VARCHAR(500)  NOT NULL, " +
            orderdetails +" VARCHAR(500) NOT NULL, "+
            gst_settings +" VARCHAR(100), "+
            bill_code + " VARCHAR(50),"+
            remark + " VARCHAR(250) DEFAULT 'NA' );";

    //********************************** Add BillDetails ******************************************************
    private static final String tbl_AddBillDetails  = "tbl_AddBillDetail";   // Table Name

    private static final String Billnofk= "bill_no";
    private static final String BillItemName = "item_name";    //Column II
    private static final String BillItemqty = "item_qty";
    private static final String BillItemrate = "item_rate";//Column II
    private static final String Billtotalrate = "item_totalrate ";//Column II
    private static final String WebBillDeatilsId = "web_bill_id ";//Column II
    private static final String BillAutoId = "billD_id ";//Column II
    private static final String BillItembasicrate = "item_basicrate";
    private static final String BillItemdiscount = "item_discount";//Column II
    private static final String BillItemtax = "item_tax";//Column II


    private static final String CREATE_TABLE_AddBillDetails = "CREATE TABLE " + tbl_AddBillDetails+ "(" +
            BillAutoId + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Billnofk + " INTEGER , " +
            BillItemName + " VARCHAR(50)  NOT NULL,"+
            BillItemqty + " DOUBLE  NOT NULL,"+
            BillItemrate + " DOUBLE  NOT NULL,"+
            Billtotalrate+" DOUBLE  NOT NULL,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP,"+
            isactive +" INTEGER,"+
            WebBillDeatilsId + " VARCHAR(255) UNIQUE, " +
            cid +" INTEGER,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER , "+
            bill_code + " VARCHAR(50) , "+
            BillItembasicrate + " DOUBLE  NOT NULL , "+
            BillItemdiscount +" DOUBLE  NOT NULL, "+
            BillItemtax +" DOUBLE  NOT NULL , "+
            BillDate + " DATETIME DEFAULT CURRENT_TIMESTAMP );";

    //*****************************************Paymnet Type********************************************************
    private static final String bil_payement_type  = "bil_payement_type";   // Table Name
    private static final String Auto_paymnet_id= "id";
    private static final String Web_paymnet_id= "web_id";
    private static final String payment_type = "payment_type";    //Column II
    private static final String is_active = "is_active";

    private static final String CREATE_TABLE_bil_payement_type = "CREATE TABLE " + bil_payement_type + "(" +
            Auto_paymnet_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Web_paymnet_id + " INTEGER UNIQUE, " +
            payment_type + " VARCHAR(100) NOT NULL, " +
            is_active + "  INTEGER, " +
            cid +" INTEGE ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag +" INTEGER,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP );";


    //*****************************************Point_of_contact**************************************************
    private static final String bil_point_of_contact  = "bil_point_of_contact";   // Table Name
    private static final String Auto_point_contact_id= "id";
    private static final String Web_point_contact_id= "web_id";
    private static final String point_of_contact = "point_of_contact";//Column II

    private static final String CREATE_TABLE_bil_point_of_contact = "CREATE TABLE " + bil_point_of_contact + "(" +
            Auto_point_contact_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Web_point_contact_id + " INTEGER UNIQUE, " +
            point_of_contact + " VARCHAR(100) NOT NULL, " +
            is_active + "  INTEGER, " +
            cid +" INTEGER ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag +" INTEGER,"+
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP );";

    //******************************************bil_sync_time***************************************************

    private static final String bil_sync_time = "bil_sync_time";
    private static final String bil_sync_id = "id";
    private static final String upload_interval = "upload_interval";
    private static final String download_interval = "download_interval";

    private static final String CREATE_TABLE_bil_sync_time = "CREATE TABLE " + bil_sync_time + "(" +
            bil_sync_id + " INTEGER, " +
            upload_interval + " VARCHAR(100), " +
            download_interval + " VARCHAR(100), " +
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            cid +" INTEGER ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";


    //*******************************************table order****************************************************

    private static final String bil_table_orders = "bil_table_orders";
    private static final String table_id = "table_id";
    private static final String table_capacity = "table_capacity";
    private static final String current_order = "current_order";
    private static final String table_number = "table_number";
    private static final String tbl_live_flag = "tbl_live_flag";
    private static final String tbl_orderflag = "tbl_orderflag";

    private static final String CREATE_TABLE_bil_table_orders = "CREATE TABLE " + bil_table_orders + "(" +
            table_id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            table_capacity + " INTEGER, " +
            current_order + " TEXT, " +
            table_number + " INTEGER, " +
            tbl_live_flag + " INTEGER, " +
            tbl_orderflag + " INTEGER, " +
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            cid +" INTEGER ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";

    //*******************************************temp table order****************************************************
    private static final String bil_temp_table_orders = "bil_temp_table_orders";
    private static final String id = "id";
    private static final String tbl_item = "tbl_item";
    private static final String tbl_qty = "qty";
    private static final String tbl_price = "price";

    private static final String CREATE_TABLE_bil_temp_table_orders = "CREATE TABLE " + bil_temp_table_orders + "(" +
            id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            table_id + " INTEGER, " +
            table_number + " INTEGER, " +
            tbl_item + " VARCHAR(100), " +
            tbl_qty + " INTEGER, " +
            tbl_price + " DOUBLE, " +
            created_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            updated_at_TIMESTAMP +" DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
            cid +" INTEGER ,"+
            lid +" INTEGER,"+
            emp_id +" INTEGER,"+
            sync_flag+" INTEGER );";

    //*********************************************************************************************************
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_Version);
        this.context = context;

        //Message.message(context, "Started...");
    }

    //    public DatabaseHelper(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//
//        Log.d("table", CREATE_TABLE_STUDENTS);
//    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        try
        {
            db.execSQL(Create_Table_Registration);
            db.execSQL(CREATE_TABLE_AddCategory);
            db.execSQL(CREATE_TABLE_AddCustomer);
            db.execSQL(CREATE_TABLE_AddItem);
            db.execSQL(CREATE_TABLE_AddTax);
            db.execSQL(CREATE_TABLE_AddUnits);
            db.execSQL(CREATE_TABLE_Login);
            db.execSQL(CREATE_TABLE_Users);
            db.execSQL(CREATE_TABLE_inventory);
            db.execSQL(CREATE_TABLE_AddBillMaster);
            db.execSQL(CREATE_TABLE_AddBillDetails);
            db.execSQL(CREATE_TABLE_AddFooter);
            db.execSQL(CREATE_TABLE_AddSupplier);
            db.execSQL(CREATE_TABLE_bil_payement_type);
            db.execSQL(CREATE_TABLE_bil_point_of_contact);
            db.execSQL(CREATE_TABLE_bil_sync_time);
            db.execSQL(CREATE_TABLE_bil_table_orders);
            db.execSQL(CREATE_TABLE_bil_temp_table_orders);
            Message.message(context, "Database Created");
        }
        catch (Exception e) {
            Message.message( context,e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        try {
            /*  Message.message(context,"OnUpgrade");

             db.execSQL("DROP TABLE IF EXISTS '" + CREATE_TABLE_AddFooter + "'");

             onCreate(db);*/
        }
        catch (Exception e)
        {
            Message.message(context,""+e);
        }
    }
}