package com.example.rajlaxmi.airbill_online;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RegisteredCustomerListActivity extends AppCompatActivity {

    ListView lv;
    private MyAppAdapter myAppAdapter;
    private boolean success = false; // boolean
    ArrayList itemArrayList;
    ArrayList<String> cust_id=new ArrayList<String>();
    ArrayList<String> company_name=new ArrayList<String>();
    int pos_i=0;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered_customer_list);

        //java code

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        lv = (ListView) findViewById(R.id.lv_items);
        itemArrayList = new ArrayList<ClassListItems>();

        SyncData orderData = new SyncData();
        orderData.execute("");

        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Intent intent = getIntent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                startActivity(intent);

                mSwipeRefreshLayout.setRefreshing(false);

            }
        });

    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+150;
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
    public class MyAppAdapter extends BaseAdapter       //has a class viewholder which holds
    {
        public List<ClassListItems> parkingList;
        public Context context;
        public ArrayList<ClassListItems> arraylist;

        private class ViewHolder {
            TextView tv_cat,tv_cat1;
        }

        private MyAppAdapter(ArrayList apps, RegisteredCustomerListActivity context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<ClassListItems>();
            arraylist.addAll(parkingList);
        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) // inflating the layout and initializing widgets
        {
            pos_i=position;
            final ViewHolder holder;
            view = getLayoutInflater().inflate(R.layout.layout_app_reg_userlist,null);
            holder = new ViewHolder(); LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            holder.tv_cat = (TextView) view.findViewById(R.id.tv_cat);
            holder.tv_cat1 = (TextView) view.findViewById(R.id.tv_cat1);

            holder.tv_cat.setText(parkingList.get(position).getSr_no() + "");
            holder.tv_cat1.setText(parkingList.get(position).getItem_name() + "");

            return view;
        }
    }

    private class SyncData extends AsyncTask<String, String, String> {
        String msg = "Internet/DB_Credentials/Windows_FireWall_TurnOn Error, See Android Monitor in the bottom For details!";
        ProgressDialog progress;

        @Override
        protected void onPreExecute() //Starts the progress dailog
        {
            progress = ProgressDialog.show(RegisteredCustomerListActivity.this, "Loading",
                    "ListView Loading! Please Wait...", true);
        }

        @Override
        protected String doInBackground(String... strings)  // Connect to the database, write query and add items to array list
        {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                Connection conn = DriverManager.getConnection("jdbc:mysql://148.72.232.154:3306/solartest", "user_solar", "solar123");

                if (conn == null) {
                    success = false;
                } else {
                    PreparedStatement st=	conn.prepareStatement("SELECT id,company_name,mobile_no FROM `tbl_airbill_registration` WHERE `login_flag` = 1");
                    ResultSet rs = st.executeQuery();
                    int i=0;

                    if (rs != null) // if resultset not null, I add items to itemArraylist using class created
                    {
                        while (rs.next()) {
                            try {
                                cust_id.add(rs.getString("mobile_no"));
                                company_name.add(rs.getString("company_name"));
                                itemArrayList.add(new ClassListItems(""+company_name.get(i),""+cust_id.get(i),"","",""));
                                i++;
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        msg = "Found";
                        success = true;
                    } else {
                        msg = "No Data found!";
                        success = false;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                msg = writer.toString();
                success = false;
            }
            progress.dismiss();
            return msg;
        }
        @Override
        protected void onPostExecute(String msg) // disimissing progress dialoge, showing error and setting up my ListView
        {
            if (success == false) {
            } else {
                try {
                    myAppAdapter = new MyAppAdapter(itemArrayList, RegisteredCustomerListActivity.this);
                    lv.setAdapter(myAppAdapter);

                    ListUtils.setDynamicHeight(lv);

                } catch (Exception ex) {
                    Log.d("Error ", "" + ex);
                    Log.e("Error ", "" + ex);
                }
            }
        }
    }
    @Override
    public void onBackPressed() {
        Intent i=new Intent(getApplicationContext(),PanelActivity.class);
        startActivity(i);
        finish();
    }

}