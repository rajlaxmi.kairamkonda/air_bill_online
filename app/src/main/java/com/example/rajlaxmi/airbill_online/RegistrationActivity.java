
package com.example.rajlaxmi.airbill_online;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    Button bt_register,bt_login;
    EditText et_company_name,et_name,et_mobile,etemail,et_address,et_username,et_password,et_company_id,et_delear_code,et_business,et_machine_model_no,et_machine_serial_no;
    EditText et_val;
    private  boolean success=false;
    int cntUsers=0;
    private RadioGroup rg_bill_location;
    private RadioButton radioButton;
    private RadioButton rb_bill_location;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        //java code
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
        progressDialog = new ProgressDialog(getApplicationContext());

        bt_register=(Button)findViewById(R.id.bt_register);
        bt_login=(Button)findViewById(R.id.bt_login);

        et_company_name=(EditText)findViewById(R.id.et_company_name);
        et_name=(EditText)findViewById(R.id.et_name);
        et_mobile=(EditText)findViewById(R.id.et_mobile);
        etemail=(EditText)findViewById(R.id.etemail);
        et_address=(EditText)findViewById(R.id.et_address);
        et_username=(EditText)findViewById(R.id.et_username);
        et_password=(EditText)findViewById(R.id.et_password);
        et_company_id=(EditText)findViewById(R.id.et_company_id);
        et_delear_code=(EditText)findViewById(R.id.et_delear_code);
        et_business=(EditText)findViewById(R.id.et_business);
        et_machine_model_no=(EditText)findViewById(R.id.et_machine_model_no);
        et_machine_serial_no=(EditText)findViewById(R.id.et_machine_serial_no);
        rg_bill_location=(RadioGroup)findViewById(R.id.rg_location);

        int selectedId = rg_bill_location.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedId);

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        bt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cntUsers=0;
//                progressDialog.setMessage("Please Wait, We are Inserting Your Data on Server");
//                progressDialog.show();
                try{

                    String HttpUrl = Config.hosturl+"insertrecord.php";
                    Log.d("URL",HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String ServerResponse) {

                                    Log.d("Server Response",ServerResponse);
//                                    progressDialog.dismiss();

                                    Toast.makeText(getApplicationContext(), ServerResponse, Toast.LENGTH_LONG).show();
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                                    Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("reg_companyname", et_company_name.getText().toString());
                            params.put("reg_personname", et_name.getText().toString());
                            params.put("reg_mobileno",  et_mobile.getText().toString());
                            params.put("reg_emailid",  etemail.getText().toString());
                            params.put("reg_address",  et_address.getText().toString());
                            params.put("reg_username",  et_username.getText().toString());
                            params.put("reg_password",  et_password.getText().toString());
                            params.put("reg_dealercode",  et_delear_code.getText().toString());
                            params.put("reg_location",  radioButton.getText().toString());
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    requestQueue.add(stringRequest);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }
    private boolean checkValidation() {
        boolean ret = true;
        if (!Validation.hasText(et_company_name,"Enter Company Name")) ret = false;
        if (!Validation.hasText(et_name,"Enter Full Name")) ret = false;
        if (!Validation.hasText(et_address,"Enter Adress")) ret = false;
        if (!Validation.hasText(et_company_id,"Enter Address")) ret = false;
        if (!Validation.hasText(et_username,"Enter Address")) ret = false;
        if (!Validation.hasText(et_password,"Enter Address")) ret = false;
        if (!Validation.isName(et_name,"Enter Only Characters",true)) ret = false;
        if (!Validation.isEmailAddress(etemail,"Invalid email",true)) ret = false;
        if (!Validation.isPhoneNumber(et_mobile, "Enter 10 Digits",true)) ret = false;
        return ret;
    }
}
