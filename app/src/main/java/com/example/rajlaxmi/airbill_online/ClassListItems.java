package com.example.rajlaxmi.airbill_online;

/**
 * Created by pooja on 9/1/2018.
 */

public class ClassListItems {

    public String sr_no,item_name,qty,discount,rate;

    public ClassListItems(String sr_no, String item_name, String qty, String discount,String rate)
    {
        this.sr_no=sr_no;
        this.item_name = item_name;
        this.qty = qty;
        this.discount = discount;
        this.rate=rate;
    }

    // public String getImg() {
    // return img;
    // }
    public String getSr_no() {
        return sr_no;
    }
    public String getItem_name() {
        return item_name;
    }
    public String getQty() {
        return qty;
    }
    public String getDiscount() {
        return discount;
    }
    public String getRate() {
        return rate;
    }

}
