package com.example.rajlaxmi.airbill_online;

public interface IRequestListener {

    void onComplete();

    void onError(String message);
}
