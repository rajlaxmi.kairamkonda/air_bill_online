package com.example.rajlaxmi.airbill_online;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.CardReader;
import com.aem.api.IAemCardScanner;
import com.aem.api.IAemScrybe;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cie.btp.DebugLog;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import com.cie.btp.CieBluetoothPrinter;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_DEVICE_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTING;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_LISTEN;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_NONE;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MESSAGES;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_FOUND;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_SAVED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_STATUS;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener ,IAemCardScanner, IAemScrybe {
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;
    private Button btnPrint;
    private static final int BARCODE_WIDTH = 384;
    private static final int BARCODE_HEIGHT = 100;
    private static final int QRCODE_WIDTH = 100;
    String status = "";
    public CieBluetoothPrinter mPrinter = CieBluetoothPrinter.INSTANCE;
    private int imageAlignment = 1;
    Button bt_submit_bill_screen;
    private RadioGroup rg_bill_screen;
    private RadioButton rb_bill_screen;
    private RadioButton printernameselected;
    private RadioGroup rg_bill_printer;
    private RadioButton rb_bill_printer;
    private RadioGroup rg_gst_printer;
    private RadioButton rb_gst_printer;
    private RadioGroup rg_select_printer;
    private RadioButton rb_select_printer;
    private RadioGroup rg_multiple_printing;
    private RadioButton rb_multiple_printing;
    private RadioGroup rg_with_bill_printing;
    private RadioButton rb_with_bill_printing;
    private RadioGroup rg_reset_bill_number;
    private RadioButton rb_reset_bill_number;
    private RadioGroup rg_openrate;
    private RadioButton rb_openrate;
    AEMScrybeDevice m_AemScrybeDevice;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    String creditData;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int glbPrinterWidth;
    EditText edit_mesg;
    Button btn_connect, billbtn, billonceclick;
    String m_message;
    Cursor res;
    RadioButton rg2inch,rg3inch;
    private static final String MY_PREFS_NAME = "myclass";
    String value;
    ArrayList<String> printerList;
    char[] batteryStatusCommand = new char[]{0x1B, 0x7E, 0x42, 0x50, 0x7C, 0x47, 0x45, 0x54, 0x7C, 0x42, 0x41, 0x54, 0x5F, 0x53, 0x54, 0x5E};
    DatabaseHelper db;
    List<String> items = new ArrayList<>();
    ArrayAdapter<String> adapter;
    String printername;
    LinearLayout ll_export,ll_import;
    String postdata="",unitpostdata="",itempostdata="",suppostdata="",custpostdata="",prchpostdata="";
    int cat_total=0,unit_total=0,item_total=0,sup_total=0,cust_total=0,invt_total=0;
    String lid="",cid="",empid="";
    String rescat="";
    List<NameValuePair> list=new ArrayList<NameValuePair>();
    List<NameValuePair> unitlist=new ArrayList<NameValuePair>();
    List<NameValuePair> itemlist=new ArrayList<NameValuePair>();
    List<NameValuePair> suplist=new ArrayList<NameValuePair>();
    List<NameValuePair> custlist=new ArrayList<NameValuePair>();
    List<NameValuePair> purchlist=new ArrayList<NameValuePair>();
    ArrayList<String> idlist=new ArrayList<String>();
    List<NameValuePair> pointconatctlist=new ArrayList<NameValuePair>();
    List<NameValuePair> paymnetlist=new ArrayList<NameValuePair>();
    List<NameValuePair> settinglist=new ArrayList<NameValuePair>();
    ArrayList<String> master_bill_id_list=new ArrayList<String>();
    ArrayList<String> details_bill_id_list=new ArrayList<String>();
    private boolean success = false;
    String reset_bill="",gst_option="";
    String cat_datetime="",unit_datetime="",item_datetime="",sup_datetime="",cust_datetime="",purc_datetime="";
    String prev_cat_datetime="",prev_unit_datetime="",prev_item_datetime="",prev_sup_datetime="",prev_cust_datetime="",prev_purc_datetime="";
    String table_screen="";

    //  Button btnSelectPrinter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        db=new DatabaseHelper(this);
        rg_bill_screen = (RadioGroup) findViewById(R.id.rg_bill_screen);
        rg_bill_printer = (RadioGroup) findViewById(R.id.rg_bill_size);
        rg_gst_printer = (RadioGroup) findViewById(R.id.rg_gst_size);
        rg_select_printer = (RadioGroup) findViewById(R.id.rg_selectprinter);
        rg_multiple_printing = (RadioGroup) findViewById(R.id.rg_multiple_print);
        rg_with_bill_printing=(RadioGroup)findViewById(R.id.rg_bill_printing_popup);
        rg_reset_bill_number=(RadioGroup)findViewById(R.id.rg_reset_bill);
        rg_openrate=(RadioGroup)findViewById(R.id.rg_openrate);
        ll_export=(LinearLayout)findViewById(R.id.ll_export);
        ll_import=(LinearLayout)findViewById(R.id.ll_import);

        printerList = new ArrayList<String>();
        creditData = new String();
        m_AemScrybeDevice = new AEMScrybeDevice(this);

        headFootSetting = new HeadFootSetting(getApplicationContext());
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        value = pref.getString("2inchprintername", "");
        final SharedPreferences.Editor editor = pref.edit();
        cid=pref.getString("cid","");
        lid=pref.getString("lid","");
        empid=pref.getString("emp_id","");
        reset_bill=pref.getString("reset_bill","");
        gst_option=pref.getString("gst_option","");
        prev_cat_datetime=pref.getString("cat_datetime","");
        prev_unit_datetime=pref.getString("unit_datetime","");
        prev_item_datetime=pref.getString("item_datetime","");
        prev_sup_datetime=pref.getString("sup_datetime","");
        prev_cust_datetime=pref.getString("cust_datetime","");
        prev_purc_datetime=pref.getString("purc_datetime","");
        table_screen=pref.getString("tblscreen","");


        if (pref.getString("print_option", "").equals("2 inch")) {
            ((RadioButton) rg_bill_printer.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_bill_printer.getChildAt(1)).setChecked(true);
        }

        if (pref.getString("gst_option", "").equals("GST Enable")) {
            ((RadioButton) rg_gst_printer.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_gst_printer.getChildAt(1)).setChecked(true);
        }

        String select=pref.getString("selectprinter_option","");
        if (pref.getString("selectprinter_option", "").equals("Airbill"))
        {
            ((RadioButton) rg_select_printer.getChildAt(0)).setChecked(true);
            rg2inch = (RadioButton) findViewById(R.id.rb_two_inch);
            registerForContextMenu(rg2inch);

//            ((RadioButton) rg_select_printer.getChildAt(1)).setChecked(true);
            rg3inch = (RadioButton) findViewById(R.id.rb_three_inch);
            registerForContextMenu(rg3inch);


        }
        else
        {
//            ((RadioButton) rg_select_printer.getChildAt(0)).setChecked(true);
            rg2inch = (RadioButton) findViewById(R.id.rb_two_inch);
            registerForContextMenu(rg2inch);

            ((RadioButton) rg_select_printer.getChildAt(1)).setChecked(true);
            rg3inch = (RadioButton) findViewById(R.id.rb_three_inch);
            registerForContextMenu(rg3inch);
        }
        if (pref.getString("multiple_printing", "").equals("Yes")) {
            ((RadioButton) rg_multiple_printing.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_multiple_printing.getChildAt(1)).setChecked(true);
        }

        if (pref.getString("bill_with_print", "").equals("Yes")) {
            ((RadioButton) rg_with_bill_printing.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_with_bill_printing.getChildAt(1)).setChecked(true);
        }

        if (pref.getString("reset_bill", "").equals("Yes")) {
            ((RadioButton) rg_reset_bill_number.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_reset_bill_number.getChildAt(1)).setChecked(true);
        }

        if (pref.getString("openrate", "").equals("Yes")) {
            ((RadioButton) rg_openrate.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_openrate.getChildAt(1)).setChecked(true);
        }

        if (pref.getString("tblscreen", "").equals("Thumbnail")) {
            ((RadioButton) rg_reset_bill_number.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) rg_reset_bill_number.getChildAt(1)).setChecked(true);
        }

        bt_submit_bill_screen = (Button) findViewById(R.id.bt_submit_bill_screen);
        bt_submit_bill_screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = rg_bill_printer.getCheckedRadioButtonId();
                rb_bill_printer = (RadioButton) findViewById(selectedId);
                int gstid = rg_gst_printer.getCheckedRadioButtonId();
                rb_gst_printer = (RadioButton) findViewById(gstid);

                int selectprinter = rg_select_printer.getCheckedRadioButtonId();
                rb_select_printer = (RadioButton) findViewById(selectprinter);

                int selectmultipleprint = rg_multiple_printing.getCheckedRadioButtonId();
                rb_multiple_printing = (RadioButton) findViewById(selectmultipleprint);

                int selectwithprint_or_withoutprint = rg_with_bill_printing.getCheckedRadioButtonId();
                rb_with_bill_printing = (RadioButton) findViewById(selectwithprint_or_withoutprint);

                int reset_bill = rg_reset_bill_number.getCheckedRadioButtonId();
                rb_reset_bill_number = (RadioButton) findViewById(reset_bill);

                int openrate = rg_openrate.getCheckedRadioButtonId();
                rb_openrate = (RadioButton) findViewById(openrate);

//                headFootSetting.setBill(rb_bill_printer.getText().toString());
                headFootSetting.setBill("\n" + rb_bill_printer.getText().toString() + "\n" + rb_select_printer.getText().toString() + "\n" + rb_gst_printer.getText().toString()
                        + "\n" + rb_with_bill_printing.getText().toString()+ "\n" + rb_multiple_printing.getText().toString()+"\n" + rb_reset_bill_number.getText().toString());

                editor.putString("print_option", rb_bill_printer.getText().toString());
                editor.putString("gst_option", rb_gst_printer.getText().toString());
                editor.putString("selectprinter_option", rb_select_printer.getText().toString());
                editor.putString("multiple_printing", rb_multiple_printing.getText().toString());
                editor.putString("bill_with_print", rb_with_bill_printing.getText().toString());
                editor.putString("reset_bill", rb_reset_bill_number.getText().toString());
                editor.putString("openrate", rb_openrate.getText().toString());
                editor.commit();
                //Toast.makeText(getApplicationContext(), headFootSetting.getBill() + " Settings Updated!!!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                startActivity(i);
                finish();
            }
        });

        ll_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SyncData orderData = new SyncData();
                orderData.execute("");
            }
        });

        ll_import.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Synctoweb();
            }
        });
    }

    public void onShowPairedPrinters(View v) {
        RadioButton rb = (RadioButton) rg_select_printer.findViewById(rg_select_printer.getCheckedRadioButtonId());
        Toast.makeText(MainActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
        if (rb.getText().toString().equals("Airbill")) {

            printerList = m_AemScrybeDevice.getPairedPrinters();
            if (printerList.size() > 0)
                openContextMenu(v);

        } else if (rb.getText().toString().equals("Dyno")) {
            BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mAdapter == null) {
                Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
//                finish();
                mAdapter.getDefaultAdapter().enable();
            } else {
                try {
                    mPrinter.initService(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mPrinter.disconnectFromPrinter();
                mPrinter.selectPrinter(MainActivity.this);
                mPrinter.connectToPrinter();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
//        for (int i = 0; i < printerList.size(); i++)
//        {
//            menu.add(0, v.getId(), 0, printerList.get(i));
//        }
//        menu.add(0, v.getId(), 0, value);
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
        for (int i = 0; i < printerList.size(); i++) {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // super.onContextItemSelected(item);
        String printerName = item.getTitle().toString();
        try {
            m_AemScrybeDevice.disConnectPrinter();
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(MainActivity.this, "Connected with " + printerName, Toast.LENGTH_SHORT).show();
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            final SharedPreferences.Editor editor = pref.edit();

            editor.putString("2inchprintername", printerName);

            editor.commit();

            //  m_cardReader.readMSR();
        } catch (IOException e) {
            if (e.getMessage().contains("Service discovery failed")) {
                Toast.makeText(MainActivity.this, "Not Connected\n" + printerName + " is unreachable or off otherwise it is connected with other device", Toast.LENGTH_SHORT).show();
            } else if (e.getMessage().contains("Device or resource busy")) {
                Toast.makeText(MainActivity.this, "the device is already connected", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Unable to connect", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPrinter.onActivityRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private final BroadcastReceiver ReceiptPrinterMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DebugLog.logTrace("Printer Message Received");
            Bundle b = intent.getExtras();
            if (b == null) {
                return;
            }
            switch (b.getInt(RECEIPT_PRINTER_STATUS)) {
                case RECEIPT_PRINTER_CONN_STATE_NONE:
                    status = String.valueOf(R.string.printer_not_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_LISTEN:
                    status = String.valueOf(R.string.ready_for_conn);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTING:
                    status = String.valueOf(R.string.printer_connecting);
                    break;
                case RECEIPT_PRINTER_CONN_STATE_CONNECTED:
                    status = String.valueOf(R.string.printer_connected);
//                    new AsyncPrint().execute();
                    break;
                case RECEIPT_PRINTER_CONN_DEVICE_NAME:
                    savePrinterMac(b.getString(RECEIPT_PRINTER_NAME, ""));
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG:
                    String n = b.getString(RECEIPT_PRINTER_MSG);
                    status = String.valueOf(n);
                    break;
                case RECEIPT_PRINTER_NOTIFICATION_MSG:
                    String m = b.getString(RECEIPT_PRINTER_MSG);
                    status = String.valueOf(m);
                    break;
                case RECEIPT_PRINTER_NOT_CONNECTED:
                    status = String.valueOf("Status : Printer Not Connected");
                    break;
                case RECEIPT_PRINTER_NOT_FOUND:
                    status = String.valueOf("Status : Printer Not Found");
                    break;
                case RECEIPT_PRINTER_SAVED:
                    status = String.valueOf(R.string.printer_saved);
                    break;
            }
        }
    };

    private void savePrinterMac(String sMacAddr) {
        if (sMacAddr.length() > 4) {
            status = String.valueOf("Preferred Printer saved");
        } else {
            status = String.valueOf("Preferred Printer cleared");
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }

    @Override
    public void onScanMSR(String s, CardReader.CARD_TRACK card_track) {

    }

    @Override
    public void onScanDLCard(String s) {

    }

    @Override
    public void onScanRCCard(String s) {

    }

    @Override
    public void onScanRFD(String s) {

    }

    @Override
    public void onScanPacket(String s) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> arrayList) {

    }

    @Override
    protected void onResume() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        if (pref.getString("selectprinter_option", "").equals("Airbill")) {
            try {
                Handler handler;
                BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;

                    socket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {

                    try {
                        if (socket != null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
                if(value==""||value==null){

                }
                else{
                    m_AemScrybeDevice.disConnectPrinter();
//                    m_AemScrybeDevice.pairPrinter(value);
//                    m_AemScrybeDevice.connectToPrinter(value);
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                    m_AemPrinter.print("On Resume connected");
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
//        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
        }
        else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            DebugLog.logTrace();
            mPrinter.onActivityResume();

        }
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        if (pref.getString("selectprinter_option", "").equals("Airbill")) {
            try {
                if(value==""||value==null){

                }
                else{
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                    m_AemPrinter.print("On Resume connected");
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            super.onRestart();
        }


    }

    @Override
    protected void onPause() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        if (pref.getString("selectprinter_option", "").equals("Airbill")) {

            try {

                Handler handler;
                BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;

                    socket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {

                    try {
                        if (socket != null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
                m_AemScrybeDevice.disConnectPrinter();
//                m_AemScrybeDevice.pairPrinter(value);
//                m_AemScrybeDevice.connectToPrinter(value);
//                m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On pause connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            DebugLog.logTrace();
            mPrinter.onActivityPause();

        }


        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        if (pref.getString("selectprinter_option", "").equals("Airbill")) {

            try {
                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                m_AemPrinter.print("On destory connected");
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            DebugLog.logTrace("onDestroy");
            mPrinter.onActivityDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        if (pref.getString("selectprinter_option", "").equals("Airbill")) {
            try {
                Handler handler;
                BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;

                    socket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {

                    try {
                        if (socket != null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
                if(value==""||value==null){

                }
                else{
                    m_AemScrybeDevice.disConnectPrinter();
//                    m_AemScrybeDevice.pairPrinter(value);
//                    m_AemScrybeDevice.connectToPrinter(value);
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
//                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                    m_AemPrinter.print("On Resume connected");
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(RECEIPT_PRINTER_MESSAGES);
            LocalBroadcastManager.getInstance(this).registerReceiver(ReceiptPrinterMessageReceiver, intentFilter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        if (pref.getString("selectprinter_option", "").equals("Airbill")) {
            try {
                m_AemScrybeDevice.disConnectPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (pref.getString("selectprinter_option", "").equals("Dyno")) {
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(ReceiptPrinterMessageReceiver);
            } catch (Exception e) {
                DebugLog.logException(e);
            }
        }
    }

    public String readResponse(HttpResponse res) {
        InputStream is = null;
        String return_text = "";
        try {
            is = res.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            return_text = sb.toString();
        } catch (Exception e) {

        }
        return return_text;
    }

    public void getcount(){
        try {
            String HttpUrl = Config.hosturl+"cat_total.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);

                                JSONArray limitsize = obj.getJSONArray("tbl_category");
                                JSONObject linitobject = limitsize.getJSONObject(0);
                                cat_total = Integer.parseInt(linitobject.getString("count"));
                                cat_datetime = linitobject.getString("cat_datetime");
                                // Toast.makeText(getApplicationContext(),""+linitobject.getString("cat_datetime"),Toast.LENGTH_LONG).show();
                                //Toast.makeText(getApplicationContext(),"tbl_category="+cat_total,Toast.LENGTH_SHORT).show();
                                int first_cat_limit=0,second_cat_limit=Config.limit;
                                for(int i=0;i<cat_total;i++){
                                    insert_cat(first_cat_limit,second_cat_limit);
                                    first_cat_limit=second_cat_limit;
                                    second_cat_limit=second_cat_limit+Config.limit;
                                }

                                JSONArray limitsizeunit = obj.getJSONArray("tbl_unit");
                                JSONObject linitobjectunit = limitsizeunit.getJSONObject(0);
                                unit_total = Integer.parseInt(linitobjectunit.getString("count"));
                                unit_datetime = linitobjectunit.getString("unit_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_unit="+unit_total,Toast.LENGTH_SHORT).show();
                                int first_unit_limit=0,second_unit_limit=Config.limit;
                                for(int i=0;i<unit_total;i++){
                                    insert_unit(first_unit_limit,second_unit_limit);
                                    first_unit_limit=second_unit_limit;
                                    second_unit_limit=second_unit_limit+Config.limit;
                                }

                                JSONArray limitsizeitem = obj.getJSONArray("tbl_item");
                                JSONObject linitobjectitem = limitsizeitem.getJSONObject(0);
                                item_total = Integer.parseInt(linitobjectitem.getString("count"));
                                item_datetime = linitobjectitem.getString("item_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_item="+item_total,Toast.LENGTH_SHORT).show();
                                int first_item_limit=0,second_item_limit=Config.limit;
                                for(int i=0;i<item_total;i++){
                                    insert_item(first_item_limit,second_item_limit);
                                    first_item_limit=second_item_limit;
                                    second_item_limit=second_item_limit+Config.limit;
                                }

                                JSONArray limitsizesup = obj.getJSONArray("tbl_supplier");
                                JSONObject linitobjectsup = limitsizesup.getJSONObject(0);
                                sup_total = Integer.parseInt(linitobjectsup.getString("count"));
                                sup_datetime = linitobjectsup.getString("sup_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_supplier="+sup_total,Toast.LENGTH_SHORT).show();
                                int first_sup_limit=0,second_sup_limit=Config.limit;
                                for(int i=0;i<sup_total;i++){
                                    insert_sup(first_sup_limit,second_sup_limit);
                                    first_sup_limit=second_sup_limit;
                                    second_sup_limit=second_sup_limit+Config.limit;
                                }

                                JSONArray limitsizecust = obj.getJSONArray("tbl_customer");
                                JSONObject linitobjectcust = limitsizecust.getJSONObject(0);
                                cust_total = Integer.parseInt(linitobjectcust.getString("count"));
                                cust_datetime = linitobjectcust.getString("cust_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_customer="+cust_total,Toast.LENGTH_SHORT).show();
                                int first_cust_limit=0,second_cust_limit=Config.limit;
                                for(int i=0;i<cust_total;i++){
                                    insert_cust(first_cust_limit,second_cust_limit);
                                    first_cust_limit=second_cust_limit;
                                    second_cust_limit=second_cust_limit+Config.limit;
                                }

                                JSONArray limitsizepurc = obj.getJSONArray("tbl_inventory");
                                JSONObject linitobjectpurc = limitsizepurc.getJSONObject(0);
                                invt_total = Integer.parseInt(linitobjectpurc.getString("count"));
                                purc_datetime = linitobjectpurc.getString("purc_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_inventory="+invt_total,Toast.LENGTH_SHORT).show();
                                int first_invt_limit=0,second_invt_limit=Config.limit;
                                for(int i=0;i<invt_total;i++){
                                    insert_invt(first_invt_limit,second_invt_limit);
                                    first_invt_limit=second_invt_limit;
                                    second_invt_limit=second_invt_limit+Config.limit;
                                }

                                setting();
                                point_of_contact();
                                payment();
                                check_user_isactive();
                                sync_times(cid,lid,empid);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_cat(int firstcatcnt,int seconcatcnt) {
        try {
            String HttpUrl = Config.hosturl+"category_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Rsponse Cat"+obj,Toast.LENGTH_LONG).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCategory(catObject.getString("cat_name"), catObject.getString("cat_id"), catObject.getString("cat_description"), catObject.getString("cat_image"),catObject.getString("type_id"),catObject.getString("created_at"),catObject.getString("updated_at"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),catObject.getString("emp_id"));
                                    syncid.put("" + i, catObject.getString("cat_id"));
                                }
                                prev_cat_datetime=cat_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("cat_datetime",prev_cat_datetime);
                                editor.commit();
                                postdata = "" + syncid;
                                list.clear();
                                list.add(new BasicNameValuePair("cat_id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_category_api.php");
                                try {
                                    if (list.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(list));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cat_datetime", "" + prev_cat_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_unit(int firstcatcnt,int seconcatcnt) {
        try {
            String HttpUrl = Config.hosturl+"unit_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject unitObject = tutorialsArray.getJSONObject(i);
                                    //Toast.makeText(getApplicationContext(),""+unitObject.getString("Unit_name")+""+unitObject.getString("Unit_Id")+""+unitObject.getString("Unit_Taxvalue")+""+unitObject.getString("created_at")+""+unitObject.getString("is_active")+""+unitObject.getString("cid")+""+unitObject.getString("lid")+""+unitObject.getString("emp_id"),Toast.LENGTH_SHORT).show();
                                    db.SyncUnit(unitObject.getString("Unit_name"), unitObject.getString("Unit_Id"),unitObject.getString("Unit_Taxvalue"),unitObject.getString("created_at"),unitObject.getString("is_active"),unitObject.getString("cid"),unitObject.getString("lid"),unitObject.getString("emp_id"));
                                    syncid.put("" + i, unitObject.getString("Unit_Id"));
                                }
                                prev_unit_datetime=unit_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("unit_datetime",prev_unit_datetime);
                                editor.commit();

                                unitpostdata = "" + syncid;
                                unitlist.clear();
                                unitlist.add(new BasicNameValuePair("Unit_Id", unitpostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_unit_api.php");
                                try {
                                    if (unitlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(unitlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("unit_datetime", "" + prev_unit_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_item(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrl = Config.hosturl+"item_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncItem(catObject.getString("item_id"),catObject.getString("item_name"), catObject.getString("item_id"), catObject.getString("item_rate"),
                                            catObject.getString("item_dis"),catObject.getString("item_disrate"),catObject.getString("item_tax"),
                                            catObject.getString("item_taxvalue"),catObject.getString("item_final_rate"),catObject.getString("item_category"),
                                            catObject.getString("item_units"),catObject.getString("item_stock"),catObject.getString("item_barcode"),
                                            catObject.getString("item_hsncode"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),
                                            catObject.getString("emp_id"),catObject.getString("sub_emp_id"),catObject.getString("item_code"));
                                    syncid.put("" + i, catObject.getString("item_id"));
                                }
                                prev_item_datetime=item_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("item_datetime",prev_item_datetime);
                                editor.commit();

                                itempostdata = "" + syncid;
                                itemlist.clear();
                                itemlist.add(new BasicNameValuePair("item_id", itempostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_item_api.php");
                                try {
                                    if (itemlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(itemlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("item_datetime", "" + prev_item_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_sup(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrlsup = Config.hosturl+"supllier_api.php";
            Log.d("URL", HttpUrlsup);
            StringRequest stringRequestsup = new StringRequest(Request.Method.POST, HttpUrlsup,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject supObject = tutorialsArray.getJSONObject(i);
                                    db.SyncSup(supObject.getString("sup_id"), supObject.getString("sup_name"),supObject.getString("sup_address"),supObject.getString("sup_mobile_no"),supObject.getString("sup_email_id"),supObject.getString("sup_gst_no"),supObject.getString("created_at"),supObject.getString("updated_at"),supObject.getString("is_active"),supObject.getString("cid"),supObject.getString("lid"),supObject.getString("emp_id"));
                                    syncid.put("" + i, supObject.getString("sup_id"));
                                }
                                prev_sup_datetime=sup_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("sup_datetime",prev_sup_datetime);
                                editor.commit();

                                suppostdata = "" + syncid;
                                suplist.clear();
                                suplist.add(new BasicNameValuePair("sup_id", suppostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_supplier_api.php");
                                try {
                                    if (suplist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(suplist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("sup_datetime", "" + prev_sup_datetime);
                    return params;
                }
            };

            RequestQueue requestQueuesup = Volley.newRequestQueue(getApplicationContext());

            requestQueuesup.add(stringRequestsup);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_cust(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrlcust = Config.hosturl+"customer_api.php";
            Log.d("URL", HttpUrlcust);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlcust,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject custObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCust(custObject.getString("cust_id"), custObject.getString("cust_CompanyName"),custObject.getString("cust_name"),custObject.getString("address"),custObject.getString("mobile_no"),custObject.getString("email_id"),custObject.getString("cust_companyId_or_GST"),custObject.getString("created_at"),custObject.getString("updated_at"),custObject.getString("is_active"),custObject.getString("cid"),custObject.getString("lid"),custObject.getString("emp_id"));
                                    syncid.put("" + i, custObject.getString("cust_id"));
                                }
                                prev_cust_datetime=cust_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("cust_datetime",prev_cust_datetime);
                                editor.commit();

                                postdata = "" + syncid;
                                list.clear();
                                list.add(new BasicNameValuePair("cust_id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_customer_api.php");
                                try {
                                    if (list.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(list));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cust_datetime", "" + prev_cust_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_invt(int firstcatcnt,int seconcatcnt) {;
        try {

            String HttpUrlinvt = Config.hosturl+"purchase_api.php";
            Log.d("URL", HttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject invtObject = tutorialsArray.getJSONObject(i);
                                    db.Syncinvetory(invtObject.getString("inventoryid"),invtObject.getString("inventorysupid"),invtObject.getString("inventoryitemid"),invtObject.getString("inventoryitemquantity"),invtObject.getString("inventorystatus"),invtObject.getString("created_at"),invtObject.getString("updated_at"),invtObject.getString("isactive"),invtObject.getString("cid"),invtObject.getString("lid"),invtObject.getString("emp_id"));
                                    syncid.put("" + i, invtObject.getString("inventoryid"));
                                }
                                prev_purc_datetime=purc_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("purc_datetime",prev_purc_datetime);
                                editor.commit();
                                postdata = "" + syncid;
                                purchlist.clear();
                                purchlist.add(new BasicNameValuePair("inventoryid", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_purchase_api.php");
                                try {
                                    if (purchlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(purchlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("purc_datetime", "" + prev_purc_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class SyncData extends AsyncTask<String, String, String> {
        String msg = "Internet/DB_Credentials/Windows_FireWall_TurnOn Error, See Android Monitor in the bottom For details!";
        //  ProgressDialog progress;
        @Override
        protected void onPreExecute() //Starts the progress dailog
        {
            //    progress = ProgressDialog.show(Servicing.this, "Loading",
            //        "ListView Loading! Please Wait...", true);

            Toast.makeText(getApplicationContext(),"Data is syncing!!!",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... strings)  // Connect to the database, write query and add items to array list
        {
            try {
                getcount();
                msg = "Found";
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                msg = writer.toString();
                success = false;
            }
            // progress.dismiss();
            return msg;
        }
        @Override
        protected void onPostExecute(String msg) // disimissing progress dialoge, showing error and setting up my ListView
        {
            //Toast.makeText(Servicing.this, msg + "", Toast.LENGTH_LONG).show();
            if (success == false) {
            } else {
                try {
                    Toast.makeText(getApplicationContext(),"Sync Done Suceesfully!!!",Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    Log.d("Error ", "" + ex);
                    Log.e("Error ", "" + ex);
                }
            }
        }
    }
    public void Synctoweb(){
        uploaddata();
        uploaddatabilldetails();
    }

    public void uploaddata()
    {
        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_master();
        Toast.makeText(getApplicationContext(),"Master Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        Toast.makeText(getApplicationContext(),"Master loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++){
            master_bill_id_list.clear();
            Cursor c=db.getbillmaster(startlimit,endlimit);
            if(c==null || c.getCount() == 0) {

            }else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();
                ArrayList<String> personNames = new ArrayList<>();
                JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        master_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("app_bill_id", c.getString(1));
                        obj.put("bill_date", c.getString(8));
                        obj.put("cust_id",c.getString(3));
                        obj.put("cash_or_credit",c.getString(4));
                        obj.put("discount",c.getString(5));
                        obj.put("gst_setting",c.getString(19));
                        obj.put("bill_totalamt",c.getString(6));
                        obj.put("bill_tax",c.getString(7));
                        obj.put("created_at_TIMESTAMP",c.getString(8));
                        obj.put("updated_at_TIMESTAMP",c.getString(9));
                        obj.put("isactive",c.getString(10));
                        obj.put("cid",cid);
                        obj.put("lid",lid);
                        obj.put("emp_id",empid);
                        obj.put("android_bill_id",c.getString(11));
                        obj.put("sync_flag",2);
                        obj.put("point_of_contact",c.getString(16));
                        obj.put("payment_details",c.getString(17));
                        obj.put("order_details",c.getString(18));
                        obj.put("bill_code",c.getString(20));
                        obj.put("remark",c.getString(21));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);
                //Toast.makeText(getApplicationContext(),""+array,Toast.LENGTH_LONG).show();

                try {
                    String HttpUrl = Config.hosturl + "app_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Toast.makeText(getApplicationContext(), "Response=" + response, Toast.LENGTH_LONG).show();
                                        Log.d("testData=",response);
                                        db.updateBillMasterIDs(master_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }
    }

    public void uploaddatabilldetails()
    {
        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_deatils();
        Toast.makeText(getApplicationContext(),"Details Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        Toast.makeText(getApplicationContext(),"Details loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++) {
            details_bill_id_list.clear();
            Cursor c = db.getbillmasterdetails(startlimit,endlimit);
            if (c == null || c.getCount() == 0) {

            } else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();

                ArrayList<String> personNames = new ArrayList<>();
                JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        details_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("bill_no", c.getString(1));
                        obj.put("item_name", c.getString(2));
                        obj.put("item_qty", c.getString(3));
                        obj.put("item_rate", c.getString(4));
                        obj.put("item_totalrate", c.getString(5));
                        obj.put("created_at_TIMESTAMP", c.getString(6));
                        obj.put("updated_at_TIMESTAMP", c.getString(7));
                        obj.put("isactive", c.getString(8));
                        obj.put("cid", cid);
                        obj.put("lid", lid);
                        obj.put("emp_id", empid);
                        obj.put("android_bill_id", c.getString(9));
                        obj.put("bill_code", c.getString(14));
                        obj.put("sync_flag", c.getString(13));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);

                try {
                    String HttpUrl = Config.hosturl + "bill_detail_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Toast.makeText(getApplicationContext(), "Response=" + response, Toast.LENGTH_LONG).show();
                                        db.updateBillDetailsIDs(details_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                                    Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }

    }

    public void point_of_contact(){
        try {
            String HttpUrlpointcontact = Config.hosturl+"point_of_contact_api.php";
            Log.d("URL", HttpUrlpointcontact);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointcontact,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject point_contactsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pointObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpointcontact(pointObject.getString("id"),pointObject.getString("point_of_contact"),pointObject.getString("is_active"),pointObject.getString("cid"),pointObject.getString("lid"),pointObject.getString("emp_id"),pointObject.getString("sync_flag"),pointObject.getString("created_at"),pointObject.getString("updated_at"));
                                    point_contactsyncid.put("" + i, pointObject.getString("id"));
                                }
                                postdata = "" + point_contactsyncid;
                                pointconatctlist.clear();
                                pointconatctlist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_point_contact_api.php");
                                try {
                                    if (pointconatctlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(pointconatctlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void payment(){

        //Toast.makeText(getApplicationContext(),"This is payment method",Toast.LENGTH_SHORT).show();

        try {
            String PaymnetHttpUrlinvt = Config.hosturl+"payment_type.php";
            Log.d("URL", PaymnetHttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaymnetHttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Response="+response,Toast.LENGTH_SHORT).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pyamnetObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpayment(pyamnetObject.getString("id"),pyamnetObject.getString("payment_type"),pyamnetObject.getString("is_active"),pyamnetObject.getString("cid"),pyamnetObject.getString("lid"),pyamnetObject.getString("emp_id"),pyamnetObject.getString("sync_flag"),pyamnetObject.getString("created_at"),pyamnetObject.getString("updated_at"));
                                    syncid.put("" + i, pyamnetObject.getString("id"));
                                    //Toast.makeText(getApplicationContext(),pyamnetObject.getString("payment_type"),Toast.LENGTH_SHORT).show();
                                }
                                postdata = "" + syncid;
                                paymnetlist.clear();
                                paymnetlist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_payment_api.php");
                                try {
                                    if (paymnetlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(paymnetlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setting(){
        try {
            String HttpUrlpointsetting = Config.hosturl+"setting_api.php";
            Log.d("URL", HttpUrlpointsetting);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointsetting,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject settingsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject settingObject = tutorialsArray.getJSONObject(i);
                                    SyncSetting(settingObject.getString("id"),settingObject.getString("h1"),settingObject.getString("h2"),settingObject.getString("h3"),settingObject.getString("h4"),settingObject.getString("h5"), settingObject.getString("f1"),settingObject.getString("f2"),settingObject.getString("f3"), settingObject.getString("f4"),settingObject.getString("f5"),settingObject.getString("created_at"), settingObject.getString("updated_at"), settingObject.getString("is_active"),settingObject.getString("cid"), settingObject.getString("lid"),settingObject.getString("emp_id"),settingObject.getString("page_size"), settingObject.getString("gst_setting"),settingObject.getString("bill_printing"),settingObject.getString("multiple_print"), settingObject.getString("reset_bill"),settingObject.getString("sync_flag"));
                                    settingsyncid.put("" + i, settingObject.getString("id"));
                                }
                                postdata = "" + settingsyncid;
                                settinglist.clear();
                                settinglist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_setting_api.php");
                                try {
                                    if (settinglist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(settinglist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SyncSetting(String id,String h1,String h2,String h3,String h4,String h5,String f1,String f2,String f3,String f4,String f5,String created_at,String updated_at,String is_active,String cid,String lid,String emp_id,String page_size,String gst_setting,String bill_printing,String multiple_print,String reset_bill,String sync_flag){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        if(gst_setting.equals("Yes")){
            editor.putString("gst_option","GST Enable");
        }else{
            editor.putString("gst_option","GST Disable");
        }
        editor.putString("reset_bill",reset_bill);
        editor.commit();
    }

    public void check_user_isactive(){
        try {
            String HttpUrluseractive = Config.hosturl+"check_user_isactive.php";
            Log.d("URL", HttpUrluseractive);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrluseractive,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();

                                JSONObject obj = new JSONObject(response);

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject isactivegObject = tutorialsArray.getJSONObject(i);
                                    Toast.makeText(getApplicationContext(),"ISactive in admin="+isactivegObject.getString("is_active"),Toast.LENGTH_SHORT).show();
                                    editor.putString("is_active",isactivegObject.getString("is_active"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sync_times(String tcid, String tlid,String tempid){
        try {
            final SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            final SharedPreferences.Editor editor = pref.edit();
            String HttpUrltime = Config.hosturl+"sync_time_api.php";
            Log.d("URL", HttpUrltime);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrltime,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject objtime = new JSONObject(response);

                                JSONArray tutorialsArraytime = objtime.getJSONArray("data");
                                for (int i = 0; i < tutorialsArraytime.length(); i++) {
                                    JSONObject timeObject = tutorialsArraytime.getJSONObject(i);
                                    editor.putString("upload_interval",timeObject.getString("upload_interval"));
                                    editor.putString("download_interval",timeObject.getString("download_interval"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + tlid);
                    params.put("cid", "" + tcid);
                    params.put("empid", "" + tempid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}