package com.example.rajlaxmi.airbill_online;

import java.util.ArrayList;

public class TableModelClass {
    public static ArrayList<String> tbl_arry_item_name = new ArrayList<String>();
    public static ArrayList<String> tbl_arr_item_rate = new ArrayList<String>();
    public static ArrayList<Double> tbl_arr_item_qty = new ArrayList<Double>();
    public static ArrayList<Double> tbl_arr_item_price = new ArrayList<Double>();
    public static ArrayList<Double> tbl_array_item_amt = new ArrayList<Double>();
    public static ArrayList<String> tbl_arr_item_basicrate = new ArrayList<>();
    public static ArrayList<String> tbl_arr_item_tax = new ArrayList<>();
}
