package com.example.rajlaxmi.airbill_online;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.Map;

public class EditItemListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;
    String str_itemname="";
    DatabaseHelper db;

    EditText et_new_item_name,et_rate,et_dis,et_after_discount,et_tax,et_tax2,et_tax3,et_stock,et_barcode;
    Spinner sp_category,sp_units;
    Button bt_submit_item,bt_cancel_item;
    List<String> categoryList=new ArrayList<>();
    List<String> unitsdata=new ArrayList<>();
    ArrayAdapter<String> adapter,adapter1,taxadapter;
    Cursor c;
    String item_id,str_repeat_cat,str_repeat_unit;
    String empid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        db=new DatabaseHelper(this);
        headFootSetting=new HeadFootSetting(getApplicationContext());
        str_itemname=getIntent().getExtras().getString("itemname");

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        empid=pref.getString("emp_id","");

        et_new_item_name=(EditText)findViewById(R.id.et_new_item_name);
        et_rate=(EditText)findViewById(R.id.et_rate);
        et_dis=(EditText)findViewById(R.id.et_dis);
        et_after_discount=(EditText)findViewById(R.id.et_after_discount);
        et_tax=(EditText)findViewById(R.id.et_tax);
        et_tax2=(EditText)findViewById(R.id.et_tax2);
        et_tax3=(EditText)findViewById(R.id.et_tax3);
        et_stock=(EditText)findViewById(R.id.et_stock);
        et_barcode=(EditText)findViewById(R.id.et_barcode);

        sp_category=(Spinner)findViewById(R.id.sp_category);
        sp_units=(Spinner)findViewById(R.id.sp_units);

        bt_submit_item=(Button)findViewById(R.id.bt_submit_item);
        bt_cancel_item=(Button)findViewById(R.id.bt_cancel_item);
        String newcat="",newunit="";
        c=db.getItemDetailsOfItemName(str_itemname);
        if(c.getCount() == 0) {
            Message.message(getApplicationContext(),"Nothing found");
        }else {
            StringBuffer buffer = new StringBuffer();
            while (c.moveToNext()) {
                item_id=c.getString(0);
                et_new_item_name.setText(c.getString(1));
                et_rate.setText(c.getString(2));
                et_dis.setText(c.getString(3));
                et_after_discount.setText(c.getString(4));
                et_tax.setText(c.getString(5));
                et_tax2.setText(c.getString(6));
                et_tax3.setText(c.getString(7));
                unitsdata.add(c.getString(9));
                newunit=c.getString(9);
                categoryList.add(c.getString(8));
                newcat=c.getString(8);
                et_stock.setText(c.getString(10));
                et_barcode.setText(c.getString(11));
            }
        }


// repeated additions:

        categoryList.addAll(db.getAllCategory());
        Set<String> set = new HashSet<>(categoryList);
//        if (!categoryList.contains(newcat)) {newcat.replace(newcat,newcat);}

        categoryList.clear();
        categoryList.addAll(set);
        adapter=new ArrayAdapter<String>(EditItemListActivity.this,android.R.layout.simple_spinner_dropdown_item,categoryList);
        sp_category.setAdapter(adapter);
        sp_category.setSelection(adapter.getPosition(newcat));

        unitsdata.addAll(db.getAllUnits());
        Set<String> set1 = new HashSet<>(unitsdata);
        if (!unitsdata.contains(newunit)) {newunit.replace(newunit,newunit);}
        unitsdata.clear();
        unitsdata.addAll(set1);
        adapter1=new ArrayAdapter<String>(EditItemListActivity.this,android.R.layout.simple_spinner_dropdown_item,unitsdata);
        sp_units.setAdapter(adapter1);
        sp_units.setSelection(adapter1.getPosition(newunit));

        bt_submit_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.EditItem(item_id,et_new_item_name.getText().toString(),et_rate.getText().toString(),et_dis.getText().toString(),et_after_discount.getText().toString(),et_tax.getText().toString(),et_tax2.getText().toString(),et_tax3.getText().toString(),sp_units.getSelectedItem().toString(),sp_category.getSelectedItem().toString(),et_stock.getText().toString(),et_barcode.getText().toString());
                Toast.makeText(getApplicationContext(),"Updated Successfully!!!",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(getApplicationContext(),ItemListActivity.class);
                startActivity(i);
                finish();
            }
        });

        bt_cancel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),ItemListActivity.class);
                startActivity(i);
                finish();
            }
        });

        et_rate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(et_rate.getText().toString()!=""||et_dis.getText().toString()!="")
                {
                    Double rate=0.0,dis=0.0,etrateval=0.0,etdisval=0.0;
                    double total;
                    if(et_rate.getText().toString().equals("")){
                        rate=0.0;
                    }else{
                        rate=Double.parseDouble(et_rate.getText().toString());
                    }
                    if(et_dis.getText().toString().equals("")){
                        dis=0.0;
                    }else {
                        dis=Double.parseDouble(et_dis.getText().toString());
                    }

                    total=rate-((rate*dis)/100);
                    et_after_discount.setText(""+total);

                    Double rate1 = 0.0, dis1 = 0.0;
                    double total1 = 0.0, discountrate1 = 0.0;
                    discountrate1 = Double.parseDouble(et_after_discount.getText().toString());
                    if (et_tax.getText().toString().equals("")) {
                        rate1= 0.0;
                    } else {
                        rate1 = Double.parseDouble(et_tax.getText().toString());
                    }

                    if (et_after_discount.getText().toString().equals("")) {
                        discountrate1 = 0;
                    } else {
                        discountrate1 = Double.parseDouble(et_after_discount.getText().toString());
                    }
                    total1 = ((rate1 * discountrate1) / 100);
                    et_tax2.setText("" + total1);
                    et_tax3.setText("" + (discountrate1 + total1));
                }
                else
                {
                    Message.message(getApplicationContext(),"Enter rate/discount value");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_dis.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(et_rate.getText().toString()!=""||et_dis.getText().toString()!="")
                {

                    Double rate=0.0,dis=0.0,etrateval=0.0,etdisval=0.0;
                    double total, discountrate;

                    if(et_rate.getText().toString().equals("")){
                        rate=0.0;
                    }else{
                        rate=Double.parseDouble(et_rate.getText().toString());
                    }
                    if(et_dis.getText().toString().equals("")){
                        dis=0.0;
                    }else {
                        dis=Double.parseDouble(et_dis.getText().toString());
                    }

                    total=rate-((rate*dis)/100);
                    et_after_discount.setText(""+total);

                    Double rate1 = 0.0, dis1 = 0.0;
                    double total1 = 0.0, discountrate1 = 0.0;
                    discountrate1 = Double.parseDouble(et_after_discount.getText().toString());
                    if (et_tax.getText().toString().equals("")) {
                        rate1= 0.0;
                    } else {
                        rate1 = Double.parseDouble(et_tax.getText().toString());
                    }

                    if (et_after_discount.getText().toString().equals("")) {
                        discountrate1 = 0.0;
                    } else {
                        discountrate1 = Double.parseDouble(et_after_discount.getText().toString());
                    }
                    total1 = ((rate1 * discountrate1) / 100);
//               total=rate-((rate*dis)/100);
                    et_tax2.setText("" + total1);
                    et_tax3.setText("" + (discountrate1 + total1));

                }
                else
                {
                    Message.message(getApplicationContext(),"Enter rate/discount value");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        et_tax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(!et_tax.getText().toString().equals("")||!et_after_discount.getText().toString().equals(""))
                {
                    Double rate = 0.0, dis = 0.0, etrateval =0.0, etdisval = 0.0;
                    double total = 0, discountrate = 0;
                    discountrate = Double.parseDouble(et_after_discount.getText().toString());
                    if (et_tax.getText().toString().equals("")) {
                        rate = 0.0;
                    } else {
                        rate = Double.parseDouble(et_tax.getText().toString());
                    }

                    if (et_after_discount.getText().toString().equals("")) {
                        discountrate = 0.0;
                    } else {
                        discountrate = Double.parseDouble(et_after_discount.getText().toString());
                    }
                    total = ((rate * discountrate) / 100);
//               total=rate-((rate*dis)/100);
                    et_tax2.setText("" + total);
                    et_tax3.setText("" + (discountrate + total));

                }
                else {

                    Message.message(getApplicationContext(),"please enter all values");
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_item_list, menu);
        return true;
    }


    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}