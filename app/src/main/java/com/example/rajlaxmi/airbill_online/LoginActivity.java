package com.example.rajlaxmi.airbill_online;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends AppCompatActivity implements IRequestListener{

    Button bt_login,bt_cancel;
    EditText ename,epass;
    Context context;
    DatabaseHelper db;
    int login_flag=2;
    int emp_id,lid,cid;
    Dialog dialog1;
    String InstallDate="",ExpireDate="",status="";
    String token="";
    private TokenService tokenService;
    private static final String TAG = "SplashActivity";
    int login_active=0;

    //Context context;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        //java code
        db=new DatabaseHelper(this);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());

        try {
            //Get token from Firebase
            FirebaseMessaging.getInstance().subscribeToTopic("test");
            final String token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Token: " + token);

            //Call the token service to save the token in the database
            tokenService = new TokenService(this, this);
            tokenService.registerTokenInDB(token);

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Error="+e,Toast.LENGTH_LONG).show();
        }

        bt_login=(Button)findViewById(R.id.bt_login);
        bt_cancel=(Button)findViewById(R.id.bt_cancel);
        ename=(EditText)findViewById(R.id.et_username);
        epass=(EditText)findViewById(R.id.et_password);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        editor.putString("print_option","2 inch");
        editor.commit();

        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                try{
                    if(ename.getText().toString().equals("")||epass.getText().toString().equals(""))
                    {
                        Message.message(getApplicationContext(),"Please enter Username/Password ");
                    }else
                    {
                        String HttpUrl = Config.hosturl+"checklogin.php";
                        Log.d("URL",HttpUrl);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String ServerResponse) {

                                        Log.d("Server Response",ServerResponse);
//                                    progressDialog.dismiss();

                                        try {
                                            //getting the whole json object from the response
                                            JSONObject obj = new JSONObject(ServerResponse);
                                            status = obj.getString("status");
                                            token=pref.getString("Token","");
                                            if(status.equals("success"))
                                            {
                                                login_active=Integer.parseInt(obj.getString("login_active"));
                                                if(login_active==0) {
                                                    sync_times(obj.getString("cid"), obj.getString("lid"), obj.getString("emp_id"));
                                                    UpdateToken(ename.getText().toString(), epass.getText().toString(), getApplicationContext(), token);
                                                    editor.putString("lid", obj.getString("lid"));
                                                    editor.putString("cid", obj.getString("cid"));
                                                    editor.putString("emp_id", obj.getString("emp_id"));
                                                    editor.putString("employee_code", obj.getString("employee_code"));
                                                    editor.putString("Username", ename.getText().toString());
                                                    editor.putString("Panel", "ShopOwner");
                                                    editor.putString("cat_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("unit_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("item_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("sup_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("cust_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("purc_datetime", "0000-00-00 00:00:00");
                                                    editor.putString("openrate", "No");
                                                    editor.commit();
                                                    sync_times(obj.getString("cid"), obj.getString("lid"), obj.getString("emp_id"));
                                                    Toast.makeText(getApplicationContext(), "Login Successfully", Toast.LENGTH_LONG).show();
                                                    Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                                                    startActivity(i);
                                                    finish();
                                                }else {
                                                    Toast.makeText(getApplicationContext(),"Please Activate Your Account",Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                            else
                                            {
                                                Toast.makeText(getApplicationContext(), obj.getString("status")+": Invalid Username and Password", Toast.LENGTH_LONG).show();
                                            }

                                        }catch (JSONException e) {
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                                        Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("username", ename.getText().toString());
                                params.put("password", epass.getText().toString());
                                return params;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(getApplicationContext(),RegistrationActivity.class);
//                startActivity(i);

                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory( Intent.CATEGORY_HOME );
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();
            }
        });
    }

    protected void CustomMessageBox3(int type,String strmsg){
        dialog1=new Dialog(this);
        dialog1.setContentView(R.layout.error_layout);
        dialog1.getWindow().getAttributes().windowAnimations=type;
        dialog1.setCancelable(false);
        dialog1.show();

        TextView tv_errortext=(TextView)dialog1.findViewById(R.id.tv_errortext);
        tv_errortext.setText(strmsg);

        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog1.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
            }
        }, 5000);
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onError(String message) {

    }

    public static void UpdateToken(final String Username, final String Password, final Context context, final String token){
        final SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        try {
            RequestQueue queue = Volley.newRequestQueue(context);
            String url = Config.hosturl+"callurl.php";

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("Token updated successfully")){
                                editor.putString("token_flag","1");
                                editor.commit();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context, "Something Went To Wrong!", Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile_no", Username);
                    params.put("password", Password);
                    params.put("token", token);
                    return params;
                }

            };
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context,"Error="+e,Toast.LENGTH_LONG).show();
        }
    }
    public void sync_times(String tcid, String tlid,String tempid){
        try {
            final SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
            final SharedPreferences.Editor editor = pref.edit();
            String HttpUrltime = Config.hosturl+"sync_time_api.php";
            Log.d("URL", HttpUrltime);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrltime,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject objtime = new JSONObject(response);

                                JSONArray tutorialsArraytime = objtime.getJSONArray("data");
                                for (int i = 0; i < tutorialsArraytime.length(); i++) {
                                    JSONObject timeObject = tutorialsArraytime.getJSONObject(i);
                                    editor.putString("upload_interval",timeObject.getString("upload_interval"));
                                    editor.putString("download_interval",timeObject.getString("download_interval"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + tlid);
                    params.put("cid", "" + tcid);
                    params.put("empid", "" + tempid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            //super.onBackPressed();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }
}