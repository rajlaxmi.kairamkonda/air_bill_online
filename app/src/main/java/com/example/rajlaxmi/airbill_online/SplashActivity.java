package com.example.rajlaxmi.airbill_online;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity implements IRequestListener{

    private static int SPLASH_TIME_OUT = 5;
    String str_panel="",str_username="";
    int flag=5;
    String CurrentDate="";
    Dialog dialog1;
    private TokenService tokenService;
    private static final String TAG = "SplashActivity";
    DatabaseHelper db;
    String lid="",cid="",empid="";
    ArrayList<String> master_bill_id_list=new ArrayList<String>();
    ArrayList<String> details_bill_id_list=new ArrayList<String>();
    DetectUninstall uninstall;
    @Override
    protected void onDestroy() {
        super.onDestroy();
     //   unregisterReceiver(uninstall);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        db=new DatabaseHelper(this);
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();

        try {
            //Get token from Firebase
            FirebaseMessaging.getInstance().subscribeToTopic("test");
            final String token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Token: " + token);
            //Call the token service to save the token in the database
            tokenService = new TokenService(this, this);
            tokenService.registerTokenInDB(token);

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Error="+e,Toast.LENGTH_LONG).show();
        }

        new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                str_panel=pref.getString("Panel","");
                str_username=pref.getString("Username","");
                cid=pref.getString("cid","");
                lid=pref.getString("lid","");
                empid=pref.getString("emp_id","");

                if(str_panel.equals("Admin")){
                    Intent i=new Intent(SplashActivity.this,PanelActivity.class);
                    startActivity(i);
                    finish();
                }else if(str_panel.equals("ShopOwner")){
                    check_user_isactive();
                    check_user_isactive();
                    Calendar cal = Calendar.getInstance();
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    int month = cal.get(Calendar.MONTH);
                    int year = cal.get(Calendar.YEAR);
                    CurrentDate = day + "-" + (month+1) + "-" + year;
                    //Toast.makeText(getApplicationContext(),"Is_Active="+pref.getString("is_active",""),Toast.LENGTH_SHORT).show();
                    if(CurrentDate.equals(pref.getString("Expire_date",""))){
                        CustomMessageBox3(R.style.DialogAnimation,"Your App is Expired, Please contact your Distributor");
                    }else if(pref.getString("is_active","").equals("1")){
                        CustomMessageBox3(R.style.DialogAnimation,"Your App is Expired, Please contact your Distributor");
                    }else {
                        Handler handler = new Handler();
                        Runnable runnableCode = new Runnable() {
                            @Override
                            public void run() {
                                uploaddata();
                                uploaddatabilldetails();
                                if(pref.getString("upload_interval","").equals("")){
                                    handler.postDelayed(this, 3600000);
                                }else {
                                    handler.postDelayed(this, Long.parseLong(pref.getString("upload_interval","")));
                                }
                            }
                        };
                        handler.post(runnableCode);
                        Intent i = new Intent(SplashActivity.this, AdminDashBoardActivity.class);
                        startActivity(i);
                        finish();
                    }
                }else{
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    protected void CustomMessageBox3(int type,String strmsg){
        dialog1=new Dialog(this);
        dialog1.setContentView(R.layout.error_layout);
        dialog1.getWindow().getAttributes().windowAnimations=type;
        dialog1.setCancelable(false);
        dialog1.show();

        TextView tv_errortext=(TextView)dialog1.findViewById(R.id.tv_errortext);
        tv_errortext.setText(strmsg);

        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog1.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
                finish();
            }
        }, 5000);
    }
    @Override
    public void onComplete() {
        Log.d(TAG, "Token registered successfully in the DB");
    }

    @Override
    public void onError(String message) {
        Log.d(TAG, "Error trying to register the token in the DB: " + message);
    }

    public void uploaddata()
    {
        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_master();
        //Toast.makeText(getApplicationContext(),"Master Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        //Toast.makeText(getApplicationContext(),"Master loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++){
            master_bill_id_list.clear();
            Cursor c=db.getbillmaster(startlimit,endlimit);
            if(c==null || c.getCount() == 0) {

            }else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();
                ArrayList<String> personNames = new ArrayList<>();
                JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        master_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("app_bill_id", c.getString(1));
                        obj.put("bill_date", c.getString(8));
                        obj.put("cust_id",c.getString(3));
                        obj.put("cash_or_credit",c.getString(4));
                        obj.put("discount",c.getString(5));
                        obj.put("gst_setting",c.getString(19));
                        obj.put("bill_totalamt",c.getString(6));
                        obj.put("bill_tax",c.getString(7));
                        obj.put("created_at_TIMESTAMP",c.getString(8));
                        obj.put("updated_at_TIMESTAMP",c.getString(9));
                        obj.put("isactive",c.getString(10));
                        obj.put("cid",cid);
                        obj.put("lid",lid);
                        obj.put("emp_id",empid);
                        obj.put("android_bill_id",c.getString(11));
                        obj.put("sync_flag",2);
                        obj.put("point_of_contact",c.getString(16));
                        obj.put("payment_details",c.getString(17));
                        obj.put("order_details",c.getString(18));
                        obj.put("bill_code",c.getString(20));
                        obj.put("remark",c.getString(21));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);

                try {
                    String HttpUrl = Config.hosturl + "app_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        //Toast.makeText(getApplicationContext(), "Response=" + response, Toast.LENGTH_LONG).show();
                                        db.updateBillMasterIDs(master_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }
    }

    public void uploaddatabilldetails()
    {
        int total_rows=0,startlimit=0,endlimit=0,divide=0,mod=0,finalcount=0;
        total_rows=db.total_rows_bill_deatils();
        //Toast.makeText(getApplicationContext(),"Details Total Rows="+total_rows, Toast.LENGTH_SHORT).show();
        startlimit=0;
        endlimit=Config.import_limit;

        if(total_rows>Config.import_limit){
            divide=total_rows/Config.import_limit;
            if(total_rows%Config.import_limit==0){
                mod=0;
            }else {
                mod=1;
            }
            finalcount=divide+mod;
        }else {
            finalcount=1;
        }
        //Toast.makeText(getApplicationContext(),"Details loops="+finalcount, Toast.LENGTH_SHORT).show();
        for(int r=0;r<finalcount;r++) {
            details_bill_id_list.clear();
            Cursor c = db.getbillmasterdetails(startlimit,endlimit);
            if (c == null || c.getCount() == 0) {

            } else {
                StringBuffer buffer = new StringBuffer();
                JSONObject jsonObject = new JSONObject();
                JSONArray arr = new JSONArray();

                ArrayList<String> personNames = new ArrayList<>();
                JSONArray array = new JSONArray();
                personNames.clear();

                while (c.moveToNext()) {
                    try {
                        details_bill_id_list.add(c.getString(0));
                        JSONObject obj = new JSONObject();
                        obj.put("bill_no", c.getString(1));
                        obj.put("item_name", c.getString(2));
                        obj.put("item_qty", c.getString(3));
                        obj.put("item_rate", c.getString(4));
                        obj.put("item_totalrate", c.getString(5));
                        obj.put("created_at_TIMESTAMP", c.getString(6));
                        obj.put("updated_at_TIMESTAMP", c.getString(7));
                        obj.put("isactive", c.getString(8));
                        obj.put("cid", cid);
                        obj.put("lid", lid);
                        obj.put("emp_id", empid);
                        obj.put("android_bill_id", c.getString(9));
                        obj.put("bill_code", c.getString(14));
                        array.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //Toast.makeText(getApplicationContext(),"Response\n"+array,Toast.LENGTH_LONG).show();
                try {
                    jsonObject.put("array", array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("Array", "" + jsonObject);

                try {
                    String HttpUrl = Config.hosturl + "bill_detail_api.php";
                    Log.d("URL", HttpUrl);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        //Toast.makeText(getApplicationContext(), "Response=" + response, Toast.LENGTH_LONG).show();
                                        db.updateBillDetailsIDs(details_bill_id_list);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "updated Error2=" + e, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                                    Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("json_array", "" + array);
                            params.put("lid", "" + lid);
                            params.put("cid", "" + cid);
                            params.put("empid", "" + empid);
                            return params;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            startlimit=endlimit;
            endlimit=endlimit+Config.import_limit;
        }

    }
    public void check_user_isactive(){
        try {
            String HttpUrluseractive = Config.hosturl+"check_user_isactive.php";
            Log.d("URL", HttpUrluseractive);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrluseractive,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();

                                JSONObject obj = new JSONObject(response);

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject isactivegObject = tutorialsArray.getJSONObject(i);
                                    //Toast.makeText(getApplicationContext(),"ISactive in admin="+isactivegObject.getString("is_active"),Toast.LENGTH_SHORT).show();
                                    editor.putString("is_active",isactivegObject.getString("is_active"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}