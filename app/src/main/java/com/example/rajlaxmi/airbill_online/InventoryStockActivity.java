package com.example.rajlaxmi.airbill_online;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.Map;

public class InventoryStockActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;

    EditText et_qyantity;
    AutoCompleteTextView et_supplier_id,et_item_name;
    Button bt_submit_stock;
    private RadioGroup rg_stock;
    private RadioButton rb_stock;
    DatabaseHelper db;

    String print_option="";
    ArrayList<String> NameList,IdList,sup_nameList,sup_idList;
    String Name="",Sup_Name="",item_id="",Sup_id="";
    String empid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_stock);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        headFootSetting=new HeadFootSetting(getApplicationContext());

        et_supplier_id=(AutoCompleteTextView)findViewById(R.id.et_supplier_id);
        et_item_name=(AutoCompleteTextView)findViewById(R.id.et_item_id);
        et_qyantity=(EditText)findViewById(R.id.et_qyantity);
        bt_submit_stock=(Button)findViewById(R.id.bt_submit_stock);
        rg_stock=(RadioGroup)findViewById(R.id.rg_stock);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        print_option=pref.getString("print_option","");
        empid=pref.getString("emp_id","");

        db = new DatabaseHelper(this);

        NameList=new ArrayList<String>();
        IdList=new ArrayList<String>();
        sup_nameList=new ArrayList<String>();
        sup_idList=new ArrayList<String>();

        Cursor c =db.getAllItems();
        if(c.getCount() == 0) {
            Message.message(getApplicationContext(),"Nothing found");
        }else {
            while (c.moveToNext()) {
                IdList.add(c.getString(0));
                NameList.add(c.getString(1));
            }
        }

        //Toast.makeText(getApplicationContext(),"List="+NameList,Toast.LENGTH_SHORT).show();
        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,NameList);
        et_item_name.setThreshold(1);
        et_item_name.setAdapter(adapter2);

        et_item_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Name=et_item_name.getText().toString();
            }
        });

        Cursor c1 =db.getSupplierDetails();
        if(c1.getCount() == 0) {
            Message.message(getApplicationContext(),"Nothing found");
        }else {
            while (c1.moveToNext()) {
                sup_idList.add(c1.getString(0));
                sup_nameList.add(c1.getString(1));
            }
        }

        ArrayAdapter<String> adapter3=new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,sup_nameList);
        et_supplier_id.setThreshold(1);
        et_supplier_id.setAdapter(adapter3);

        et_supplier_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Sup_Name=et_supplier_id.getText().toString();
            }
        });


        bt_submit_stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(et_item_name.getText().toString().equals("")||et_qyantity.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Please, Fill All Details!!!", Toast.LENGTH_LONG).show();
                }else {
                    int selectedId=rg_stock.getCheckedRadioButtonId();
                    rb_stock=(RadioButton)findViewById(selectedId);

                    if(et_supplier_id.getText().toString().equals("")||et_supplier_id.getText().toString().equals(null)){
                        Sup_id="0";
                        et_supplier_id.setText("NA");
                    }else {
                        Sup_id=sup_idList.get(sup_nameList.indexOf(Sup_Name));
                    }
                    item_id=IdList.get(NameList.indexOf(Name));
                    db.AddInventory(Sup_id,et_supplier_id.getText().toString(),item_id,et_item_name.getText().toString(),et_qyantity.getText().toString(),rb_stock.getText().toString());
                    Toast.makeText(getApplicationContext(),"Inserted!!!",Toast.LENGTH_SHORT).show();
                    Intent i=new Intent(getApplicationContext(),InventoryStockActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inventory_stock, menu);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}