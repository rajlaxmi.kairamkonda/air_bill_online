package com.example.rajlaxmi.airbill_online;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.Map;

public class AddItemActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;

    EditText itemname, itemrate, itdis, disrate, itemtax, ettax, taxvalue, finalrate, itemstock, itembarcode;
    Spinner sunits, scategory, sptax;
    Button bsubmit, bcancel;
    DatabaseHelper db;
    Context context;
    List<String> users = new ArrayList<>();
    List<String> unitsdata = new ArrayList<>();

    ArrayAdapter<String> adapter, adapter1, taxadapter;

    String empid="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        headFootSetting = new HeadFootSetting(getApplicationContext());

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        empid=pref.getString("emp_id","");

        db = new DatabaseHelper(this);

        context = this.context;
        itemname = (EditText) findViewById(R.id.et_new_item_name);
        itemtax = (EditText) findViewById(R.id.et_tax_name);
        itembarcode = (EditText) findViewById(R.id.et_barcode);
        itemstock = (EditText) findViewById(R.id.et_stock);
        itemrate = (EditText) findViewById(R.id.et_rate);
        itdis = (EditText) findViewById(R.id.et_dis);
        disrate = (EditText) findViewById(R.id.et_after_discount);
        ettax = (EditText) findViewById(R.id.et_tax);
        taxvalue = (EditText) findViewById(R.id.et_tax2);
        finalrate = (EditText) findViewById(R.id.et_tax3);


        sunits = (Spinner) findViewById(R.id.sp_units);
        scategory = (Spinner) findViewById(R.id.sp_category);

        bsubmit = (Button) findViewById(R.id.bt_submit_item);
        bcancel = (Button) findViewById(R.id.bt_cancel_item);

        users = db.getAllCategory();
        users.add(0, "Select Category");
        //adapter for spinner
        // Toast.makeText(getApplicationContext(),"Values are="+users,Toast.LENGTH_LONG).show();
        adapter = new ArrayAdapter<String>(AddItemActivity.this, android.R.layout.simple_spinner_dropdown_item, users);
        //attach adapter to spinner
        scategory.setAdapter(adapter);

        unitsdata = db.getAllUnits();
        unitsdata.add(0, "Select Unit");
        adapter1 = new ArrayAdapter<String>(AddItemActivity.this, android.R.layout.simple_spinner_dropdown_item, unitsdata);
        sunits.setAdapter(adapter1);

        itemname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

//        boolean b=db.checkAlreadyExist(itemname.getText().toString());
            }
        });

        itemrate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (itemrate.getText().toString() != "" || itdis.getText().toString() != "") {
                    Double rate = 0.0, dis = 0.0, etrateval = 0.0, etdisval = 0.0;
                    double total;
                    if (itemrate.getText().toString().equals("")) {
                        rate = 0.0;
                    } else {
                        rate = Double.parseDouble(itemrate.getText().toString());
                    }
                    if (itdis.getText().toString().equals("")) {
                        dis = 0.0;
                    } else {
                        dis = Double.parseDouble(itdis.getText().toString());
                    }

                    total = rate - ((rate * dis) / 100);
                    disrate.setText("" + total);

                    Double rate1 = 0.0, dis1 = 0.0;
                    double total1 = 0.0, discountrate1 = 0.0;
                    discountrate1 = Double.parseDouble(disrate.getText().toString());
                    if (ettax.getText().toString().equals("")) {
                        rate1 = 0.0;
                    } else {
                        rate1 = Double.parseDouble(ettax.getText().toString());
                    }

                    if (disrate.getText().toString().equals("")) {
                        discountrate1 = 0;
                    } else {
                        discountrate1 = Double.parseDouble(disrate.getText().toString());
                    }
                    total1 = ((rate1 * discountrate1) / 100);
//               total=rate-((rate*dis)/100);
                    taxvalue.setText("" + total1);
                    finalrate.setText("" + (discountrate1 + total1));


                } else {
                    Message.message(getApplicationContext(), "Enter rate/discount value");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        itdis.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (itemrate.getText().toString() != "" || itdis.getText().toString() != "") {

                    Double rate = 0.0, dis = 0.0, etrateval = 0.0, etdisval = 0.0;
                    double total, discountrate;

                    if (itemrate.getText().toString().equals("")) {
                        rate = 0.0;
                    } else {
                        rate = Double.parseDouble(itemrate.getText().toString());
                    }
                    if (itdis.getText().toString().equals("")) {
                        dis = 0.0;
                    } else {
                        dis = Double.parseDouble(itdis.getText().toString());
                    }

                    total = rate - ((rate * dis) / 100);
                    disrate.setText("" + total);

                    Double rate1 = 0.0, dis1 = 0.0;
                    double total1 = 0.0, discountrate1 = 0.0;
                    discountrate1 = Double.parseDouble(disrate.getText().toString());
                    if (ettax.getText().toString().equals("")) {
                        rate1 = 0.0;
                    } else {
                        rate1 = Double.parseDouble(ettax.getText().toString());
                    }

                    if (disrate.getText().toString().equals("")) {
                        discountrate1 = 0.0;
                    } else {
                        discountrate1 = Double.parseDouble(disrate.getText().toString());
                    }
                    total1 = ((rate1 * discountrate1) / 100);
//               total=rate-((rate*dis)/100);
                    taxvalue.setText("" + total1);
                    finalrate.setText("" + (discountrate1 + total1));

                } else {
                    Message.message(getApplicationContext(), "Enter rate/discount value");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        ettax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (!ettax.getText().toString().equals("") || !disrate.getText().toString().equals("")) {
                    Double rate = 0.0, dis = 0.0, etrateval = 0.0, etdisval = 0.0;
                    double total = 0, discountrate = 0;
                    discountrate = Double.parseDouble(disrate.getText().toString());
                    if (ettax.getText().toString().equals("")) {
                        rate = 0.0;
                    } else {
                        rate = Double.parseDouble(ettax.getText().toString());
                    }

                    if (disrate.getText().toString().equals("")) {
                        discountrate = 0.0;
                    } else {
                        discountrate = Double.parseDouble(disrate.getText().toString());
                    }
                    total = ((rate * discountrate) / 100);
//               total=rate-((rate*dis)/100);
                    taxvalue.setText("" + total);
                    finalrate.setText("" + (discountrate + total));

                } else {

                    Message.message(getApplicationContext(), "please enter all values");
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        bsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String data = helper.getData();
                // Message.message(RegistrationActivity.this,data);

                try {
                    String iname = itemname.getText().toString();

                    String rate = itemrate.getText().toString();
                    String dis = itdis.getText().toString();
                    String disrate1 = disrate.getText().toString();
                    String ettax1 = ettax.getText().toString();
                    String taxvalue1 = taxvalue.getText().toString();
                    String finalrate1 = finalrate.getText().toString();
                    int sid = scategory.getSelectedItemPosition();
                    int uid = sunits.getSelectedItemPosition();

                    //  String itax = "0";//itemtax.getText().toString();
                    String scat = scategory.getSelectedItem().toString();
                    String sunit = sunits.getSelectedItem().toString();

                    String istock = itemstock.getText().toString();
                    String ibarcode = itembarcode.getText().toString();

                    if (iname.equals("") || rate.equals("") || dis.equals("") || disrate1.equals("") || ettax1.equals("") || taxvalue1.equals("") || finalrate1.equals("") || sid == -1 || uid == -1 || istock.equals("")) {
                        Message.message(getApplicationContext(), "Please enter all values");
                    } else {
                        db.addItem(iname, rate, dis, disrate1, ettax1, taxvalue1, finalrate1, scat, sunit, istock, ibarcode);


                        finish();
                        startActivity(getIntent());
                    }

                } catch (Exception ex) {
                    Message.message(getApplicationContext(), "Item Error" + ex.getMessage());
                }


            }


        });
        bcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent i=new Intent(getApplicationContext(),ItemListActivity.class);
                startActivity(i);*/


                Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                startActivity(i);
                finish();

            }
        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }
}