package com.example.rajlaxmi.airbill_online;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import android.os.Handler;

public class AdminDashBoardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    HeadFootSetting headFootSetting;
    DatabaseHelper db;
    private boolean success = false;
    List<NameValuePair> list=new ArrayList<NameValuePair>();
    List<NameValuePair> unitlist=new ArrayList<NameValuePair>();
    List<NameValuePair> itemlist=new ArrayList<NameValuePair>();
    List<NameValuePair> suplist=new ArrayList<NameValuePair>();
    List<NameValuePair> custlist=new ArrayList<NameValuePair>();
    List<NameValuePair> purchlist=new ArrayList<NameValuePair>();
    List<NameValuePair> pointconatctlist=new ArrayList<NameValuePair>();
    List<NameValuePair> paymnetlist=new ArrayList<NameValuePair>();
    List<NameValuePair> settinglist=new ArrayList<NameValuePair>();
    ArrayList<String> idlist=new ArrayList<String>();
    int cat_total=0,unit_total=0,item_total=0,sup_total=0,cust_total=0,invt_total=0;
    String postdata="",unitpostdata="",itempostdata="",suppostdata="",custpostdata="",prchpostdata="";
    String rescat="";
    String cat_respons="";
    String lid="",cid="",empid="";
    int total_bill=0,todays_bill=0,total_deleted_bill=0,today_deleted_bill=0;
    double total_sale=0,todays_sale=0;
    TextView tv_today_sale,tv_today_bills,tv_total_sale,tv_total_bills,tv_today_deleted_bills,tv_total_deleted_bills;
    String cat_datetime="",unit_datetime="",item_datetime="",sup_datetime="",cust_datetime="",purc_datetime="";
    String prev_cat_datetime="",prev_unit_datetime="",prev_item_datetime="",prev_sup_datetime="",prev_cust_datetime="",prev_purc_datetime="";

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dash_board);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                try {
                    InputMethodManager inputMethodManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //java code
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tv_today_sale=(TextView)findViewById(R.id.tv_today_sale);
        tv_today_bills=(TextView)findViewById(R.id.tv_today_bills);
        tv_total_sale=(TextView)findViewById(R.id.tv_total_sale);
        tv_total_bills=(TextView)findViewById(R.id.tv_total_bills);
        tv_today_deleted_bills=(TextView)findViewById(R.id.tv_today_deleted_bills);
        tv_total_deleted_bills=(TextView)findViewById(R.id.tv_total_deleted_bills);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        cid=pref.getString("cid","");
        lid=pref.getString("lid","");
        empid=pref.getString("emp_id","");
        prev_cat_datetime=pref.getString("cat_datetime","");
        prev_unit_datetime=pref.getString("unit_datetime","");
        prev_item_datetime=pref.getString("item_datetime","");
        prev_sup_datetime=pref.getString("sup_datetime","");
        prev_cust_datetime=pref.getString("cust_datetime","");
        prev_purc_datetime=pref.getString("purc_datetime","");
        prev_purc_datetime=pref.getString("upload_interval","");
        prev_purc_datetime=pref.getString("download_interval","");
        //Toast.makeText(getApplicationContext(),"upload_interval code = "+pref.getString("upload_interval","")+"\ndownload_interval ="+pref.getString("download_interval",""),Toast.LENGTH_SHORT).show();

        Toast.makeText(getApplicationContext(),"cid="+cid+"\nLid="+lid+"\nEmpid="+empid,Toast.LENGTH_LONG).show();
        Log.d("Cid=",cid);
        Log.d("Lid=",lid);
        Log.d("Empid=",empid);
        //Toast.makeText(getApplicationContext(),""+headFootSetting.getShopname(),Toast.LENGTH_SHORT).show();
        db=new DatabaseHelper(this);

        Handler handler = new Handler();
        Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                //Looper.prepare();
                try {
                    //Toast.makeText(getApplicationContext(),"upload_interval code = "+pref.getString("upload_interval","")+"\ndownload_interval ="+pref.getString("download_interval",""),Toast.LENGTH_SHORT).show();
                    getcount();
                }catch (Exception e){
                    Log.d("Error testing",""+e);
                }
                if(pref.getString("upload_interval","").equals("")){
                    handler.postDelayed(this, 7200000);
                }else {
                    handler.postDelayed(this, Long.parseLong(pref.getString("download_interval","")));
                }
            }
        };
        handler.post(runnableCode);

        try {
            total_bill = db.get_total_bills();
            total_sale = db.get_total_sale();
            total_deleted_bill=db.get_todays_deleted_bill();
            todays_bill=db.get_todays_bill();
            todays_sale=db.get_todays_sales();
            today_deleted_bill=db.get_todays_deleted_bill();

            tv_total_bills.setText("" + total_bill);
            tv_total_sale.setText("" + total_sale);
            tv_total_deleted_bills.setText(""+total_deleted_bill);
            tv_today_bills.setText(""+todays_bill);
            tv_today_sale.setText(""+todays_sale);
            tv_today_deleted_bills.setText(""+today_deleted_bill);

        }catch (Exception e){
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(),"Error"+e,Toast.LENGTH_SHORT).show();
        }
    }

    public String readResponse(HttpResponse res) {
        InputStream is = null;
        String return_text = "";
        try {
            is = res.getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            return_text = sb.toString();
        } catch (Exception e) {

        }
        return return_text;
    }

    public void getcount(){
        try {
            //Toast.makeText(getApplicationContext(),"web",Toast.LENGTH_SHORT).show();
            String HttpUrl = Config.hosturl+"cat_total.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject obj = new JSONObject(response);

                                JSONArray limitsize = obj.getJSONArray("tbl_category");
                                JSONObject linitobject = limitsize.getJSONObject(0);
                                cat_total = Integer.parseInt(linitobject.getString("count"));
                                cat_datetime = linitobject.getString("cat_datetime");
                                // Toast.makeText(getApplicationContext(),""+linitobject.getString("cat_datetime"),Toast.LENGTH_LONG).show();
                                //Toast.makeText(getApplicationContext(),"tbl_category="+cat_total,Toast.LENGTH_SHORT).show();
                                int first_cat_limit=0,second_cat_limit=Config.limit;
                                for(int i=0;i<cat_total;i++){
                                    insert_cat(first_cat_limit,second_cat_limit);
                                    first_cat_limit=second_cat_limit;
                                    second_cat_limit=second_cat_limit+Config.limit;
                                }

                                JSONArray limitsizeunit = obj.getJSONArray("tbl_unit");
                                JSONObject linitobjectunit = limitsizeunit.getJSONObject(0);
                                unit_total = Integer.parseInt(linitobjectunit.getString("count"));
                                unit_datetime = linitobjectunit.getString("unit_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_unit="+unit_total,Toast.LENGTH_SHORT).show();
                                int first_unit_limit=0,second_unit_limit=Config.limit;
                                for(int i=0;i<unit_total;i++){
                                    insert_unit(first_unit_limit,second_unit_limit);
                                    first_unit_limit=second_unit_limit;
                                    second_unit_limit=second_unit_limit+Config.limit;
                                }

                                JSONArray limitsizeitem = obj.getJSONArray("tbl_item");
                                JSONObject linitobjectitem = limitsizeitem.getJSONObject(0);
                                item_total = Integer.parseInt(linitobjectitem.getString("count"));
                                item_datetime = linitobjectitem.getString("item_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_item="+item_total,Toast.LENGTH_SHORT).show();
                                int first_item_limit=0,second_item_limit=Config.limit;
                                for(int i=0;i<item_total;i++){
                                    insert_item(first_item_limit,second_item_limit);
                                    first_item_limit=second_item_limit;
                                    second_item_limit=second_item_limit+Config.limit;
                                }

                                JSONArray limitsizesup = obj.getJSONArray("tbl_supplier");
                                JSONObject linitobjectsup = limitsizesup.getJSONObject(0);
                                sup_total = Integer.parseInt(linitobjectsup.getString("count"));
                                sup_datetime = linitobjectsup.getString("sup_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_supplier="+sup_total,Toast.LENGTH_SHORT).show();
                                int first_sup_limit=0,second_sup_limit=Config.limit;
                                for(int i=0;i<sup_total;i++){
                                    insert_sup(first_sup_limit,second_sup_limit);
                                    first_sup_limit=second_sup_limit;
                                    second_sup_limit=second_sup_limit+Config.limit;
                                }

                                JSONArray limitsizecust = obj.getJSONArray("tbl_customer");
                                JSONObject linitobjectcust = limitsizecust.getJSONObject(0);
                                cust_total = Integer.parseInt(linitobjectcust.getString("count"));
                                cust_datetime = linitobjectcust.getString("cust_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_customer="+cust_total,Toast.LENGTH_SHORT).show();
                                int first_cust_limit=0,second_cust_limit=Config.limit;
                                for(int i=0;i<cust_total;i++){
                                    insert_cust(first_cust_limit,second_cust_limit);
                                    first_cust_limit=second_cust_limit;
                                    second_cust_limit=second_cust_limit+Config.limit;
                                }

                                JSONArray limitsizepurc = obj.getJSONArray("tbl_inventory");
                                JSONObject linitobjectpurc = limitsizepurc.getJSONObject(0);
                                invt_total = Integer.parseInt(linitobjectpurc.getString("count"));
                                purc_datetime = linitobjectpurc.getString("purc_datetime");
                                //Toast.makeText(getApplicationContext(),"tbl_inventory="+invt_total,Toast.LENGTH_SHORT).show();
                                int first_invt_limit=0,second_invt_limit=Config.limit;
                                for(int i=0;i<invt_total;i++){
                                    insert_invt(first_invt_limit,second_invt_limit);
                                    first_invt_limit=second_invt_limit;
                                    second_invt_limit=second_invt_limit+Config.limit;
                                }

                                setting();
                                point_of_contact();
                                payment();
                                check_user_isactive();
                                sync_times(cid,lid,empid);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_cat(int firstcatcnt,int seconcatcnt) {
        try {
            String HttpUrl = Config.hosturl+"category_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Rsponse Cat"+obj,Toast.LENGTH_LONG).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCategory(catObject.getString("cat_name"), catObject.getString("cat_id"), catObject.getString("cat_description"), catObject.getString("cat_image"),catObject.getString("type_id"),catObject.getString("created_at"),catObject.getString("updated_at"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),catObject.getString("emp_id"));
                                    syncid.put("" + i, catObject.getString("cat_id"));
                                }
                                prev_cat_datetime=cat_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("cat_datetime",prev_cat_datetime);
                                editor.commit();
                                postdata = "" + syncid;
                                list.clear();
                                list.add(new BasicNameValuePair("cat_id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_category_api.php");
                                try {
                                    if (list.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(list));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cat_datetime", "" + prev_cat_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_unit(int firstcatcnt,int seconcatcnt) {
        try {
            String HttpUrl = Config.hosturl+"unit_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject unitObject = tutorialsArray.getJSONObject(i);
                                    //Toast.makeText(getApplicationContext(),""+unitObject.getString("Unit_name")+""+unitObject.getString("Unit_Id")+""+unitObject.getString("Unit_Taxvalue")+""+unitObject.getString("created_at")+""+unitObject.getString("is_active")+""+unitObject.getString("cid")+""+unitObject.getString("lid")+""+unitObject.getString("emp_id"),Toast.LENGTH_SHORT).show();
                                    db.SyncUnit(unitObject.getString("Unit_name"), unitObject.getString("Unit_Id"),unitObject.getString("Unit_Taxvalue"),unitObject.getString("created_at"),unitObject.getString("is_active"),unitObject.getString("cid"),unitObject.getString("lid"),unitObject.getString("emp_id"));
                                    syncid.put("" + i, unitObject.getString("Unit_Id"));
                                }
                                prev_unit_datetime=unit_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("unit_datetime",prev_unit_datetime);
                                editor.commit();

                                unitpostdata = "" + syncid;
                                unitlist.clear();
                                unitlist.add(new BasicNameValuePair("Unit_Id", unitpostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_unit_api.php");
                                try {
                                    if (unitlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(unitlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("unit_datetime", "" + prev_unit_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_item(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrl = Config.hosturl+"item_api.php";
            Log.d("URL", HttpUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject catObject = tutorialsArray.getJSONObject(i);
                                    db.SyncItem(catObject.getString("item_id"),catObject.getString("item_name"), catObject.getString("item_id"), catObject.getString("item_rate"),
                                            catObject.getString("item_dis"),catObject.getString("item_disrate"),catObject.getString("item_tax"),
                                            catObject.getString("item_taxvalue"),catObject.getString("item_final_rate"),catObject.getString("item_category"),
                                            catObject.getString("item_units"),catObject.getString("item_stock"),catObject.getString("item_barcode"),
                                            catObject.getString("item_hsncode"),catObject.getString("is_active"),catObject.getString("cid"),catObject.getString("lid"),
                                            catObject.getString("emp_id"),catObject.getString("sub_emp_id"),catObject.getString("item_code"));
                                    syncid.put("" + i, catObject.getString("item_id"));
                                }
                                prev_item_datetime=item_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("item_datetime",prev_item_datetime);
                                editor.commit();

                                itempostdata = "" + syncid;
                                itemlist.clear();
                                itemlist.add(new BasicNameValuePair("item_id", itempostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_item_api.php");
                                try {
                                    if (itemlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(itemlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("item_datetime", "" + prev_item_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_sup(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrlsup = Config.hosturl+"supllier_api.php";
            Log.d("URL", HttpUrlsup);
            StringRequest stringRequestsup = new StringRequest(Request.Method.POST, HttpUrlsup,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject supObject = tutorialsArray.getJSONObject(i);
                                    db.SyncSup(supObject.getString("sup_id"), supObject.getString("sup_name"),supObject.getString("sup_address"),supObject.getString("sup_mobile_no"),supObject.getString("sup_email_id"),supObject.getString("sup_gst_no"),supObject.getString("created_at"),supObject.getString("updated_at"),supObject.getString("is_active"),supObject.getString("cid"),supObject.getString("lid"),supObject.getString("emp_id"));
                                    syncid.put("" + i, supObject.getString("sup_id"));
                                }
                                prev_sup_datetime=sup_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("sup_datetime",prev_sup_datetime);
                                editor.commit();

                                suppostdata = "" + syncid;
                                suplist.clear();
                                suplist.add(new BasicNameValuePair("sup_id", suppostdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_supplier_api.php");
                                try {
                                    if (suplist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(suplist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("sup_datetime", "" + prev_sup_datetime);
                    return params;
                }
            };

            RequestQueue requestQueuesup = Volley.newRequestQueue(getApplicationContext());

            requestQueuesup.add(stringRequestsup);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_cust(int firstcatcnt,int seconcatcnt) {
        try {

            String HttpUrlcust = Config.hosturl+"customer_api.php";
            Log.d("URL", HttpUrlcust);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlcust,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject custObject = tutorialsArray.getJSONObject(i);
                                    db.SyncCust(custObject.getString("cust_id"), custObject.getString("cust_CompanyName"),custObject.getString("cust_name"),custObject.getString("address"),custObject.getString("mobile_no"),custObject.getString("email_id"),custObject.getString("cust_companyId_or_GST"),custObject.getString("created_at"),custObject.getString("updated_at"),custObject.getString("is_active"),custObject.getString("cid"),custObject.getString("lid"),custObject.getString("emp_id"));
                                    syncid.put("" + i, custObject.getString("cust_id"));
                                }
                                prev_cust_datetime=cust_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("cust_datetime",prev_cust_datetime);
                                editor.commit();

                                postdata = "" + syncid;
                                list.clear();
                                list.add(new BasicNameValuePair("cust_id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_customer_api.php");
                                try {
                                    if (list.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(list));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("cust_datetime", "" + prev_cust_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert_invt(int firstcatcnt,int seconcatcnt) {;
        try {

            String HttpUrlinvt = Config.hosturl+"purchase_api.php";
            Log.d("URL", HttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject invtObject = tutorialsArray.getJSONObject(i);
                                    db.Syncinvetory(invtObject.getString("inventoryid"),invtObject.getString("inventorysupid"),invtObject.getString("inventoryitemid"),invtObject.getString("inventoryitemquantity"),invtObject.getString("inventorystatus"),invtObject.getString("created_at"),invtObject.getString("updated_at"),invtObject.getString("isactive"),invtObject.getString("cid"),invtObject.getString("lid"),invtObject.getString("emp_id"));
                                    syncid.put("" + i, invtObject.getString("inventoryid"));
                                }
                                prev_purc_datetime=purc_datetime;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();
                                editor.putString("purc_datetime",prev_purc_datetime);
                                editor.commit();
                                postdata = "" + syncid;
                                purchlist.clear();
                                purchlist.add(new BasicNameValuePair("inventoryid", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_purchase_api.php");
                                try {
                                    if (purchlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(purchlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

//                                    progressDialog.dismiss();

                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("firstlimt", "" + firstcatcnt);
                    params.put("secondlimit", "" + seconcatcnt);
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    params.put("purc_datetime", "" + prev_purc_datetime);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void point_of_contact(){
        try {
            String HttpUrlpointcontact = Config.hosturl+"point_of_contact_api.php";
            Log.d("URL", HttpUrlpointcontact);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointcontact,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject point_contactsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pointObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpointcontact(pointObject.getString("id"),pointObject.getString("point_of_contact"),pointObject.getString("is_active"),pointObject.getString("cid"),pointObject.getString("lid"),pointObject.getString("emp_id"),pointObject.getString("sync_flag"),pointObject.getString("created_at"),pointObject.getString("updated_at"));
                                    point_contactsyncid.put("" + i, pointObject.getString("id"));
                                }
                                postdata = "" + point_contactsyncid;
                                pointconatctlist.clear();
                                pointconatctlist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_point_contact_api.php");
                                try {
                                    if (pointconatctlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(pointconatctlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void payment(){

        //Toast.makeText(getApplicationContext(),"This is payment method",Toast.LENGTH_SHORT).show();

        try {
            String PaymnetHttpUrlinvt = Config.hosturl+"payment_type.php";
            Log.d("URL", PaymnetHttpUrlinvt);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, PaymnetHttpUrlinvt,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject syncid = new JSONObject();
                                //Toast.makeText(getApplicationContext(),"Response="+response,Toast.LENGTH_SHORT).show();
                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject pyamnetObject = tutorialsArray.getJSONObject(i);
                                    db.Syncpayment(pyamnetObject.getString("id"),pyamnetObject.getString("payment_type"),pyamnetObject.getString("is_active"),pyamnetObject.getString("cid"),pyamnetObject.getString("lid"),pyamnetObject.getString("emp_id"),pyamnetObject.getString("sync_flag"),pyamnetObject.getString("created_at"),pyamnetObject.getString("updated_at"));
                                    syncid.put("" + i, pyamnetObject.getString("id"));
                                    //Toast.makeText(getApplicationContext(),pyamnetObject.getString("payment_type"),Toast.LENGTH_SHORT).show();
                                }
                                postdata = "" + syncid;
                                paymnetlist.clear();
                                paymnetlist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_payment_api.php");
                                try {
                                    if (paymnetlist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(paymnetlist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setting(){
        try {
            String HttpUrlpointsetting = Config.hosturl+"setting_api.php";
            Log.d("URL", HttpUrlpointsetting);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrlpointsetting,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject obj = new JSONObject(response);
                                JSONObject settingsyncid = new JSONObject();

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject settingObject = tutorialsArray.getJSONObject(i);
                                    SyncSetting(settingObject.getString("id"),settingObject.getString("h1"),settingObject.getString("h2"),settingObject.getString("h3"),settingObject.getString("h4"),settingObject.getString("h5"), settingObject.getString("f1"),settingObject.getString("f2"),settingObject.getString("f3"), settingObject.getString("f4"),settingObject.getString("f5"),settingObject.getString("created_at"), settingObject.getString("updated_at"), settingObject.getString("is_active"),settingObject.getString("cid"), settingObject.getString("lid"),settingObject.getString("emp_id"),settingObject.getString("page_size"), settingObject.getString("gst_setting"),settingObject.getString("bill_printing"),settingObject.getString("multiple_print"), settingObject.getString("reset_bill"),settingObject.getString("sync_flag"));
                                    settingsyncid.put("" + i, settingObject.getString("id"));
                                }
                                postdata = "" + settingsyncid;
                                settinglist.clear();
                                settinglist.add(new BasicNameValuePair("id", postdata));
                                HttpClient httpClient = new DefaultHttpClient();
                                HttpPost httpPost;
                                String s = "";
                                httpPost = new HttpPost(Config.hosturl+"flag_update_setting_api.php");
                                try {
                                    if (settinglist.isEmpty()) {
                                        Toast.makeText(getApplicationContext(), "No Data To Sync!!!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        httpPost.setEntity(new UrlEncodedFormEntity(settinglist));
                                        HttpResponse httpResponse = httpClient.execute(httpPost);
                                        s = readResponse(httpResponse);
                                        //Toast.makeText(getApplicationContext(), "Response=" + s, Toast.LENGTH_LONG).show();
                                        rescat = s;
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Error1=" + ex, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + lid);
                    params.put("cid", "" + cid);
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            //super.onBackPressed();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory( Intent.CATEGORY_HOME );
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_dash_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

    }

    @Override
    protected void onPause() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }


        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
                Message.message(getApplicationContext(),"Please connect to printer from main settings.");
            }
        }

    }

    public void SyncSetting(String id,String h1,String h2,String h3,String h4,String h5,String f1,String f2,String f3,String f4,String f5,String created_at,String updated_at,String is_active,String cid,String lid,String emp_id,String page_size,String gst_setting,String bill_printing,String multiple_print,String reset_bill,String sync_flag){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();
        if(gst_setting.equals("Yes")){
            editor.putString("gst_option","GST Enable");
        }else{
            editor.putString("gst_option","GST Disable");
        }
        editor.putString("reset_bill",reset_bill);
        editor.commit();
    }

    public void check_user_isactive(){
        try {
            String HttpUrluseractive = Config.hosturl+"check_user_isactive.php";
            Log.d("URL", HttpUrluseractive);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrluseractive,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                                final SharedPreferences.Editor editor = pref.edit();

                                JSONObject obj = new JSONObject(response);

                                JSONArray tutorialsArray = obj.getJSONArray("data");
                                for (int i = 0; i < tutorialsArray.length(); i++) {
                                    JSONObject isactivegObject = tutorialsArray.getJSONObject(i);
                                    //Toast.makeText(getApplicationContext(),"ISactive in admin="+isactivegObject.getString("is_active"),Toast.LENGTH_SHORT).show();
                                    editor.putString("is_active",isactivegObject.getString("is_active"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("empid", "" + empid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void sync_times(String tcid, String tlid,String tempid){
        try {
            final SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            final SharedPreferences.Editor editor = pref.edit();
            String HttpUrltime = Config.hosturl+"sync_time_api.php";
            Log.d("URL", HttpUrltime);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrltime,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                //getting the whole json object from the response
                                JSONObject objtime = new JSONObject(response);

                                JSONArray tutorialsArraytime = objtime.getJSONArray("data");
                                for (int i = 0; i < tutorialsArraytime.length(); i++) {
                                    JSONObject timeObject = tutorialsArraytime.getJSONObject(i);
                                    editor.putString("upload_interval",timeObject.getString("upload_interval"));
                                    editor.putString("download_interval",timeObject.getString("download_interval"));
                                    editor.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Error2=" + e, Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("lid", "" + tlid);
                    params.put("cid", "" + tcid);
                    params.put("empid", "" + tempid);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}