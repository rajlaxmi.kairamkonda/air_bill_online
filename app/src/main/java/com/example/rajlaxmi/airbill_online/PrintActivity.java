package com.example.rajlaxmi.airbill_online;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.CardReader;
import com.aem.api.IAemCardScanner;
import com.aem.api.IAemScrybe;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PrintActivity extends AppCompatActivity implements IAemCardScanner, IAemScrybe {
    AEMScrybeDevice m_AemScrybeDevice;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    String creditData;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int glbPrinterWidth;
    EditText edit_mesg;
    Button btn_connect,billbtn,billonceclick;
    String m_message;
    Cursor res;
    private static final String MY_PREFS_NAME ="myclass" ;
    String value;
    ArrayList<String> printerList;
    char[] batteryStatusCommand=new char[]{0x1B,0x7E,0x42,0x50,0x7C,0x47,0x45,0x54,0x7C,0x42,0x41,0x54,0x5F,0x53,0x54,0x5E};
    DatabaseHelper db;
    List<String> items=new ArrayList<>();
    ArrayAdapter<String> adapter; String printername;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        printerList = new ArrayList<String>();
        creditData = new String();
        m_AemScrybeDevice = new AEMScrybeDevice(this);
//         btn_connect=findViewById(R.id.btn_connect);
        billbtn=findViewById(R.id.bill);
        billonceclick=findViewById(R.id.btnoneclick);
        billonceclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try
                {
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
//                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                    Toast.makeText(PrintActivity.this,"Connected with " + value,Toast.LENGTH_SHORT ).show();
                    String data=new String(batteryStatusCommand);
                    m_AemPrinter.print(data);

                    onPrintBill(view);
                    //  m_cardReader.readMSR();
                }
                catch (IOException e)
                {
                    if (e.getMessage().contains("Service discovery failed"))
                    {
                        Toast.makeText(PrintActivity.this,"Not Connected\n"+ value + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
                    }
                    else if (e.getMessage().contains("Device or resource busy"))
                    {
                        Toast.makeText(PrintActivity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
                    }
                    else
                    {
                        Toast.makeText(PrintActivity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
                    }
                }


            }
        });
        //registerForContextMenu(btn_connect);
        registerForContextMenu(billbtn);
        db=new DatabaseHelper(this);
        res= db.getAllItems();
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);

//        String printer = intent.getStringExtra("Printername");
//        sharedPreferences = getSharedPreferences(MY_PREFS_NAME,Context.MODE_PRIVATE);
//        printername =sharedPreferences.getString("Printername", "");


        //Intent intent = getIntent();
        SharedPreferences sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        value= sharedPreferences.getString("value","");
        Toast.makeText(getApplicationContext(),"printname"+value,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
//        for (int i = 0; i < printerList.size(); i++)
//        {
//            menu.add(0, v.getId(), 0, printerList.get(i));
//        }
        menu.add(0, v.getId(), 0, value);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        // super.onContextItemSelected(item);
        String printerName = item.getTitle().toString();
        try
        {
            m_AemScrybeDevice.connectToPrinter(value);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(PrintActivity.this,"Connected with " + printerName,Toast.LENGTH_SHORT ).show();
            String data=new String(batteryStatusCommand);
            m_AemPrinter.print(data);


            //  m_cardReader.readMSR();
        }
        catch (IOException e)
        {
            if (e.getMessage().contains("Service discovery failed"))
            {
                Toast.makeText(PrintActivity.this,"Not Connected\n"+ printerName + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
            }
            else if (e.getMessage().contains("Device or resource busy"))
            {
                Toast.makeText(PrintActivity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Toast.makeText(PrintActivity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
            }
        }
        return true;
    }

    @Override
    public void onScanMSR(String s, CardReader.CARD_TRACK card_track) {

    }

    @Override
    public void onScanDLCard(String s) {

    }

    @Override
    public void onScanRCCard(String s) {

    }

    @Override
    public void onScanRFD(String s) {

    }

    @Override
    public void onScanPacket(String s) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> arrayList) {

    }

//    @Override
//    protected void onDestroy() {
//        if (m_AemScrybeDevice != null)
//        {
//            try
//            {
//                m_AemScrybeDevice.disConnectPrinter();
//            }
//            catch (IOException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        super.onDestroy();
//    }

    public void onShowPairedPrinters(View v)
    {
        String p = m_AemScrybeDevice.pairPrinter("BTprinter0314");
        // showAlert(p);
        printerList = m_AemScrybeDevice.getPairedPrinters();
        if (printerList.size() > 0)
            openContextMenu(v);
//        try
//        {
//            onPrintBill(v);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //   showAlert("No Paired Printers found");
    }


    public void onPrintBill(View v) throws IOException {
        m_message = "hello";
//        m_AemScrybeDevice.connectToPrinter(value);
//        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();

//        m_AemScrybeDevice.pairPrinter(value);
//        m_AemScrybeDevice.connectToPrinter(value);
//        m_cardReader = m_AemScrybeDevice.getCardReader(this);
//        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();

        if (m_AemPrinter == null) {

            Toast.makeText(PrintActivity.this, "Printer not connected Please select", Toast.LENGTH_SHORT).show();

            printerList = m_AemScrybeDevice.getPairedPrinters();
            if (printerList.size() > 0)
                openContextMenu(v);

            //return;
        }
//        m_AemPrinter.print(m_message);
//        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
//        m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
//        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//        m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
//        m_AemPrinter.setFontType(AEMPrinter.FONT_002);
//        m_AemPrinter.setLineFeed(4);
//        m_AemPrinter.setCarriageReturn();
//        m_AemPrinter.setCarriageReturn();
//        m_AemPrinter.setCarriageReturn();
//        m_AemPrinter.setCarriageReturn();
//        m_AemPrinter.setCarriageReturn();


        String data = "TWO INCH PRINTER: TEST PRINT \n";
        String d = "_________________________________\n";
        try {
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

            m_AemPrinter.print(data);
            m_AemPrinter.print(d);

            data = "No.|   Item Name   |   RATE(Rs)  \n";

            m_AemPrinter.print(data);
            m_AemPrinter.print(d);
            Gson gson = new Gson();
            String data1 = gson.toJson(items);


            if(res.getCount() == 0) {
                // show message
                Message.message(getApplicationContext(),"Nothing found");
                return;
            }

            StringBuffer buffer = new StringBuffer();
            while (res.moveToNext())
            {

                buffer.append(res.getString(0)+"|   "+ res.getString(1)+"     |     "+res.getString(2)+"\n");
                Log.i("data:-", res.getString(8)+"  "+res.getString(9));

            }
            // data= String.valueOf(buffer);

            // Show all data
            //Message.message(getApplicationContext(),buffer.toString());
            //data= String.valueOf(items);
            /*data = "13|ColgateGel |35.00|02|70.00\n" +
                    "29|Pears Soap |25.00|01|25.00\n" +
                    "88|Lux Shower |46.00|01|46.00\n" +
                    "15|Dabur Honey|65.00|01|65.00\n" +
                    "52|Dairy Milk |20.00|10|200.00\n" +
                    "128|Maggie TS |36.00|04|144.00\n" +
                    "_______________________________\n";*/

            m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
            m_AemPrinter.print(String.valueOf(buffer));
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            data = "   TOTAL AMOUNT (Rs.)   550.00\n";
            m_AemPrinter.print(data);
            m_AemPrinter.setFontType(AEMPrinter.FONT_002);
            m_AemPrinter.print(d);
            data = "   Thank you! \n";
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
            m_AemPrinter.print(data);
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();


        } catch (Exception ex) {
            Toast.makeText(PrintActivity.this, "Error:-"+ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
