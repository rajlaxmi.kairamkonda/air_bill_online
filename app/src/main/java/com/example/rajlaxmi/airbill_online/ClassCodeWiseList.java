package com.example.rajlaxmi.airbill_online;

public class ClassCodeWiseList {

    public String sr_no,item_name,qty,amount,tax,rate;

    public ClassCodeWiseList(String sr_no, String item_name,String rate, String qty,String amount, String discount)
    {
        this.sr_no=sr_no;
        this.item_name = item_name;
        this.qty = qty;
        this.amount = amount;
        this.tax = tax;
        this.rate=rate;
    }

    // public String getImg() {
    // return img;
    // }
    public String getSr_no() {
        return sr_no;
    }
    public String getItem_name() {
        return item_name;
    }
    public String getQty() {
        return qty;
    }
    public String getAmount() {
        return amount;
    }
    public String getTax() {
        return tax;
    }
    public String getRate() {
        return rate;
    }

}
