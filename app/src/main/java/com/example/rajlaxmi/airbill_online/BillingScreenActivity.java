package com.example.rajlaxmi.airbill_online;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;
import com.aem.api.CardReader;
import com.aem.api.IAemCardScanner;
import com.aem.api.IAemScrybe;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import static android.util.Log.i;
import android.bluetooth.BluetoothDevice;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cie.btp.CieBluetoothPrinter;
import com.cie.btp.DebugLog;
import com.cie.btp.PrinterWidth;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_DEVICE_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_CONNECTING;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_LISTEN;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_CONN_STATE_NONE;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MESSAGES;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NAME;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOTIFICATION_MSG;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_CONNECTED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_NOT_FOUND;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_SAVED;
import static com.cie.btp.BtpConsts.RECEIPT_PRINTER_STATUS;

public class BillingScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , IAemCardScanner, IAemScrybe {
    private boolean success = false; // boolean
    private static final String MY_PREFS_NAME ="myclass" ;

    private  List<String> checkArray = new ArrayList<>();

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    AEMScrybeDevice m_AemScrybeDevice;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    int glbPrinterWidth;
    EditText edit_mesg;
    int pos_i=0;
    double total_bill_amount=0.0;
    int cnt=1;
    ArrayList itemArrayList;
    int bno=1;  int counter = 1,showdiadlogcounter=1;
    Button btn_connect;
    String m_message;
    Spinner sunits, scategory, sptax,stransaction,sppoint_of_contact,sp_payment;
    AutoCompleteTextView at_customer_name,tid,tdetails,todetails;
    int countday=0;
    int countdaywih1=0;
    String encoding = "US-ASCII";
    MyAppAdapter myAppAdapter;
    Cursor res1,res2, ids,header;     int k=1;
    List<String> headerfooter=new ArrayList<String >();
    SharedPreferences sharedpreferences;
    private static int nFontSize, nTextAlign, nScaleTimesWidth,
            nScaleTimesHeight, nFontStyle, nLineHeight = 32, nRightSpace;
    char[] batteryStatusCommand=new char[]{0x1B,0x7E,0x42,0x50,0x7C,0x47,0x45,0x54,0x7C,0x42,0x41,0x54,0x5F,0x53,0x54,0x5E};
    DatabaseHelper db;
    List<String> items=new ArrayList<>();
    ArrayAdapter<String> adapter;
    List<String> Categorylist;
    List<String> CategoryIdlist;
    List<String> itemlist=new ArrayList<String>();
    List<String> printerList=new ArrayList<>();
    List<String> printlist=new ArrayList<String>();
    List<String> tempbilling=new ArrayList<String>();
    int r=1;
    ListView lv;   List<String> disratelist=new ArrayList<String>(),dis_list=new ArrayList<String>();
    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    Double totalrate1=0.0,totalamount=0.0,newqty=0.0;
    String str_item="";
    List<String> basicratelist=new ArrayList<String>();
    List<String> taxlist=new ArrayList<String>();
    List<String> taxlistupdated=new ArrayList<String>();
    List<String> samevalulist =new ArrayList<String>();
    String price="";
    Double str_qty=0.0;
    String rate="";
    GridView catgridvew,itemgridview1;
    TextView tv_total_amt,tvbill;
    public double cost=0;
    TextView in;
    Button btn_print;
    String itemname;
    Double qty_item=0.0;
    List<String> ITEM_LIST,Category_LIST,price_list=new ArrayList<String>();;
    ArrayAdapter<String> arrayadapter,adapter1,couteradapter;
    String GetItem;
    Button btn_customer,btn_discount,btn_setting,btn_all_clr;
    HeadFootSetting headFootSetting;
    int index=0;
    private String[] myImageNameList = new String[]{"Benz", "Bike",
            "Car","Carrera"
            ,"Ferrari","Harly",
            "Lamborghini","Silver"};
    String print_option="";
    String gst_option="";
    String selectprinter_option="";
    String billwithprint="";
    String multipleprint="";
    String resetbill="";
    String value="";
    Dialog dialog,dialog1;
    ArrayList<String> NameList,IdList;
    int cust_flag=0;
    String str_set_cust_name="",str_set_id_cust="",str_set_payment="";
    private RadioGroup rg_bill_screen;
    private RadioButton rb_bill_screen;
    String Name="";
    int name_index=0;
    private Integer[] mThumbIds = {
            R.drawable.wafers,
            R.drawable.idli,
            R.drawable.aloo_ticki,
            R.drawable.cholebhature,
            R.drawable.dokhla,
            R.drawable.kandapoha,
            R.drawable.meduvada,
            R.drawable.puribhaji,
            R.drawable.samosa,
            R.drawable.upma,
    };
    Context context;
    BluetoothAdapter localDevice;
    BluetoothDevice remoteDevice;
    BluetoothSocket bluetoothSocket;
    private BluetoothSocket accBTSocket;
    final String VERSION = "1.0";
    Object mutex;
    private static final UUID MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ArrayList<BluetoothDevice> remoteDeviceList = new ArrayList();
    IAemScrybe scrybeDeviceInterface;
    private boolean deviceFound = false;
    private boolean pairDevice = false;
    private boolean scanFlag = true;
    //Dyno printing
    private TextView tvStatus;
    private Button btnPrint;
    private CheckBox cbFindBlackMark;
    private boolean bFindBlackMark;
    ProgressDialog pdWorkInProgress;
    private static final int BARCODE_WIDTH = 384;
    private static final int BARCODE_HEIGHT = 100;
    private static final int QRCODE_WIDTH = 100;
    static int kl=1,kb=1;;
    public CieBluetoothPrinter mPrinter = CieBluetoothPrinter.INSTANCE;
    private int imageAlignment = 1;
    Button bt_ok, bt_cancel1;
    TextView tv_title, tv_errortext;
    String lid="",cid="",empid="";
    SimpleDateFormat dateFormat;
    Date date;
    String payment_mode_id="";
    String order_details="",payment_deatils="",empcode="",emp_code="";
    LinearLayout ll_all_items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        expandableListView = findViewById(R.id.expandableListView);
        prepareMenuData();
        populateExpandableList();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        itemArrayList = new ArrayList<ClassCodeWiseList>();

        //Java code
        headFootSetting=new HeadFootSetting(getApplicationContext());
        dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        date = new Date();

        ModelSale.arr_item_rate.clear();
        ModelSale.arry_item_name.clear();
        ModelSale.arr_item_qty.clear();
        ModelSale.arr_item_price.clear();
        ModelSale.arr_item_basicrate.clear();
        ModelSale.arr_item_dis_rate.clear();
        ModelSale.arr_item_dis.clear();
        ModelSale.arr_item_tax.clear();
        ModelSale.array_item_amt.clear(); checkArray.clear();

        itemArrayList.clear();
        payment_deatils="";
        order_details="";
        str_set_cust_name="";
        str_set_payment="";
        payment_mode_id="";
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        print_option=pref.getString("print_option","");
        gst_option=pref.getString("gst_option","");
        selectprinter_option=pref.getString("selectprinter_option","");
        value=pref.getString("2inchprintername","");
        billwithprint=pref.getString("bill_with_print","");
        multipleprint=pref.getString("multiple_printing","");
        resetbill=pref.getString("reset_bill","");
        cid=pref.getString("cid","");
        lid=pref.getString("lid","");
        empid=pref.getString("emp_id","");
        empcode=pref.getString("employee_code","");

        tvbill=(TextView)findViewById(R.id.billno);
        btn_all_clr=(Button)findViewById(R.id.btn_clr_all);
        ll_all_items=(LinearLayout)findViewById(R.id.ll_all_items);

        tv_total_amt=(TextView)findViewById(R.id.tv_total_amt);
        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        //   editor = sharedPreferences.edit();
        db=new DatabaseHelper(this);

        ids= db.getAllbillID();
        autoinnc();
        emp_code= empcode+tvbill.getText().toString();
        if(selectprinter_option.equals("Airbill")) {
            btn_print=(Button)findViewById(R.id.btn_print1);
            m_AemScrybeDevice = new AEMScrybeDevice(this);

        }
        //Dyno
        else if(selectprinter_option.equals("Dyno")) {

            btnPrint = (Button) findViewById(R.id.btn_print1);
            pdWorkInProgress = new ProgressDialog(this);
            pdWorkInProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            tvStatus = findViewById(R.id.status_msg);

            BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mAdapter == null) {
                Toast.makeText(this, R.string.bt_not_supported, Toast.LENGTH_SHORT).show();
                finish();
            }

            try {
                mPrinter.initService(BillingScreenActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    boolean exe = false;
                    try {
                        if(selectprinter_option.equals("Dyno")&&print_option.equals("2 inch")&&billwithprint.equals("No")){
                            if(billwithprint.equals("No")){


                                taxlistupdated.clear();
                                for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)), Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))), Double.valueOf(tv_total_amt.getText().toString()),cid,lid,empid,""+tvbill.getText().toString(),payment_mode_id,payment_deatils,order_details,gst_option,emp_code,ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                }


                                ModelSale.arr_item_rate.clear();
                                ModelSale.arry_item_name.clear();
                                ModelSale.arr_item_qty.clear();
                                ModelSale.arr_item_price.clear();
                                ModelSale.arr_item_basicrate.clear();
                                ModelSale.arr_item_dis_rate.clear();
                                ModelSale.arr_item_dis.clear();
                                ModelSale.arr_item_tax.clear();
                                ModelSale.array_item_amt.clear(); checkArray.clear();

                                payment_deatils="";
                                order_details="";
                                str_set_cust_name="";
                                str_set_payment="";
                                payment_mode_id="";

                                cost = 0;
                                tv_total_amt.setText("0");
                                cnt = 1;
                                itemArrayList.clear();
                                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                                ids = db.getAllbillID();
                                autoinnc();
                                Message.message(getApplicationContext(), "Bill saved");
                            }
                        }
                        else
                        {
                            mPrinter.connectToPrinter();
                        }
                    }catch(Exception ex)
                    {
                        Message.message(getApplicationContext(),""+ex.getMessage());
                    }
                }
            });
        }


        String bid=tvbill.getText().toString();
        Categorylist  =db.getAllCategory();
        CategoryIdlist  =db.getAllCategoryId();

        catgridvew = (GridView)findViewById(R.id.gd_category);
        itemgridview1 = (GridView)findViewById(R.id.gd_items);

        Category_LIST = new ArrayList<String>((Categorylist));
        ITEM_LIST = new ArrayList<String>();
        price_list = new ArrayList<String>();

        catgridvew.setAdapter(new ArrayAdapter<String>(
                this,android.R.layout.simple_list_item_1, Category_LIST){
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position,convertView,parent);
                TextView tv = (TextView) view;
                RelativeLayout.LayoutParams lp =  new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                tv.setLayoutParams(lp);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)tv.getLayoutParams();
                params.width = getPixelsFromDPs(BillingScreenActivity.this,90);
                params.height = getPixelsFromDPs(BillingScreenActivity.this,50);
                tv.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                tv.setGravity(Gravity.CENTER);
                tv.setGravity(Gravity.CENTER);
                tv.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC), Typeface.NORMAL);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);

                String getname=Category_LIST.get(position);
                int len=getname.length();
                if(len>10)
                {
                    getname= getname.substring(0,10);
                    getname=getname+"...";
                }
                tv.setText(getname);
                tv.setBackgroundColor(Color.parseColor("#63e6e0"));
                tv.setTypeface(tv.getTypeface(), Typeface.BOLD);

                tv.setBackground(getResources().getDrawable(R.drawable.thubnail_back));
                return tv;
            }
        });

        //java code
        btn_customer=(Button)findViewById(R.id.btn_customer);
        btn_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomMessageBox();
            }
        });
        btn_discount=(Button)findViewById(R.id.btn_discount);
        btn_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(),DiscountActivity.class);
                startActivity(i);
                finish();
            }
        });
        btn_setting=(Button)findViewById(R.id.btn_setting);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        catgridvew.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                AlertDialog.Builder alert = new AlertDialog.Builder(
                        BillingScreenActivity.this);
                String catname=Category_LIST.get(i);
                alert.setMessage(""+catname);
                alert.show();
                return true;
            }
        });
        itemgridview1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                AlertDialog.Builder alert = new AlertDialog.Builder(
                        BillingScreenActivity.this);
                String itemname=ITEM_LIST.get(i);
                alert.setMessage(""+itemname);
                alert.show();
                return true;
            }
        });
        catgridvew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemlist.clear();
                price_list.clear();
                basicratelist.clear();
                taxlist.clear();
                ITEM_LIST=new ArrayList<String>(itemlist);
                price_list=new ArrayList<String>(price_list);

                String selectedItem= adapterView.getItemAtPosition(i).toString();
                String name=Categorylist.get(i);
                String catid=CategoryIdlist.get(i);
                Cursor c =db.getAllCategorywiseItem(catid);
                try {
                    if (c.getCount() == 0) {
                        Message.message(getApplicationContext(), "Item not exists");
                    } else {
                        while (c.moveToNext()) {
                            if (gst_option.equals("GST Disable")) {


                                itemlist.add(c.getString(1));
                                price_list.add(c.getString(7));
                                taxlist.add(c.getString(5));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                dis_list.add(c.getString(3));

                            } else if (gst_option.equals("GST Enable")) {
                                itemlist.add(c.getString(1));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                taxlist.add(c.getString(5));
                                price_list.add(c.getString(7));
                                dis_list.add(c.getString(3));
                            }
                        }
                    }
                }catch
                (Exception ex)
                {
                    Message.message(getApplicationContext(),"Error "+ex.getMessage());
                }
                ITEM_LIST=new ArrayList<String>(itemlist);
                itemgridview1.setAdapter(new ArrayAdapter<String>(BillingScreenActivity.this, android.R.layout.simple_list_item_1, ITEM_LIST)
                {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public View getView(int position1, View convertView1, ViewGroup parent1) {

                        View view1 = super.getView(position1,convertView1,parent1);
                        TextView tv1 = (TextView) view1;
                        RelativeLayout.LayoutParams lp1 =  new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                        );
                        tv1.setLayoutParams(lp1);
                        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)tv1.getLayoutParams();
                        params1.width = getPixelsFromDPs(BillingScreenActivity.this,110);
                        tv1.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                        tv1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);

                        String itemname=ITEM_LIST.get(position1);
                        if(itemname.length()>10)
                        {
                            itemname=itemname.substring(0, 10);
                            itemname=itemname+"...";
                        }
                        tv1.setText(itemname);
                        ShapeDrawable sd = new ShapeDrawable();
                        sd.setShape(new RectShape());
                        sd.getPaint().setColor(Color.parseColor("#00e6b0"));
                        sd.getPaint().setStrokeWidth(10f);
                        sd.getPaint().setStyle(Paint.Style.STROKE);
                        tv1.setBackground(sd);
                        tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                        return tv1;
                    }
                });
            }
        });

        itemgridview1.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        try {
                            if (gst_option.equals("GST Disable")) {
                                if (gst_option.equals("GST Disable")) {
                                    if (ModelSale.arry_item_name.contains(itemlist.get(i))) {
                                        String itemname = itemlist.get(i);
                                        int index1 = ModelSale.arry_item_name.indexOf(itemlist.get(i));

                                        Double value = ModelSale.arr_item_qty.get(index1);
                                        ModelSale.arr_item_qty.set(index1, value + 1.0);
                                        totalrate1 = Double.valueOf(ModelSale.arr_item_rate.get(index1)) * Double.valueOf(ModelSale.arr_item_qty.get(index1));
                                        totalamount = totalrate1 + totalamount;
                                        ModelSale.arr_item_price.set(index1, totalrate1);
                                        itemArrayList.set(index1, new ClassCodeWiseList("" + (index1 + 1), "" + ModelSale.arry_item_name.get(index1), "" + ModelSale.arr_item_qty.set(index1, value + 1.0), "" + Double.valueOf(ModelSale.arr_item_rate.get(index1)), "" + String.format("%.2f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                    }
                                    else {

                                        int j = 0;
                                        newqty = 1.0;

                                        totalrate1 = 1.0 * Double.parseDouble(price_list.get(i));
                                        totalamount = totalamount + totalrate1;
                                        ModelSale.arry_item_name.add(itemlist.get(i));
                                        ModelSale.arr_item_qty.add(newqty);
                                        ModelSale.arr_item_rate.add(price_list.get(i));
                                        ModelSale.arr_item_price.add(totalrate1);
                                        ModelSale.array_item_amt.add(totalamount);
                                        ModelSale.arr_item_tax.add(taxlist.get(i));
                                        ModelSale.arr_item_basicrate.add(basicratelist.get(i));
                                        ModelSale.arr_item_dis.add(dis_list.get(i));
                                        ModelSale.arr_item_dis_rate.add(disratelist.get(i));
                                        itemArrayList.add(new ClassCodeWiseList("" + cnt, "" + itemlist.get(i), "" + newqty, "" + price_list.get(i), "" + String.format("%.2f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                        cnt++;
                                    }

                                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                                    cost = cost + Double.parseDouble(price_list.get(i));
//                                tv_total_amt.setText("" + String.format("%.2f",cost));
                                    tv_total_amt.setText(String.format("%.2f",cost));
                                    itemname = itemlist.get(i);
                                    printlist.add(itemlist.get(i) + " " + price_list.get(i) + " " + i + "\n");
                                    int listSize = printlist.size();
                                    for (int j = 0; j < listSize; j++) {
                                        i("Member name: ", printlist.get(j));
                                    }
                                    bno = 1;
                                    bno++;
                                    i("data=:", str_item + "  " + "  " + str_qty + "  " + rate + "  " + "  " + totalrate1 + "  " + totalamount);
                                }

                            }
                            else if(gst_option.equals("GST Enable")){
                                if (ModelSale.arry_item_name.contains(itemlist.get(i))) {
                                    String itemname=itemlist.get(i);
                                    int index1 =ModelSale.arry_item_name.indexOf(itemlist.get(i));
                                    Double value=ModelSale.arr_item_qty.get(index1);
                                    ModelSale.arr_item_qty.set(index1, value + 1.0);
                                    totalrate1 = Double.valueOf(ModelSale.arr_item_rate.get(index1)) * Double.valueOf(ModelSale.arr_item_qty.get(index1));
                                    totalamount = totalrate1 + totalamount;
                                    ModelSale.arr_item_price.set(index1, totalrate1);

//                                    ModelSale.array_item_amt.add(Double.valueOf(String.format("%.2f",totalamount)));
                                    itemArrayList.set(index1,new ClassCodeWiseList(""+(index1+1) , "" + ModelSale.arry_item_name.get(index1), "" + ModelSale.arr_item_qty.set(index1, value + 1.0), "" +Double.valueOf(ModelSale.arr_item_basicrate.get(index1)), "" + String.format("%.2f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                } else {
                                    int j=0;
                                    newqty = 1.0;
                                    totalrate1 = 1.0 * Double.parseDouble(price_list.get(i));
                                    totalamount = totalamount + totalrate1;
                                    ModelSale.arry_item_name.add(itemlist.get(i));
                                    ModelSale.arr_item_qty.add(newqty);
                                    ModelSale.arr_item_rate.add(price_list.get(i));
                                    ModelSale.arr_item_basicrate.add(basicratelist.get(i));
                                    ModelSale.arr_item_tax.add(taxlist.get(i));
                                    ModelSale.arr_item_price.add(Double.valueOf(String.format("%.2f",totalrate1)));
                                    ModelSale.array_item_amt.add(Double.valueOf(String.format("%.2f",totalamount)));
                                    ModelSale.arr_item_dis.add(dis_list.get(i));
                                    ModelSale.arr_item_dis_rate.add(disratelist.get(i));
                                    itemArrayList.add(new ClassCodeWiseList("" + cnt, "" +itemlist.get(i), "" +newqty, "" + basicratelist.get(i), "" + String.format("%.2f",Double.parseDouble(String.valueOf(totalrate1))), ""));
                                    cnt++;
                                }

                                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                                cost = cost + Double.parseDouble(price_list.get(i));
//                                tv_total_amt.setText("" + String.format("%.2f",cost));
                                tv_total_amt.setText(""+String.format("%.2f",cost));
                                itemname = itemlist.get(i);
                                printlist.add(itemlist.get(i) + " " + basicratelist.get(i) + " " + i + "\n");
                                int listSize = printlist.size();
                                for (int j = 0; j < listSize; j++) {
                                    i("Member name: ", printlist.get(j));
                                }
                                bno = 1;
                                bno++;
                                i("data=:", str_item + "  " + "  " + str_qty + "  " + rate + "  " + "  " + totalrate1 + "  " + totalamount);
                            }
                        }
                        catch (Exception ex)
                        {
                            Message.message(getApplicationContext(),""+ex.getMessage());
                        }
                    }
                });

        tv_total_amt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog();
            }
        });

        btn_all_clr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();

                tv_total_amt.setText("0.0");
                itemArrayList.clear();
                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";
                cost=0.0;
                cnt=1;
            }
        });


        ll_all_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemlist.clear();
                price_list.clear();
                basicratelist.clear();
                taxlist.clear();
                ITEM_LIST=new ArrayList<String>(itemlist);
                price_list=new ArrayList<String>(price_list);
                Cursor c =db.getAllItems();
                try {
                    if (c.getCount() == 0) {
                        Message.message(getApplicationContext(), "Item not exists");
                    } else {
                        while (c.moveToNext()) {
                            if (gst_option.equals("GST Disable")) {
                                itemlist.add(c.getString(1));
                                price_list.add(c.getString(7));
                                taxlist.add(c.getString(5));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                dis_list.add(c.getString(3));

                            } else if (gst_option.equals("GST Enable")) {
                                itemlist.add(c.getString(1));
                                basicratelist.add(c.getString(2));
                                disratelist.add(c.getString(4));

                                taxlist.add(c.getString(5));
                                price_list.add(c.getString(7));
                                dis_list.add(c.getString(3));
                            }
                        }
                    }
                }catch
                (Exception ex)
                {
                    Message.message(getApplicationContext(),"Error "+ex.getMessage());
                }
                ITEM_LIST=new ArrayList<String>(itemlist);
                itemgridview1.setAdapter(new ArrayAdapter<String>(BillingScreenActivity.this, android.R.layout.simple_list_item_1, ITEM_LIST)
                {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    public View getView(int position1, View convertView1, ViewGroup parent1) {

                        View view1 = super.getView(position1,convertView1,parent1);
                        TextView tv1 = (TextView) view1;
                        RelativeLayout.LayoutParams lp1 =  new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT
                        );
                        tv1.setLayoutParams(lp1);
                        RelativeLayout.LayoutParams params1 = (RelativeLayout.LayoutParams)tv1.getLayoutParams();
                        params1.width = getPixelsFromDPs(BillingScreenActivity.this,110);
                        tv1.setLayoutParams(new GridView.LayoutParams(GridView.AUTO_FIT, 120));
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setGravity(Gravity.CENTER);
                        tv1.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                        tv1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);

                        String itemname=ITEM_LIST.get(position1);
                        if(itemname.length()>10)
                        {
                            itemname=itemname.substring(0, 10);
                            itemname=itemname+"...";
                        }
                        tv1.setText(itemname);
                        ShapeDrawable sd = new ShapeDrawable();
                        sd.setShape(new RectShape());
                        sd.getPaint().setColor(Color.parseColor("#00e6b0"));
                        sd.getPaint().setStrokeWidth(10f);
                        sd.getPaint().setStyle(Paint.Style.STROKE);
                        tv1.setBackground(sd);
                        tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                        return tv1;
                    }
                });
            }
        });

    }

    @Override
    protected void onResume() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        DebugLog.logTrace();if(selectprinter_option.equals("Dyno")){
            mPrinter.onActivityResume();
        }
        else if(selectprinter_option.equals("Airbill")) {
            m_AemScrybeDevice.getPairedPrinters();
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();
                m_AemScrybeDevice.disConnectPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onResume();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if(selectprinter_option.equals("Airbill")) {
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();

                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onPause() {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();

            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
            try {
                Handler handler; BluetoothSocket socket = null;
                try {
                    BluetoothSocket bluetoothSocket;
                    BluetoothDevice bluetoothDevice = null;
                    socket =  bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
                    socket.connect();

                } catch (Exception e) {
                    try {
                        if (socket!=null)
                            socket.close();
                    } catch (Exception closeException) {

                    }
                }
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();
                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            DebugLog.logTrace();
            mPrinter.onActivityPause();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (selectprinter_option.equals("Airbill"))
        {
            try {
                m_AemScrybeDevice.getPairedPrinters();
                boolean b=m_AemScrybeDevice.BtConnStatus();
                m_AemScrybeDevice.disConnectPrinter();
                m_AemScrybeDevice.pairPrinter(value);
                m_AemScrybeDevice.connectToPrinter(value);
                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(selectprinter_option.equals("Dyno"))
        {
            DebugLog.logTrace("onDestroy");
            mPrinter.onActivityDestroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
            try {
                Thread.sleep(30L);
            } catch (InterruptedException var5) {
                var5.printStackTrace();
            }
        }
        if (selectprinter_option.equals("Airbill"))
        {
            try {
                boolean b=m_AemScrybeDevice.BtConnStatus();
                m_AemScrybeDevice.getPairedPrinters();
                m_AemScrybeDevice.getPairedPrinters();
                m_AemScrybeDevice.disConnectPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(RECEIPT_PRINTER_MESSAGES);
            LocalBroadcastManager.getInstance(this).registerReceiver(ReceiptPrinterMessageReceiver, intentFilter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (selectprinter_option.equals("Airbill"))
        {
            try {
                m_AemScrybeDevice.disConnectPrinter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(ReceiptPrinterMessageReceiver);
            } catch (Exception e) {
                DebugLog.logException(e);
            }
        }
    }
    public void onPrintBillDyno() {
        if (print_option.equals("2 inch") && gst_option.equals("GST Disable") && selectprinter_option.equals("Dyno"))
        {
            if(billwithprint.equals("No")){
                taxlistupdated.clear();
                for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)), Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))), Double.valueOf(tv_total_amt.getText().toString()),cid,lid,empid,""+tvbill.getText().toString(),payment_mode_id,payment_deatils,order_details,gst_option,emp_code,ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                }


                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();



                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";

                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                ids = db.getAllbillID();
                autoinnc();

                Message.message(getApplicationContext(), "Bill saved");
            }

            else if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    taxlistupdated.clear();
                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray!=null && checkArray.size()!=0){
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0, totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));

                            }
                            else {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                    emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        }
                    }
                    if(ModelSale.arry_item_name.size()==0){
                        Message.message(getApplicationContext(),"Please select items");
                    }
                    else {

                        gst_data_Dyno_two_inch_gst_disable();
                    }
                }

            }
            else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes"))
            {
                if(counter<=1)
                {
                    taxlistupdated.clear();

                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray!=null && checkArray.size()!=0) {
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            //totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0, totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            }
                            else {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                    ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        }
                    }
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if(counter==1)
                    {
                        counter++;
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else{
                            gst_data_Dyno_two_inch_gst_disable();;
                        }
                    }
                    if(counter>=2) {
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else {
                            RemoveMessageBox("gst_data_Dyno_two_inch_gst_disable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }

        }

        if (print_option.equals("2 inch") && gst_option.equals("GST Enable") && selectprinter_option.equals("Dyno")) {
            if(billwithprint.equals("No")){
                taxlistupdated.clear();
                Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                if (checkArray!=null && checkArray.size()!=0) {
                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                        // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        for (int j = 0; j < checkArray.size(); j++) {
                            if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                Double value = ModelSale.arr_item_price.get(i);
                                totalPrice = totalPrice - value;
                            }
                        }
                    }
                    Log.v("TAGGF", String.valueOf(totalPrice));
                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                        if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    0.0,totalPrice, cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                    gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                        else{
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    totalPrice, cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                    gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                        }
                    }
                }else{
                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                    }
                }

                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();



                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";
                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                ids = db.getAllbillID();
                autoinnc();
                Message.message(getApplicationContext(), "Bill saved");
            }

            else if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    taxlistupdated.clear();

                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray!=null && checkArray.size()!=0) {
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            //totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0, totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            } else {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                    emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                    }
                    if(ModelSale.arry_item_name.size()==0){
                        Message.message(getApplicationContext(),"Please select items");
                    }
                    else {
                        gst_data_Dyno_two_inch_gst_enable();
                    }
                }

            }
            else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes"))
            {
                if(counter<=1)
                {
                    taxlistupdated.clear();
                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray!=null && checkArray.size()!=0){
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                        str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0, totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                            else {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                        str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                    str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                    emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                    }

                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    if(counter==1)
                    {
                        counter++;
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else{
                            gst_data_Dyno_two_inch_gst_enable();
                        }
                    }
                    if(counter>=2) {
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else {
                            RemoveMessageBox("gst_data_Dyno_two_inch_gst_enable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                        }
                    }
                }
            }
        }
    }
    public void gst_data_Dyno_two_inch_gst_enable()
    {
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mPrinter.setPrinterWidth(PrinterWidth.PRINT_WIDTH_72MM);
                mPrinter.resetPrinter();
                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);
                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = "";
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data =  h1 ;
                    } else {
                        data =  h1;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();
                    mPrinter.printTextLine("\n" + data);

                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data =  h2 ;
                    } else {
                        data =h2 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data =  h3 ;
                    } else {
                        data = h3 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data =  h4 ;
                    } else {
                        data =h4 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = h5;
                    } else {
                        data = h5;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data );
                }
                if (res2.getCount() == 0) {

                    // show message
                    Message.message(getApplicationContext(), "please add items");

                }

                mPrinter.setAlignmentCenter();
                data = "\nCash Memo    bill no:" + emp_code + "\n";
                mPrinter.setBoldOn();
                mPrinter.printTextLine("\n" + data + "\n");
                mPrinter.setBoldOff();
                data = "Date:-" + dateToStr;
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.printTextLine(data + "\n");

                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";

                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    mPrinter.printTextLine("\n" + data + "\n");
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
//                m_AemPrinter.print(d);

                String totalamtrate = "";
                int a = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                        //checkArray.remove(q);
                                        // break;
                                        //Log.v("TAGG", "CHECKOUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);
                    String amount = String.valueOf(amtlist.get(k));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        result = taxlist1.stream().filter(aObject -> {
                            return itemnamelist.contains(aObject);
                        }).collect(Collectors.toList());
                    }
                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }


                        } else {

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
//
                        } else {

                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }

                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;


                }


                mPrinter.printTextLine(data);
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
//                m_AemPrinter.print(d);


                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) - finalgst;
                mPrinter.setAlignmentLeft();
                mPrinter.printTextLine(String.valueOf(buffer));
                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                mPrinter.setBoldOn();
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.setAlignmentRight();
//                    data ="NET TOTAL (Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n";

                mPrinter.printTextLine("\nNET TOTAL (Rs): " + f.format(totalAmount) + "\n");
                mPrinter.printTextLine("\nTotal GST (Rs): " + f.format(grandgst) + "\n");
                mPrinter.printTextLine("\nTOTAL With GST(Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n");
                mPrinter.setBoldOff();

                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data =  f1 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);

                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = f2 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = f3;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = f4;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data =  f5;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }

                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();


                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();



                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";

                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                str_set_cust_name = "";

                ids = db.getAllbillID();
                autoinnc();
                Message.message(getApplicationContext(),"Bill Saved");
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                mPrinter.setPrinterWidth(PrinterWidth.PRINT_WIDTH_72MM);
                mPrinter.resetPrinter();
                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);
                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = "";
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data =  h1 ;
                    } else {
                        data =  h1;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();
                    mPrinter.printTextLine("\n" + data);

                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data =  h2 ;
                    } else {
                        data =h2 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data =  h3 ;
                    } else {
                        data = h3 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data =  h4 ;
                    } else {
                        data =h4 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = h5;
                    } else {
                        data = h5;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data );
                }
                if (res2.getCount() == 0) {

                    // show message
                    Message.message(getApplicationContext(), "please add items");

                }

                mPrinter.setAlignmentCenter();
                data = "\nCash Memo    bill no:" + emp_code + "\n";
                mPrinter.setBoldOn();
                mPrinter.printTextLine("\n" + data + "\n");
                mPrinter.setBoldOff();
                data = "Date:-" + dateToStr;
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.printTextLine(data + "\n");

                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";

                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    mPrinter.printTextLine("\n" + data + "\n");
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
//                m_AemPrinter.print(d);

                String totalamtrate = "";
                int a = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                        //checkArray.remove(q);
                                        // break;
                                        //Log.v("TAGG", "CHECKOUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);
                    String amount = String.valueOf(amtlist.get(k));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        result = taxlist1.stream().filter(aObject -> {
                            return itemnamelist.contains(aObject);
                        }).collect(Collectors.toList());
                    }
                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }


                        } else {

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
//
                        } else {

                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
                                    buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }

                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;


                }


                mPrinter.printTextLine(data);
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
//                m_AemPrinter.print(d);


                Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) - finalgst;
                mPrinter.setAlignmentLeft();
                mPrinter.printTextLine(String.valueOf(buffer));
                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                mPrinter.setBoldOn();
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.setAlignmentRight();
//                    data ="NET TOTAL (Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n";

                mPrinter.printTextLine("\nNET TOTAL (Rs): " + f.format(totalAmount) + "\n");
                mPrinter.printTextLine("\nTotal GST (Rs): " + f.format(grandgst) + "\n");
                mPrinter.printTextLine("\nTOTAL With GST(Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n");
                mPrinter.setBoldOff();

                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data =  f1 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);

                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = f2 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = f3;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = f4;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data =  f5;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                if (counter >= 2) {
                } else {

                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();

                    payment_deatils="";
                    order_details="";
                    str_set_cust_name="";
                    str_set_payment="";
                    payment_mode_id="";

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                    ids = db.getAllbillID();
                    autoinnc();
                }
                Message.message(getApplicationContext(), "Bill saved");
            }
        }
    }
    public void gst_data_Dyno_two_inch_gst_disable(){

        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                mPrinter.setPrinterWidth(PrinterWidth.PRINT_WIDTH_72MM);
                mPrinter.resetPrinter();


                //res1= db.getAllbill();


                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));


                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {

                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = "";
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data =  h1 ;
                    } else {
                        data =  h1;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();
                    mPrinter.printTextLine("\n" + data);

                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data =  h2 ;
                    } else {
                        data =h2 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data =  h3 ;
                    } else {
                        data = h3 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data =  h4 ;
                    } else {
                        data =h4 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = h5;
                    } else {
                        data = h5;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data );
                }
                if (res2.getCount() == 0) {

                    // show message
                    Message.message(getApplicationContext(), "please add items");

                }

                mPrinter.setAlignmentCenter();
                data = "\nCash Memo    bill no:" + emp_code + "\n";
                mPrinter.setBoldOn();
                mPrinter.printTextLine("\n" + data + "\n");
                mPrinter.setBoldOff();
                data = "Date:-" + dateToStr;
//                    mPrinter.printTextLine("\n"+data+"\n");


                // Bill Header End

                // Bill Details Start
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.printTextLine(data + "\n");

                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";

                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    mPrinter.printTextLine("\n" + data + "\n");
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
//                m_AemPrinter.print(d);


                mPrinter.printTextLine(data);
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
//                m_AemPrinter.print(d);

                String totalamtrate = "";
                int a = 1;
                StringBuffer buffer = new StringBuffer();
                while (res2.moveToNext()) {

                    String itemname = res2.getString(3).toString();
                    String largname = "";
                    String qty = res2.getString(4).toString();
                    String rate = res2.getString(5).toString();
                    ;
                    String amount = res2.getString(6).toString();
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (itemname.length() > 15) {


                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }


                }

                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                        //checkArray.remove(q);
                                        // break;
                                        //Log.v("TAGG", "CHECKOUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);

                    String amount = String.valueOf(qty*Double.parseDouble(rate));// String.valueOf(amtlist.get(k));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        result = taxlist1.stream().filter(aObject -> {
                            return itemnamelist.contains(aObject);
                        }).collect(Collectors.toList());
                    }
                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                            }


                        } else {

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
//
                        } else {

                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }

                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;


                }


                mPrinter.setAlignmentLeft();
                mPrinter.printTextLine(String.valueOf(buffer));
                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                mPrinter.setBoldOn();
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.setAlignmentRight();
                mPrinter.printTextLine("\nTOTAL(Rs): " + f.format(ddata) + "\n");
                mPrinter.setBoldOff();
                mPrinter.setBoldOn();
                mPrinter.setAlignmentCenter();

                if(finalgst==0.0){}
                else {
                    data = "\n          GST Details \n";
                    mPrinter.printTextLine(data);
                    mPrinter.setBoldOff();
                    mPrinter.setBoldOn();
                    data = "  GST%    CGST+SGST     Total \n";
                    mPrinter.setBoldOff();
                    mPrinter.printTextLine(data);
                    mPrinter.printTextLine(String.valueOf(buffer1));
                }
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data =  f1 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);

                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = f2 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = f3;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = f4;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data =  f5;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }

                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();


                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();

                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";

                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                str_set_cust_name = "";

                ids = db.getAllbillID();
                autoinnc();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                mPrinter.setPrinterWidth(PrinterWidth.PRINT_WIDTH_72MM);
                mPrinter.resetPrinter();


                //res1= db.getAllbill();


                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));


                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {

                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                String dateToStr = format.format(today);
                String data = "";
                if (h1 == null || h1.length() == 0 || h1 == "") {
                    h1 = null;
                } else {
                    if (h1.length() < h2.length()) {
                        data =  h1 ;
                    } else {
                        data =  h1;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();
                    mPrinter.printTextLine("\n" + data);

                }

                if (h2 == null || h2.length() == 0 || h2 == "") {
                    h2 = null;
                } else {
                    if (h2.length() > 16) {
                        data =  h2 ;
                    } else {
                        data =h2 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h3 == null || h3.length() == 0 || h3 == "") {
                    h3 = null;
                } else {
                    if (h3.length() > 16) {
                        data =  h3 ;
                    } else {
                        data = h3 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h4 == null || h4.length() == 0 || h4 == "") {
                    h4 = null;
                } else {
                    if (h4.length() > 16) {
                        data =  h4 ;
                    } else {
                        data =h4 ;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);
                }
                if (h5 == null || h5.length() == 0 || h5 == "") {
                    h5 = null;
                } else {
                    if (h4.length() > 16) {
                        data = h5;
                    } else {
                        data = h5;
                    }
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data );
                }
                if (res2.getCount() == 0) {

                    // show message
                    Message.message(getApplicationContext(), "please add items");

                }

                mPrinter.setAlignmentCenter();
                data = "\nCash Memo    bill no:" + emp_code + "\n";
                mPrinter.setBoldOn();
                mPrinter.printTextLine("\n" + data + "\n");
                mPrinter.setBoldOff();
                data = "Date:-" + dateToStr;
//                    mPrinter.printTextLine("\n"+data+"\n");


                // Bill Header End

                // Bill Details Start
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.printTextLine(data + "\n");

                if (str_set_cust_name == "" || str_set_cust_name == null) {
                    str_set_cust_name = "";
                    data = "\nItem Name         |Qty|Rate|Amt\n";

                } else {
                    data = "Customer Name:- " + str_set_cust_name + "\n";
                    mPrinter.printTextLine("\n" + data + "\n");
                    data = "Item Name        |Qty|Rate|Amt\n";
                }
//                m_AemPrinter.print(d);


                mPrinter.printTextLine(data);
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
//                m_AemPrinter.print(d);

                String totalamtrate = "";
                int a = 1;
                StringBuffer buffer = new StringBuffer();
                while (res2.moveToNext()) {

                    String itemname = res2.getString(3).toString();
                    String largname = "";
                    String qty = res2.getString(4).toString();
                    String rate = res2.getString(5).toString();
                    ;
                    String amount = res2.getString(6).toString();
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (itemname.length() > 15) {


                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (res2.getString(5).length() >= 3) {
                            buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                        } else {
                            buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                        }
                    }


                }

                String totalamtrategst = "";
                int a1 = 1, j = 0;
                Double itemtax = 0.0;
                int[] positions = new int[ModelSale.arry_item_name.size()];
                StringBuffer buffer1 = new StringBuffer();
                Double totalwithgst = 0.0, finalgst = 0.0;
                Double oldtax = 0.0;
                int l = 0;

                DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                Double grandgst = 0.0;
                int count1 = 0;
                Double cgst = 0.0, sgst = 0.0;
                List<String> result = new ArrayList<>();

                Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                List<String> taxlist1 = new ArrayList<>();
                final List<String> itemnamelist = new ArrayList<>();
                List<Double> qtylist = new ArrayList<>();
                List<String> ratelist = new ArrayList<>();
                List<Double> amtlist = new ArrayList<>();
                List<String> indexlist1 = new ArrayList<>();
                for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                    itemtax = Double.parseDouble(taxlistupdated.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));
                        String stringName = c.getString(0);
                        if(checkArray==null || checkArray.size()==0){
                            if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    itemnamelist.add(c.getString(0));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                                if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                } else {
                                    itemnamelist.add(c.getString(0));
                                    indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                }
                            }
                        }
                        else{
                            if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                    //   for(int q=0;q<checkArray.size();q++) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                        //checkArray.remove(q);
                                        // break;
                                        //Log.v("TAGG", "CHECKOUT");
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                                else {
                                    if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, 0.0);
                                        double current = Double.parseDouble(tv_total_amt.getText().toString());
                                        double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                        total_val = current - total_val;
                                        tv_total_amt.setText(String.valueOf(total_val));
                                        Log.v("TAG", String.valueOf(total_val));
                                    } else {
                                        Log.v("TAGG", "CHECKIN4");
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        Log.v("TAGG", "CHECKOUT4");
                                    }
                                }
                            }
                        }
                    }
                }
                taxlist1.clear();
                for (int k = 0; k < itemnamelist.size(); k++) {
                    DecimalFormat formats = new DecimalFormat("#####.00");
                    taxlist1.clear();
                    int count = itemnamelist.size();
                    String itemname = itemnamelist.get(k);
                    String largname = "";
                    Double qty = qtylist.get(k);
                    String rate = ratelist.get(k);

                    String amount = String.valueOf(qty*Double.parseDouble(rate));// String.valueOf(amtlist.get(k));
                    itemtax = Double.valueOf(indexlist1.get(k));
                    Cursor c = db.getdistinct(String.valueOf(itemtax));
                    while (c.moveToNext()) {
                        taxlist1.add(c.getString(0));

                    }
                    cgst = (itemtax / 2);
                    sgst = cgst;
                    if (amount.length() > 4) {
                        amount = amount.substring(0, 4);

                    } else {
                        int length = amount.length();
                        int l1 = 0;
                        if (length < 4) {
                            l1 = 4 - length;
                            amount = String.format(amount + "%" + (l1) + "s", "");
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        result = taxlist1.stream().filter(aObject -> {
                            return itemnamelist.contains(aObject);
                        }).collect(Collectors.toList());
                    }
                    i("dataname", String.valueOf(result));
                    int resultcount = result.size();

                    if (itemname.length() > 15) {


                        if (rate.length() == 3) {

                            count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                            }


                        } else {

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }

                    } else {

                        int length = itemname.length();
                        int l1 = 0;
                        if (length < 15) {
                            l1 = 15 - length;
                            String itemname1 = itemname.concat(l1 + " ");
                            itemname = String.format(itemname + "%" + (l1) + "s", "");
                        }
                        if (rate.length() == 3) {
                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
//
                        } else {

                            count1++;

                            if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                int p = result.indexOf(itemnamelist.get(k));
                                int f = taxlist1.size();
                                int h = itemnamelist.size();
                                if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    sameitemgst = sameitemgst + Double.parseDouble(amount);
                                    sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                    finalgst = finalgst + sameitemtotalgst;
                                    grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                } else {
                                    if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);

                                    }
                                }


                            } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                grandgst = grandgst + totalwithgst;
                            }
                        }
                    }

                    j++;
                    finalgst = totalwithgst + finalgst;
                    totalwithgst = 0.0;
                    cgst = 0.0;
                    sgst = 0.0;


                }


                mPrinter.setAlignmentLeft();
                mPrinter.printTextLine(String.valueOf(buffer));
                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                DecimalFormat f = new DecimalFormat("#####.00");
                //System.out.println(f.format(dda));

                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                mPrinter.printTextLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                mPrinter.setBoldOn();
                mPrinter.setCharRightSpacing(0);
                mPrinter.pixelLineFeed(50);
                mPrinter.setAlignmentRight();
                mPrinter.printTextLine("\nTOTAL(Rs): " + f.format(ddata) + "\n");
                mPrinter.setBoldOff();
                mPrinter.setBoldOn();
                mPrinter.setAlignmentCenter();

                if(finalgst==0.0){}
                else {
                    data = "\n          GST Details \n";
                    mPrinter.printTextLine(data);
                    mPrinter.setBoldOff();
                    mPrinter.setBoldOn();
                    data = "  GST%    CGST+SGST     Total \n";
                    mPrinter.setBoldOff();
                    mPrinter.printTextLine(data);
                    mPrinter.printTextLine(String.valueOf(buffer1));
                }
                if (f1 == null || f1.length() == 0 || f1 == "") {
                    f1 = null;
                } else {

                    data =  f1 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOn();

                    mPrinter.printTextLine("\n" + data);

                }
                if (f2 == null || f2.length() == 0 || f2 == "") {
                    f2 = null;
                } else {
                    data = f2 ;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f3 == null || f3.length() == 0 || f3 == "") {
                    f3 = null;
                } else {

                    data = f3;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f4 == null || f4.length() == 0 || f4 == "") {
                    f4 = null;
                } else {
                    data = f4;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }
                if (f5 == null || f5.length() == 0 || f5 == "") {
                    f5 = null;
                } else {
                    data =  f5;
                    mPrinter.setAlignmentCenter();
                    mPrinter.setBoldOff();

                    mPrinter.printTextLine("\n" + data );
                }

                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();
                mPrinter.printLineFeed();

                if (counter >= 2) {
                } else {

                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();

                    payment_deatils="";
                    order_details="";
                    str_set_cust_name="";
                    str_set_payment="";
                    payment_mode_id="";

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);



                    ids = db.getAllbillID();
                    autoinnc();
                }
                Message.message(getApplicationContext(), "Bill saved");
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPrinter.onActivityRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private final BroadcastReceiver ReceiptPrinterMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DebugLog.logTrace("Printer Message Received");
            Bundle b = intent.getExtras();
            if (b == null) {
                return;
            }
            else
            {

                if (selectprinter_option.equals("Dyno")) {

                    switch (b.getInt(RECEIPT_PRINTER_STATUS)) {
                        case RECEIPT_PRINTER_CONN_STATE_NONE:
                            tvStatus.setText(R.string.printer_not_conn);
                            break;
                        case RECEIPT_PRINTER_CONN_STATE_LISTEN:
                            tvStatus.setText(R.string.ready_for_conn);
                            break;
                        case RECEIPT_PRINTER_CONN_STATE_CONNECTING:
                            tvStatus.setText(R.string.printer_connecting);
                            break;
                        case RECEIPT_PRINTER_CONN_STATE_CONNECTED:
                            tvStatus.setText(R.string.printer_connected);
                            // new AsyncPrint().execute();
                            itemArrayList.clear();
//                            new AsyncPrint().execute();
                            onPrintBillDyno();
                            break;
                        case RECEIPT_PRINTER_CONN_DEVICE_NAME:
                            savePrinterMac(b.getString(RECEIPT_PRINTER_NAME, ""));
                            break;
                        case RECEIPT_PRINTER_NOTIFICATION_ERROR_MSG:
                            String n = b.getString(RECEIPT_PRINTER_MSG);
                            tvStatus.setText(n);
                            break;
                        case RECEIPT_PRINTER_NOTIFICATION_MSG:
                            String m = b.getString(RECEIPT_PRINTER_MSG);
                            tvStatus.setText(m);
                            break;
                        case RECEIPT_PRINTER_NOT_CONNECTED:
                            tvStatus.setText("Status : Printer Not Connected");
                            break;
                        case RECEIPT_PRINTER_NOT_FOUND:
                            tvStatus.setText("Status : Printer Not Found");
                            break;
                        case RECEIPT_PRINTER_SAVED:
                            tvStatus.setText(R.string.printer_saved);
                            break;
                    }
                }
            }
        }
    };
    private void savePrinterMac(String sMacAddr) {
        if (sMacAddr.length() > 4) {
            tvStatus.setText("Preferred Printer saved");
        } else {
            tvStatus.setText("Preferred Printer cleared");
        }
    }
    public void showDialog(){

        try {

            final Dialog dialog = new Dialog(BillingScreenActivity.this);
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_listview);

            Button btndialog = (Button) dialog.findViewById(R.id.btndialog);
            btndialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

            lv= (ListView) dialog.findViewById(R.id.listview);
//            lv=listView;
//            ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.list_item, R.id.tv, myImageNameList);
//            ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.layout_codewise,  itemArrayList);
//            MyAppAdapter myAppAdapter=new MyAppAdapter(this,R.layout.layout_codewise,itemArrayList);
            if(itemArrayList.size()<=0){
//                    itemArrayList.add(new ClassCodeWiseList("No records found" , "", "", "", "", ""));
//                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                lv.setAdapter(null);
                cnt=1;
            }
            else{

                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                lv.setAdapter(myAppAdapter);
            }
          /*  myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
            listView.setAdapter(myAppAdapter);
*/
           /* listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {s
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                textView.setText("You have clicked : "+myImageNameList[position]);
                    Log.i("You have clicked", String.valueOf(itemArrayList.get(position)));
                    dialog.dismiss();
                }
            });*/
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT,  RelativeLayout.LayoutParams.WRAP_CONTENT);

            dialog.show();


        }

        catch(Exception ex)
        {
            Message.message(getApplicationContext(),""+ex.getMessage());
        }



    }


    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1))+5;
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    // Method for converting DP value to pixels
    public static int getPixelsFromDPs(Activity activity, int dps){
        Resources r = activity.getResources();
        int  px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.billing_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        if (id == R.id.nav_dasgboard) {
//            Intent i=new Intent(getApplicationContext(),AdminDashBoardActivity.class);
//            startActivity(i);
//        } else if (id == R.id.nav_add_item) {
//            Intent i=new Intent(getApplicationContext(),AddItemActivity.class);
//            startActivity(i);
//
//        } else if (id == R.id.nav_add_customer) {
//            Intent i=new Intent(getApplicationContext(),AddCustomerActivity.class);
//            startActivity(i);
//
//        } else if (id == R.id.nav_add_category) {
//            Intent i=new Intent(getApplicationContext(),AddCategoryActivity.class);
//            startActivity(i);
//
//        } else if (id == R.id.nav_add_tax) {
//            Intent i=new Intent(getApplicationContext(),AddTaxActivity.class);
//            startActivity(i);
//
//        } else if (id == R.id.nav_add_units) {
//            Intent i=new Intent(getApplicationContext(),AddUnitsActivity.class);
//            startActivity(i);
//
//        }else if (id == R.id.nav_sale) {
//            Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
//            startActivity(i);
//        } else if (id == R.id.nav_setting) {
//
//        }else if (id == R.id.nav_purchase) {
//
//        }else if(id == R.id.nav_item_master){
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareMenuData() {

        MenuModel menuModel = new MenuModel("Dashboard", true, false,"Dashboard"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Master", true, true,"Master"); //Menu of Java Tutorials
        headerList.add(menuModel);
        List<MenuModel> childModelsList = new ArrayList<>();
        MenuModel childModel = new MenuModel("Add Item", false, false,"Add Item");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Customer", false, false,"Add Customer");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Category", false, false,"Add Category");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Units", false, false,"Add Units");
        childModelsList.add(childModel);

        childModel = new MenuModel("Add Supplier", false, false,"Add Supplier");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            Log.d("API123","here");
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Sales", true, true, "Sales"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("ThumbNail", false, false, "ThumbNail");
        childModelsList.add(childModel);

        childModel = new MenuModel("Codewise", false, false, "Codewise");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Reports", true, true, "Reports"); //Menu of Python Tutorials
        headerList.add(menuModel);

        childModel = new MenuModel("Sales Bill Report", false, false, "Sales Bill Report");
        childModelsList.add(childModel);

        childModel = new MenuModel("Deleted SalesBill Report", false, false, "Deleted SalesBill Report");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        childModelsList = new ArrayList<>();
        menuModel = new MenuModel("Setting", true, true, "Setting"); //Menu of Python Tutorials
        headerList.add(menuModel);
        childModel = new MenuModel("Header Footer Setting", false, false, "Header Footer Setting");
        childModelsList.add(childModel);

        childModel = new MenuModel("Main Setting", false, false, "Main Setting");
        childModelsList.add(childModel);

        if (menuModel.hasChildren) {
            childList.put(menuModel, childModelsList);
        }

        menuModel = new MenuModel("Help", true, false,"Help"); //Menu of Android Tutorial. No sub menus
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }

        menuModel = new MenuModel("Logout", true, false,"Logout");
        headerList.add(menuModel);

        if (!menuModel.hasChildren) {
            childList.put(menuModel, null);
        }
    }

    private void populateExpandableList() {

        expandableListAdapter = new ExpandableListAdapter(this, headerList, childList);
        expandableListView.setAdapter(expandableListAdapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                if (headerList.get(groupPosition).isGroup) {
                    if (!headerList.get(groupPosition).hasChildren) {
                        //Toast.makeText(getApplicationContext(),""+headerList.get(groupPosition).url,Toast.LENGTH_LONG).show();
                        if(headerList.get(groupPosition).url.equals("Dashboard")) {
                            Intent i = new Intent(getApplicationContext(), AdminDashBoardActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Help")) {
                            Intent i = new Intent(getApplicationContext(), HelpActivity.class);
                            startActivity(i);
                            finish();
                        }else if(headerList.get(groupPosition).url.equals("Logout")) {
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                            final SharedPreferences.Editor editor = pref.edit();
                            editor.putString("lid","");
                            editor.putString("cid","");
                            editor.putString("emp_id","");
                            editor.putString("employee_code","");
                            editor.putString("Username","");
                            editor.putString("Panel","");
                            editor.putString("cat_datetime","0000-00-00 00:00:00");
                            editor.putString("unit_datetime","0000-00-00 00:00:00");
                            editor.putString("item_datetime","0000-00-00 00:00:00");
                            editor.putString("sup_datetime","0000-00-00 00:00:00");
                            editor.putString("cust_datetime","0000-00-00 00:00:00");
                            editor.putString("purc_datetime","0000-00-00 00:00:00");
                            editor.commit();

                            try {
                                String HttpUrl = Config.hosturl+"logout_api.php";
                                Log.d("URL", HttpUrl);
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                try {
                                                    Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_SHORT).show();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Toast.makeText(getApplicationContext(), "Error=" + e, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError volleyError) {

                                                Toast.makeText(getApplicationContext(), volleyError.toString(), Toast.LENGTH_LONG).show();
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("empid", "" + empid);
                                        return params;
                                    }
                                };

                                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                                requestQueue.add(stringRequest);

                            }catch (Exception e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                        //onBackPressed();
                    }
                }

                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                if (childList.get(headerList.get(groupPosition)) != null) {
                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
                    Toast.makeText(getApplicationContext(),""+model.url,Toast.LENGTH_LONG).show();
                    if(model.url.equals("Add Item")) {
                        Intent i = new Intent(getApplicationContext(), ItemListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Customer")) {
                        Intent i = new Intent(getApplicationContext(), CustomerListActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Category")) {
                        Intent i = new Intent(getApplicationContext(), AddCategoryActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Add Units")) {
                        Intent i = new Intent(getApplicationContext(), AddUnitsActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("ThumbNail")) {
                        Intent i = new Intent(getApplicationContext(), BillingScreenActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Codewise")) {
                        Intent i = new Intent(getApplicationContext(), CodeWiseBillingActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Sales Bill Report")) {
                        Intent i=new Intent(getApplicationContext(),SalesBillReport.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Deleted SalesBill Report")) {
                        Intent i = new Intent(getApplicationContext(), Deleted_Bill_Report_Activity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(model.url.equals("Header Footer Setting")) {
                        Intent i = new Intent(getApplicationContext(), HaederFooterActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Main Setting")) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();
                    }else if(model.url.equals("Add Supplier")){
                        Intent i=new Intent(getApplicationContext(),SupplierActivity.class);
                        startActivity(i);
                        finish();
                    }
                }

                return false;
            }
        });
    }



    public void autoinnc()
    {
        // ids;

        try {
            // DateTime.Now.ToString("hh:mm:ss");
//            SimpleDateFormat dateFormat = new SimpleDateFormat(
//                    "HH:mm:ss", Locale.getDefault());
//            Date date = new Date();
//            System.out.println( ""+dateFormat.format(date));
//            String d1=dateFormat.format((date));
//            if(d1.equals("23:59.00")){
//                tvbill.setText("1");
//
//            }
//            else {
            ids = db.getAllbillID();String d="";
            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String dateToday = format.format(today);
            String yesterday="";
            if(resetbill.equals("Yes")) {

                while (ids.moveToNext()) {

                    d = ids.getString(0);
                    yesterday = ids.getString(1);
//                    if (d == null || d == "") {
//
//                    } else {
//                        r = Integer.parseInt(ids.getString(0));
//                    }
                }
                if (yesterday == null||d==null||d=="" ) {
                    tvbill.setText("" + r);
                }
                else if (!dateToday.equals(yesterday)) {

                    tvbill.setText("" + r);
                }
                else if (dateToday.equals(yesterday)) {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    tvbill.setText("" + r);
                }

                else {
                    r = Integer.parseInt(d);
                    r = r + 1;
                    tvbill.setText("" + r);

                }
            }
            else if(resetbill.equals("No"))
            {
                while (ids.moveToNext()) {
                    d = ids.getString(0);
                    if (d == ""||d==null) {
                        tvbill.setText("1");

                    } else {
                        r = Integer.parseInt(ids.getString(0));
                        r = r + 1;

                        tvbill.setText("" + r);

                    }

                }
            }
        }
        catch (Exception ex)
        {
            Message.message(getApplicationContext(),""+ex.getMessage());
        }


//
////                    countday ++;
////                    tvbill.setText("1");
//                    if (Integer.parseInt(d) >=1 && dateToday.equals(yesterday))
//                    {
//                        r = Integer.parseInt(d);
//                        r = r + 1;
//
////idvalue=true;
//
//                    }
//                    else
//                    {
////                        if(idvalue==true) {
////                            r = Integer.parseInt(d);
////                            r = r + 1;
//                            tvbill.setText("" + r);
////                            idvalue=false;
////                        }
////                        tvbill.setText("" + r);
//                    }

//                }

//            }




    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select Printer to connect");
        for (int i = 0; i < printerList.size(); i++)
        {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        super.onContextItemSelected(item);

        String printerName = item.getTitle().toString();
        try
        {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(BillingScreenActivity.this,"Connected with " + printerName,Toast.LENGTH_SHORT ).show();
            value = printerName;
            SharedPreferences sharedPref = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("value", value);
            editor.apply();
        }
        catch (IOException e)
        {
            if (e.getMessage().contains("Service discovery failed"))
            {
                Toast.makeText(BillingScreenActivity.this,"Not Connected\n"+ printerName + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
            }
            else if (e.getMessage().contains("Device or resource busy"))
            {
                Toast.makeText(BillingScreenActivity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Log.i("socket status", String.valueOf(m_AemScrybeDevice.BtConnStatus()));
                Toast.makeText(BillingScreenActivity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
            }
        }
        return true;
    }

    @Override
    public void onScanMSR(String s, CardReader.CARD_TRACK card_track) {

    }

    @Override
    public void onScanDLCard(String s) {

    }

    @Override
    public void onScanRCCard(String s) {

    }

    @Override
    public void onScanRFD(String s) {

    }

    @Override
    public void onScanPacket(String s) {

    }

    @Override
    public void onDiscoveryComplete(ArrayList<String> arrayList) {

    }

    public void onShowPairedPrinters(View v)
    {
        printerList = m_AemScrybeDevice.getPairedPrinters();
        if (printerList.size() > 0)
            openContextMenu(v);

    }

    public void onPrintBill(View v) throws IOException {
        if (m_AemPrinter == null) {
            try {
                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                        BluetoothAdapter.getDefaultAdapter().enable();

                        try {
                            Thread.sleep(30L);
                        } catch (InterruptedException var5) {
                            var5.printStackTrace();
                        }
                    }
                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Printer not connected please connect to printer");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        if(print_option.equals("3 inch"))
        {
//            taxlistupdated.clear();
//
//            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
//
//                db.Addbill(Integer.parseInt(tvbill.getText().toString()),str_set_cust_name,str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)), Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))), Double.valueOf(tv_total_amt.getText().toString()),ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
//                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
//                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(),ModelSale.arr_item_qty.get(i).toString());
//            }
            //res1= db.getAllbill();
            if(print_option.equals("3 inch")&&selectprinter_option.equals("Airbill")&& gst_option.equals("GST Disable"))
            {
                if(billwithprint.equals("No"))
                {
                    taxlistupdated.clear();
                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++)
                    {

                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)), Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))), Double.valueOf(tv_total_amt.getText().toString()),cid,lid,empid,""+Integer.parseInt(tvbill.getText().toString()),payment_mode_id,payment_deatils,order_details,gst_option,emp_code,ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(),ModelSale.arr_item_qty.get(i).toString());
                    }

                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();


                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);


                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getApplicationContext(), "Bill saved");
                }

                else if(billwithprint.equals("Yes")&&multipleprint.equals("No"))
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        taxlistupdated.clear();
                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        if (checkArray!=null && checkArray.size()!=0){
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                //     totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                                        Double value = ModelSale.arr_item_price.get(i);
                                        totalPrice = totalPrice - value;
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                                else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                            }
                        }else{
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                                        empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                        payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                        ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                            }
                        }

                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else
                        {
                            gst_data_blueprints_three_inch_gst_disable();
                        }
                    }

                }
                else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes"))
                {
                    if(counter<=1) {
                        taxlistupdated.clear();

                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        if (checkArray != null && checkArray.size() != 0) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                //  totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                        Double value = ModelSale.arr_item_price.get(i);
                                        totalPrice = totalPrice - value;
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                                else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                            payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                                }
                            }
                        }
                        else{
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                                        empid, "" + Integer.parseInt(tvbill.getText().toString()), payment_mode_id,
                                        payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                        ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        if(counter==1)
                        {
                            counter++;
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else{
                                gst_data_blueprints_three_inch_gst_disable();;

                            }


                        }
                        if(counter>=2) {
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else {
                                RemoveMessageBox("gst_data_blueprints_three_inch_gst_disable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                            }
                        }
                    }
                }

            }
            if(print_option.equals("3 inch")&&selectprinter_option.equals("Airbill")&& gst_option.equals("GST Enable"))
            {
                if(billwithprint.equals("No")){
                    taxlistupdated.clear();
                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {

                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i), Double.parseDouble(ModelSale.arr_item_rate.get(i)), Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))), Double.valueOf(tv_total_amt.getText().toString()),cid,lid,empid,""+Integer.parseInt(tvbill.getText().toString()),payment_mode_id,payment_deatils,order_details,gst_option,emp_code,ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                    }

                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();


                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);


                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getApplicationContext(), "Bill saved");
                }

                else if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        taxlistupdated.clear();
                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        if (checkArray != null && checkArray.size() != 0) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                        Double value = ModelSale.arr_item_price.get(i);
                                        totalPrice = totalPrice - value;
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));

                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                            payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                                else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid,
                                            empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                            payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                            ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                                }
                            }
                        }else {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                                        empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                        payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else {

                            gst_data_blueprints_three_inch_gst_enable();
                        }
                    }

                }
                else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes"))
                {   if(counter<=1)
                {
                    taxlistupdated.clear();
                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray != null && checkArray.size() != 0) {
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            //    totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0,totalPrice, cid, lid,
                                        empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                        payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                            else{
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid,
                                        empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                        payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                        ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                                    empid, "" + Integer.parseInt(tvbill.getText().toString()),
                                    payment_mode_id, payment_deatils, order_details, gst_option, emp_code,
                                    ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                    }
                }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        if(counter==1)
                        {
                            counter++;
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else{
                                gst_data_blueprints_three_inch_gst_enable();

                            }
                        }
                        if(counter>=2) {
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else {
                                RemoveMessageBox("gst_data_blueprints_three_inch_gst_enable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                            }
                        }
                    }
                }
            }
//            gst_data_blueprints_three_inch_gst_disable();

//
//            res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
//
//            String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
//
//
//
//
//            header = db.getAllHeaderFooter();
//            while (header.moveToNext()) {
//
//                h1 = header.getString(1);
//                h1size = header.getString(2);
//                h2 = header.getString(3);
//                h2size = header.getString(4);
//                h3 = header.getString(5);
//                h3size = header.getString(6);
//                h4 = header.getString(7);
//                h4size = header.getString(8);
//                h5 = header.getString(9);
//                h5size = header.getString(10);
//
//                f1 = header.getString(11);
//                f1size = header.getString(12);
//                f2 = header.getString(13);
//                f2size = header.getString(14);
//                f3 = header.getString(15);
//                f3size = header.getString(16);
//                f4 = header.getString(17);
//                f4size = header.getString(18);
//                f5 = header.getString(19);
//                f5size = header.getString(20);
//            }
//
//            Date today = new Date();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
//            String dateToStr = format.format(today);
//            String data = null;
//
//            data = "                 " + "AirBill App" + "             \n";
//            String d = "_______________________________________________\n";
//
//            if (h1 == null || h1.length() == 0 || h1 == "") {
//                h1 = null;
//            } else {
//                data = "           " + h1 + "                    ";
//                m_AemPrinter.print(data);
//                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
////                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            }
//
//            if (h2 == null || h2.length() == 0 || h2 == "") {
//                h2 = null;
//            } else {
//                data = "                    " + h2 + "                   ";
//                m_AemPrinter.print(data);
//                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            }
//            if (h3 == null || h3.length() == 0 || h3 == "") {
//                h3 = null;
//            } else {
//                data = "                    " + h3 + "                     ";
//                m_AemPrinter.print(data);
//                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            }
//            if (h4 == null || h4.length() == 0 || h4 == "") {
//                h4 = null;
//            } else {
//                data = "                     " + h4 + "                   ";
//                m_AemPrinter.print(data);
//                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            }
//            if (h5 == null || h5.length() == 0 || h5 == "") {
//                h5 = null;
//            } else {
//                data = "                     " + h5 + "                   ";
//                m_AemPrinter.print(data);
//                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            }
//            try {
//
//                data = "\nBill No:-" + tvbill.getText().toString() + "       Date:-" + dateToStr + "\n";
////                data = "  Cash Memo      BILL No:" + tvbill.getText().toString()+"\n";
//                m_AemPrinter.print(data);
////                m_AemPrinter.print(d);
//                /*data="Date:-" + dateToStr + "\n";
//                m_AemPrinter.print(data);
//                m_AemPrinter.print(d);*/
//                if(str_set_cust_name==""||str_set_cust_name==null) {
//                    str_set_cust_name="";
//                    data = "\nNo|       Item Name       | Qty | Rate | Amount ";
//
//                }
//                else {
//                    data = "Customer Name:- " + str_set_cust_name + "\n";
//                    m_AemPrinter.print(data);
//                    data = "No|       Item Name       | Qty | Rate | Amount ";
//
//                }
////                m_AemPrinter.print(d);
//                data = "No|   Item Name    | Qty | Rate | Dis | Tax | Amount ";
//
//                m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
//                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
//                String totalamtrate = "";
//                int a = 1;
//                if (res2.getCount() == 0) {
//                    // show message
//                    Message.message(getApplicationContext(), "please add items");
//                    return;
//                }
//
//                StringBuffer buffer = new StringBuffer();
//                while (res2.moveToNext())
//                {
//
//                    String itemname = res2.getString(3).toString();
//                    String largname = "";
//                    String qty = res2.getString(4).toString();
//                    String rate = res2.getString(5).toString();
//                    ;
//                    String amount = res2.getString(6).toString();
//                    if (amount.length() > 4) {
//                        amount = amount.substring(0, 4);
//
//                    } else {
//                        int length = amount.length();
//                        int l1 = 0;
//                        if (length < 4) {
//                            l1 = 4 - length;
//                            amount = String.format(amount + "%" + (l1) + "s", "");
//                        }
//                    }
//                    if (itemname.length() > 23) {
//
//                      /*  if(res2.getString(5).length()==3){
//                            buffer.append(a + "   " + itemname.substring(0, 22) + "   " + res2.getString(4) + "   " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                        }
//                        else {
//                            buffer.append(a + "   " + itemname.substring(0, 22) + "   " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                        }*/
//                        if (a >= 10) {
//                            if(res2.getString(4).length()==2||res2.getString(5).length()==3)
//                                buffer.append(a + "  " + itemname.substring(0, 22) + "   " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                        }
//                        else
//                        {
//                            buffer.append(a + "   " + itemname.substring(0, 22) + "   " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//
//                        }
//                    }
//                    else {
//                        int length = itemname.length();
//                        int l1 = 0;
//                        if (length < 22) {
//                            l1 = 22 - length;
//                            String itemname1 = itemname.concat(l1 + " ");
//                            itemname = String.format(itemname + "%" + (l1 - 2) + "s", "");
//                        }
//                       /* if(res2.getString(5).length()==3) {
//                            buffer.append(a + "   " + itemname + "     " + res2.getString(4) + "    " + res2.getString(5) + "  " + amount + "\n");//res2.getString(6)+"\n");
//                        }
//                        else
//                        {
//                            buffer.append(a + "   " + itemname + "     " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
//                        }*/
//                        if (a >= 10) {
//                            buffer.append(a + "  " + itemname + "     " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
//                        }
//                        else
//                        {
//                            buffer.append(a + "   " + itemname + "     " + res2.getString(4) + "    " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
//                        }
//                    }
//
//                    a++;
//                }
//                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
//                m_AemPrinter.print(String.valueOf(buffer));
//                m_AemPrinter.setFontType(AEMPrinter.FONT_001);
//                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                double ddata = Double.parseDouble(tv_total_amt.getText().toString());
//                DecimalFormat f = new DecimalFormat("#####.00");
//                //System.out.println(f.format(dda));
//
//                //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
//                data = "                           TOTAL(Rs): " + f.format(ddata) + "\n";
//                m_AemPrinter.print(d);
//                m_AemPrinter.POS_Font_bold_ThreeInch();
//                m_AemPrinter.print(data);
//                //m_AemPrinter.setFontType(AEMPrinter.FONT_003);
//
//////            m_AemPrinter.print(d);
//                // data = "                 Thank you!             \n";
//                if (f1 == null || f1.length() == 0 || f1 == "") {
//                    f1 = null;
//                } else {
//
//                    data = "              " + f1 + "                     ";
//                    m_AemPrinter.print(data);
//                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//
//                }
//                if (f2 == null || f2.length() == 0 || f2 == "") {
//                    f2 = null;
//                } else {
//                    data = "          " + f2 + "                    ";
//                    m_AemPrinter.print(data);
//                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                }
//                if (f3 == null || f3.length() == 0 || f3 == "") {
//                    f3 = null;
//                } else {
//
//                    data = "          " + f3 + "                   ";
//                    m_AemPrinter.print(data);
//                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                }
//                if (f4 == null || f4.length() == 0 || f4 == "") {
//                    f4 = null;
//                } else {
//                    data = "            " + f4 + "                   ";
//                    m_AemPrinter.print(data);
//                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                }
//                if (f5 == null || f5.length() == 0 || f5 == "") {
//                    f5 = null;
//                } else {
//                    data = "            " + f5 + "                    ";
//                    m_AemPrinter.print(data);
//                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//                }
//                m_AemPrinter.setCarriageReturn();
//                m_AemPrinter.setCarriageReturn();
//                m_AemPrinter.setCarriageReturn();
//                m_AemPrinter.setCarriageReturn();
//
//                ModelSale.arr_item_rate.clear();
//                ModelSale.arr_item_tax.clear();
//                ModelSale.arry_item_name.clear();
//                ModelSale.arr_item_qty.clear();
//                ModelSale.arr_item_price.clear();
//                ModelSale.array_item_amt.clear();
//                ModelSale.arr_item_basicrate.clear();
//                itemArrayList.clear();
//
//
//                tv_total_amt.setText("0");
//                cost = 0;
//
//
//                cnt=1;
//
//
//                ids = db.getAllbillID();
//                autoinnc();
//
//                Message.message(getApplicationContext(), "Bill saved");
//
//            } catch (Exception ex) {
//                Toast.makeText(BillingScreenActivity.this, "Error:-" + ex.getMessage(), Toast.LENGTH_SHORT).show();
//            }
        }

        if(print_option.equals("2 inch"))
        {
            if(print_option.equals("2 inch")&&selectprinter_option.equals("Airbill")&& gst_option.equals("GST Disable"))
            {
                Toast.makeText(getApplicationContext(),""+tvbill.getText().toString(),Toast.LENGTH_LONG).show();
                if(billwithprint.equals("No")){
                    taxlistupdated.clear();

                    for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                Double.valueOf(tv_total_amt.getText().toString()),cid,lid,empid,
                                ""+tvbill.getText().toString(),payment_mode_id,payment_deatils,order_details,gst_option,
                                emp_code,ModelSale.arr_item_dis.get(i),ModelSale.arr_item_tax.get(i),
                                Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(),ModelSale.arr_item_qty.get(i).toString());
                    }


                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();


                    payment_deatils="";
                    order_details="";
                    str_set_cust_name="";
                    str_set_payment="";
                    payment_mode_id="";

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getApplicationContext(), "Bill saved");
                }

                else if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        taxlistupdated.clear();
                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        if (checkArray!=null && checkArray.size()!=0) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                //  totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                        Double value = ModelSale.arr_item_price.get(i);
                                        Log.v("TAG TOTAL Value", String.valueOf(value));
                                        totalPrice = totalPrice - value;
                                        Log.v("TAG TOTAL", String.valueOf(totalPrice));
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));

                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                } else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                            }
                        }else{
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                            }
                        }

                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else {
                            gst_data_blueprints_two_inch_gst_disable();
                        }
                    }
                }
                else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {

                    if(counter<=1) {
                        taxlistupdated.clear();

                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                        if(checkArray!=null && checkArray.size()!=0) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (ModelSale.arry_item_name.get(i).equals(checkArray.get(j))) {
                                        Double value = ModelSale.arr_item_price.get(i);
                                        Log.v("TAG TOTAL Value", String.valueOf(value));
                                        totalPrice = totalPrice - value;
                                        Log.v("TAG TOTAL", String.valueOf(totalPrice));
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid, empid, "" + tvbill.getText().toString(),
                                            payment_mode_id, payment_deatils, order_details,
                                            gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                } else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid,
                                            empid, "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                            gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                            }
                        }else{
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid,
                                        empid, "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                        gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());

                            }
                        }
                        //  totalPrice=0.0;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        if(counter==1)
                        {
                            counter++;
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else{
                                gst_data_blueprints_two_inch_gst_disable();;
                            }
                        }
                        if(counter>=2) {
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else {
                                RemoveMessageBox("gst_data_blueprints_two_inch_gst_disable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                            }
                        }
                    }
                }

            }
            if(print_option.equals("2 inch")&&selectprinter_option.equals("Airbill")&& gst_option.equals("GST Enable"))
            {
                if(billwithprint.equals("No")){
                    taxlistupdated.clear();

                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray != null && checkArray.size() != 0) {
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            Log.v("TAG", String.valueOf(checkArray.size()));
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))) {
                                    Double value = ModelSale.arr_item_price.get(i);
                                    Log.v("TAG TOTAL Value", String.valueOf(value));
                                    totalPrice = totalPrice - value;
                                    Log.v("TAG TOTAL", String.valueOf(totalPrice));
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        0.0, totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                        gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                            else {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                        ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        totalPrice, cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                        gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details,
                                    gst_option, emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                    }


                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();


                    payment_deatils="";
                    order_details="";
                    str_set_cust_name="";
                    str_set_payment="";
                    payment_mode_id="";

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);


                    ids = db.getAllbillID();
                    autoinnc();

                    Message.message(getApplicationContext(), "Bill saved");
                }

                else if(billwithprint.equals("Yes")&&multipleprint.equals("No")){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    {
                        taxlistupdated.clear();

                        Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());;
                        if (checkArray != null && checkArray.size() != 0) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                // totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                                Log.v("TAG Size", String.valueOf(checkArray.size()));
                                for (int j = 0; j < checkArray.size(); j++) {
                                    if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                                        Double value = ModelSale.arr_item_price.get(i);
                                        Log.v("TAG TOTAL Value", String.valueOf(value));
                                        totalPrice = totalPrice - value;
                                        Log.v("TAG TOTAL", String.valueOf(totalPrice));
                                    }
                                }
                            }
                            Log.v("TAGGF", String.valueOf(totalPrice));

                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                            str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0,totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                                else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                            str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                            }
                        }else {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name,
                                        str_set_payment, ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                        Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                        Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                        Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                        "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                        emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                        Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                            }
                        }
                        if(ModelSale.arry_item_name.size()==0){
                            Message.message(getApplicationContext(),"Please select items");
                        }
                        else {
                            gst_data_blueprints_two_inch_gst_enable();
                        }
                    }

                }
                else if(billwithprint.equals("Yes")&&multipleprint.equals("Yes"))
                    Log.v("TAG","GST ENABLE AIR BILL");
                {   if(counter<=1)
                {
                    taxlistupdated.clear();
                    Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    if (checkArray != null && checkArray.size() != 0) {
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            //totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                            for (int j = 0; j < checkArray.size(); j++) {
                                if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                                    Double value = ModelSale.arr_item_price.get(i);
                                    totalPrice = totalPrice - value;
                                }
                            }
                        }
                        Log.v("TAGGF", String.valueOf(totalPrice));
                        if (db.checkBillNoExistOrNot(Integer.parseInt(tvbill.getText().toString()))) {
                            for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                                if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            0.0, totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                                else {
                                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                            totalPrice, cid, lid, empid,
                                            "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                            emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                            Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                                    taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                                }
                            }
                        }
                    }else{
                        for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                            db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                    ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                    Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                    Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                    Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid,
                                    "" + tvbill.getText().toString(), payment_mode_id, payment_deatils, order_details, gst_option,
                                    emp_code, ModelSale.arr_item_dis.get(i), ModelSale.arr_item_tax.get(i),
                                    Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                            taxlistupdated.add(ModelSale.arr_item_tax.get(i));
                            db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        }
                    }

                }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        if(counter==1)
                        {
                            counter++;
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else{
                                gst_data_blueprints_two_inch_gst_enable();

                            }


                        }
                        if(counter>=2) {
                            if(ModelSale.arry_item_name.size()==0){
                                Message.message(getApplicationContext(),"Please select items");
                            }
                            else {
                                RemoveMessageBox("gst_data_blueprints_two_inch_gst_enable", "Reprinting", "Do you want bill again ?", tvbill.getText().toString() + "?");
                            }
                        }
                    }
                }
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void gst_data_blueprints_three_inch_gst_enable()
    {
        if (m_AemPrinter == null) {

            try {



                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                        BluetoothAdapter.getDefaultAdapter().enable();

                        try {
                            Thread.sleep(30L);
                        } catch (InterruptedException var5) {
                            var5.printStackTrace();
                        }
                    }
                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                  //m_AemPrinter.print("On async connected");

                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Printer not connected please connect to printer");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (print_option.equals("3 inch") && selectprinter_option.equals("Airbill") && gst_option.equals("GST Enable")) {


//            res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                    res1 = db.getAllHeaderFooter();
                    String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                    while (res1.moveToNext()) {

                        h1 = res1.getString(1);
                        h1size = res1.getString(2);
                        h2 = res1.getString(3);
                        h2size = res1.getString(4);
                        h3 = res1.getString(5);
                        h3size = res1.getString(6);
                        h4 = res1.getString(7);
                        h4size = res1.getString(8);
                        h5 = res1.getString(9);
                        h5size = res1.getString(10);

                        f1 = res1.getString(11);
                        f1size = res1.getString(12);
                        f2 = res1.getString(13);
                        f2size = res1.getString(14);
                        f3 = res1.getString(15);
                        f3size = res1.getString(16);
                        f4 = res1.getString(17);
                        f4size = res1.getString(18);
                        f5 = res1.getString(19);
                        f5size = res1.getString(20);
                    }

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";


                    try {
//            m_AemPrinter.print(data);
//            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
//            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            m_AemPrinter.print(d);
                        if (h1 == null || h1.length() == 0 || h1 == "") {
                            h1 = null;
                        } else {
                            if (h1.length() < h2.length()) {
                                data = "                " + h1 + "\n";
                            } else {
                                data = "               " + h1 + "\n";
                            }
                            try {
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                            }catch (Exception ex)
                            {
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");

                                    m_AemPrinter.print(data);
                                    m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                                }catch (Exception e){
                                    Message.message(getApplicationContext(), "connecting to printer");
                                    try {
                                        m_AemScrybeDevice = new AEMScrybeDevice(this);
                                        m_AemScrybeDevice.disConnectPrinter();
                                        m_AemScrybeDevice.pairPrinter(value);
                                        m_AemScrybeDevice.connectToPrinter(value);
                                        m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                        Message.message(getApplicationContext(), "connected");
                                        m_AemPrinter.print(data);
                                        m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);


                                    }catch (Exception e1){
                                        Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                    }
                                }

                            }
                        }

                        if (h2 == null || h2.length() == 0 || h2 == "") {
                            h2 = null;
                        } else {
                            if (h2.length() > 16) {
                                data = "           " + h2 + "\n";
                            } else {
                                data = "                 " + h2 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h3 == null || h3.length() == 0 || h3 == "") {
                            h3 = null;
                        } else {
                            if (h3.length() > 16) {
                                data = "           " + h3 + "\n";
                            } else {
                                data = "                " + h3 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h4 == null || h4.length() == 0 || h4 == "") {
                            h4 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "           " + h4 + "\n";
                            } else {
                                data = "                " + h4 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h5 == null || h5.length() == 0 || h5 == "") {
                            h5 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "          " + h5 + "\n";
                            } else {
                                data = "               " + h5 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        data = "\n                 BILL No:" + tvbill.getText().toString() + "\n";
                        try {
                            m_AemPrinter.print(data);
                        }catch (Exception ex) {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);

                            }catch (Exception e){
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");
                                    m_AemPrinter.print(data);
                                }catch (Exception e1){
                                    Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                }
                            }

                        }

                        data = "Date:-" + dateToStr + "\n";
                        m_AemPrinter.print(data);
                        if (str_set_cust_name == "" || str_set_cust_name == null) {
                            str_set_cust_name = "";
                            data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";


                        } else {
                            data = "Customer Name:- " + str_set_cust_name + "\n";
                            m_AemPrinter.print(data);
                            data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                        }
//                m_AemPrinter.print(d);


                        m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        String totalamtrate = "";
                        int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                        StringBuffer buffer = new StringBuffer();

                        String totalamtrategst = "";
                        int a1 = 1, j = 0;
                        Double itemtax = 0.0;
                        int[] positions = new int[ModelSale.arry_item_name.size()];
                        StringBuffer buffer1 = new StringBuffer();
                        Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                        Double oldtax = 0.0;
                        int l = 0;
                        res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                        while (res2.moveToNext()) {

                            String itemname = res2.getString(3).toString();
                            String largname = "";
                            String qty = res2.getString(4).toString();
                            String rate = res2.getString(5).toString();
                            String discount = res2.getString(7).toString();
                            String tax = res2.getString(8).toString();
                            String Basicrate=res2.getString(9);

                            String amount = res2.getString(6).toString();

                            if (amount.length() > 6) {
                                amount = amount.substring(0, 6);

                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 6) {
                                    l1 = 6 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (qty.length() >=5) {
                                qty = qty.substring(0, 5);

                            } else {
                                int length = qty.length();
                                int l1 = 0;
                                if (length < 5) {
                                    l1 = 5 - length;
                                    qty = String.format(qty + "%" + (l1) + "s", "");
                                }
                            }
                            if (Basicrate.length() > 5) {
                                Basicrate = Basicrate.substring(0, 5);

                            } else {
                                int length = Basicrate.length();
                                int l1 = 0;
                                if (length < 5) {
                                    l1 = 5 - length;
                                    Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                                }
                            }
                            if (discount.length() > 2) {
                                discount = discount.substring(0, 2);

                            } else {
                                int length = discount.length();
                                int l1 = 0;
                                if (length <2) {
                                    l1 = 2 - length;
                                    discount = String.format(discount + "%" + (l1) + "s", "");
                                }
                            }
                            if (tax.length() > 2) {
                                tax = tax.substring(0, 2);

                            } else {
                                int length = tax.length();
                                int l1 = 0;
                                if (length < 2) {
                                    l1 = 2 - length;
                                    tax = String.format(tax + "%" + (l1) + "s", "");
                                }
                            }

                            if (itemname.length() > 15) {
                                if(a>=10) {

//                                if (res2.getString(4).length() >= 3) {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "   " + res2.getString(5) + "  " + res2.getString(7) + "  " + res2.getString(8) + "  " + amount + "\n"); //res2.getString(6)+"\n");
//                                } else {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "    " + res2.getString(5) + "    " + res2.getString(7) + "   " + res2.getString(8) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                                }
                                    buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                                }
                                else
                                {
                                    buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                                }

                            }
                            else {

                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if(a>=10) {

                                    buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                                }
                                else
                                {
                                    buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                                }
                            }

                            a++;

                            discaltotal=discaltotal+Double.parseDouble(Basicrate);
                            discal=discaltotal*(Double.parseDouble(discount)/100);
                            totaldiscount=totaldiscount+discal;
                            discal=0.0;
                            discaltotal=0.0;
                            subtotal=subtotal+Double.parseDouble(Basicrate);
                        }
                        DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                        Double grandgst = 0.0;
                        int count1 = 0;
                        Double cgst = 0.0, sgst = 0.0;
                        List<String> result = new ArrayList<>();

                        Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                        List<String> taxlist1 = new ArrayList<>();
                        final List<String> itemnamelist = new ArrayList<>();
                        List<Double> qtylist = new ArrayList<>();
                        List<String> ratelist = new ArrayList<>();
                        List<Double> amtlist = new ArrayList<>();
                        List<String> indexlist1 = new ArrayList<>();
                        List<String> disclist = new ArrayList<>();
                        List<String> disclistrate = new ArrayList<>();
                        for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                            itemtax = Double.parseDouble(taxlistupdated.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));
                                String stringName = c.getString(0);
                                if(checkArray==null || checkArray.size()==0){
                                    Log.v("TAG","NO COMPLEMENT");
                                    if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                        if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        } else {
                                            itemnamelist.add(c.getString(0));
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                }
                                else{
                                    if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                        Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            //   for(int q=0;q<checkArray.size();q++) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","1 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                //ratelist.add(k,"0.0");
                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","1 OUT");
                                            } else {
                                                Log.v("TAG","2 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAG","2 OUT");
                                            }
                                        }
                                        else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","3 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","3 OUT");
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAG","4 OUT");
                                            }
                                        }
                                        else {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","4 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","4 OUT");
                                            } else {
                                                Log.v("TAGG", "CHECKIN4");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAGG", "CHECKOUT4");
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        taxlist1.clear();
                        for (int k = 0; k < itemnamelist.size(); k++) {
                            DecimalFormat formats = new DecimalFormat("#####.00");
                            taxlist1.clear();
                            int count = itemnamelist.size();
                            String itemname = itemnamelist.get(k);
                            String largname = "";
                            Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                            String rate=disclistrate.get(k);
                            String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                            itemtax = Double.valueOf(indexlist1.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));

                            }
                            cgst = (itemtax / 2);
                            sgst = cgst;
                            if (amount.length() > 4) {
                                amount = amount.substring(0, 4);

                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 4) {
                                    l1 = 4 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                result = taxlist1.stream().filter(aObject -> {
                                    return itemnamelist.contains(aObject);
                                }).collect(Collectors.toList());
                            }
                            i("dataname", String.valueOf(result));
                            int resultcount = result.size();

                            if (itemname.length() > 15) {


                                if (rate.length() == 3) {

                                    count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                        grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    }


                                } else {

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }

                            } else {

                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if (rate.length() == 3) {
                                    count1++;

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
//
                                } else {

                                    count1++;

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }

                            j++;
                            finalgst = totalwithgst + finalgst;
                            totalwithgst = 0.0;
                            cgst = 0.0;
                            sgst = 0.0;


                        }



                        Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;

                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        //m_AemPrinter.print(String.valueOf(buffer));
                        m_AemPrinter.print(String.valueOf(buffer));
                        m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        double ddata = Double.parseDouble(tv_total_amt.getText().toString());
//                    Double subtotal=ddata-totaldiscount-finalgst;
                        DecimalFormat f = new DecimalFormat("#####.00");
                        //System.out.println(f.format(dda));

                        //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                        d = "________________________________________________\n";
                        data = "                       Sub TOTAL(Rs): " + f.format(subtotal) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

//                    data = "                      -Dis(%)      : " + f.format(totaldiscount) + "\n";
                        if(totaldiscount==0.0){}else {
                            data = "                      -Dis(Rs)      : " + f.format(totaldiscount) + "\n";
                            d = "                     __________________________";
                            m_AemPrinter.POS_Font_bold_ThreeInch();
                            m_AemPrinter.print(data);
                            m_AemPrinter.print(d);


                            data = "                                      " + f.format((subtotal - totaldiscount));
                            m_AemPrinter.print(data);
                        }if(finalgst==0.0){}else{
//                    data = "                      +GST(Rs): " + f.format(finalgst) + "\n";
                            data = "\n                      +GST(Rs)      : " + f.format(finalgst) + "\n";

                            m_AemPrinter.print(data);
                            m_AemPrinter.print(d);
                        }


                        data = "                       Net Total(Rs): " + f.format(ddata) + "\n";


















                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);
//                    if(finalgst==0.0){}
//                    else {
//                        data = "\n               GST Details \n";
//                        m_AemPrinter.print(data);
//                        data = "    GST%       CGST+SGST       Total \n";
//                        m_AemPrinter.print(data);
//
//                        m_AemPrinter.POS_Font_bold_ThreeInch();
//                        m_AemPrinter.print(String.valueOf(buffer1));
//                    }
                        //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                        // data = "                 Thank you!             \n";
                        if (f1 == null || f1.length() == 0 || f1 == "") {
                            f1 = null;
                        } else {

                            data = "           " + f1 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                        }
                        if (f2 == null || f2.length() == 0 || f2 == "") {
                            f2 = null;
                        } else {
                            data = "           " + f2 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f3 == null || f3.length() == 0 || f3 == "") {
                            f3 = null;
                        } else {

                            data = "            " + f3 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f4 == null || f4.length() == 0 || f4 == "") {
                            f4 = null;
                        } else {
                            data = "           " + f4 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f5 == null || f5.length() == 0 || f5 == "") {
                            f5 = null;
                        } else {
                            data = "           " + f5 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();


                        ModelSale.arr_item_rate.clear();
                        ModelSale.arry_item_name.clear();
                        ModelSale.arr_item_qty.clear();
                        ModelSale.arr_item_price.clear();
                        ModelSale.arr_item_basicrate.clear();
                        ModelSale.arr_item_dis_rate.clear();
                        ModelSale.arr_item_dis.clear();
                        ModelSale.arr_item_tax.clear();
                        ModelSale.array_item_amt.clear(); checkArray.clear();

                        cost = 0;
                        tv_total_amt.setText("0");
                        cnt = 1;
                        itemArrayList.clear();
                        myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                        // Set the adapter


//            runOnUiThread(updateTextView);

//            finish();
//            startActivity(getIntent());
                        // this.recreate();

                        ids = db.getAllbillID();
                        autoinnc();

                        Message.message(getApplicationContext(), "Bill saved");

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                }
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (print_option.equals("3 inch") && selectprinter_option.equals("Airbill") && gst_option.equals("GST Enable")) {


//            res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                    res1 = db.getAllHeaderFooter();
                    String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                    while (res1.moveToNext()) {

                        h1 = res1.getString(1);
                        h1size = res1.getString(2);
                        h2 = res1.getString(3);
                        h2size = res1.getString(4);
                        h3 = res1.getString(5);
                        h3size = res1.getString(6);
                        h4 = res1.getString(7);
                        h4size = res1.getString(8);
                        h5 = res1.getString(9);
                        h5size = res1.getString(10);

                        f1 = res1.getString(11);
                        f1size = res1.getString(12);
                        f2 = res1.getString(13);
                        f2size = res1.getString(14);
                        f3 = res1.getString(15);
                        f3size = res1.getString(16);
                        f4 = res1.getString(17);
                        f4size = res1.getString(18);
                        f5 = res1.getString(19);
                        f5size = res1.getString(20);
                    }

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";


                    try {
//            m_AemPrinter.print(data);
//            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
//            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
//            m_AemPrinter.print(d);
                        if (h1 == null || h1.length() == 0 || h1 == "") {
                            h1 = null;
                        } else {
                            if (h1.length() < h2.length()) {
                                data = "               " + h1 + "\n";
                            } else {
                                data = "              " + h1 + "\n";
                            }
                            try {
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                            }catch (Exception ex)
                            {
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");

                                    m_AemPrinter.print(data);
                                    m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                                }catch (Exception e){
                                    Message.message(getApplicationContext(), "connecting to printer");
                                    try {
                                        m_AemScrybeDevice = new AEMScrybeDevice(this);
                                        m_AemScrybeDevice.disConnectPrinter();
                                        m_AemScrybeDevice.pairPrinter(value);
                                        m_AemScrybeDevice.connectToPrinter(value);
                                        m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                        Message.message(getApplicationContext(), "connected");
                                        m_AemPrinter.print(data);
                                        m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);


                                    }catch (Exception e1){
                                        Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                    }
                                }

                            }
                        }

                        if (h2 == null || h2.length() == 0 || h2 == "") {
                            h2 = null;
                        } else {
                            if (h2.length() > 16) {
                                data = "             " + h2 + "\n";
                            } else {
                                data = "                " + h2 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h3 == null || h3.length() == 0 || h3 == "") {
                            h3 = null;
                        } else {
                            if (h3.length() > 16) {
                                data = "     " + h3 + "\n";
                            } else {
                                data = "           " + h3 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h4 == null || h4.length() == 0 || h4 == "") {
                            h4 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "            " + h4 + "\n";
                            } else {
                                data = "                 " + h4 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h5 == null || h5.length() == 0 || h5 == "") {
                            h5 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "            " + h5 + "\n";
                            } else {
                                data = "                 " + h5 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        data = "\n                 BILL No:" + tvbill.getText().toString() + "\n";
                        try {
                            m_AemPrinter.print(data);
                        }catch (Exception ex) {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);

                            }catch (Exception e){
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");
                                    m_AemPrinter.print(data);
                                }catch (Exception e1){
                                    Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                }
                            }

                        }

                        data = "Date:-" + dateToStr + "\n";
                        m_AemPrinter.print(data);
                        if (str_set_cust_name == "" || str_set_cust_name == null) {
                            str_set_cust_name = "";
                            data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";


                        } else {
                            data = "Customer Name:- " + str_set_cust_name + "\n";
                            m_AemPrinter.print(data);
                            data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                        }
//                m_AemPrinter.print(d);


                        m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        String totalamtrate = "";
                        int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                        StringBuffer buffer = new StringBuffer();

                        String totalamtrategst = "";
                        int a1 = 1, j = 0;
                        Double itemtax = 0.0;
                        int[] positions = new int[ModelSale.arry_item_name.size()];
                        StringBuffer buffer1 = new StringBuffer();
                        Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                        Double oldtax = 0.0;
                        int l = 0;
                        res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                        while (res2.moveToNext()) {

                            String itemname = res2.getString(3).toString();
                            String largname = "";
                            String qty = res2.getString(4).toString();
                            String rate = res2.getString(5).toString();
                            String discount = res2.getString(7).toString();
                            String tax = res2.getString(8).toString();
                            String Basicrate=res2.getString(9);

                            String amount = res2.getString(6).toString();

                            if (amount.length() > 6) {
                                amount = amount.substring(0, 6);

                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 6) {
                                    l1 = 6 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (qty.length() >=5) {
                                qty = qty.substring(0, 5);

                            } else {
                                int length = qty.length();
                                int l1 = 0;
                                if (length < 5) {
                                    l1 = 5 - length;
                                    qty = String.format(qty + "%" + (l1) + "s", "");
                                }
                            }
                            if (Basicrate.length() > 5) {
                                Basicrate = Basicrate.substring(0, 5);

                            } else {
                                int length = Basicrate.length();
                                int l1 = 0;
                                if (length < 5) {
                                    l1 = 5 - length;
                                    Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                                }
                            }
                            if (discount.length() > 2) {
                                discount = discount.substring(0, 2);

                            } else {
                                int length = discount.length();
                                int l1 = 0;
                                if (length <2) {
                                    l1 = 2 - length;
                                    discount = String.format(discount + "%" + (l1) + "s", "");
                                }
                            }
                            if (tax.length() > 2) {
                                tax = tax.substring(0, 2);

                            } else {
                                int length = tax.length();
                                int l1 = 0;
                                if (length < 2) {
                                    l1 = 2 - length;
                                    tax = String.format(tax + "%" + (l1) + "s", "");
                                }
                            }

                            if (itemname.length() > 15) {
                                if(a>=10) {

//                                if (res2.getString(4).length() >= 3) {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "   " + res2.getString(5) + "  " + res2.getString(7) + "  " + res2.getString(8) + "  " + amount + "\n"); //res2.getString(6)+"\n");
//                                } else {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "    " + res2.getString(5) + "    " + res2.getString(7) + "   " + res2.getString(8) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                                }
                                    buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                                }
                                else
                                {
                                    buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                                }

                            }
                            else {

                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if(a>=10) {

                                    buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                                }
                                else
                                {
                                    buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                                }
                            }

                            a++;

                            discaltotal=discaltotal+Double.parseDouble(Basicrate);
                            discal=discaltotal*(Double.parseDouble(discount)/100);
                            totaldiscount=totaldiscount+discal;
                            discal=0.0;
                            discaltotal=0.0;
                            subtotal=subtotal+Double.parseDouble(Basicrate);
                        }
                        DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                        Double grandgst = 0.0;
                        int count1 = 0;
                        Double cgst = 0.0, sgst = 0.0;
                        List<String> result = new ArrayList<>();

                        Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                        List<String> taxlist1 = new ArrayList<>();
                        final List<String> itemnamelist = new ArrayList<>();
                        List<Double> qtylist = new ArrayList<>();
                        List<String> ratelist = new ArrayList<>();
                        List<Double> amtlist = new ArrayList<>();
                        List<String> indexlist1 = new ArrayList<>();
                        List<String> disclist = new ArrayList<>();
                        List<String> disclistrate = new ArrayList<>();
                        for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                            itemtax = Double.parseDouble(taxlistupdated.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));
                                String stringName = c.getString(0);
                                if(checkArray==null || checkArray.size()==0){
                                    Log.v("TAG","NO COMPLEMENT");
                                    if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                        if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        } else {
                                            itemnamelist.add(c.getString(0));
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                }
                                else{
                                    if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                        Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            //   for(int q=0;q<checkArray.size();q++) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","1 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                //ratelist.add(k,"0.0");
                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","1 OUT");
                                            } else {
                                                Log.v("TAG","2 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAG","2 OUT");
                                            }
                                        }
                                        else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","3 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","3 OUT");
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAG","4 OUT");
                                            }
                                        }
                                        else {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                Log.v("TAG","4 IN");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                if (counter<=2) {
                                                    double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                    double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                    total_val = current - total_val;
                                                    tv_total_amt.setText(String.valueOf(total_val));
                                                    Log.v("TAG", String.valueOf(total_val));
                                                }
                                                Log.v("TAG","4 OUT");
                                            } else {
                                                Log.v("TAGG", "CHECKIN4");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAGG", "CHECKOUT4");
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        taxlist1.clear();
                        for (int k = 0; k < itemnamelist.size(); k++) {
                            DecimalFormat formats = new DecimalFormat("#####.00");
                            taxlist1.clear();
                            int count = itemnamelist.size();
                            String itemname = itemnamelist.get(k);
                            String largname = "";
                            Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                            String rate=disclistrate.get(k);
                            String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                            itemtax = Double.valueOf(indexlist1.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));

                            }
                            cgst = (itemtax / 2);
                            sgst = cgst;
                            if (amount.length() > 4) {
                                amount = amount.substring(0, 4);

                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 4) {
                                    l1 = 4 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                result = taxlist1.stream().filter(aObject -> {
                                    return itemnamelist.contains(aObject);
                                }).collect(Collectors.toList());
                            }
                            i("dataname", String.valueOf(result));
                            int resultcount = result.size();

                            if (itemname.length() > 15) {


                                if (rate.length() == 3) {

                                    count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                        grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    }


                                } else {

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }

                            } else {

                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if (rate.length() == 3) {
                                    count1++;

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
//
                                } else {

                                    count1++;

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                                buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                            } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }


                                    } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }

                            j++;
                            finalgst = totalwithgst + finalgst;
                            totalwithgst = 0.0;
                            cgst = 0.0;
                            sgst = 0.0;


                        }



                        Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;

                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        //m_AemPrinter.print(String.valueOf(buffer));
                        m_AemPrinter.print(String.valueOf(buffer));
                        m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        double ddata = Double.parseDouble(tv_total_amt.getText().toString());
//                    Double subtotal=ddata-totaldiscount-finalgst;
                        DecimalFormat f = new DecimalFormat("#####.00");
                        //System.out.println(f.format(dda));

                        //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                        d = "________________________________________________\n";
                        data = "                       Sub TOTAL(Rs): " + f.format(subtotal) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);
                        if(totaldiscount==0.0){}else {
                            data = "                      -Dis(Rs)      : " + f.format(totaldiscount) + "\n";
                            d = "                     __________________________";
                            m_AemPrinter.POS_Font_bold_ThreeInch();
                            m_AemPrinter.print(data);
                            m_AemPrinter.print(d);


                            data = "                                      " + f.format((subtotal - totaldiscount));
                            m_AemPrinter.print(data);
                        }if(finalgst==0.0){}else{
//                    data = "                      +GST(Rs): " + f.format(finalgst) + "\n";
                            data = "\n                      +GST(Rs)      : " + f.format(finalgst) + "\n";

                            m_AemPrinter.print(data);
                            m_AemPrinter.print(d);
                        }


                        data = "                       Net Total(Rs): " + f.format(ddata) + "\n";

                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);
//                    if(finalgst==0.0){}
//                    else {
//                        data = "\n               GST Details \n";
//                        m_AemPrinter.print(data);
//                        data = "    GST%       CGST+SGST       Total \n";
//                        m_AemPrinter.print(data);
//
//                        m_AemPrinter.POS_Font_bold_ThreeInch();
//                        m_AemPrinter.print(String.valueOf(buffer1));
//                    }
                        //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                        // data = "                 Thank you!             \n";
                        if (f1 == null || f1.length() == 0 || f1 == "") {
                            f1 = null;
                        } else {

                            data = "             " + f1 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                        }
                        if (f2 == null || f2.length() == 0 || f2 == "") {
                            f2 = null;
                        } else {
                            data = "             " + f2 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f3 == null || f3.length() == 0 || f3 == "") {
                            f3 = null;
                        } else {

                            data = "            " + f3 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f4 == null || f4.length() == 0 || f4 == "") {
                            f4 = null;
                        } else {
                            data = "              " + f4 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f5 == null || f5.length() == 0 || f5 == "") {
                            f5 = null;
                        } else {
                            data = "             " + f5 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();


                        if (counter >= 2) {
                        } else {

                            ModelSale.arr_item_rate.clear();
                            ModelSale.arry_item_name.clear();
                            ModelSale.arr_item_qty.clear();
                            ModelSale.arr_item_price.clear();
                            ModelSale.arr_item_basicrate.clear();
                            ModelSale.arr_item_dis_rate.clear();
                            ModelSale.arr_item_dis.clear();
                            ModelSale.arr_item_tax.clear();
                            ModelSale.array_item_amt.clear(); checkArray.clear();

                            cost = 0;
                            tv_total_amt.setText("0");
                            cnt = 1;
                            itemArrayList.clear();
                            myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                            // Set the adapter


//            runOnUiThread(updateTextView);

//            finish();
//            startActivity(getIntent());
                            // this.recreate();

                            ids = db.getAllbillID();
                            autoinnc();
                        }
                        Message.message(getApplicationContext(), "Bill saved");

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                }
            }
        }

    }
    public void gst_data_blueprints_three_inch_gst_disable()
    {
        if (m_AemPrinter == null) {

            try {



                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                        BluetoothAdapter.getDefaultAdapter().enable();

                        try {
                            Thread.sleep(30L);
                        } catch (InterruptedException var5) {
                            var5.printStackTrace();
                        }
                    }
                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
//                  //m_AemPrinter.print("On async connected");

                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Printer not connected please connect to printer");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {



                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {

                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }


                try {
//
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";

                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "                 " + h1 + "\n";
                        } else {
                            data = "                " + h1 + "\n";
                        }
                        try {
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }catch (Exception ex)
                        {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                            }catch (Exception e){
                                Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                            }
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "             " + h2 + "\n";
                        } else {
                            data = "                 " + h2 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "            " + h3 + "\n";
                        } else {
                            data = "                 " + h3 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "          " + h4 + "\n";
                        } else {
                            data = "                " + h4 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "           " + h5 + "\n";
                        } else {
                            data = "                  " + h5 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
//                    if (res2.getCount() == 0) {
//
//
//                    }
                    data = "\nCash Memo    BILL No:" + tvbill.getText().toString() + "\n";
                    try {
                        m_AemPrinter.print(data);
                    }catch (Exception ex) {
                        Message.message(getApplicationContext(), "connecting to printer");
                        try {
                            m_AemScrybeDevice = new AEMScrybeDevice(this);
                            m_AemScrybeDevice.disConnectPrinter();
                            m_AemScrybeDevice.pairPrinter(value);
                            m_AemScrybeDevice.connectToPrinter(value);
                            m_cardReader = m_AemScrybeDevice.getCardReader(this);
                            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                            Message.message(getApplicationContext(), "connected");
                            m_AemPrinter.print(data);
                        }catch (Exception e){
                            Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                        }

                    }
                    data = "Date:-" + dateToStr + "\n";
                    m_AemPrinter.print(data);
         /*       data="Customer Name:- "+str_set_cust_name+"\n";
                m_AemPrinter.print(data);*/
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";


                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        m_AemPrinter.print(data);
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                    }
//                m_AemPrinter.print(d);


                    m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    String totalamtrate = "";
                    int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                    StringBuffer buffer = new StringBuffer();

                    String totalamtrategst = "";
                    int a1 = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer1 = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                    Double oldtax = 0.0;
                    int l = 0;
                    res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                    while (res2.moveToNext()) {

                        String itemname = res2.getString(3).toString();
                        String largname = "";
                        String qty = res2.getString(4).toString();
                        String rate = res2.getString(5).toString();
                        String discount = res2.getString(7).toString();
                        String tax = res2.getString(8).toString();
                        String Basicrate=res2.getString(9);

                        String amount = res2.getString(6).toString();

                        if (amount.length() > 6) {
                            amount = amount.substring(0, 6);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 6) {
                                l1 = 6 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (qty.length() >5) {
                            qty = qty.substring(0, 5);

                        } else {
                            int length = qty.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                qty = String.format(qty + "%" + (l1) + "s", "");
                            }
                        }
                        if (Basicrate.length() > 5) {
                            Basicrate = Basicrate.substring(0, 5);

                        } else {
                            int length = Basicrate.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                            }
                        }
                        if (discount.length() > 2) {
                            discount = discount.substring(0, 2);

                        } else {
                            int length = discount.length();
                            int l1 = 0;
                            if (length <2) {
                                l1 = 2 - length;
                                discount = String.format(discount + "%" + (l1) + "s", "");
                            }
                        }
                        if (tax.length() > 2) {
                            tax = tax.substring(0, 2);

                        } else {
                            int length = tax.length();
                            int l1 = 0;
                            if (length < 2) {
                                l1 = 2 - length;
                                tax = String.format(tax + "%" + (l1) + "s", "");
                            }
                        }

                        if (itemname.length() > 15) {
                            if(a>=10) {

//                                if (res2.getString(4).length() >= 3) {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "   " + res2.getString(5) + "  " + res2.getString(7) + "  " + res2.getString(8) + "  " + amount + "\n"); //res2.getString(6)+"\n");
//                                } else {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "    " + res2.getString(5) + "    " + res2.getString(7) + "   " + res2.getString(8) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                                }
                                buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }
                            else
                            {
                                buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }

                        }
                        else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if(a>=10) {

                                buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                            }
                            else
                            {
                                buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                            }
                        }

                        a++;

                        discaltotal=discaltotal+Double.parseDouble(Basicrate);
                        discal=discaltotal*(Double.parseDouble(discount)/100);
                        totaldiscount=totaldiscount+discal;
                        discal=0.0;
                        discaltotal=0.0;
                        subtotal=subtotal+Double.parseDouble(Basicrate);
                    }
                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;
                    List<String> result = new ArrayList<>();

                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();
                    List<String> disclist = new ArrayList<>();
                    List<String> disclistrate = new ArrayList<>();
                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){
                                Log.v("TAG","NO COMPLEMENT");
                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else{
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","1 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //ratelist.add(k,"0.0");
                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","1 OUT");
                                        } else {
                                            Log.v("TAG","2 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","2 OUT");
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","3 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","3 OUT");
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","4 OUT");
                                        }
                                    }
                                    else {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","4 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","4 OUT");
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }


                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        DecimalFormat formats = new DecimalFormat("#####.00");
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                        String rate=disclistrate.get(k);
                        String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                        itemtax = Double.valueOf(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));

                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            result = taxlist1.stream().filter(aObject -> {
                                return itemnamelist.contains(aObject);
                            }).collect(Collectors.toList());
                        }
                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();

                        if (itemname.length() > 15) {


                            if (rate.length() == 3) {

                                count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                    grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                }


                            } else {

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }

                        } else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
//
                            } else {

                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }

                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                        cgst = 0.0;
                        sgst = 0.0;


                    }



                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;

                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    //m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
//                    Double subtotal=ddata-totaldiscount-finalgst;
                    DecimalFormat f = new DecimalFormat("#####.00");

                    //System.out.println(f.format(dda));

                    //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                    d = "________________________________________________\n";
                    data = "                       Sub TOTAL(Rs): " + f.format(subtotal) + "\n";
                    m_AemPrinter.print(d);
                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
//                    data = "                      -Dis(%)      : " + f.format(totaldiscount) + "\n";
                    if(totaldiscount==0.0){}else {
                        data = "                      -Dis(Rs)      : " + f.format(totaldiscount) + "\n";
                        d = "                     __________________________";
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);
                        m_AemPrinter.print(d);


                        data = "                                      " + f.format((subtotal - totaldiscount));
                        m_AemPrinter.print(data);
                    }if(finalgst==0.0){}else{
//                    data = "                      +GST(Rs): " + f.format(finalgst) + "\n";
                        data = "\n                      +GST(Rs)      : " + f.format(finalgst) + "\n";

                        m_AemPrinter.print(data);
                        m_AemPrinter.print(d);
                    }


                    data = "                       Net Total(Rs): " + f.format(ddata) + "\n";

                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
//                    if(finalgst==0.0){}
//                    else {
//                        data = "\n               GST Details \n";
//                        m_AemPrinter.print(data);
//                        data = "    GST%       CGST+SGST       Total \n";
//                        m_AemPrinter.print(data);
//
//                        m_AemPrinter.POS_Font_bold_ThreeInch();
//                        m_AemPrinter.print(String.valueOf(buffer1));
//                    }
                    //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                    // data = "                 Thank you!             \n";
                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {

                        data = "           " + f1 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "           " + f2 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "           " + f3 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "           " + f4 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "            " + f5 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }

                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();



                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                    str_set_cust_name = "";

                    ids = db.getAllbillID();
                    autoinnc();


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));


                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {

                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }


                try {
//
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";

                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "                " + h1 + "\n";
                        } else {
                            data = "               " + h1 + "\n";
                        }
                        try {
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }catch (Exception ex)
                        {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                            }catch (Exception e){
                                Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                            }
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "            " + h2 + "\n";
                        } else {
                            data = "                " + h2 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "           " + h3 + "\n";
                        } else {
                            data = "                 " + h3 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "            " + h4 + "\n";
                        } else {
                            data = "                " + h4 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "              " + h5 + "\n";
                        } else {
                            data = "               " + h5 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (res2.getCount() == 0) {


                    }
                    data = "\nCash Memo    BILL No:" + tvbill.getText().toString() + "\n";
                    try {
                        m_AemPrinter.print(data);
                    }catch (Exception ex) {
                        Message.message(getApplicationContext(), "connecting to printer");
                        try {
                            m_AemScrybeDevice = new AEMScrybeDevice(this);
                            m_AemScrybeDevice.disConnectPrinter();
                            m_AemScrybeDevice.pairPrinter(value);
                            m_AemScrybeDevice.connectToPrinter(value);
                            m_cardReader = m_AemScrybeDevice.getCardReader(this);
                            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                            Message.message(getApplicationContext(), "connected");
                            m_AemPrinter.print(data);
                        }catch (Exception e){
                            Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                        }

                    }
                    data = "Date:-" + dateToStr + "\n";
                    m_AemPrinter.print(data);
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";


                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        m_AemPrinter.print(data);
                        data = "\nNo|   Item Name   | Qty | Rate |Dis|Tax| Amt  \n";

                    }
//                m_AemPrinter.print(d);


                    m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    String totalamtrate = "";
                    int a = 1;Double totaldiscount=0.0,discal=0.0,discaltotal=0.0;
                    StringBuffer buffer = new StringBuffer();

                    String totalamtrategst = "";
                    int a1 = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer1 = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0,subtotal=0.0;
                    Double oldtax = 0.0;
                    int l = 0;
                    res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
                    while (res2.moveToNext()) {

                        String itemname = res2.getString(3).toString();
                        String largname = "";
                        String qty = res2.getString(4).toString();
                        String rate = res2.getString(5).toString();
                        String discount = res2.getString(7).toString();
                        String tax = res2.getString(8).toString();
                        String Basicrate=res2.getString(9);

                        String amount = res2.getString(6).toString();

                        if (amount.length() > 6) {
                            amount = amount.substring(0, 6);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 6) {
                                l1 = 6 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (qty.length() >=5) {
                            qty = qty.substring(0, 5);

                        } else {
                            int length = qty.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                qty = String.format(qty + "%" + (l1) + "s", "");
                            }
                        }
                        if (Basicrate.length() > 5) {
                            Basicrate = Basicrate.substring(0, 5);

                        } else {
                            int length = Basicrate.length();
                            int l1 = 0;
                            if (length < 5) {
                                l1 = 5 - length;
                                Basicrate = String.format(Basicrate + "%" + (l1) + "s", "");
                            }
                        }
                        if (discount.length() > 2) {
                            discount = discount.substring(0, 2);

                        } else {
                            int length = discount.length();
                            int l1 = 0;
                            if (length <2) {
                                l1 = 2 - length;
                                discount = String.format(discount + "%" + (l1) + "s", "");
                            }
                        }
                        if (tax.length() > 2) {
                            tax = tax.substring(0, 2);

                        } else {
                            int length = tax.length();
                            int l1 = 0;
                            if (length < 2) {
                                l1 = 2 - length;
                                tax = String.format(tax + "%" + (l1) + "s", "");
                            }
                        }

                        if (itemname.length() > 15) {
                            if(a>=10) {

//                                if (res2.getString(4).length() >= 3) {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "   " + res2.getString(5) + "  " + res2.getString(7) + "  " + res2.getString(8) + "  " + amount + "\n"); //res2.getString(6)+"\n");
//                                } else {
//                                    buffer.append(a + "   " + itemname.substring(0, 15) + "  " + res2.getString(4) + "    " + res2.getString(5) + "    " + res2.getString(7) + "   " + res2.getString(8) + "   " + amount + "\n"); //res2.getString(6)+"\n");
//                                }
                                buffer.append(a + "   " + itemname.substring(0, 15) + "" + qty+"  "+ Basicrate+"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }
                            else
                            {
                                buffer.append(a + "    " + itemname.substring(0, 15) + "" + qty +"  "+ Basicrate +"   "+ discount +"  "+ tax +" "+ amount + "\n"); //res2.getString(6)+"\n");
                            }

                        }
                        else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if(a>=10) {

                                buffer.append(a + "   " + itemname + "" + qty + "  " +Basicrate + "   " + discount + "  " + tax + " " + amount + "\n");//res2.getString(6)+"\n");
                            }
                            else
                            {
                                buffer.append(a+"    "+itemname + "" + qty + "  " + Basicrate +"   "+discount+"  "+tax+ " " + amount + "\n");//res2.getString(6)+"\n");
                            }
                        }

                        a++;

                        discaltotal=discaltotal+Double.parseDouble(Basicrate);
                        discal=discaltotal*(Double.parseDouble(discount)/100);
                        totaldiscount=totaldiscount+discal;
                        discal=0.0;
                        discaltotal=0.0;
                        subtotal=subtotal+Double.parseDouble(Basicrate);
                    }
                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;
                    List<String> result = new ArrayList<>();

                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();
                    List<String> disclist = new ArrayList<>();
                    List<String> disclistrate = new ArrayList<>();
                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){
                                Log.v("TAG","NO COMPLEMENT");
                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else{
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","1 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //ratelist.add(k,"0.0");
                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","1 OUT");
                                        } else {
                                            Log.v("TAG","2 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","2 OUT");
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","3 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));

                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","3 OUT");
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAG","4 OUT");
                                        }
                                    }
                                    else {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            Log.v("TAG","4 IN");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            Log.v("TAG","4 OUT");
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclist.add(ModelSale.arr_item_dis.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            disclistrate.add(ModelSale.arr_item_dis_rate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        DecimalFormat formats = new DecimalFormat("#####.00");
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
//                        String rate = ratelist.get(k);
                        String rate=disclistrate.get(k);
                        String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                        itemtax = Double.valueOf(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));

                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            result = taxlist1.stream().filter(aObject -> {
                                return itemnamelist.contains(aObject);
                            }).collect(Collectors.toList());
                        }
                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();

                        if (itemname.length() > 15) {


                            if (rate.length() == 3) {

                                count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");

                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                    grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                }


                            } else {

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }

                        } else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
//
                            } else {

                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("    " + itemtax + "%       " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "       " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                    buffer1.append("    " + itemtax + "%       " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "       " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }

                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                        cgst = 0.0;
                        sgst = 0.0;


                    }



                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;

                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    //m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
//                    Double subtotal=ddata-totaldiscount-finalgst;
                    DecimalFormat f = new DecimalFormat("#####.00");
                    //System.out.println(f.format(dda));

                    //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                    d = "________________________________________________\n";
                    data = "                       Sub TOTAL(Rs): " + f.format(subtotal) + "\n";
                    m_AemPrinter.print(d);
                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
//                    data = "                      -Dis(%)      : " + f.format(totaldiscount) + "\n";
                    if(totaldiscount==0.0){}else {
                        data = "                      -Dis(Rs)      : " + f.format(totaldiscount) + "\n";
                        d = "                     __________________________";
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);
                        m_AemPrinter.print(d);


                        data = "                                      " + f.format((subtotal - totaldiscount));
                        m_AemPrinter.print(data);
                    }if(finalgst==0.0){}else{
//                    data = "                      +GST(Rs): " + f.format(finalgst) + "\n";
                        data = "\n                      +GST(Rs)      : " + f.format(finalgst) + "\n";

                        m_AemPrinter.print(data);
                        m_AemPrinter.print(d);
                    }


                    data = "                       Net Total(Rs): " + f.format(ddata) + "\n";

                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
//                    if(finalgst==0.0){}
//                    else {
//                        data = "\n               GST Details \n";
//                        m_AemPrinter.print(data);
//                        data = "    GST%       CGST+SGST       Total \n";
//                        m_AemPrinter.print(data);
//
//                        m_AemPrinter.POS_Font_bold_ThreeInch();
//                        m_AemPrinter.print(String.valueOf(buffer1));
//                    }
                    //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                    // data = "                 Thank you!             \n";
                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {

                        data = "           " + f1 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "           " + f2 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "           " + f3 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "           " + f4 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "           " + f5 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }

                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();

                    if (counter >= 2) {
                    } else {

                        ModelSale.arr_item_rate.clear();
                        ModelSale.arry_item_name.clear();
                        ModelSale.arr_item_qty.clear();
                        ModelSale.arr_item_price.clear();
                        ModelSale.arr_item_basicrate.clear();
                        ModelSale.arr_item_dis_rate.clear();
                        ModelSale.arr_item_dis.clear();
                        ModelSale.arr_item_tax.clear();
                        ModelSale.array_item_amt.clear(); checkArray.clear();

                        cost = 0;
                        tv_total_amt.setText("0");
                        cnt = 1;
                        itemArrayList.clear();
                        myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                        // Set the adapter


//            runOnUiThread(updateTextView);

//            finish();
//            startActivity(getIntent());
                        // this.recreate();

                        ids = db.getAllbillID();
                        autoinnc();
                    }
                    Message.message(getApplicationContext(), "Bill saved");



                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void gst_data_blueprints_two_inch_gst_enable()
    {
        if (m_AemPrinter == null) {
            try {
                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                        BluetoothAdapter.getDefaultAdapter().enable();

                        try {
                            Thread.sleep(30L);
                        } catch (InterruptedException var5) {
                            var5.printStackTrace();
                        }
                    }
                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Printer not connected please connect to printer");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (print_option.equals("2 inch") && selectprinter_option.equals("Airbill") && gst_option.equals("GST Enable")) {
                    res1 = db.getAllHeaderFooter();
                    String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                    while (res1.moveToNext()) {
                        h1 = res1.getString(1);
                        h1size = res1.getString(2);
                        h2 = res1.getString(3);
                        h2size = res1.getString(4);
                        h3 = res1.getString(5);
                        h3size = res1.getString(6);
                        h4 = res1.getString(7);
                        h4size = res1.getString(8);
                        h5 = res1.getString(9);
                        h5size = res1.getString(10);
                        f1 = res1.getString(11);
                        f1size = res1.getString(12);
                        f2 = res1.getString(13);
                        f2size = res1.getString(14);
                        f3 = res1.getString(15);
                        f3size = res1.getString(16);
                        f4 = res1.getString(17);
                        f4size = res1.getString(18);
                        f5 = res1.getString(19);
                        f5size = res1.getString(20);
                    }
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";


                    try {
                        if (h1 == null || h1.length() == 0 || h1 == "") {
                            h1 = null;
                        } else {
                            if (h1.length() < h2.length()) {
                                data = "          " + h1 + "\n";
                            } else {
                                data = "         " + h1 + "\n";
                            }
                            try {
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                            }catch (Exception ex)
                            {
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");

                                    m_AemPrinter.print(data);
                                    m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                                }catch (Exception e){
                                    Message.message(getApplicationContext(), "connecting to printer");
                                    try {
                                        m_AemScrybeDevice = new AEMScrybeDevice(this);
                                        m_AemScrybeDevice.disConnectPrinter();
                                        m_AemScrybeDevice.pairPrinter(value);
                                        m_AemScrybeDevice.connectToPrinter(value);
                                        m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                        m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                        Message.message(getApplicationContext(), "connected");
                                        m_AemPrinter.print(data);
                                        m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);


                                    }catch (Exception e1){
                                        Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                    }
                                }

                            }
                        }

                        if (h2 == null || h2.length() == 0 || h2 == "") {
                            h2 = null;
                        } else {
                            if (h2.length() > 16) {
                                data = "     " + h2 + "\n";
                            } else {
                                data = "           " + h2 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h3 == null || h3.length() == 0 || h3 == "") {
                            h3 = null;
                        } else {
                            if (h3.length() > 16) {
                                data = "     " + h3 + "\n";
                            } else {
                                data = "           " + h3 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h4 == null || h4.length() == 0 || h4 == "") {
                            h4 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "     " + h4 + "\n";
                            } else {
                                data = "           " + h4 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h5 == null || h5.length() == 0 || h5 == "") {
                            h5 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "     " + h5 + "\n";
                            } else {
                                data = "           " + h5 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        data = "\nCash Memo    bill no:" + emp_code + "\n";
                        try {
                            m_AemPrinter.print(data);
                        }catch (Exception ex) {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);

                            }catch (Exception e){
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");
                                    m_AemPrinter.print(data);
                                }catch (Exception e1){
                                    Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                }
                            }

                        }

                        data = "Date:-" + dateToStr + "\n";
                        m_AemPrinter.print(data);
                        if (str_set_cust_name == "" || str_set_cust_name == null) {
                            str_set_cust_name = "";
                            data = "\nItem Name         |Qty|Rate|Amt\n";

                        } else {
                            data = "Customer Name:- " + str_set_cust_name + "\n";
                            m_AemPrinter.print(data);
                            data = "Item Name        |Qty|Rate|Amt\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        String totalamtrate = "";
                        int a = 1, j = 0;
                        Double itemtax = 0.0;
                        int[] positions = new int[ModelSale.arry_item_name.size()];
                        StringBuffer buffer = new StringBuffer();
                        Double totalwithgst = 0.0, finalgst = 0.0;
                        Double oldtax = 0.0;
                        int l = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                        Double grandgst = 0.0;
                        int count1 = 0;
                        Double cgst = 0.0, sgst = 0.0;
                        List<String> result = new ArrayList<>();

                        Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                        List<String> taxlist1 = new ArrayList<>();
                        final List<String> itemnamelist = new ArrayList<>();
                        List<Double> qtylist = new ArrayList<>();
                        List<String> ratelist = new ArrayList<>();
                        List<Double> amtlist = new ArrayList<>();
                        List<String> indexlist1 = new ArrayList<>();
                        for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                            itemtax = Double.parseDouble(taxlistupdated.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));
                                String stringName = c.getString(0);
                                if(checkArray==null || checkArray.size()==0){
                                    if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                        if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        } else {
                                            itemnamelist.add(c.getString(0));
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                }
                                else{
                                    if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                        Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            //   for(int q=0;q<checkArray.size();q++) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                                Log.v("TAG", tv_total_amt.getText().toString());
                                                //checkArray.remove(q);
                                                // break;
                                                //Log.v("TAGG", "CHECKOUT");
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            }
                                        }
                                        else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                                Log.v("TAG", tv_total_amt.getText().toString());
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            }
                                        }
                                        else {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                                Log.v("TAG", tv_total_amt.getText().toString());
                                            } else {
                                                Log.v("TAGG", "CHECKIN4");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAGG", "CHECKOUT4");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        taxlist1.clear();
                        for (int k = 0; k < itemnamelist.size(); k++) {
                            taxlist1.clear();
                            int count = itemnamelist.size();
                            String itemname = itemnamelist.get(k);
                            String largname = "";
                            Double qty = qtylist.get(k);
                            String rate = ratelist.get(k);
                            String amount = String.valueOf(amtlist.get(k));
                            itemtax = Double.valueOf(indexlist1.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));

                            }
                            cgst = (itemtax / 2);
                            sgst = cgst;
                            if (amount.length() > 4) {
                                amount = amount.substring(0, 4);

                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 4) {
                                    l1 = 4 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                result = taxlist1.stream().filter(aObject -> {
                                    return itemnamelist.contains(aObject);
                                }).collect(Collectors.toList());
                            }
                            i("dataname", String.valueOf(result));
                            int resultcount = result.size();

                            if (itemname.length() > 15) {
                                if (rate.length() == 3) {

                                    count1++;
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                } else {
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            } else {
                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if (rate.length() == 3) {
                                    count1++;

                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                } else {
                                    count1++;
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                            j++;
                            finalgst = totalwithgst + finalgst;
                            totalwithgst = 0.0;
                        }
                        Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString())- finalgst;
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        m_AemPrinter.print(String.valueOf(buffer));


                        m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                        DecimalFormat f = new DecimalFormat("#####.00");

                        data = "         NET TOTAL (Rs): " + f.format(totalAmount) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

                        data = "         Total GST (Rs): " + f.format(grandgst) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);


                        data = "     TOTAL With GST(Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

                        if (f1 == null || f1.length() == 0 || f1 == "") {
                            f1 = null;
                        } else {
                            data = "     " + f1 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f2 == null || f2.length() == 0 || f2 == "") {
                            f2 = null;
                        } else {
                            data = "     " + f2 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f3 == null || f3.length() == 0 || f3 == "") {
                            f3 = null;
                        } else {

                            data = "     " + f3 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f4 == null || f4.length() == 0 || f4 == "") {
                            f4 = null;
                        } else {
                            data = "     " + f4 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f5 == null || f5.length() == 0 || f5 == "") {
                            f5 = null;
                        } else {
                            data = "     " + f5 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();



                        ModelSale.arr_item_rate.clear();
                        ModelSale.arry_item_name.clear();
                        ModelSale.arr_item_qty.clear();
                        ModelSale.arr_item_price.clear();
                        ModelSale.arr_item_basicrate.clear();
                        ModelSale.arr_item_dis_rate.clear();
                        ModelSale.arr_item_dis.clear();
                        ModelSale.arr_item_tax.clear();
                        ModelSale.array_item_amt.clear(); checkArray.clear();



                        payment_deatils="";
                        order_details="";
                        str_set_cust_name="";
                        str_set_payment="";
                        payment_mode_id="";

                        cost = 0;
                        tv_total_amt.setText("0");
                        cnt = 1;
                        itemArrayList.clear();
                        myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                        ids = db.getAllbillID();
                        autoinnc();
                        Message.message(getApplicationContext(), "Bill saved");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (print_option.equals("2 inch") && selectprinter_option.equals("Airbill") && gst_option.equals("GST Enable")) {
                    res1 = db.getAllHeaderFooter();
                    String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                    while (res1.moveToNext()) {
                        h1 = res1.getString(1);
                        h1size = res1.getString(2);
                        h2 = res1.getString(3);
                        h2size = res1.getString(4);
                        h3 = res1.getString(5);
                        h3size = res1.getString(6);
                        h4 = res1.getString(7);
                        h4size = res1.getString(8);
                        h5 = res1.getString(9);
                        h5size = res1.getString(10);
                        f1 = res1.getString(11);
                        f1size = res1.getString(12);
                        f2 = res1.getString(13);
                        f2size = res1.getString(14);
                        f3 = res1.getString(15);
                        f3size = res1.getString(16);
                        f4 = res1.getString(17);
                        f4size = res1.getString(18);
                        f5 = res1.getString(19);
                        f5size = res1.getString(20);
                    }

                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";


                    try {
                        if (h1 == null || h1.length() == 0 || h1 == "") {
                            h1 = null;
                        } else {
                            if (h1.length() < h2.length()) {
                                data = "          " + h1 + "\n";
                            } else {
                                data = "         " + h1 + "\n";
                            }
                            try {
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                            }catch (Exception ex)
                            {
                                Message.message(getApplicationContext(), "connecting to printer");
                                try {
                                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                                    m_AemScrybeDevice.disConnectPrinter();
                                    m_AemScrybeDevice.pairPrinter(value);
                                    m_AemScrybeDevice.connectToPrinter(value);
                                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                    Message.message(getApplicationContext(), "connected");
                                    m_AemPrinter.print(data);
                                    m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                                }catch (Exception e){
                                    Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                                }
                            }
                        }

                        if (h2 == null || h2.length() == 0 || h2 == "") {
                            h2 = null;
                        } else {
                            if (h2.length() > 16) {
                                data = "     " + h2 + "\n";
                            } else {
                                data = "           " + h2 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h3 == null || h3.length() == 0 || h3 == "") {
                            h3 = null;
                        } else {
                            if (h3.length() > 16) {
                                data = "     " + h3 + "\n";
                            } else {
                                data = "           " + h3 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h4 == null || h4.length() == 0 || h4 == "") {
                            h4 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "     " + h4 + "\n";
                            } else {
                                data = "           " + h4 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (h5 == null || h5.length() == 0 || h5 == "") {
                            h5 = null;
                        } else {
                            if (h4.length() > 16) {
                                data = "     " + h5 + "\n";
                            } else {
                                data = "           " + h5 + "\n";
                            }
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        data = "\nCash Memo    bill no:" + emp_code + "\n";
                        try {
                            m_AemPrinter.print(data);
                        }catch (Exception ex) {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);

                            }catch (Exception e){
                                Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                            }

                        }
                        data = "Date:-" + dateToStr + "\n";
                        m_AemPrinter.print(data);
                        if (str_set_cust_name == "" || str_set_cust_name == null) {
                            str_set_cust_name = "";
                            data = "\nItem Name         |Qty|Rate|Amt\n";

                        } else {
                            data = "Customer Name:- " + str_set_cust_name + "\n";
                            m_AemPrinter.print(data);
                            data = "Item Name        |Qty|Rate|Amt\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        String totalamtrate = "";
                        int a = 1, j = 0;
                        Double itemtax = 0.0;
                        int[] positions = new int[ModelSale.arry_item_name.size()];
                        StringBuffer buffer = new StringBuffer();
                        Double totalwithgst = 0.0, finalgst = 0.0;
                        Double oldtax = 0.0;
                        int l = 0;

                        DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                        Double grandgst = 0.0;
                        int count1 = 0;
                        Double cgst = 0.0, sgst = 0.0;
                        List<String> result = new ArrayList<>();

                        Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                        List<String> taxlist1 = new ArrayList<>();
                        final List<String> itemnamelist = new ArrayList<>();
                        List<Double> qtylist = new ArrayList<>();
                        List<String> ratelist = new ArrayList<>();
                        List<Double> amtlist = new ArrayList<>();
                        List<String> indexlist1 = new ArrayList<>();
                        for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                            itemtax = Double.parseDouble(taxlistupdated.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));
                                String stringName = c.getString(0);
                                if(checkArray==null || checkArray.size()==0){
                                    if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                        if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        } else {
                                            itemnamelist.add(c.getString(0));
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                }
                                else{
                                    if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                        Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                        if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                            //   for(int q=0;q<checkArray.size();q++) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                                //checkArray.remove(q);
                                                // break;
                                                //Log.v("TAGG", "CHECKOUT");
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            }
                                        }
                                        else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            } else {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            }

                                        }
                                        else {

                                            if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, 0.0);
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            } else {
                                                Log.v("TAGG", "CHECKIN4");
                                                indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                itemnamelist.add(c.getString(0));
                                                qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                                Log.v("TAGG", "CHECKOUT4");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        taxlist1.clear();
                        for (int k = 0; k < itemnamelist.size(); k++) {
                            taxlist1.clear();
                            int count = itemnamelist.size();
                            String itemname = itemnamelist.get(k);
                            String largname = "";
                            Double qty = qtylist.get(k);
                            String rate = ratelist.get(k);
                            String amount = String.valueOf(amtlist.get(k));
                            itemtax = Double.valueOf(indexlist1.get(k));
                            Cursor c = db.getdistinct(String.valueOf(itemtax));
                            while (c.moveToNext()) {
                                taxlist1.add(c.getString(0));
                            }
                            cgst = (itemtax / 2);
                            sgst = cgst;
                            if (amount.length() > 4) {
                                amount = amount.substring(0, 4);
                            } else {
                                int length = amount.length();
                                int l1 = 0;
                                if (length < 4) {
                                    l1 = 4 - length;
                                    amount = String.format(amount + "%" + (l1) + "s", "");
                                }
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                result = taxlist1.stream().filter(aObject -> {
                                    return itemnamelist.contains(aObject);
                                }).collect(Collectors.toList());
                            }
                            i("dataname", String.valueOf(result));
                            int resultcount = result.size();

                            if (itemname.length() > 15) {
                                if (rate.length() == 3) {
                                    count1++;
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                } else {
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            }
                                        }
                                    } else {
                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            } else {
                                int length = itemname.length();
                                int l1 = 0;
                                if (length < 15) {
                                    l1 = 15 - length;
                                    String itemname1 = itemname.concat(l1 + " ");
                                    itemname = String.format(itemname + "%" + (l1) + "s", "");
                                }
                                if (rate.length() == 3) {
                                    count1++;
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                } else {
                                    count1++;
                                    if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                        int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                        int p = result.indexOf(itemnamelist.get(k));
                                        int f = taxlist1.size();
                                        int h = itemnamelist.size();
                                        if (p == (result.size() - 1)) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else if (result.size() == 1) {
                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                        } else {
                                            if (g == f || count == count1) {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);
                                                sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                                finalgst = finalgst + sameitemtotalgst;
                                                grandgst = grandgst + sameitemtotalgst;
                                                buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",sameitemtotalgst) + "\n");
                                            } else {
                                                buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                                sameitemgst = sameitemgst + Double.parseDouble(amount);

                                            }
                                        }
                                    } else {
                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                        totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                        buffer.append("CGST:" + cgst + "% + " + "SGST:" + sgst + "%     " + String.format("%.2f",totalwithgst) + "\n");
                                        grandgst = grandgst + totalwithgst;
                                    }
                                }
                            }
                            j++;
                            finalgst = totalwithgst + finalgst;
                            totalwithgst = 0.0;
                        }
                        Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) - finalgst;
                        m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                        m_AemPrinter.print(String.valueOf(buffer));
                        m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                        DecimalFormat f = new DecimalFormat("#####.00");

                        data = "         NET TOTAL (Rs): " + f.format(totalAmount) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

                        data = "         Total GST (Rs): " + f.format(grandgst) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

                        data = "     TOTAL With GST(Rs): " + f.format(Double.parseDouble(tv_total_amt.getText().toString())) + "\n";
                        m_AemPrinter.print(d);
                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(data);

                        if (f1 == null || f1.length() == 0 || f1 == "") {
                            f1 = null;
                        } else {
                            data = "     " + f1 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f2 == null || f2.length() == 0 || f2 == "") {
                            f2 = null;
                        } else {
                            data = "     " + f2 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f3 == null || f3.length() == 0 || f3 == "") {
                            f3 = null;
                        } else {
                            data = "     " + f3 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f4 == null || f4.length() == 0 || f4 == "") {
                            f4 = null;
                        } else {
                            data = "     " + f4 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }
                        if (f5 == null || f5.length() == 0 || f5 == "") {
                            f5 = null;
                        } else {
                            data = "     " + f5 + "\n";
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }

                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();
                        m_AemPrinter.setCarriageReturn();

                        if (counter >= 2) {
                        } else {
                            ModelSale.arr_item_rate.clear();
                            ModelSale.arry_item_name.clear();
                            ModelSale.arr_item_qty.clear();
                            ModelSale.arr_item_price.clear();
                            ModelSale.arr_item_basicrate.clear();
                            ModelSale.arr_item_dis_rate.clear();
                            ModelSale.arr_item_dis.clear();
                            ModelSale.arr_item_tax.clear();
                            ModelSale.array_item_amt.clear(); checkArray.clear();


                            payment_deatils="";
                            order_details="";
                            str_set_cust_name="";
                            str_set_payment="";
                            payment_mode_id="";

                            cost = 0;
                            tv_total_amt.setText("0");
                            cnt = 1;
                            itemArrayList.clear();
                            myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                            ids = db.getAllbillID();
                            autoinnc();
                        }
                        Message.message(getApplicationContext(), "Bill saved");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    public void gst_data_blueprints_two_inch_gst_disable()
    {
        if (m_AemPrinter == null) {
            try {
                BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mAdapter == null) {

                }

                try {
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                        BluetoothAdapter.getDefaultAdapter().enable();
                        try {
                            Thread.sleep(30L);
                        } catch (InterruptedException var5) {
                            var5.printStackTrace();
                        }
                    }
                    m_AemScrybeDevice = new AEMScrybeDevice(this);
                    m_AemScrybeDevice.disConnectPrinter();
                    m_AemScrybeDevice.pairPrinter(value);
                    m_AemScrybeDevice.connectToPrinter(value);
                    m_cardReader = m_AemScrybeDevice.getCardReader(this);
                    m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Message.message(getApplicationContext(),"Printer not connected please connect to printer");
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        if(billwithprint.equals("Yes")&&multipleprint.equals("No")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));

                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {
                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);
                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }

                try {
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";

                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "          " + h1 + "\n";
                        } else {
                            data = "         " + h1 + "\n";
                        }
                        try {
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }catch (Exception ex)
                        {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                            }catch (Exception e){
                                Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                            }
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "     " + h2 + "\n";
                        } else {
                            data = "           " + h2 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "     " + h3 + "\n";
                        } else {
                            data = "           " + h3 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h4 + "\n";
                        } else {
                            data = "           " + h4 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h5 + "\n";
                        } else {
                            data = "           " + h5 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (res2.getCount() == 0) {

                    }
                    data = "\nCash Memo    bill no:" + emp_code + "\n";
                    try {
                        m_AemPrinter.print(data);
                    }catch (Exception ex) {
                        Message.message(getApplicationContext(), "connecting to printer");
                        try {
                            m_AemScrybeDevice = new AEMScrybeDevice(this);
                            m_AemScrybeDevice.disConnectPrinter();
                            m_AemScrybeDevice.pairPrinter(value);
                            m_AemScrybeDevice.connectToPrinter(value);
                            m_cardReader = m_AemScrybeDevice.getCardReader(this);
                            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                            Message.message(getApplicationContext(), "connected");
                            m_AemPrinter.print(data);
                        }catch (Exception e){
                            Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                        }

                    }
                    data = "Date:-" + dateToStr + "\n";
                    m_AemPrinter.print(data);
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nItem Name         |Qty|Rate|Amt\n";

                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        m_AemPrinter.print(data);
                        data = "Item Name        |Qty|Rate|Amt\n";
                    }
                    m_AemPrinter.print(data);
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    String totalamtrate = "";
                    int a = 1;
                    StringBuffer buffer = new StringBuffer();
                    while (res2.moveToNext()) {

                        String itemname = res2.getString(3).toString();
                        String largname = "";
                        String qty = res2.getString(4).toString();
                        String rate = res2.getString(5).toString();
                        ;
                        String amount = res2.getString(6).toString();
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (itemname.length() > 15) {


                            if (res2.getString(5).length() >= 3) {
                                buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                            }
                        } else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (res2.getString(5).length() >= 3) {
                                buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                            } else {
                                buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                            }
                        }
                    }
                    String totalamtrategst = "";
                    int a1 = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer1 = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0;
                    Double oldtax = 0.0;
                    int l = 0;
                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");

                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;

                    List<String> result = new ArrayList<>();
                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();

                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){
                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else {
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG", ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            tv_total_amt.setText(String.valueOf(total_val));
                                            Log.v("TAG", String.valueOf(total_val));
                                            //checkArray.remove(q);
                                            // break;
                                            //Log.v("TAGG", "CHECKOUT");
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            tv_total_amt.setText(String.valueOf(total_val));
                                            Log.v("TAG", String.valueOf(total_val));
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else {

                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, 0.0);
                                            double current = Double.parseDouble(tv_total_amt.getText().toString());
                                            double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                            total_val = current - total_val;
                                            tv_total_amt.setText(String.valueOf(total_val));
                                            Log.v("TAG", String.valueOf(total_val));
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        DecimalFormat formats = new DecimalFormat("#####.00");
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
                        String rate = ratelist.get(k);
                        String amount = String.valueOf(qty*Double.parseDouble(rate));
                        itemtax = Double.valueOf(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);
                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            result = taxlist1.stream().filter(aObject -> {
                                return itemnamelist.contains(aObject);
                            }).collect(Collectors.toList());
                        }
                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();
                        if (itemname.length() > 15) {
                            if (rate.length() == 3) {
                                count1++;
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    grandgst = grandgst + totalwithgst;
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                }
                            } else {

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        }
                                    }
                                } else {
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        } else {
                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
//
                            } else {

                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }

                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                        cgst = 0.0;
                        sgst = 0.0;


                    }


                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    //m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                    DecimalFormat f = new DecimalFormat("#####.00");
                    //System.out.println(f.format(dda));

                    //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                    data = "              TOTAL(Rs): " + f.format(ddata) + "\n";
                    m_AemPrinter.print(d);
                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
                    if(finalgst==0.0){}
                    else {
                        data = "\n          GST Details \n";
                        m_AemPrinter.print(data);
                        data = "  GST%    CGST+SGST     Total \n";
                        m_AemPrinter.print(data);

                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(String.valueOf(buffer1));
                    }
                    //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                    // data = "                 Thank you!             \n";
                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {

                        data = "     " + f1 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "     " + f2 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "     " + f3 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "     " + f4 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "     " + f5 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }

                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();


                    ModelSale.arr_item_rate.clear();
                    ModelSale.arry_item_name.clear();
                    ModelSale.arr_item_qty.clear();
                    ModelSale.arr_item_price.clear();
                    ModelSale.arr_item_basicrate.clear();
                    ModelSale.arr_item_dis_rate.clear();
                    ModelSale.arr_item_dis.clear();
                    ModelSale.arr_item_tax.clear();
                    ModelSale.array_item_amt.clear(); checkArray.clear();
                    payment_deatils="";
                    order_details="";
                    str_set_cust_name="";
                    str_set_payment="";
                    payment_mode_id="";

                    cost = 0;
                    tv_total_amt.setText("0");
                    cnt = 1;
                    itemArrayList.clear();
                    myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                    str_set_cust_name = "";

                    ids = db.getAllbillID();
                    autoinnc();


                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if(billwithprint.equals("Yes")&&multipleprint.equals("Yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));


                res1 = db.getAllHeaderFooter();
                String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
                while (res1.moveToNext()) {

                    h1 = res1.getString(1);
                    h1size = res1.getString(2);
                    h2 = res1.getString(3);
                    h2size = res1.getString(4);
                    h3 = res1.getString(5);
                    h3size = res1.getString(6);
                    h4 = res1.getString(7);
                    h4size = res1.getString(8);
                    h5 = res1.getString(9);
                    h5size = res1.getString(10);

                    f1 = res1.getString(11);
                    f1size = res1.getString(12);
                    f2 = res1.getString(13);
                    f2size = res1.getString(14);
                    f3 = res1.getString(15);
                    f3size = res1.getString(16);
                    f4 = res1.getString(17);
                    f4size = res1.getString(18);
                    f5 = res1.getString(19);
                    f5size = res1.getString(20);
                }


                try {
//
                    Date today = new Date();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    String dateToStr = format.format(today);
                    String data = null;

                    data = "                 " + "TechMart Cafe" + "             \n";
                    String d = "________________________________\n";

                    if (h1 == null || h1.length() == 0 || h1 == "") {
                        h1 = null;
                    } else {
                        if (h1.length() < h2.length()) {
                            data = "          " + h1 + "\n";
                        } else {
                            data = "         " + h1 + "\n";
                        }
                        try {
                            m_AemPrinter.print(data);
                            m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                        }catch (Exception ex)
                        {
                            Message.message(getApplicationContext(), "connecting to printer");
                            try {
                                m_AemScrybeDevice = new AEMScrybeDevice(this);
                                m_AemScrybeDevice.disConnectPrinter();
                                m_AemScrybeDevice.pairPrinter(value);
                                m_AemScrybeDevice.connectToPrinter(value);
                                m_cardReader = m_AemScrybeDevice.getCardReader(this);
                                m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                                Message.message(getApplicationContext(), "connected");
                                m_AemPrinter.print(data);
                                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                            }catch (Exception e){
                                Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                            }
                        }
                    }

                    if (h2 == null || h2.length() == 0 || h2 == "") {
                        h2 = null;
                    } else {
                        if (h2.length() > 16) {
                            data = "     " + h2 + "\n";
                        } else {
                            data = "           " + h2 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h3 == null || h3.length() == 0 || h3 == "") {
                        h3 = null;
                    } else {
                        if (h3.length() > 16) {
                            data = "     " + h3 + "\n";
                        } else {
                            data = "           " + h3 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h4 == null || h4.length() == 0 || h4 == "") {
                        h4 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h4 + "\n";
                        } else {
                            data = "           " + h4 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (h5 == null || h5.length() == 0 || h5 == "") {
                        h5 = null;
                    } else {
                        if (h4.length() > 16) {
                            data = "     " + h5 + "\n";
                        } else {
                            data = "           " + h5 + "\n";
                        }
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (res2.getCount() == 0) {


                    }
                    data = "\nCash Memo    bill no:" + emp_code + "\n";
                    try {
                        m_AemPrinter.print(data);
                    }catch (Exception ex) {
                        Message.message(getApplicationContext(), "connecting to printer");
                        try {
                            m_AemScrybeDevice = new AEMScrybeDevice(this);
                            m_AemScrybeDevice.disConnectPrinter();
                            m_AemScrybeDevice.pairPrinter(value);
                            m_AemScrybeDevice.connectToPrinter(value);
                            m_cardReader = m_AemScrybeDevice.getCardReader(this);
                            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
                            Message.message(getApplicationContext(), "connected");
                            m_AemPrinter.print(data);

                        }catch (Exception e){
                            Message.message(getApplicationContext(),"Printer not connected please connect from main settings");
                        }

                    }
                    data = "Date:-" + dateToStr + "\n";
                    m_AemPrinter.print(data);
         /*       data="Customer Name:- "+str_set_cust_name+"\n";
                m_AemPrinter.print(data);*/
                    if (str_set_cust_name == "" || str_set_cust_name == null) {
                        str_set_cust_name = "";
                        data = "\nItem Name         |Qty|Rate|Amt\n";

                    } else {
                        data = "Customer Name:- " + str_set_cust_name + "\n";
                        m_AemPrinter.print(data);
                        data = "Item Name        |Qty|Rate|Amt\n";
                    }
//                m_AemPrinter.print(d);


                    m_AemPrinter.print(data);
//                m_AemPrinter.print(d);
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    String totalamtrate = "";
                    int a = 1;
                    StringBuffer buffer = new StringBuffer();
                    while (res2.moveToNext()) {

                        String itemname = res2.getString(3).toString();
                        String largname = "";
                        String qty = res2.getString(4).toString();
                        String rate = res2.getString(5).toString();
                        ;
                        String amount = res2.getString(6).toString();
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (itemname.length() > 15) {


                            if (res2.getString(5).length() >= 3) {
                                buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                            } else {
                                buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                            }

                        } else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (res2.getString(5).length() >= 3) {
                                buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                            } else {
                                buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                            }
                        }


                    }
                    String totalamtrategst = "";
                    int a1 = 1, j = 0;
                    Double itemtax = 0.0;
                    int[] positions = new int[ModelSale.arry_item_name.size()];
                    StringBuffer buffer1 = new StringBuffer();
                    Double totalwithgst = 0.0, finalgst = 0.0;
                    Double oldtax = 0.0;
                    int l = 0;

                    DecimalFormat decimalFormat = new DecimalFormat("#####.00");
                    Double grandgst = 0.0;
                    int count1 = 0;
                    Double cgst = 0.0, sgst = 0.0;
                    List<String> result = new ArrayList<>();

                    Double sameitemgst = 0.0, sameitemtotalgst = 0.0;
                    List<String> taxlist1 = new ArrayList<>();
                    final List<String> itemnamelist = new ArrayList<>();
                    List<Double> qtylist = new ArrayList<>();
                    List<String> ratelist = new ArrayList<>();
                    List<Double> amtlist = new ArrayList<>();
                    List<String> indexlist1 = new ArrayList<>();
                    for (int k = 0; k < ModelSale.arry_item_name.size(); k++) {
                        itemtax = Double.parseDouble(taxlistupdated.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));
                            String stringName = c.getString(0);
                            if(checkArray==null || checkArray.size()==0){
                                if (ModelSale.arry_item_name.contains(c.getString(0))) {
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        itemnamelist.add(c.getString(0));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                    if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                    } else {
                                        itemnamelist.add(c.getString(0));
                                        indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        amtlist.add(ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                    }
                                }
                            }
                            else{
                                if (ModelSale.arry_item_name.get(k).equals(stringName)) {
                                    Log.v("TAGG",ModelSale.arry_item_name.get(k));
                                    if (itemnamelist.size() == 0 || indexlist1.size() == 0) {
                                        //   for(int q=0;q<checkArray.size();q++) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //amtlist.add(k, 0.0);
                                            if(counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                            //checkArray.remove(q);
                                            // break;
                                            //Log.v("TAGG", "CHECKOUT");
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else if (itemnamelist.contains(c.getString(0)) || indexlist1.contains(c.getString(0))) {
                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //amtlist.add(k, 0.0);
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                        } else {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                        }
                                    }
                                    else {

                                        if (checkArray.contains(ModelSale.arry_item_name.get(k))) {
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            //amtlist.add(k, 0.0);
                                            if (counter<=2) {
                                                double current = Double.parseDouble(tv_total_amt.getText().toString());
                                                double total_val = ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0)));
                                                total_val = current - total_val;
                                                tv_total_amt.setText(String.valueOf(total_val));
                                                Log.v("TAG", String.valueOf(total_val));
                                            }
                                        } else {
                                            Log.v("TAGG", "CHECKIN4");
                                            indexlist1.add(ModelSale.arr_item_tax.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            itemnamelist.add(c.getString(0));
                                            qtylist.add(ModelSale.arr_item_qty.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            ratelist.add(ModelSale.arr_item_basicrate.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            amtlist.add(k, ModelSale.arr_item_price.get(ModelSale.arry_item_name.indexOf(c.getString(0))));
                                            Log.v("TAGG", "CHECKOUT4");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    taxlist1.clear();
                    for (int k = 0; k < itemnamelist.size(); k++) {
                        DecimalFormat formats = new DecimalFormat("#####.00");
                        taxlist1.clear();
                        int count = itemnamelist.size();
                        String itemname = itemnamelist.get(k);
                        String largname = "";
                        Double qty = qtylist.get(k);
                        String rate = ratelist.get(k);

                        String amount = String.valueOf(qty*Double.parseDouble(rate));
//                        String amount = String.valueOf(amtlist.get(k));
                        itemtax = Double.valueOf(indexlist1.get(k));
                        Cursor c = db.getdistinct(String.valueOf(itemtax));
                        while (c.moveToNext()) {
                            taxlist1.add(c.getString(0));

                        }
                        cgst = (itemtax / 2);
                        sgst = cgst;
                        if (amount.length() > 4) {
                            amount = amount.substring(0, 4);

                        } else {
                            int length = amount.length();
                            int l1 = 0;
                            if (length < 4) {
                                l1 = 4 - length;
                                amount = String.format(amount + "%" + (l1) + "s", "");
                            }
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            result = taxlist1.stream().filter(aObject -> {
                                return itemnamelist.contains(aObject);
                            }).collect(Collectors.toList());
                        }
                        i("dataname", String.valueOf(result));
                        int resultcount = result.size();

                        if (itemname.length() > 15) {


                            if (rate.length() == 3) {

                                count1++;
//                            buffer.append(itemname.substring(0, 15) + "    " +qty+ "  " + rate + "  " + amount + "\n"); //res2.getString(6)+"\n");
                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");

                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+ String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));

                                    grandgst = grandgst + totalwithgst;
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                }


                            } else {

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append(itemtax+"%"+String.format("%.2f",sameitemtotalgst) + "\n");
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname.substring(0, 15) + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n"); //res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append(itemtax+"%"+String.format("%.2f", totalwithgst )+ "\n");
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }

                        } else {

                            int length = itemname.length();
                            int l1 = 0;
                            if (length < 15) {
                                l1 = 15 - length;
                                String itemname1 = itemname.concat(l1 + " ");
                                itemname = String.format(itemname + "%" + (l1) + "s", "");
                            }
                            if (rate.length() == 3) {
                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append(itemtax+"%"+String.format("%.2f", sameitemtotalgst) + "\n");
//                                        buffer1.append("      "+itemtax+"%         "+ (sameitemgst/2)+"+"+(sameitemgst/2)+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f",totalwithgst) + "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
//
                            } else {

                                count1++;

                                if (taxlist1.contains(itemnamelist.get(k)) && taxlist1.size() > 1) {
                                    int g = taxlist1.indexOf(itemnamelist.get(k)) + 1;
                                    int p = result.indexOf(itemnamelist.get(k));
                                    int f = taxlist1.size();
                                    int h = itemnamelist.size();
                                    if (p == (result.size() - 1)) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
//                                        Log.i("calculation",""+(sameitemtotalgst/2));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else if (result.size() == 1) {
//                                        buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " +String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                        sameitemgst = sameitemgst + Double.parseDouble(amount);
                                        sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                        finalgst = finalgst + sameitemtotalgst;
                                        grandgst = grandgst + sameitemtotalgst;
//                                        buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                        buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                    } else {
                                        if (g == f || count == count1) {
//                                            buffer.append(itemname + "    " + qty.intValue() + "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount)) + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);
                                            sameitemtotalgst = (sameitemgst * (itemtax / 100));
                                            finalgst = finalgst + sameitemtotalgst;
                                            grandgst = grandgst + sameitemtotalgst;
//                                            buffer1.append("      "+itemtax+"%         "+String.format("%.2f",sameitemtotalgst) + "\n");
                                            buffer1.append("  " + itemtax + "%    " + formats.format(sameitemtotalgst / 2) + "+" + formats.format(sameitemtotalgst / 2) + "     " + formats.format(sameitemtotalgst) + "\n");
                                        } else {
//                                            buffer1.append(itemtax+"%        "++"+"+ String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                            sameitemgst = sameitemgst + Double.parseDouble(amount);

                                        }
                                    }


                                } else {
//                                    buffer.append(itemname + "    " + qty.intValue()+ "  " + rate + "  " + String.format("%.2f",Double.valueOf(amount))  + "\n");//res2.getString(6)+"\n");
                                    totalwithgst = (Double.parseDouble(amount) * (itemtax / 100));
//                                    buffer1.append("      "+itemtax+"%         "+String.format("%.2f", totalwithgst )+ "\n");
                                    buffer1.append("  " + itemtax + "%    " + formats.format(totalwithgst / 2) + "+" + formats.format(totalwithgst / 2) + "     " + formats.format(totalwithgst) + "\n");
                                    grandgst = grandgst + totalwithgst;
                                }
                            }
                        }

                        j++;
                        finalgst = totalwithgst + finalgst;
                        totalwithgst = 0.0;
                        cgst = 0.0;
                        sgst = 0.0;


                    }


                    Double totalAmount = Double.parseDouble(tv_total_amt.getText().toString()) + finalgst;
                    m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                    //m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.print(String.valueOf(buffer));
                    m_AemPrinter.setFontType(AEMPrinter.FONT_001);
                    m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    double ddata = Double.parseDouble(tv_total_amt.getText().toString());
                    DecimalFormat f = new DecimalFormat("#####.00");
                    //System.out.println(f.format(dda));

                    //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
                    data = "              TOTAL(Rs): " + f.format(ddata) + "\n";
                    m_AemPrinter.print(d);
                    m_AemPrinter.POS_Font_bold_ThreeInch();
                    m_AemPrinter.print(data);
                    if(finalgst==0.0){}
                    else {
                        data = "\n          GST Details \n";
                        m_AemPrinter.print(data);
                        data = "  GST%    CGST+SGST     Total \n";
                        m_AemPrinter.print(data);

                        m_AemPrinter.POS_Font_bold_ThreeInch();
                        m_AemPrinter.print(String.valueOf(buffer1));
                    }
                    //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
                    // data = "                 Thank you!             \n";
                    if (f1 == null || f1.length() == 0 || f1 == "") {
                        f1 = null;
                    } else {

                        data = "     " + f1 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

                    }
                    if (f2 == null || f2.length() == 0 || f2 == "") {
                        f2 = null;
                    } else {
                        data = "     " + f2 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f3 == null || f3.length() == 0 || f3 == "") {
                        f3 = null;
                    } else {

                        data = "     " + f3 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f4 == null || f4.length() == 0 || f4 == "") {
                        f4 = null;
                    } else {
                        data = "     " + f4 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }
                    if (f5 == null || f5.length() == 0 || f5 == "") {
                        f5 = null;
                    } else {
                        data = "     " + f5 + "\n";
                        m_AemPrinter.print(data);
                        m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                    }

                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();



                    if (counter >= 2) {
                    } else {
                        ModelSale.arr_item_rate.clear();
                        ModelSale.arry_item_name.clear();
                        ModelSale.arr_item_qty.clear();
                        ModelSale.arr_item_price.clear();
                        ModelSale.arr_item_basicrate.clear();
                        ModelSale.arr_item_dis_rate.clear();
                        ModelSale.arr_item_dis.clear();
                        ModelSale.arr_item_tax.clear();
                        ModelSale.array_item_amt.clear(); checkArray.clear();


                        payment_deatils="";
                        order_details="";
                        str_set_cust_name="";
                        str_set_payment="";
                        payment_mode_id="";

                        cost = 0;
                        tv_total_amt.setText("0");
                        cnt = 1;
                        itemArrayList.clear();
                        myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);

                        // Set the adapter
//            runOnUiThread(updateTextView);
//            finish();
//            startActivity(getIntent());
                        // this.recreate();
                        ids = db.getAllbillID();
                        autoinnc();
                    }
                    Message.message(getApplicationContext(), "Bill saved");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    Runnable updateTextView = new Runnable() {

        @Override
        public void run() {
            autoinnc();
        }
    };

    protected void CustomMessageBox1(){
        Button btn_save_cashcredit;
        dialog1=new Dialog(this);
        dialog1.setContentView(R.layout.layout_payment_details);
        dialog1.setTitle("Payment Details");
        dialog1.setTitle("Payment Details");

        List<String> users = new ArrayList<>();
        stransaction=(Spinner)dialog1.findViewById(R.id.TransactionStatus);

        users.add(0, "Transaction status");
        users.add(1, "Success");
        users.add(2, "Fail");
        users.add(3, "Processing");

        adapter = new ArrayAdapter<String>(BillingScreenActivity.this, android.R.layout.simple_spinner_dropdown_item, users);
        //attach adapter to spinner
        stransaction.setAdapter(adapter);
        btn_save_cashcredit=(Button)dialog1.findViewById(R.id.btn_save_cashcredit1);
        tid=(AutoCompleteTextView)dialog1.findViewById(R.id.TransactionID);
        tdetails=(AutoCompleteTextView)dialog1.findViewById(R.id.TransactionDetails);
        todetails=(AutoCompleteTextView)dialog1.findViewById(R.id.OrderDetails);

        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray array = new JSONArray();
                    JSONObject obj = new JSONObject();
                    obj.put("t_id", tid.getText().toString());
                    obj.put("t_details", tdetails.getText().toString());
                    obj.put("t_status", stransaction.getSelectedItem());
                    array.put(obj);
                    payment_deatils=""+array;

                    JSONArray array1 = new JSONArray();
                    JSONObject obj1 = new JSONObject();
                    obj1.put("o_details", tdetails.getText().toString());
                    array1.put(obj1);
                    order_details=""+array1;

                    dialog1.dismiss();

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        dialog1.setCancelable(true);
        dialog1.show();
    }
    protected void CustomMessageBox(){

        Button btn_save_cashcredit,payment_details;
        dialog=new Dialog(this);
        dialog.setContentView(R.layout.layout_cash_credit);
        dialog.setTitle("Payment Details");
//        rg_bill_screen=(RadioGroup)dialog.findViewById(R.id.rg_cash_credit);

        List<String> users = new ArrayList<>();
        List<String> users1 = new ArrayList<>();
        List<String> users2 = new ArrayList<>();

        sp_payment=(Spinner)dialog.findViewById(R.id.sp_cash_credit);
        sppoint_of_contact=(Spinner)dialog.findViewById(R.id.sp_modeofpayment);

//        rg_bill_screen=(RadioGroup)dialog.findViewById(R.id.rg_cash_credit);
        users.add(0, "Select Payment Type");
        Cursor cpm=db.payment_details();
        if(cpm==null || cpm.getCount() == 0) {

        }else {
            while (cpm.moveToNext()) {
                users.add(""+cpm.getString(2));
            }
        }

        users1.add(0, "Select Point Of Contact");
        users2.add("0");

        Cursor cpc=db.point_contact_details();
        if(cpc==null || cpc.getCount() == 0) {

        }else {
            while (cpc.moveToNext()) {
                users2.add(""+cpc.getString(1));
                users1.add(""+cpc.getString(2));
            }
        }

        //adapter for spinner
        // Toast.makeText(getApplicationContext(),"Values are="+users,Toast.LENGTH_LONG).show();
        adapter = new ArrayAdapter<String>(BillingScreenActivity.this, android.R.layout.simple_spinner_dropdown_item, users);
        //attach adapter to spinner
        sp_payment.setAdapter(adapter);
        //adapter for spinner
        // Toast.makeText(getApplicationContext(),"Values are="+users,Toast.LENGTH_LONG).show();
        adapter1 = new ArrayAdapter<String>(BillingScreenActivity.this, android.R.layout.simple_spinner_dropdown_item, users1);
        //attach adapter to spinner
        sppoint_of_contact.setAdapter(adapter1);

        btn_save_cashcredit=(Button)dialog.findViewById(R.id.btn_save_cashcredit);

        at_customer_name=(AutoCompleteTextView)dialog.findViewById(R.id.at_customer_name);

        payment_details=(Button)dialog.findViewById(R.id.btn_payment_details);
        payment_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomMessageBox1();
            }
        });

        NameList=new ArrayList<String>();
        IdList=new ArrayList<String>();

        Cursor c =db.getCustomerDetails();
        if(c.getCount() == 0) {
            Message.message(getApplicationContext(),"Nothing found");
        }else {
            while (c.moveToNext()) {
                IdList.add(c.getString(0));
                NameList.add(c.getString(2));
            }
        }

        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(this,android.R.layout.select_dialog_item,NameList);
        at_customer_name.setThreshold(1);
        at_customer_name.setAdapter(adapter2);

        at_customer_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Name=at_customer_name.getText().toString();
            }
        });

        btn_save_cashcredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sp_payment.getSelectedItem().equals("Select Payment Type")){
                    Toast.makeText(getApplicationContext(),"Please select Payment Type",Toast.LENGTH_SHORT).show();
                }else if(sppoint_of_contact.getSelectedItem().equals("Select Point Of Contact")){
                    Toast.makeText(getApplicationContext(),"Please Select Point Of Contact",Toast.LENGTH_SHORT).show();
                }else {
                    str_set_payment = "" + sp_payment.getSelectedItem();
                    int point_of_saleid= (int) sppoint_of_contact.getSelectedItemId();
                    payment_mode_id = users2.get(point_of_saleid);

                    if(NameList.contains(Name)){
                        name_index=NameList.indexOf(Name);
                        Toast.makeText(getApplicationContext(),""+name_index,Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        dialog.dismiss();
                    }else {
                        Toast.makeText(getApplicationContext(),"New Customer added!!!",Toast.LENGTH_SHORT).show();
                        str_set_cust_name=Name;
                        db.AddCutomer("",str_set_cust_name,"","","","");
                        dialog.dismiss();
                    }
                }
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }
    public class MyAppAdapter extends BaseAdapter       //has a class viewholder which holds
    {
        public List<ClassCodeWiseList> parkingList;
        public Context context;
        public ArrayList<ClassCodeWiseList> arraylist;

        public class ViewHolder {
            TextView tv_sr_no,tv_item_name1,tv_amount,tv_delete;
            EditText tv_rate,tv_qty1;
            CheckBox chkIos;
        }

        private MyAppAdapter(ArrayList apps, BillingScreenActivity context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<ClassCodeWiseList>();
            arraylist.addAll(parkingList);

        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) // inflating the layout and initializing widgets
        {
            ArrayList<String> myItems = new ArrayList<>();
            pos_i=position;
            final ViewHolder holder;
            view = getLayoutInflater().inflate(R.layout.layout_billscreen,null);
            holder = new ViewHolder(); LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            holder.tv_sr_no = (TextView) view.findViewById(R.id.tv_sr_no);
            holder.tv_item_name1 = (TextView) view.findViewById(R.id.tv_item_name);
            holder.tv_rate = (EditText) view.findViewById(R.id.tv_rate);
            holder.tv_qty1 = (EditText) view.findViewById(R.id.tv_qty);
            holder.tv_amount = (TextView) view.findViewById(R.id.tv_amount);
            holder.tv_delete = (TextView) view.findViewById(R.id.tv_delete);
            holder.chkIos = view.findViewById(R.id.chkIos);

            holder.tv_sr_no.setText(parkingList.get(position).getSr_no() + "");
            holder.tv_item_name1.setText(parkingList.get(position).getItem_name() + "");
            holder.tv_rate.setText(parkingList.get(position).getRate() + "");
            holder.tv_qty1.setText(parkingList.get(position).getQty() + "");
            holder.tv_amount.setText(parkingList.get(position).getAmount() + "");
            //checkArray.add(parkingList.get(model_pos).getItem_name());

            holder.tv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        //int model_pos = Integer.parseInt(holder.tv_sr_no.getText().toString()) - 1;

                        cost = cost - (ModelSale.arr_item_price.get(position));
                        tv_total_amt.setText("" + String.format("%.2f", cost));

                        ModelSale.arr_item_rate.remove(position);
                        ModelSale.arry_item_name.remove(position);
                        ModelSale.arr_item_qty.remove(position);
                        ModelSale.arr_item_price.remove(position);
                        ModelSale.arr_item_basicrate.remove(position);
                        ModelSale.arr_item_dis_rate.remove(position);
                        ModelSale.arr_item_dis.remove(position);
                        ModelSale.arr_item_tax.remove(position);
                        ModelSale.array_item_amt.remove(position);

                        itemArrayList.remove(position);
                        if(itemArrayList.size()==0){
                            cnt=1;
                        }
                        for(int i = (position);i<itemArrayList.size();i++){
//                            itemArrayList.set(i,itemArrayList.get(i));
                            itemArrayList.set(i,new ClassCodeWiseList(""+(i+1) , "" + ModelSale.arry_item_name.get(i), "" + ModelSale.arr_item_qty.get(i), "" +Double.valueOf(ModelSale.arr_item_basicrate.get(i)), "" + ModelSale.arr_item_price.get(i), ""));
                            cnt=i+2;
                        }

                        myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                        lv.setAdapter(myAppAdapter);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            holder.chkIos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int model_pos = Integer.parseInt(holder.tv_sr_no.getText().toString()) - 1;
                    if (compoundButton.isChecked()) {
                        checkArray.add(parkingList.get(model_pos).getItem_name());
                        Log.v("TAG", parkingList.get(model_pos).getItem_name());
                    }
                }
            });

            for(int i=0;i<checkArray.size();i++)
            {
                if(parkingList.get(position).getItem_name().equals(checkArray.get(i).toString()))
                {
                    holder.chkIos.setChecked(true);
                }
            }

            return view;
        }
    }

    public void two_inch_blueprints_gst_disable(int billno) {
        if(counter==1){

            Double totalPrice = Double.valueOf(tv_total_amt.getText().toString());
            if (checkArray!=null && checkArray.size()!=0){
                for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                    //totalPrice = Double.valueOf(tv_total_amt.getText().toString());
                    for (int j = 0; j < checkArray.size(); j++) {
                        if (checkArray.get(j).equals(ModelSale.arry_item_name.get(i))){
                            Double value = ModelSale.arr_item_price.get(i);
                            Log.v("TAG TOTAL Value", String.valueOf(value));
                            totalPrice = totalPrice - value;
                            Log.v("TAG TOTAL", String.valueOf(totalPrice));
                        }
                    }
                }
                Log.v("TAGGF", String.valueOf(totalPrice));
                for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                    if (checkArray.contains(ModelSale.arry_item_name.get(i))) {
                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                0.0, totalPrice, cid, lid, empid, "" + tvbill.getText().toString(),
                                payment_mode_id, payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        counter++;

                    }
                    else {
                        db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                                ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                                Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                                Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                                totalPrice, cid, lid, empid, "" + tvbill.getText().toString(),
                                payment_mode_id, payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                                ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                        db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                        counter++;
                    }
                }
            }else{
                for (int i = 0; i < ModelSale.arry_item_name.size(); i++) {
                    db.Addbill(Integer.parseInt(tvbill.getText().toString()), str_set_cust_name, str_set_payment,
                            ModelSale.arry_item_name.get(i), ModelSale.arr_item_qty.get(i),
                            Double.parseDouble(ModelSale.arr_item_rate.get(i)),
                            Double.parseDouble(String.valueOf(ModelSale.arr_item_price.get(i))),
                            Double.valueOf(tv_total_amt.getText().toString()), cid, lid, empid, "" + tvbill.getText().toString(),
                            payment_mode_id, payment_deatils, order_details, gst_option, emp_code, ModelSale.arr_item_dis.get(i),
                            ModelSale.arr_item_tax.get(i), Double.parseDouble(ModelSale.arr_item_basicrate.get(i)));
                    db.Update_inventory(ModelSale.arry_item_name.get(i).toString(), ModelSale.arr_item_qty.get(i).toString());
                    counter++;
                }
            }
        }

        if(counter==1) {
            res2 = db.getAllbillWithID(Integer.parseInt(tvbill.getText().toString()));
        }else
        {
            res2 = db.getAllbillWithID(billno);
        }

        res1 = db.getAllHeaderFooter();
        String h1 = null, h1size = "10", h2 = null, h2size = "9", h3 = null, h3size = "8", h4 = null, h4size = "7", h5 = null, h5size = "6", f1 = null, f1size = "10", f2 = null, f2size = "10", f3 = null, f3size = "10", f4 = null, f4size = "10", f5 = null, f5size = "10";
        while (res1.moveToNext()) {
            h1 = res1.getString(1);
            h1size = res1.getString(2);
            h2 = res1.getString(3);
            h2size = res1.getString(4);
            h3 = res1.getString(5);
            h3size = res1.getString(6);
            h4 = res1.getString(7);
            h4size = res1.getString(8);
            h5 = res1.getString(9);
            h5size = res1.getString(10);
            f1 = res1.getString(11);
            f1size = res1.getString(12);
            f2 = res1.getString(13);
            f2size = res1.getString(14);
            f3 = res1.getString(15);
            f3size = res1.getString(16);
            f4 = res1.getString(17);
            f4size = res1.getString(18);
            f5 = res1.getString(19);
            f5size = res1.getString(20);
        }

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String dateToStr = format.format(today);
        String data = null;

        data = "                 " + "TechMart Cafe" + "             \n";
        String d = "________________________________\n";

        try {

            if (h1 == null || h1.length() == 0 || h1 == "") {
                h1 = null;
            } else {
                if (h1.length() < h2.length()) {
                    data = "          " + h1 + "\n";
                } else {
                    data = "         " + h1 + "\n";
                }
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.FONT_003);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }

            if (h2 == null || h2.length() == 0 || h2 == "") {
                h2 = null;
            } else {
                if (h2.length() > 16) {
                    data = "     " + h2 + "\n";
                } else {
                    data = "           " + h2 + "\n";
                }
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (h3 == null || h3.length() == 0 || h3 == "") {
                h3 = null;
            } else {
                if (h3.length() > 16) {
                    data = "     " + h3 + "\n";
                } else {
                    data = "           " + h3 + "\n";
                }
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (h4 == null || h4.length() == 0 || h4 == "") {
                h4 = null;
            } else {
                if (h4.length() > 16) {
                    data = "     " + h4 + "\n";
                } else {
                    data = "           " + h4 + "\n";
                }
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (h5 == null || h5.length() == 0 || h5 == "") {
                h5 = null;
            } else {
                if (h4.length() > 16) {
                    data = "     " + h5 + "\n";
                } else {
                    data = "           " + h5 + "\n";
                }
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }

            if (res2.getCount() == 0) {
                // show message
                Message.message(getApplicationContext(), "please add items");
            }
            data = "\nCash Memo    bill no:" + emp_code + "\n";
            m_AemPrinter.print(data);
            data = "Date:-" + dateToStr + "\n";
            m_AemPrinter.print(data);
         /*       data="Customer Name:- "+str_set_cust_name+"\n";
                m_AemPrinter.print(data);*/
            if (str_set_cust_name == "" || str_set_cust_name == null) {
                str_set_cust_name = "";
                data = "\nItem Name         |Qty|Rate|Amt\n";

            } else {
                data = "Customer Name:- " + str_set_cust_name + "\n";
                m_AemPrinter.print(data);
                data = "Item Name        |Qty|Rate|Amt\n";
            }

            m_AemPrinter.print(data);
            m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
            String totalamtrate = "";
            int a = 1;
            StringBuffer buffer = new StringBuffer();
            while (res2.moveToNext()) {

                String itemname = res2.getString(3).toString();
                String largname = "";
                String qty = res2.getString(4).toString();
                String rate = res2.getString(5).toString();
                ;
                String amount = res2.getString(6).toString();
                if (amount.length() > 4) {
                    amount = amount.substring(0, 4);

                } else {
                    int length = amount.length();
                    int l1 = 0;
                    if (length < 4) {
                        l1 = 4 - length;
                        amount = String.format(amount + "%" + (l1) + "s", "");
                    }
                }
                if (itemname.length() > 15) {


                    if (res2.getString(5).length() >= 3) {
                        buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + " " + res2.getString(5) + " " + amount + "\n"); //res2.getString(6)+"\n");
                    } else {
                        buffer.append(itemname.substring(0, 15) + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n"); //res2.getString(6)+"\n");
                    }

                } else {

                    int length = itemname.length();
                    int l1 = 0;
                    if (length < 15) {
                        l1 = 15 - length;
                        String itemname1 = itemname.concat(l1 + " ");
                        itemname = String.format(itemname + "%" + (l1) + "s", "");
                    }
                    if (res2.getString(5).length() >= 3) {
                        buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + " " + amount + "\n");//res2.getString(6)+"\n");
                    } else {
                        buffer.append(itemname + "    " + res2.getString(4) + "  " + res2.getString(5) + "   " + amount + "\n");//res2.getString(6)+"\n");
                    }
                }


            }
            m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
            //m_AemPrinter.print(String.valueOf(buffer));
            m_AemPrinter.PrintHindi(String.valueOf(buffer));
            m_AemPrinter.setFontType(AEMPrinter.FONT_001);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            double ddata = Double.parseDouble(tv_total_amt.getText().toString());
            DecimalFormat f = new DecimalFormat("#####.00");
            //System.out.println(f.format(dda));

            //  data = "                           TOTAL(Rs): "+tv_total_amt.getText().toString()+"\n";
            data = "              TOTAL(Rs): " + f.format(ddata) + "\n";
            m_AemPrinter.print(d);
            m_AemPrinter.POS_Font_bold_ThreeInch();
            m_AemPrinter.print(data);
            //m_AemPrinter.setFontType(AEMPrinter.FONT_003);

////            m_AemPrinter.print(d);
            // data = "                 Thank you!             \n";
            if (f1 == null || f1.length() == 0 || f1 == "") {
                f1 = null;
            } else {

                data = "     " + f1 + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);

            }
            if (f2 == null || f2.length() == 0 || f2 == "") {
                f2 = null;
            } else {
                data = "     " + f2 + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (f3 == null || f3.length() == 0 || f3 == "") {
                f3 = null;
            } else {

                data = "     " + f3 + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (f4 == null || f4.length() == 0 || f4 == "") {
                f4 = null;
            } else {
                data = "     " + f4 + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }
            if (f5 == null || f5.length() == 0 || f5 == "") {
                f5 = null;
            } else {
                data = "     " + f5 + "\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            }

            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();

            if(counter>1)
            {
                RemoveMessageBox("two_inch_blueprints_gst_enable", "Reprinting", "Do you want to Reprint Bill no", tvbill.getText().toString()+"?");
            }

            else
            {
                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();


                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";
                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                str_set_cust_name = "";

                ids = db.getAllbillID();
                autoinnc();


            }
//            Message.message(getApplicationContext(), "Bill saved");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    protected void RemoveMessageBox(final String cat_id, String catname, String title, String msg) {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.warning_layout);
        bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        bt_cancel1 = (Button) dialog.findViewById(R.id.bt_cancel);
        tv_title = (TextView) dialog.findViewById(R.id.tv_title);
        tv_errortext = (TextView) dialog.findViewById(R.id.tv_errortext);
        tv_title.setText(title);
        tv_errortext.setText(msg);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(cat_id.equals("gst_data_blueprints_two_inch_gst_enable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        gst_data_blueprints_two_inch_gst_enable();
                    }
                }

                if(cat_id.equals("gst_data_blueprints_two_inch_gst_disable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        gst_data_blueprints_two_inch_gst_disable();
                    }
                }
                if(cat_id.equals("gst_data_Dyno_two_inch_gst_enable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        gst_data_Dyno_two_inch_gst_enable();
                    }
                }
                if(cat_id.equals("gst_data_Dyno_two_inch_gst_disable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        gst_data_Dyno_two_inch_gst_disable();
                    }
                }
                if(cat_id.equals("gst_data_blueprints_three_inch_gst_enable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        counter++;
                        gst_data_blueprints_three_inch_gst_enable();
                    }
                }
                if(cat_id.equals("gst_data_blueprints_three_inch_gst_disable")){
                    //two_inch_blueprints_gst_disable(Integer.parseInt(tvbill.getText().toString()));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        counter++;
                        gst_data_blueprints_three_inch_gst_disable();
                    }
                }
                counter++;

//                dialog.dismiss();


            }
        });
        bt_cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                counter=1;

                ModelSale.arr_item_rate.clear();
                ModelSale.arry_item_name.clear();
                ModelSale.arr_item_qty.clear();
                ModelSale.arr_item_price.clear();
                ModelSale.arr_item_basicrate.clear();
                ModelSale.arr_item_dis_rate.clear();
                ModelSale.arr_item_dis.clear();
                ModelSale.arr_item_tax.clear();
                ModelSale.array_item_amt.clear(); checkArray.clear();

                payment_deatils="";
                order_details="";
                str_set_cust_name="";
                str_set_payment="";
                payment_mode_id="";
                cost = 0;
                tv_total_amt.setText("0");
                cnt = 1;
                itemArrayList.clear();
                myAppAdapter = new MyAppAdapter(itemArrayList, BillingScreenActivity.this);
                str_set_cust_name = "";

                ids = db.getAllbillID();
                autoinnc();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }
}