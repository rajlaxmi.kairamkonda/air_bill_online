package example.aem.blueprints;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import example.aem.blueprints.AEMPrinter.BARCODE_HEIGHT;
import example.aem.blueprints.AEMPrinter.BARCODE_TYPE;
    import example.aem.blueprints.CardReader.CARD_TRACK;


public class BluetoothActivity extends AppCompatActivity implements IAemCardScanner, IAemScrybe   {

    int effectivePrintWidth = 48;
    AEMScrybeDevice m_AemScrybeDevice;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Bitmap imageBitmap;
    CardReader m_cardReader = null;
    AEMPrinter m_AemPrinter = null;
    ArrayList<String> printerList;
    String creditData;
    ProgressDialog m_WaitDialogue;
    CARD_TRACK cardTrackType;
    int glbPrinterWidth;
    MyClient mcl = null;
    EditText editText,rfText;
    private PrintWriter printOut;
    private Socket socketConnection;
    private String txtIP="";
    Spinner spinner;
    String encoding = "US-ASCII";
    private static int nFontSize, nTextAlign, nScaleTimesWidth,
            nScaleTimesHeight, nFontStyle, nLineHeight = 32, nRightSpace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        printerList = new ArrayList<String>();
        creditData = new String();
        editText = (EditText) findViewById(R.id.edittext);
        spinner=(Spinner)findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.printer_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {


            public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {

                if(position==1)
                {
                    glbPrinterWidth=48;
                    onSetPrinterType(view);
                }
                else{
                    glbPrinterWidth=32;
                    onSetPrinterType(view);
                }
            }


            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        // btnSetPrnType = (Button) findViewById(R.id.printerType);
        //btnSetPrnType.setText("Set 3 Inch Printer");
        m_AemScrybeDevice = new AEMScrybeDevice(this);
        Button discoverButton = (Button) findViewById(R.id.pairing);
        registerForContextMenu(discoverButton);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Select Printer to connect");

        for (int i = 0; i < printerList.size(); i++)
        {
            menu.add(0, v.getId(), 0, printerList.get(i));
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        super.onContextItemSelected(item);
        String printerName = item.getTitle().toString();
        try
        {
            m_AemScrybeDevice.connectToPrinter(printerName);
            m_cardReader = m_AemScrybeDevice.getCardReader(this);
            m_AemPrinter = m_AemScrybeDevice.getAemPrinter();
            Toast.makeText(BluetoothActivity.this,"Connected with " + printerName,Toast.LENGTH_SHORT ).show();

          //  m_cardReader.readMSR();
        }
        catch (IOException e)
        {
            if (e.getMessage().contains("Service discovery failed"))
            {
                Toast.makeText(BluetoothActivity.this,"Not Connected\n"+ printerName + " is unreachable or off otherwise it is connected with other device",Toast.LENGTH_SHORT ).show();
            }
            else if (e.getMessage().contains("Device or resource busy"))
            {
                Toast.makeText(BluetoothActivity.this,"the device is already connected",Toast.LENGTH_SHORT ).show();
            }
            else
            {
                Toast.makeText(BluetoothActivity.this,"Unable to connect",Toast.LENGTH_SHORT ).show();
            }
        }
        return true;
    }
    @Override
    protected void onDestroy()
    {
        if (m_AemScrybeDevice != null)
        {
            try
            {
                m_AemScrybeDevice.disConnectPrinter();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    public void onShowPairedPrinters(View v)
    {

        String p = m_AemScrybeDevice.pairPrinter("BTprinter0314");
       // showAlert(p);
        printerList = m_AemScrybeDevice.getPairedPrinters();

        if (printerList.size() > 0)
            openContextMenu(v);
        else
            showAlert("No Paired Printers found");
    }
    public void onDisconnectDevice(View v)
    {
        if (m_AemScrybeDevice != null)
        {
            try
            {
                m_AemScrybeDevice.disConnectPrinter();
                Toast.makeText(BluetoothActivity.this, "disconnected", Toast.LENGTH_SHORT).show();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void onPrintHindiBillBluetooth(int numChars)
    {
        String data = "";
        String d = "";
        if(numChars == 32)
        {
            data = "     दो ईन्च प्रिन्टर:टेस्ट प्रिन्ट             ";
            d =    "________________________________";
            try
            {

                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data = "कोड। विवरण | रेट । मात्रा । राशि(रू)";
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data = "12।कोलगेट    ।70.00 । 2 । 140.00\n"+
                        "22।मैगी सूप   ।25.00 । 2 । 50.00\n"+
                        "32।लक्स सोप  ।45.00 । 2 । 90.00\n"+
                        "42।डाबर हनी ।60.00 । 1 । 120.00\n"+
                        "62।मैगीनूडल   ।10.00 । 9 । 90.00\n";
                //   "___________________________";
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data="	टोटल राशि (रू):          590.00";


                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);

                data="      	धन्यवाद                      ";
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
                m_AemPrinter.PrintHindi(data);

                data="	       आपका दिन मंगलमय हो             ";

                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);

            }
            catch (IOException e)
            {
                if (e.getMessage().contains("socket closed"))
                    showAlert("Printer not connected");
            }
        }

        else
        {

            data = "         तीन इन्च प्रिन्टर: टेस्ट प्रिन्ट            \n";
            d =    "________________________________________________\n";

            try
            {
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data = " कोड |  विवरण     | रेट  ।  मात्रा  ।     राशि(रू) ";
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data =   " 12 ।  कोलगेट     ।  70.00  ।  2   ।    140.00\n"+
                        " 22 ।  मैगी सूप    ।  25.00  ।  2   ।     50.00\n"+
                        " 32 ।  लक्स सोप   ।  45.00  ।  2   ।     90.00\n"+
                        " 42 ।  डाबर हनी  ।  60.00  ।  1   ।    120.00\n"+
                        " 62 ।  मैगीनूडल    ।  10.00  ।  9   ।     90.00\n";
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);
                data = "             टोटल राशि (रू):   590.00\n";


                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);
                m_AemPrinter.print(d);

                data = "\n                धन्यवाद                         ";
                //m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);

                data="             आपका दिन मंगलमय हो                     ";
                //m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.PrintHindi(data);

            }
            catch (IOException e)
            {
                if (e.getMessage().contains("socket closed"))
                    showAlert("Printer not connected");
            }

        }
    }



    public void onPrintHindiBill(View v)
    {
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        int numChars = glbPrinterWidth;//CheckPrinterWidth();
        Toast.makeText(BluetoothActivity.this, "Printing " + numChars + " Character/Line Bill", Toast.LENGTH_SHORT).show();
        onPrintHindiBillBluetooth(numChars);
    }
    public void onSetPrinterType(View v)
    {
        if(glbPrinterWidth == 32)
        {
            glbPrinterWidth = 32;
            //showAlert("32 Characters / Line or 2 Inch (58mm) Printer Selected!");

        }
        else
        {
            glbPrinterWidth = 48;
            showAlert("48 Characters / Line or 3 Inch (80mm) Printer Selected!");
        }
    }
    public void onPrintBill(View v)
    {
        int numChars = glbPrinterWidth;//CheckPrinterWidth();
        Toast.makeText(BluetoothActivity.this, "Printing " + numChars + " Character/Line Bill", Toast.LENGTH_SHORT).show();
        onPrintBillBluetooth(numChars);
    }
    public void onPrintBillBluetooth(int numChars)
    {

        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }

        String data = "TWO INCH PRINTER: TEST PRINT \n";
        String d =    "_________________________________\n";
        try
        {
            m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
            m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
            if(numChars == 32){
                m_AemPrinter.print(data);
                m_AemPrinter.print(d);
                data ="CODE|DESC|RATE(Rs)|QTY |AMT(Rs)\n";
                m_AemPrinter.print(data);
                m_AemPrinter.print(d);
                data =   "13|ColgateGel |35.00|02|70.00\n"+
                        "29|Pears Soap |25.00|01|25.00\n"+
                        "88|Lux Shower |46.00|01|46.00\n"+
                        "15|Dabur Honey|65.00|01|65.00\n"+
                        "52|Dairy Milk |20.00|10|200.00\n"+
                        "128|Maggie TS |36.00|04|144.00\n"+
                        "_______________________________\n";

                m_AemPrinter.setFontType(AEMPrinter.FONT_NORMAL);
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT);
                m_AemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER);
                data = "   TOTAL AMOUNT (Rs.)   550.00\n";
                m_AemPrinter.print(data);
                m_AemPrinter.setFontType(AEMPrinter.FONT_002);
                m_AemPrinter.print(d);
                data = "   Thank you! \n";
                m_AemPrinter.setFontType(AEMPrinter.DOUBLE_WIDTH);
                m_AemPrinter.print(data);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            }

            else
            {

                data = "         THREE INCH PRINTER: TEST PRINT\n";
                m_AemPrinter.printThreeInch(data);
                m_AemPrinter.setLineFeed(1);

                // d =    "________________________________________________\n";

                data = 		  "CODE|   DESCRIPTION   |RATE(Rs)|QTY |AMOUNT(Rs)\n";
                m_AemPrinter.printThreeInch(data);
                data = " 13 |Colgate Total Gel | 35.00  | 02 |  70.00\n"+
                        " 29 |Pears Soap 250g   | 25.00  | 01 |  25.00\n"+
                        " 88 |Lux Shower Gel 500| 46.00  | 01 |  46.00\n"+
                        " 15 |Dabur Honey 250g  | 65.00  | 01 |  65.00\n"+
                        " 52 |Cadbury Dairy Milk| 20.00  | 10 | 200.00\n"+
                        "128 |Maggie Totamto Sou| 36.00  | 04 | 144.00\n";

                m_AemPrinter.printThreeInch(data);
                data = "          TOTAL AMOUNT (Rs.)   550.00\n";
                m_AemPrinter.printThreeInch(data);
                data = "        Thank you! \n";
                m_AemPrinter.printThreeInch(data);
                m_AemPrinter.setLineFeed(4);
            }


        }
        catch (IOException e)
        {
            if (e.getMessage().contains("socket closed"))
                Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();

        }

    }

    public void onPrint(View v)
    {
        String data = editText.getText().toString();
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        else if(data.isEmpty()){
            showAlert("Write Text");
        }
        else if(m_AemPrinter != null )
        {
            try
            {
                if (glbPrinterWidth==32)
                {
                    m_AemPrinter.print(data);
                    // m_AemPrinter.PrintHindi(data);
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                }
                else {
                    m_AemPrinter.POS_S_TextOutThreeInch(data,encoding,0,nScaleTimesWidth,nScaleTimesHeight,nFontSize,nFontStyle);
                   // m_AemPrinter.printThreeInch(data);
                    // m_AemPrinter.PrintHindi(data);
                    m_AemPrinter.setLineFeed(4);
                }

            }
            catch (IOException e)
            {
                if (e.getMessage().contains("socket closed"))
                    Toast.makeText(BluetoothActivity.this,"Printer not connected", Toast.LENGTH_SHORT).show();

            }
        }
        else
        {
            mcl.sendDataOnSocket(data, printOut);
            mcl.sendDataOnSocket("\n\n\n", printOut);
            //mcl.sendDataOnSocket("\n\n", printOut);
            //mcl.sendDataOnSocket("\n\n", printOut);
        }
    }

    public void onPrintQRCodeRaster(View v) throws WriterException, IOException
    {
        if (m_AemPrinter == null)
    {
        Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
        return;
    }
        String text= editText.getText().toString();
        if(text.isEmpty()){
            showAlert("Write Text To Generate QR Code");
        }
        else {
            Writer writer = new QRCodeWriter();
            String finalData = Uri.encode(text, "UTF-8");
            showAlert("QR " + text);
            try {

                BitMatrix bm = writer.encode(finalData, BarcodeFormat.QR_CODE, 300, 300);
                Bitmap bitmap = Bitmap.createBitmap(300, 300, Config.ARGB_8888);
                for (int i = 0; i < 300; i++) {
                    for (int j = 0; j < 300; j++) {
                        bitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                    }
                }
                //showAlert("generating qr bitmap " + wifiConnection);
                Bitmap resizedBitmap = null;
                int numChars = glbPrinterWidth;

                  /*if(numChars == 32)
                       	resizedBitmap = Bitmap.createScaledBitmap(bitmap, 350, 140, false);
                  else*/
                resizedBitmap = Bitmap.createScaledBitmap(bitmap, 384, 384, false);


                  /*if(wifiConnection == 0)
                	  	m_AemPrinter.printImage(resizedBitmap, getApplicationContext(),AEMPrinter.IMAGE_CENTER_ALIGNMENT);
                  else
                  {
                	  byte[] imagePacket = aemWifi.printImage(resizedBitmap, getApplicationContext(),AEMPrinter.IMAGE_CENTER_ALIGNMENT);

                	  mcl.sendBytesOnSocket(imagePacket, imagePacket.length, socketConnection);
                  }*/


                m_AemPrinter.printImage(resizedBitmap);


            } catch (WriterException e) {
                showAlert("Error WrQR: " + e.toString());
            }
        }
    }
    public void onPrintQRCode(View v) throws WriterException, IOException
    {
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        String text= editText.getText().toString();
        if(text.isEmpty())
        {
            showAlert("Write Text To Generate QR Code");
        }
        else {
            Writer writer = new QRCodeWriter();
            String finalData = Uri.encode(text, "UTF-8");
            showAlert("QR " + text);
            try {

                BitMatrix bm = writer.encode(finalData, BarcodeFormat.QR_CODE, 300, 300);
                Bitmap bitmap = Bitmap.createBitmap(300, 300, Config.ARGB_8888);
                for (int i = 0; i < 300; i++) {
                    for (int j = 0; j < 300; j++) {
                        bitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                    }
                }
                //showAlert("generating qr bitmap " + wifiConnection);
                Bitmap resizedBitmap = null;
                int numChars = glbPrinterWidth;

                if (numChars == 32)
                    resizedBitmap = Bitmap.createScaledBitmap(bitmap, 350, 140, false);
                else
                    resizedBitmap = Bitmap.createScaledBitmap(bitmap, 384, 160, false);


                m_AemPrinter.printBitImage(resizedBitmap, getApplicationContext(), AEMPrinter.IMAGE_CENTER_ALIGNMENT);


            } catch (IOException e) {
                showAlert("Error QR: " + e.toString());
            } catch (WriterException e) {
                showAlert("Error WrQR: " + e.toString());
            }
        }
    }

    public void onSaveMultilingual(View v) {
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        String data = editText.getText().toString();
        if (data.isEmpty()) {
            showAlert("Write Text To Save");
        } else {
            Converter convert = new Converter();
            Bitmap bmp = convert.textAsBitmap(data, 30, 5, Color.BLACK, Typeface.MONOSPACE);
            //Bitmap image = convert.addBorder(bmp, 2, Color.BLACK);
            // file name appending with system date
            Calendar c = Calendar.getInstance();
            SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
            String filename = "ScreenShot_" + f.format(c.getTime());
            // saving image to sdcard
            SaveImage saveimg = new SaveImage();
            // pass bit and filename
            saveimg.storeImage(bmp, filename);
            Toast.makeText(getApplicationContext(), "Saved As :n" + filename, Toast.LENGTH_SHORT).show();
        }
    }
    public void onPrintMultilingual(View v) {
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        String data = editText.getText().toString();
        if (data.isEmpty()) {
            showAlert("Write Text");
        } else {
            try {
                if (glbPrinterWidth==32)
                {
                    m_AemPrinter.printTextAsImage(data);
                }
                else {
                    m_AemPrinter.printTextAsImageThreeInch(data);
                }
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void onPrintBarcode(View v)
    {
        onPrintBarcodeBT();
    }
    public void onPrintBarcodeBT() {
        if (m_AemPrinter == null) {
            showAlert("Printer not connected");
            return;
        }

        String text = editText.getText().toString();
        if (text.isEmpty()) {
            showAlert("Write Text TO Generate Barcode");
        } else {
            try {
                if(glbPrinterWidth==32) {
                    m_AemPrinter.printBarcode(text, BARCODE_TYPE.CODE39, BARCODE_HEIGHT.DOUBLEDENSITY_FULLHEIGHT);
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                    m_AemPrinter.setCarriageReturn();
                } else {
                    m_AemPrinter.printBarcodeThreeInch(text, BARCODE_TYPE.CODE39, BARCODE_HEIGHT.DOUBLEDENSITY_FULLHEIGHT);
                    m_AemPrinter.setLineFeed(4);

                }
            } catch (IOException e) {
                showAlert("Printer not connected");
            }
        }
    }

    public void onPrintImage(View v)
    {
        onPrintImageBT();

    }
    public void onPrintImageRaster(View v)
    {
        if (m_AemPrinter == null)
        {
            Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        try
        {
            InputStream is = getAssets().open("aadharAEM.jpg");

            Bitmap inputBitmap = BitmapFactory.decodeStream(is);
            Bitmap resizedBitmap = null;
           /*if(glbPrinterWidth == 32)
    		     resizedBitmap = Bitmap.createScaledBitmap(inputBitmap, 350, 140, false);
           else*/
            resizedBitmap = Bitmap.createScaledBitmap(inputBitmap, 384, 384, false);

            RasterBT(resizedBitmap);
        }
        catch (IOException e)
        {
            showAlert("IO Exception: " + e.toString());

        }
    }
    public void onPrintRasterImage(View v)
    {
        selectImage();
       // selectImageFromSDCard();
       // dispatchTakePictureIntent();
    }

    public void selectImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUEST_IMAGE_CAPTURE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && null !=data){
            Uri selectedImageUri = data.getData();
            uriToBitmap(selectedImageUri);

        } else {
            Toast.makeText(BluetoothActivity.this, "You have not selected and image", Toast.LENGTH_SHORT).show();
        }
    }
    private void uriToBitmap(Uri selectedFileUri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            Bitmap resizedBitmap=null;
            if(glbPrinterWidth == 32)
                resizedBitmap = Bitmap.createScaledBitmap(image, 384, 384, false);
            else
                resizedBitmap = Bitmap.createScaledBitmap(image, 577, 700, false);
           // resizedBitmap = Bitmap.createScaledBitmap(image, 384, 384, false);
            if (m_AemPrinter == null)
            {
                Toast.makeText(BluetoothActivity.this, "Printer not connected", Toast.LENGTH_SHORT).show();
                return;
            }
            RasterBT(resizedBitmap);
            parcelFileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*  private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            Bitmap resizedBitmap=null;
            resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 650, 450, false);
            RasterBT(resizedBitmap);
            //imageView.setImageBitmap(imageBitmap);
        }
    }
    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    } */
    public void onPrintImageBT()
    {
        if (m_AemPrinter == null)
        {
            showAlert("Printer not connected");
            return;
        }

        try
        {
//    			m_AemPrinter.printAEMLogo();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            InputStream is = getAssets().open("aadharAEM.jpg");
            Bitmap inputBitmap = BitmapFactory.decodeStream(is);
            Bitmap resizedBitmap = null;
            if(glbPrinterWidth == 32)
                resizedBitmap = Bitmap.createScaledBitmap(inputBitmap, 350, 140, false);
            else
                resizedBitmap = Bitmap.createScaledBitmap(inputBitmap, 384, 140, false);
            m_AemPrinter.printBitImage(resizedBitmap, getApplicationContext(),AEMPrinter.IMAGE_CENTER_ALIGNMENT);
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
            m_AemPrinter.setCarriageReturn();
        }
        catch (IOException e)
        {}
    }

    CardReader.MSRCardData creditDetails;
    public void onScanMSR(final String buffer, CARD_TRACK cardTrack)
    {
        cardTrackType = cardTrack;

        creditData = buffer;
        BluetoothActivity.this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                editText.setText(buffer.toString());
            }
        });
    }
    public void onScanDLCard(final String buffer)
    {
        CardReader.DLCardData dlCardData = m_cardReader.decodeDLData(buffer);
        String name = "NAME:" + dlCardData.NAME + "\n";
        String SWD = "SWD Of: " + dlCardData.SWD_OF + "\n";
        String dob = "DOB: " + dlCardData.DOB + "\n";
        String dlNum = "DLNUM: " + dlCardData.DL_NUM + "\n";
        String issAuth = "ISS AUTH: " + dlCardData.ISS_AUTH + "\n";
        String doi = "DOI: " + dlCardData.DOI + "\n";
        String tp = "VALID TP: " + dlCardData.VALID_TP + "\n";
        String ntp = "VALID NTP: " + dlCardData.VALID_NTP + "\n";

        final String data = name + SWD + dob + dlNum + issAuth + doi + tp + ntp;

        runOnUiThread(new Runnable()
        {
            public void run()
            {
                editText.setText(data);
            }
        });
    }

    public void onScanRCCard(final String buffer)
    {
        CardReader.RCCardData rcCardData = m_cardReader.decodeRCData(buffer);

        String regNum = "REG NUM: " + rcCardData.REG_NUM + "\n";
        String regName = "REG NAME: " + rcCardData.REG_NAME + "\n";
        String regUpto = "REG UPTO: " + rcCardData.REG_UPTO + "\n";

        final String data = regNum + regName + regUpto;

        runOnUiThread(new Runnable()
        {
            public void run()
            {
                editText.setText(data);
            }
        });
    }
    @Override
    public void onScanRFD(final String buffer)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(buffer);
        String temp = "";
        try
        {
            temp = stringBuffer.deleteCharAt(8).toString();
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        final String data = temp;

        BluetoothActivity.this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                //rfText.setText("RF ID:   " + data);
                editText.setText("ID " + data);
                try {
                    m_AemPrinter.print(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void onDiscoveryComplete(ArrayList<String> aemPrinterList)
    {
        printerList = aemPrinterList;
        for(int i=0;i<aemPrinterList.size();i++)
        {
            String Device_Name=aemPrinterList.get(i);
            String status = m_AemScrybeDevice.pairPrinter(Device_Name);
            Log.e("STATUS", status);
        }

    }

    public void onDecodeCreditData(View v)
    {
        if (m_cardReader == null)
        {
            showAlert("Printer not connected");
            return;
        }

        if (!(creditData.length() > 0))
        {
            showAlert("The data is unavailable");
            return;
        }

        creditDetails = m_cardReader.decodeCreditCard(creditData, cardTrackType);
        String cardNumber = "cardNumber: " + creditDetails.m_cardNumber;
        String HolderName = "HolderName: " + creditDetails.m_AccoundHolderName;
        String ExpirayDate = "Expiray Date: " + creditDetails.m_expiryDate;
        String ServiceCode = "Service Code: " + creditDetails.m_serviceCode;
        String pvki = "PVKI: " + creditDetails.m_pvkiNumber;
        String pvv = "PVV: " + creditDetails.m_pvvNumber;
        String cvv = "CVV: " + creditDetails.m_cvvNumber;

        showAlert(cardNumber + "\n" + HolderName + "\n" + ExpirayDate + "\n"
                + ServiceCode + "\n" + pvki + "\n" + pvv + "\n" + cvv);
    }

    public void showAlert(String alertMsg)
    {
        AlertDialog.Builder alertBox = new AlertDialog.Builder(
                BluetoothActivity.this);

        alertBox.setMessage(alertMsg).setCancelable(false)
                .setPositiveButton("OK", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        return;
                    }
                });

        AlertDialog alert = alertBox.create();
        alert.show();
    }
    public void onSuccess(String response, Socket socket, String dstAddress, int dstPort, PrintWriter out)
    {
        txtIP = dstAddress;
        printOut = out;
        socketConnection = socket;
        Log.i("PrintWriter",out+"");
        Log.i("SocketOut", socket+"'");
        editText.setText(txtIP);
        showAlert("Printer Connected"+" "+response);
    }

    public void onError(String response) {
        showAlert("Please try again... "+" "+response);
    }

    @Override
    public void onScanPacket(String buffer) {
        // TODO Auto-generated method stub

    }
    protected void selectImageFromSDCard()
    {
        FileDialog fileDialog;
        File mPath = new File(Environment.getExternalStorageDirectory() + "//DIR//");
        fileDialog = new FileDialog(BluetoothActivity.this, mPath);
        fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
            @Override
            public void fileSelected(File file) {
                String img_file_path=file.toString();
                String filenameArray[] = img_file_path.split("\\.");
                String extension = filenameArray[filenameArray.length-1];
                String[] img = { "png", "jpg", "bmp","PNG", "JPG", "BMP" };

                if (Arrays.asList(img).contains(extension))
                {
                    printRasterImageBT(img_file_path);

                }
                else{
                    Toast.makeText(getApplicationContext(), "invalid file type", Toast.LENGTH_LONG).show();
                }
            }
        });
        fileDialog.showDialog();
    }

    protected void printRasterImageBT(String img_file_path)
    {
        if (m_AemPrinter == null)
        {
            showAlert("Printer not connected");
            return;
        }
        if(new File(img_file_path).exists())
        {
            Bitmap image = BitmapFactory.decodeFile(img_file_path);
            if(image!=null)
            {
                RasterBT(image);
            }
        }
    }

    protected void RasterBT(Bitmap image) {
        try {
            if (glbPrinterWidth == 32)
            {
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.printImage(image);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
            }
            else
            {
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.printImageThreeInch(image);
                m_AemPrinter.setCarriageReturn();
                m_AemPrinter.setCarriageReturn();
    }
        }
        catch (IOException e)
        {
            showAlert("IO EX:  " + e.toString());
        }
    }

}
